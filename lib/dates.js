import moment from 'moment';
import date from 'date-and-time';

export const isValidDate = d => {
  if (Object.prototype.toString.call(d) === '[object Date]') {
    if (isNaN(d.getTime())) {
      return false;
    }
    return true;
  }
  return false;
};

export const parseDate = date => {
  const m = date.slice(3, 5);
  const d = date.slice(0, 2);
  const y = date.slice(-4);
  return new Date(y, m - 1, d);
};

export const getTomorrow = () => {
  const today = new Date();
  const tomorrow = new Date(today.valueOf());
  tomorrow.setDate(tomorrow.getDate() + 1);
  return tomorrow;
};

export const getTodayPlus = days => {
  const today = new Date();
  const plusDate = new Date(today.valueOf());
  plusDate.setDate(plusDate.getDate() + days);
  return plusDate;
};

export const weekMoreString = () => {
  const today = new Date();
  const weekMore = new Date(today.valueOf());
  weekMore.setDate(weekMore.getDate() + 7);
  return date.format(weekMore, 'DD-MM-YYYY');
};

export const getToday = () => {
  const today = new Date();
  return today;
};

export const getTodaystring = () => {
  const today = new Date();
  const todayString = date.format(today, 'DD-MM-YYYY');
  return todayString;
};

export const daysBetweenToday = date => {
  const dateStart = moment(new Date());
  const dateEnd = moment(date);
  const duration = moment.duration(dateStart.diff(dateEnd));
  const days = duration.asDays();
  return parseInt(days, 10);
};

export const daysBetweenToday2 = date => {
  const dateStart = moment(new Date());
  const dateEnd = moment(parseDate(date));
  const duration = moment.duration(dateStart.diff(dateEnd));
  const days = duration.asDays();
  return parseInt(days, 10);
};
