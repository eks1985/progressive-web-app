const fullAccess = 'full-access';
const director = 'director';
const brandManager = 'brand-manager';
const manager = 'manager';
const service = 'service';
const backOffice = 'back-office';
const projectOwner = 'project-owner';
const dealer = 'dealer';
const dealerNoOrders = 'dealer-no-orders';
const dealerHigh = 'dealer-high';
const dealerLow = 'dealer-low';
const dealerService = 'dealer-service';
const orders = 'orders';

const priceClient = 'price-client';
const priceDealer = 'price-dealer';
const stockDealer = 'stock-dealer';
const ordersList = 'orders-list';
const ordersSend = 'orders-send';
const openItems = 'open-items';
const warrantyClaims = 'warranty-claims';
const productRegistration = 'product-registration';
const showCost = 'show-cost';
const showProdPlan = 'prod-plan';
const requestSend = 'request-send';

export default {
  pages: {
    companyPageCompareYears: [fullAccess],
  },
  catalog: {
    visManager: [fullAccess, manager, brandManager, backOffice],
    editCat: [fullAccess, brandManager],
    editMod: [fullAccess, brandManager],
    editSet: [fullAccess, brandManager, backOffice],
    showDealerPrice: [fullAccess, brandManager, manager, backOffice, dealer, dealerNoOrders],
    showClientPrice: [fullAccess, brandManager, manager, backOffice, dealer, dealerNoOrders],
  },
  common: {
    switchCountry: [fullAccess, director, manager, backOffice, service],
    loadCorpData: [fullAccess, director, manager, backOffice, service],
    showBottomNav: [dealer, dealerNoOrders, dealerLow, fullAccess, director, manager, backOffice, service],
    showCompany: [fullAccess, director, manager, backOffice, service],
    showPriceList: [dealer, dealerLow, dealerNoOrders],
    showAvail: [fullAccess, director, manager, backOffice, service],
    showOrders: [fullAccess, director, manager, backOffice, service, dealer, orders],
    showWc: [fullAccess, director, manager, backOffice, service, dealer, dealerNoOrders, dealerService],
    contentRef: [projectOwner],
    showTurnover: [dealerHigh],
    showOpenItems: [dealerHigh],
  },
  orders: {
    placeOrders: [fullAccess, director, manager, backOffice, service, dealer, orders],
    loadAll: [fullAccess, director, manager, backOffice],
    showFilters: [fullAccess, director, manager, backOffice],
    showPoRef: [fullAccess, manager, brandManager, backOffice],
  },
  atoms: {
    priceClient: [priceClient],
    priceDealer: [priceDealer],
    stockDealer: [stockDealer],
    ordersList: [ordersList],
    ordersSend: [ordersSend],
    openItems: [openItems],
    warrantyClaims: [warrantyClaims],
    productRegistration: [productRegistration],
    showCost: [showCost],
    showProdPlan: [showProdPlan],
    requestSend: [requestSend],
  },
};

// dealer: dealer
// efimov: full-access
// mikolenko-sokolenko: director
// chevychelov-bogacheva: brand-manager
