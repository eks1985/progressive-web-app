export const moveArrayElem = (arr, ind, step) => {

  const arrCopy = [...arr];
  let newInd = ind + step;
  while (ind < 0) {
      ind += arr.length;
  }
  while (newInd < 0) {
    newInd += arr.length;
  }
  if (newInd >= arr.length) {
      let k = newInd - arr.length;
      while ((k--) + 1) {
        arrCopy.push(undefined);
      }
  }
  arrCopy.splice(newInd, 0, arrCopy.splice(ind, 1)[0]);
  return arrCopy;
};

export const replaceArrElem = (arr, newElem, ind) => {
  return arr.slice(0, ind).concat(newElem).concat(arr.slice(ind + 1));
};

export const shifObjArrayElem = (array, ind, step) => {
  const arrayCopy = [...array];
  const { length } = array;
  if (ind >= length - 1 && step === 1) {
    const elem = arrayCopy.pop();
    arrayCopy.unshift(elem);
    return arrayCopy;
  }
  if (ind === 0 && step === -1) {
    const elem = arrayCopy.shift();
    arrayCopy.push(elem);
    return arrayCopy;
  }
  if (step === 1) {
    const p1 = arrayCopy.slice(0, ind);
    const p2 = arrayCopy.slice(ind + 1, ind + 2);
    const p3 = arrayCopy.slice(ind, ind + 1);
    const p4 = arrayCopy.slice(ind + 2, length);
    return [...p1, ...p2, ...p3, ...p4];
  }
  if (step === -1) {
    const p1 = arrayCopy.slice(0, ind - 1);
    const p2 = arrayCopy.slice(ind, ind + 1);
    const p3 = arrayCopy.slice(ind - 1, ind);
    const p4 = arrayCopy.slice(ind + 1, length);
    return [...p1, ...p2, ...p3, ...p4];у
  }
  return arrayCopy;
};

export const shiftArrayElem = (list, index, step) => {
  const newList = shifObjArrayElem(list, index, step);
  newList.forEach((item, index) => {
    item.order = index;
  });
  return newList;
};
