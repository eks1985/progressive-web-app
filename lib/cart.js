
import date from 'date-and-time';
import { getTodayPlus } from 'lib/dates';
import { getPrice } from 'lib/internals/catalog';
import { orderBy } from 'lodash';
import { initOpt } from 'components/product-card/internals';

class Cart {

  constructor(data) {
    if (data) {
      this.data = data;
    } else {
      this.data = {
        reqType: 'request',
        totalQty: 0,
        totalValue: 0,
        customer: false,
        validMsg: ['Не заполнен комментарий'],
        items: [],
        delivDate: date.format(getTodayPlus(14), 'DD-MM-YYYY'),
        payment: [
          {
            type: 'date',
            percent: 100,
            value: date.format(getTodayPlus(7), 'DD-MM-YYYY'),
          }
        ],
        notice: '',
      };
    }
  }

  calcTotals() {
    const qty = this.data.items.reduce((res, item) => res + item.qty, 0);
    const value = this.data.items.reduce((res, item) => res + item.value, 0);
    this.data.totalQty = qty;
    this.data.totalValue = value;
  }

  calcValidMsg() {
    this.data.validMsg = [];
    if (!this.data.customer) {
      this.data.validMsg.push('Не задан контрагент'); 
    }
    if (!this.data.notice) {
      this.data.validMsg.push('Не задан комментарий'); 
    }
  }

  update(action) {
    const { event, setCart } = action;
    if (event === 'changeProp') {
      const { prop, value } = action;
      this.data[prop] = value;
    } else if (event === 'changeProductPrice') {
      const { index, price } = action;
      const item  =  this.data.items[index];
      item.price = price;
      item.priceManual = true;
      item.value = price * item.qty;
    } else if (event === 'changeProductOpt') {
      const { index, opt } = action;
      const item  =  this.data.items[index];
      item.opt = opt;
      item.price = getPrice('dealer', item.product, opt).price;
      item.priceManual = false;
      item.value = item.price * item.qty;
    } else if (event === 'changeProductQty') {
      const { index, step } = action;
      const item  =  this.data.items[index];
      item.qty = item.qty += step;
      item.value = item.price * item.qty;
    } else if (event === 'removeItem') {
      const { index } = action;
      console.log('index', index);
      this.data.items = this.data.items.filter((_, ind) => ind !== index);
      console.log('here', this.data.items);
    }
    this.calcTotals();
    this.calcValidMsg();
    if (setCart) {
      setCart(this);
    }
  }

  addProduct(product, globalOpt) {
    let opt = orderBy(initOpt(product.opt), ['order'], ['asc']);
    if (globalOpt && product.id === globalOpt.id) {
      opt = orderBy(globalOpt.opt);
    }
    const price = product.accessory ? product.price : getPrice('dealer', product, opt).price;
    const qty = 1;
    const newRow = {
      product,
      opt,
      price,
      qty,
      value: price * qty,
    };
    this.data.items.push(newRow);
    this.calcTotals();
  }

  setActiveVal(index, setCart, title, value) {

    const cartItem = this.data.items[index];

    const updated = cartItem.opt.map(item => {
      if (item.title === title) {
        item.values = item.values.map(valueItem => {
          if (valueItem.value === value) {
            return { ...valueItem, active: true };
          }
          const { active, ...other } = valueItem;
          return other;
        });  
      }
      return item;
    });

    this.update({ event: 'changeProductOpt', opt: updated, index, setCart });

  }

}

export default Cart;