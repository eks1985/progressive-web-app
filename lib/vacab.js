/* eslint-disable no-tabs */
/* eslint-disable comma-dangle */
/* eslint-disable quotes */
/* eslint-disable quote-props */

// const vacab = {
//   'Войти': {
//     ru: 'Войти',
//     ua: 'Войти',
//     en: 'Log in',
//     it: 'Entrata',
//   },
//   'Работать оффлайн': {
//     ru: 'Работать оффлайн',
//     ua: 'Работать оффлайн',
//     en: 'Work offline',
//     it: 'Work offline',
//   },
//   'Сменить пароль': {
//     ru: 'Сменить пароль',
//     ua: 'Сменить пароль',
//     en: 'Change password',
//     it: 'Change password',
//   },
// };

export const translate = (phrase, lang) => {
  const findPhrase = vacab[phrase];
  if (findPhrase === undefined) {
    return phrase;
  }
  const transl = findPhrase[lang];
  if (transl === undefined) {
    return findPhrase.ru;
  }
  return transl;
};

const getCategoryTitlePropAlias = lang => {
  switch (lang) {
    case 'ru':
      return 'descr';
    case 'ua':
      return 'descr-ua';
    case 'en':
      return 'descr-en';
    case 'it':
      return 'descr-it';
    default:
      return 'descr';
  }
};

const getSetTitlePropAlias = lang => {
  switch (lang) {
    case 'ru':
      return 'title';
    case 'ua':
      return 'title-ua';
    case 'en':
      return 'title-en';
    case 'it':
      return 'title-it';
    default:
      return 'title';
  }
};

export const getCategoryTitle = (category, lang) => {
	if (!category) {
		return '';
	}
	const alias = getCategoryTitlePropAlias(lang);
	return category[alias] || category.descr;
};

export const getSetTitle = (set, lang) => {
	const alias = getSetTitlePropAlias(lang);
	return set[alias] || set.title;
};

const vacab = {

	"Все предложения представлены в pdf и word вариантах": {

		"ru": "Все предложения представлены в pdf и word вариантах",

		"ua": "Все предложения представлены в pdf и word вариантах",

		"en": "All offers presented in pdf and word formats",

		"it": "All offers presented in pdf and word formats"

	},

	"Автоматически обновлять оффлайн данные каждые 3 дня": {

		"ru": "Автоматически обновлять оффлайн данные каждые 3 дня",

		"ua": "Автоматически обновлять оффлайн данные каждые 3 дня",

		"en": "Automatically update offline data every 3 days",

		"it": "Automatically update offline data every 3 days"

	},

	"09:00 - 18:00 МСК (перерыв 13:00 - 14:00)": {

		"ru": "09:00 - 18:00 МСК (перерыв 13:00 - 14:00)",

		"ua": "09:00 - 18:00 МСК (перерыв 13:00 - 14:00)",

		"en": "09:00 - 18:00 MSK (break 13:00 - 14:00)",

		"it": "09:00 - 18:00 МСК (break 13:00 - 14:00)"

	},

	"Отменить": {

		"ru": "Отменить",

		"ua": "Отменить",

		"en": "Cancel",

		"it": "Cancel"

	},

	"Все клиенты": {

		"ru": "Все клиенты",

		"ua": "Все клиенты",

		"en": "All clients",

		"it": "All clients"

	},

	"Скидка на технику": {

		"ru": "Скидка на технику",

		"ua": "Скидка на технику",

		"en": "Discount for machines",

		"it": "Discount for machines"

	},

	"Email": {

		"ru": "Email",

		"ua": "Email",

		"en": "Email",

		"it": "Email"

	},

	"404101, г. Волжский, ул. Пушкина 117 Б, офис 114": {

		"ru": "404101, г. Волжский, ул. Пушкина 117 Б, офис 114",

		"ua": "404101, г. Волжский, ул. Пушкина 117 Б, офис 114",

		"en": "404101, Volzhesky city, st. Pushkina 117 B, office 114",

		"it": "404101, Volzhesky city, st. Pushkina 117 B, office 114"

	},

	"Завершен": {

		"ru": "Завершен",

		"ua": "Завершен",

		"en": "Completed",

		"it": "Completed"

	},

	"Аксессуар": {

		"ru": "Аксессуар",

		"ua": "Аксессуар",

		"en": "Accessory",

		"it": "Accessory"

	},

	"В предложениях проставлены клиентские цены": {

		"ru": "В предложениях проставлены клиентские цены",

		"ua": "В предложениях проставлены клиентские цены",

		"en": "Final customers prices has been inserted into offers",

		"it": "Final customers prices has been inserted into offers"

	},

	"Google drive ссылка": {

		"ru": "Google drive ссылка",

		"ua": "Google drive ссылка",

		"en": "Google drive link",

		"it": "Google drive link"

	},

	"c НДС": {

		"ru": "c НДС",

		"ua": "c НДС",

		"en": "with VAT",

		"it": "with VAT"

	},

	"генеральный директор": {

		"ru": "генеральный директор",

		"ua": "генеральный директор",

		"en": "branch director",

		"it": "branch director"

	},

	"Графики": {

		"ru": "Графики",

		"ua": "Графики",

		"en": "Charts",

		"it": "Charts"

	},

	"бренд менеджер": {

		"ru": "бренд менеджер",

		"ua": "бренд менеджер",

		"en": "brand manager",

		"it": "brand manager"

	},

	"Youtube video id": {

		"ru": "Youtube video id",

		"ua": "Youtube video id",

		"en": "Youtube video id",

		"it": "Youtube video id"

	},

	"Адрес": {

		"ru": "Адрес",

		"ua": "Адрес",

		"en": "Address",

		"it": "Address"

	},

	"Аккаунт": {

		"ru": "Аккаунт",

		"ua": "Аккаунт",

		"en": "Account",

		"it": "Account"

	},

	"Скопируйте ссылку": {

		"ru": "Скопируйте ссылку",

		"ua": "Скопируйте ссылку",

		"en": "Copy link",

		"it": "Copy link"

	},

	"Варианты": {

		"ru": "Варианты",

		"ua": "Варианты",

		"en": "Variants",

		"it": "Variants"

	},

	"Аксессуары": {

		"ru": "Аксессуары",

		"ua": "Аксессуары",

		"en": "Accessories",

		"it": "Accessories"

	},

	"Все категории": {

		"ru": "Все категории",

		"ua": "Все категории",

		"en": "All categories",

		"it": "All categories"

	},

	"Дата поставки": {

		"ru": "Дата поставки",

		"ua": "Дата поставки",

		"en": "Delivery date",

		"it": "Delivery date"

	},

	"Продана": {

		"ru": "Продана",

		"ua": "Продана",

		"en": "Sold",

		"it": "Sold"

	},

	"Аксуссуары": {

		"ru": "Аксуссуары",

		"ua": "Аксуссуары",

		"en": "Accessories",

		"it": "Accessories"

	},

	"Ответственность": {

		"ru": "Ответственность",

		"ua": "Ответственность",

		"en": "Responsibility",

		"it": "Responsibility"

	},

	"Выбрать склад для привязки машин": {

		"ru": "Выбрать склад для привязки машин",

		"ua": "Выбрать склад для привязки машин",

		"en": "Select stock for machine assignement",

		"it": "Select stock for machine assignement"

	},

	"Дата": {

		"ru": "Дата",

		"ua": "Дата",

		"en": "Date",

		"it": "Date"

	},

	"База регистрации машин": {

		"ru": "База регистрации машин",

		"ua": "База регистрации машин",

		"en": "Machines registration",

		"it": "Machines registration"

	},

	"Гар срок": {

		"ru": "Гар срок",

		"ua": "Гар срок",

		"en": "War. expire",

		"it": "War. expire"

	},

	"В работе": {

		"ru": "В работе",

		"ua": "В работе",

		"en": "In progress",

		"it": "In progress"

	},

	"В том числе внешних пользователей": {

		"ru": "В том числе внешних пользователей",

		"ua": "В том числе внешних пользователей",

		"en": "Including external users",

		"it": "Including external users"

	},

	"Ссылка на google drive": {

		"ru": "Ссылка на google drive",

		"ua": "Ссылка на google drive",

		"en": "Link for google drive",

		"it": "Link for google drive"

	},

	"Дебиторская задолженность": {

		"ru": "Дебиторская задолженность",

		"ua": "Дебиторская задолженность",

		"en": "Open items",

		"it": "Open items"

	},

	"Вниз": {

		"ru": "Вниз",

		"ua": "Вниз",

		"en": "Down",

		"it": "Down"

	},

	"Вверх": {

		"ru": "Вверх",

		"ua": "Вверх",

		"en": "Вверх",

		"it": "Вверх"

	},

	"Год": {

		"ru": "Год",

		"ua": "Год",

		"en": "Year",

		"it": "Year"

	},

	"Видео": {

		"ru": "Видео",

		"ua": "Видео",

		"en": "Video",

		"it": "Video"

	},

	"Цена дилер": {

		"ru": "Цена дилер",

		"ua": "Цена дилер",

		"en": "Price dealer",

		"it": "Price dealer"

	},

	"Текст будет использован в качестве заголовка видео": {

		"ru": "Текст будет использован в качестве заголовка видео",

		"ua": "Текст будет использован в качестве заголовка видео",

		"en": "Text will be using as video title",

		"it": "Text will be using as video title"

	},

	"На складе | На тесте": {

		"ru": "На складе | На тесте",

		"ua": "На складе | На тесте",

		"en": "On stock | On test",

		"it": "On stock | On test"

	},

	"Для менеджера": {

		"ru": "Для менеджера",

		"ua": "Для менеджера",

		"en": "For managers",

		"it": ""

	},

	"Код": {

		"ru": "Код",

		"ua": "Код",

		"en": "Code",

		"it": "Code"

	},

	"Видео техники в работе": {

		"ru": "Видео техники в работе",

		"ua": "Видео техники в работе",

		"en": "Video machines on the field",

		"it": "Video machines on the field"

	},

	"Видимость": {

		"ru": "Видимость",

		"ua": "Видимость",

		"en": "Visibility",

		"it": "Visibility"

	},

	"Войти": {

		"ru": "Войти",

		"ua": "Войти",

		"en": "Log in",

		"it": "Log in"

	},

	"Вопрос": {

		"ru": "Вопрос",

		"ua": "Вопрос",

		"en": "Question",

		"it": "Question"

	},

	"Причина поломки": {

		"ru": "Причина поломки",

		"ua": "Причина поломки",

		"en": "Fault reason",

		"it": "Fault reason"

	},

	"Контент": {

		"ru": "Контент",

		"ua": "Контент",

		"en": "Content",

		"it": "Content"

	},

	"Вспомогательная": {

		"ru": "Вспомогательная",

		"ua": "Вспомогательная",

		"en": "Addition",

		"it": "Addition"

	},

	"Вперед": {

		"ru": "Вперед",

		"ua": "Вперед",

		"en": "Forward",

		"it": "Forward"

	},

	"Код или наименование": {

		"ru": "Код или наименование",

		"ua": "Код или наименование",

		"en": "Code or description",

		"it": "Code or description"

	},

	"Гарантийные запросы": {

		"ru": "Гарантийные запросы",

		"ua": "Гарантийные запросы",

		"en": "Warranty claims",

		"it": "Warranty claims"

	},

	"Все машины": {

		"ru": "Все машины",

		"ua": "Все машины",

		"en": "All machines",

		"it": "All machines"

	},

	"Сравнение по годам": {

		"ru": "Сравнение по годам",

		"ua": "Сравнение по годам",

		"en": "Two years comparison",

		"it": "Two years comparison"

	},

	"Движения": {

		"ru": "Движения",

		"ua": "Движения",

		"en": "Movements",

		"it": "Movements"

	},

	"Все менеджеры": {

		"ru": "Все менеджеры",

		"ua": "Все менеджеры",

		"en": "All managers",

		"it": "All managers"

	},

	"Все модели": {

		"ru": "Все модели",

		"ua": "Все модели",

		"en": "All models",

		"it": "All models"

	},

	"Всего позиций": {

		"ru": "Всего позиций",

		"ua": "Всего позиций",

		"en": "Totally items",

		"it": "Totally items"

	},

	"Должность": {

		"ru": "Должность",

		"ua": "Должность",

		"en": "Role",

		"it": "Role"

	},

	"Машина": {

		"ru": "Машина",

		"ua": "Машина",

		"en": "Machine",

		"it": "Machine"

	},

	"Всего пользователей в системе по текущей стране": {

		"ru": "Всего пользователей в системе по текущей стране",

		"ua": "Всего пользователей в системе по текущей стране",

		"en": "Total users in system by current country",

		"it": "Total users in system by current country"

	},

	"Вы можете сразу отправлять клиентам предложение в pdf версии, либо скачать word файл и модифицировать по своему усмотрению": {

		"ru": "Вы можете сразу отправлять клиентам предложение в pdf версии, либо скачать word файл и модифицировать по своему усмотрению",

		"ua": "Вы можете сразу отправлять клиентам предложение в pdf версии, либо скачать word файл и модифицировать по своему усмотрению",

		"en": "You can send offers to clients in pdf or download word file and modify it as you want",

		"it": "You can send offers to clients in pdf or download word file and modify it as you want"

	},

	"Вы также можете сделать любые файлы доступными оффлайн на ваших мобильных устройствах, воспользуйтесь функцией оффлайн-доступ (у вас должно быть установлено мобильное приложение Google Drive)": {

		"ru": "Вы также можете сделать любые файлы доступными оффлайн на ваших мобильных устройствах, воспользуйтесь функцией оффлайн-доступ (у вас должно быть установлено мобильное приложение Google Drive)",

		"ua": "Вы также можете сделать любые файлы доступными оффлайн на ваших мобильных устройствах, воспользуйтесь функцией оффлайн-доступ (у вас должно быть установлено мобильное приложение Google Drive)",

		"en": "You can make all files available offline on your mobile devices, by using function \"make offline\" (Google Drive app should be installed on your mobile device)",

		"it": "You can make all files available offline on your mobile devices, by using function \"make offline\" (Google Drive app should be installed on your mobile device)"

	},

	"Год/регион": {

		"ru": "Год/регион",

		"ua": "Год/регион",

		"en": "City/region",

		"it": "City/region"

	},

	"Применить": {

		"ru": "Применить",

		"ua": "Применить",

		"en": "Apply",

		"it": "Apply"

	},

	"Выбрать еще": {

		"ru": "Выбрать еще",

		"ua": "Выбрать еще",

		"en": "Select more",

		"it": "Select more"

	},

	"Регионы": {

		"ru": "Регионы",

		"ua": "Регионы",

		"en": "Regions",

		"it": "Regions"

	},

	"Выбрать модель": {

		"ru": "Выбрать модель",

		"ua": "Выбрать модель",

		"en": "Select model",

		"it": "Select model"

	},

	"Выйти": {

		"ru": "Выйти",

		"ua": "Выйти",

		"en": "Log out",

		"it": "Log out"

	},

	"Гарантия": {

		"ru": "Гарантия",

		"ua": "Гарантия",

		"en": "Warranty",

		"it": "Warrantyc"

	},

	"Дата оплаты": {

		"ru": "Дата оплаты",

		"ua": "Дата оплаты",

		"en": "Payment date",

		"it": "Payment date"

	},

	"Дата последнего обновления оффлайн данных": {

		"ru": "Дата последнего обновления оффлайн данных",

		"ua": "Дата последнего обновления оффлайн данных",

		"en": "Date of last offline data update",

		"it": "Date of last offline data update"

	},

	"Требуются доработки": {

		"ru": "Требуются доработки",

		"ua": "Требуются доработки",

		"en": "Modifications required",

		"it": "Modifications required"

	},

	"Каталог": {

		"ru": "Каталог",

		"ua": "Каталог",

		"en": "Catalog",

		"it": "Catalog"

	},

	"Дилер": {

		"ru": "Дилер",

		"ua": "Дилер",

		"en": "Dealer",

		"it": "Dealer"

	},

	"Деб задолженность": {

		"ru": "Деб задолженность",

		"ua": "Деб задолженность",

		"en": "Open items",

		"it": "Open items"

	},

	"Дебиторка": {

		"ru": "Дебиторка",

		"ua": "Дебиторка",

		"en": "Open items",

		"it": "Open items"

	},

	"Денежные средства": {

		"ru": "Денежные средства",

		"ua": "Денежные средства",

		"en": "Cash",

		"it": "Cash"

	},

	"Список заказов": {

		"ru": "Список заказов",

		"ua": "Список заказов",

		"en": "Orders list",

		"it": "Orders list"

	},

	"Сервис": {

		"ru": "Сервис",

		"ua": "Сервис",

		"en": "Warranty claims",

		"it": "Warranty claims"

	},

	"По клиенту": {

		"ru": "По клиенту",

		"ua": "По клиенту",

		"en": "By client",

		"it": "By client"

	},

	"Для менеджера и клиента": {

		"ru": "Для менеджера и клиента",

		"ua": "Для менеджера и клиента",

		"en": "For managers and dealers",

		"it": "For managers and dealers\n"

	},

	"Добавить": {

		"ru": "Добавить",

		"ua": "Добавить",

		"en": "Add",

		"it": "Add"

	},

	"Добавить код": {

		"ru": "Добавить код",

		"ua": "Добавить код",

		"en": "Add code",

		"it": "Add code"

	},

	"Ефимов Кирилл": {

		"ru": "Ефимов Кирилл",

		"ua": "Ефимов Кирилл",

		"en": "Efimov Kirill",

		"it": "Efimov Kirill"

	},

	"Завершенные": {

		"ru": "Завершенные",

		"ua": "Завершенные",

		"en": "Completed",

		"it": "Completed"

	},

	"Завод": {

		"ru": "Завод",

		"ua": "Завод",

		"en": "Plant",

		"it": "Plant"

	},

	"Заголовок": {

		"ru": "Заголовок",

		"ua": "Заголовок",

		"en": "Title",

		"it": "Title"

	},

	"Модель": {

		"ru": "Модель",

		"ua": "Модель",

		"en": "Model",

		"it": "Model"

	},

	"Загрузить": {

		"ru": "Загрузить",

		"ua": "Загрузить",

		"en": "Upload",

		"it": "Upload"

	},

	"Загрузить данные для работы offline": {

		"ru": "Загрузить данные для работы offline",

		"ua": "Загрузить данные для работы offline",

		"en": "Download data to work offline",

		"it": "Download data to work offline"

	},

	"Загрузить контент": {

		"ru": "Загрузить контент",

		"ua": "Загрузить контент",

		"en": "Load content",

		"it": "Load content"

	},

	"Заказ": {

		"ru": "Заказ",

		"ua": "Заказ",

		"en": "Order",

		"it": "Order"

	},

	"Заказы": {

		"ru": "Заказы",

		"ua": "Заказы",

		"en": "Orders",

		"it": "Orders"

	},

	"Заказы поставщику": {

		"ru": "Заказы поставщику",

		"ua": "Заказы поставщику",

		"en": "Purchase orders",

		"it": "Purchase orders"

	},

	"Закрыть": {

		"ru": "Закрыть",

		"ua": "Закрыть",

		"en": "Close",

		"it": "Close"

	},

	"Запрос дилера": {

		"ru": "Запрос дилера",

		"ua": "Запрос дилера",

		"en": "Dealer request",

		"it": "Dealer request"

	},

	"Запрос менеджера": {

		"ru": "Запрос менеджера",

		"ua": "Запрос менеджера",

		"en": "Manager request",

		"it": "Manager request"

	},

	"Запросить": {

		"ru": "Запросить",

		"ua": "Запросить",

		"en": "Request",

		"it": "Request"

	},

	"Польз @work": {

		"ru": "Польз @work",

		"ua": "Польз @work",

		"en": "User @work",

		"it": "User @work"

	},

	"Значение": {

		"ru": "Значение",

		"ua": "Значение",

		"en": "Value",

		"it": "Value"

	},

	"Изменить": {

		"ru": "Изменить",

		"ua": "Изменить",

		"en": "Edit",

		"it": "Edit"

	},

	"Изменить порядок": {

		"ru": "Изменить порядок",

		"ua": "Изменить порядок",

		"en": "Change order",

		"it": "Change order"

	},

	"Имя файла": {

		"ru": "Имя файла",

		"ua": "Имя файла",

		"en": "File name",

		"it": "File name"

	},

	"Индекс / город": {

		"ru": "Индекс / город",

		"ua": "Индекс / город",

		"en": "Postal code / city",

		"it": "Postal code / city"

	},

	"Цена": {

		"ru": "Цена",

		"ua": "Цена",

		"en": "Price",

		"it": "Price"

	},

	"История": {

		"ru": "История",

		"ua": "История",

		"en": "History",

		"it": "History"

	},

	"исполнительный директор": {

		"ru": "исполнительный директор",

		"ua": "исполнительный директор",

		"en": "executive director",

		"it": "executive director"

	},

	"Итого не прод у всех дилеров": {

		"ru": "Итого не прод у всех дилеров",

		"ua": "Итого не прод у всех дилеров",

		"en": "Total unsold machines dealers stock",

		"it": "Total unsold machines dealers stock"

	},

	"Картинка": {

		"ru": "Картинка",

		"ua": "Картинка",

		"en": "Picture",

		"it": "Picture"

	},

	"Картинки": {

		"ru": "Картинки",

		"ua": "Картинки",

		"en": "Картинки",

		"it": "Картинки"

	},

	"Цены клиент": {

		"ru": "Цены клиент",

		"ua": "Цены клиент",

		"en": "Prices client",

		"it": "Prices client"

	},

	"Пр - машины будут произведены в следующие 2 месяца": {

		"ru": "Пр - машины будут произведены в следующие 2 месяца",

		"ua": "Пр - машины будут произведены в следующие 2 месяца",

		"en": "Pr - machines to be produced in next 2 month",

		"it": "Pr - machines to be produced in next 2 month"

	},

	"Каталоги": {

		"ru": "Каталоги",

		"ua": "Каталоги",

		"en": "Leaflets",

		"it": "Leaflets"

	},

	"Категории": {

		"ru": "Категории",

		"ua": "",

		"en": "",

		"it": ""

	},

	"Категория": {

		"ru": "Категория",

		"ua": "Категория",

		"en": "Category",

		"it": "Category"

	},

	"Категория поломки": {

		"ru": "Категория поломки",

		"ua": "Категория поломки",

		"en": "Fault category",

		"it": "Fault category"

	},

	"Клиент": {

		"ru": "Клиент",

		"ua": "Клиент",

		"en": "Client",

		"it": "Client"

	},

	"Клиенты": {

		"ru": "Клиенты",

		"ua": "Клиенты",

		"en": "Customers",

		"it": "Customers"

	},

	"Код запчасти": {

		"ru": "Код запчасти",

		"ua": "Код запчасти",

		"en": "Part code",

		"it": "Part code"

	},

	"Код и количество": {

		"ru": "Код и количество",

		"ua": "Код и количество",

		"en": "Code and quantity",

		"it": "Code and quantity"

	},

	"Код, наим, номер": {

		"ru": "Код, наим, номер",

		"ua": "Код, наим, номер",

		"en": "Code, descr, nr",

		"it": "Code, descr, nr"

	},

	"Редактирование": {

		"ru": "Редактирование",

		"ua": "Редактирование",

		"en": "Edit",

		"it": "Edit"

	},

	"Коды": {

		"ru": "Коды",

		"ua": "Коды",

		"en": "Codes",

		"it": "Codes"

	},

	"На стадии разработки": {

		"ru": "На стадии разработки",

		"ua": "На стадии разработки",

		"en": "Development in progress",

		"it": "Development in progress"

	},

	"Количество": {

		"ru": "Количество",

		"ua": "Количество",

		"en": "Quantity",

		"it": "Quantity"

	},

	"Комментарий": {

		"ru": "Комментарий",

		"ua": "Комментарий",

		"en": "Comment",

		"it": "Comment"

	},

	"Коммерческие предложения": {

		"ru": "Коммерческие предложения",

		"ua": "Коммерческие предложения",

		"en": "Final customers offers",

		"it": "Final customers offers"

	},

	"Коммерческие предложения клиентам": {

		"ru": "Коммерческие предложения клиентам",

		"ua": "Коммерческие предложения клиентам",

		"en": "Final customers offers",

		"it": "Final customers offers"

	},

	"Конкуренты": {

		"ru": "Конкуренты",

		"ua": "Конкуренты",

		"en": "Competitors",

		"it": "Competitors"

	},

	"Сохранить": {

		"ru": "Сохранить",

		"ua": "Сохранить",

		"en": "Save",

		"it": "Save"

	},

	"Редактирование цен": {

		"ru": "Редактирование цен",

		"ua": "Редактирование цен",

		"en": "Edit prices",

		"it": "Edit prices"

	},

	"Контакт": {

		"ru": "Контакт",

		"ua": "Контакт",

		"en": "Contact",

		"it": "Contact"

	},

	"Контактное лицо / телефон": {

		"ru": "Контактное лицо / телефон",

		"ua": "Контактное лицо / телефон",

		"en": "Contact person / phone",

		"it": "Contact person / phone"

	},

	"Контактные лица": {

		"ru": "Контактные лица",

		"ua": "Контактные лица",

		"en": "Контактные лица",

		"it": "Контактные лица"

	},

	"Контрагент": {

		"ru": "Контрагент",

		"ua": "Контрагент",

		"en": "Customer",

		"it": "Customer"

	},

	"Подкатегория": {

		"ru": "Подкатегория",

		"ua": "Подкатегория",

		"en": "Subcategory",

		"it": "Subcategory"

	},

	"Модели": {

		"ru": "Модели",

		"ua": "Модели",

		"en": "Models",

		"it": "Models"

	},

	"Контрагентов": {

		"ru": "Контрагентов",

		"ua": "Контрагентов",

		"en": "Customers",

		"it": "Customers"

	},

	"Контрагенты": {

		"ru": "Контрагенты",

		"ua": "Контрагенты",

		"en": "Customers",

		"it": "Customers"

	},

	"Кредит дней": {

		"ru": "Кредит дней",

		"ua": "Кредит дней",

		"en": "Days of credit",

		"it": "Days of credit"

	},

	"Маркетинг": {

		"ru": "Маркетинг",

		"ua": "Маркетинг",

		"en": "Marketing",

		"it": "Marketing"

	},

	"Машины": {

		"ru": "Машины",

		"ua": "Машины",

		"en": "Machines",

		"it": "Machines"

	},

	"Машины на складах": {

		"ru": "Машины на складах",

		"ua": "Машины на складах",

		"en": "Machines on stock",

		"it": "Machines on stock"

	},

	"СН": {

		"ru": "СН",

		"ua": "СН",

		"en": "SN",

		"it": "SN"

	},

	"Машины проданные дилеру": {

		"ru": "Машины проданные дилеру",

		"ua": "Машины проданные дилеру",

		"en": "Machines sold to dealer",

		"it": "Machines sold to dealer"

	},

	"Машины свободные от заказов": {

		"ru": "Машины свободные от заказов",

		"ua": "Машины свободные от заказов",

		"en": "Free machines",

		"it": "Free machines"

	},

	"Удалить аксессуар": {

		"ru": "Удалить аксессуар",

		"ua": "Удалить аксессуар",

		"en": "Remove accessory",

		"it": "Remove accessory"

	},

	"Медиа": {

		"ru": "Медиа",

		"ua": "Медиа",

		"en": "Media",

		"it": "Media"

	},

	"Медиа библиотека": {

		"ru": "Медиа библиотека",

		"ua": "Медиа библиотека",

		"en": "Media library",

		"it": "Media library"

	},

	"Менеджер": {

		"ru": "Менеджер",

		"ua": "Менеджер",

		"en": "Manager",

		"it": "Manager"

	},

	"Модификации": {

		"ru": "Модификации",

		"ua": "Модификации",

		"en": "Variants",

		"it": "Variants"

	},

	"на сумму": {

		"ru": "на сумму",

		"ua": "на сумму",

		"en": "for amount",

		"it": "for amount"

	},

	"Назад": {

		"ru": "Назад",

		"ua": "Назад",

		"en": "Back",

		"it": "Back"

	},

	"Очистить корзину": {

		"ru": "Очистить корзину",

		"ua": "Очистить корзину",

		"en": "Clear cart",

		"it": "Clear cart"

	},

	"Наименование": {

		"ru": "Наименование",

		"ua": "Наименование",

		"en": "Description",

		"it": "Description"

	},

	"Наименование поломки": {

		"ru": "Наименование поломки",

		"ua": "Наименование поломки",

		"en": "Fault description",

		"it": "Fault description"

	},

	"Найти": {

		"ru": "Найти",

		"ua": "Найти",

		"en": "Find",

		"it": "Find"

	},

	"Текст": {

		"ru": "Текст",

		"ua": "Текст",

		"en": "Text",

		"it": "Text"

	},

	"Наличие": {

		"ru": "Наличие",

		"ua": "Наличие",

		"en": "Availability",

		"it": "Availability"

	},

	"Отправка заказов": {

		"ru": "Отправка заказов",

		"ua": "Отправка заказов",

		"en": "Send orders",

		"it": "Send orders"

	},

	"Не задан комментарий": {

		"ru": "Не задан комментарий",

		"ua": "Не задан комментарий",

		"en": "Comnment is missing",

		"it": "Comnment is missing"

	},

	"Не задана дата поставки": {

		"ru": "Не задана дата поставки",

		"ua": "Не задана дата поставки",

		"en": "Delivery date is missing",

		"it": "Delivery date is missing"

	},

	"Не отображать": {

		"ru": "Не отображать",

		"ua": "Не отображать",

		"en": "Hide",

		"it": "Hide"

	},

	"НЕ проданные дилером": {

		"ru": "НЕ проданные дилером",

		"ua": "НЕ проданные дилером",

		"en": "Unsold by dealer",

		"it": "Unsold by dealer"

	},

	"Номер запроса": {

		"ru": "Номер запроса",

		"ua": "Номер запроса",

		"en": "Claim nr",

		"it": "Claim nr"

	},

	"Обновить": {

		"ru": "Обновить",

		"ua": "Обновить",

		"en": "Update",

		"it": "Update"

	},

	"Оборот": {

		"ru": "Оборот",

		"ua": "Оборот",

		"en": "Turnover",

		"it": "Turnover"

	},

	"обработал": {

		"ru": "обработал",

		"ua": "обработал",

		"en": "processed by",

		"it": "processed by"

	},

	"Обучающий материал": {

		"ru": "Обучающий материал",

		"ua": "Обучающий материал",

		"en": "Education material",

		"it": "Education material"

	},

	"Одобрен": {

		"ru": "Одобрен",

		"ua": "Одобрен",

		"en": "Approved",

		"it": "Approved"

	},

	"ООО \"МАСКИО-ГАСПАОДО РУССИЯ\"": {

		"ru": "ООО \"МАСКИО-ГАСПАОДО РУССИЯ\"",

		"ua": "ООО \"МАСКИО-ГАСПАОДО РУССИЯ\"",

		"en": "ООО \"МАСКИО-ГАСПАОДО РУССИЯ\"",

		"it": "ООО \"МАСКИО-ГАСПАОДО РУССИЯ\""

	},

	"Описание": {

		"ru": "Описание",

		"ua": "Описание",

		"en": "Description",

		"it": "Description"

	},

	"Опции": {

		"ru": "Опции",

		"ua": "Опции",

		"en": "Options",

		"it": "Options"

	},

	"Опция": {

		"ru": "Опция",

		"ua": "Опция",

		"en": "Option",

		"it": "Option"

	},

	"Опция по-умолчанию": {

		"ru": "Опция по-умолчанию",

		"ua": "Опция по-умолчанию",

		"en": "Default option",

		"it": "Default option"

	},

	"Основной менеджер": {

		"ru": "Основной менеджер",

		"ua": "Основной менеджер",

		"en": "Main manager",

		"it": "Main manager"

	},

	"Отклонен": {

		"ru": "Отклонен",

		"ua": "Отклонен",

		"en": "Rejected",

		"it": "Rejected"

	},

	"Отмена": {

		"ru": "Отмена",

		"ua": "Отмена",

		"en": "Cancel",

		"it": "Cancel"

	},

	"Отображать дилерские цены": {

		"ru": "Отображать дилерские цены",

		"ua": "Отображать дилерские цены",

		"en": "Show dealers prices",

		"it": "Show dealers prices"

	},

	"Отображать инлайн": {

		"ru": "Отображать инлайн",

		"ua": "Отображать инлайн",

		"en": "Show inline",

		"it": "Show inline"

	},

	"Отображать свернуто": {

		"ru": "Отображать свернуто",

		"ua": "Отображать свернуто",

		"en": "Show collapsed",

		"it": "Show collapsed"

	},

	"Отправить заказ": {

		"ru": "Отправить заказ",

		"ua": "Отправить заказ",

		"en": "Send request",

		"it": "Send request"

	},

	"Отправить на email": {

		"ru": "Отправить на email",

		"ua": "Отправить на email",

		"en": "Send to email",

		"it": "Send to email"

	},

	"Отправьте предложение на свой электронный адрес или откройте ссылку на google drive": {

		"ru": "Отправьте предложение на свой электронный адрес или откройте ссылку на google drive",

		"ua": "Отправьте предложение на свой электронный адрес или откройте ссылку на google drive",

		"en": "Send offer for you email or open link on google drive",

		"it": "Send offer for you email or open link on google drive"

	},

	"Пароль": {

		"ru": "Пароль",

		"ua": "Пароль",

		"en": "Пароль",

		"it": "Пароль"

	},

	"Перейти": {

		"ru": "Перейти",

		"ua": "Перейти",

		"en": "Show more",

		"it": "Show more"

	},

	"Характеристики": {

		"ru": "Характеристики",

		"ua": "Характеристики",

		"en": "Technical data",

		"it": "Technical data"

	},

	"Перейти к видео": {

		"ru": "Перейти к видео",

		"ua": "Перейти к видео",

		"en": "Go to video",

		"it": "Go to video"

	},

	"План производства": {

		"ru": "План производства",

		"ua": "План производства",

		"en": "Production plan",

		"it": "Production plan"

	},

	"По датам": {

		"ru": "По датам",

		"ua": "По датам",

		"en": "By dates",

		"it": "By dates"

	},

	"По менеджеру": {

		"ru": "По менеджеру",

		"ua": "По менеджеру",

		"en": "By manager",

		"it": "By manager"

	},

	"Поделиться": {

		"ru": "Поделиться",

		"ua": "Поделиться",

		"en": "Share",

		"it": "Share"

	},

	"По наименованию": {

		"ru": "По наименованию",

		"ua": "По наименованию",

		"en": "By description",

		"it": "By description"

	},

	"Регистрационные данные": {

		"ru": "Регистрационные данные",

		"ua": "Регистрационные данные",

		"en": "Registration data",

		"it": "Registration data"

	},

	"Регион": {

		"ru": "Регион",

		"ua": "Регион",

		"en": "Region",

		"it": "Region"

	},

	"Подробнее": {

		"ru": "Подробнее",

		"ua": "Подробнее",

		"en": "Details",

		"it": "Details"

	},

	"Подтвержд заказ": {

		"ru": "Подтвержд заказ",

		"ua": "Подтвержд заказ",

		"en": "Confirmed order",

		"it": "Confirmed order"

	},

	"Поиск по любому полю": {

		"ru": "Поиск по любому полю",

		"ua": "Поиск по любому полю",

		"en": "Search by any field",

		"it": "Search by any field"

	},

	"Показать": {

		"ru": "Показать",

		"ua": "Показать",

		"en": "Show",

		"it": "Show"

	},

	"Склад дилера": {

		"ru": "Склад дилера",

		"ua": "Склад дилера",

		"en": "Stock dealer",

		"it": "Stock dealer"

	},

	"Показать только моих": {

		"ru": "Показать только моих",

		"ua": "Показать только моих",

		"en": "Show only mine",

		"it": "Show only mine"

	},

	"Пользователи": {

		"ru": "Пользователи",

		"ua": "Пользователи",

		"en": "Users",

		"it": "Users"

	},

	"Удалить опцию": {

		"ru": "Удалить опцию",

		"ua": "Удалить опцию",

		"en": "Remove option",

		"it": "Remove option"

	},

	"Пользователь @work / дата": {

		"ru": "Пользователь @work / дата",

		"ua": "Пользователь @work / дата",

		"en": "User @work / date",

		"it": "User @work / date"

	},

	"Поставка / регистрация / гарантия": {

		"ru": "Поставка / регистрация / гарантия",

		"ua": "Поставка / регистрация / гарантия",

		"en": "Shipment / registration / warranty",

		"it": "Shipment / registration / warranty"

	},

	"Пр": {

		"ru": "Пр",

		"ua": "Пр",

		"en": "Pr",

		"it": "Pr"

	},

	"Прайс-лист": {

		"ru": "Прайс-лист",

		"ua": "Прайс-лист",

		"en": "Price list",

		"it": "Price list"

	},

	"Редактирование контента": {

		"ru": "Редактирование контента",

		"ua": "Редактирование контента",

		"en": "Edit content",

		"it": "Edit content"

	},

	"Предложение клиенту": {

		"ru": "Предложение клиенту",

		"ua": "Предложение клиенту",

		"en": "Offer for client",

		"it": "Offer for client"

	},

	"Привязка машин": {

		"ru": "Привязка машин",

		"ua": "Привязка машин",

		"en": "Machine assign",

		"it": "Machine assign"

	},

	"Продажи": {

		"ru": "Продажи",

		"ua": "Продажи",

		"en": "Turrnover and profit",

		"it": "Turrnover and profit"

	},

	"Пульс компании": {

		"ru": "Пульс компании",

		"ua": "Пульс компании",

		"en": "Company pulse",

		"it": "Company pulse"

	},

	"Тестовые машины": {

		"ru": "Тестовые машины",

		"ua": "Тестовые машины",

		"en": "Fields tests",

		"it": "Fields tests"

	},

	"Работать оффлайн": {

		"ru": "Работать оффлайн",

		"ua": "Работать оффлайн",

		"en": "Работать оффлайн",

		"it": "Работать оффлайн"

	},

	"Рег дата": {

		"ru": "Рег дата",

		"ua": "Рег дата",

		"en": "Reg date",

		"it": "Reg date"

	},

	"Условия оплаты": {

		"ru": "Условия оплаты",

		"ua": "Условия оплаты",

		"en": "Payment terms",

		"it": "Payment terms"

	},

	"Регион / клиент": {

		"ru": "Регион / клиент",

		"ua": "Регион / клиент",

		"en": "Region / client",

		"it": "Region / client"

	},

	"Регистрация машин": {

		"ru": "Регистрация машин",

		"ua": "Регистрация машин",

		"en": "Machines registration",

		"it": "Machines registration"

	},

	"Редактирование аксессуара": {

		"ru": "Редактирование аксессуара",

		"ua": "Редактирование аксессуара",

		"en": "Edit accessory",

		"it": "Edit accessory"

	},

	"Редактировать": {

		"ru": "Редактировать",

		"ua": "Редактировать",

		"en": "Edit",

		"it": "Edit"

	},

	"Рекламные каталоги": {

		"ru": "Рекламные каталоги",

		"ua": "Рекламные каталоги",

		"en": "Leaflets",

		"it": "Leaflets"

	},

	"Серийный номер / модель / тип": {

		"ru": "Серийный номер / модель / тип",

		"ua": "Серийный номер / модель / тип",

		"en": "SN / model / type",

		"it": "SN / model / type"

	},

	"рублей": {

		"ru": "рублей",

		"ua": "рублей",

		"en": "rubles",

		"it": "rubles"

	},

	"евро": {

		"ru": "евро",

		"ua": "евро",

		"en": "eur",

		"it": "eur"

	},

	"Цены дилер": {

		"ru": "Цены дилер",

		"ua": "Цены дилер",

		"en": "Prices dealer",

		"it": "Prices dealer"

	},

	"Свойства": {

		"ru": "Свойства",

		"ua": "Свойства",

		"en": "Properties",

		"it": "Properties"

	},

	"Серийный номер": {

		"ru": "Серийный номер",

		"ua": "Серийный номер",

		"en": "Serial number",

		"it": "Serial number"

	},

	"Ск": {

		"ru": "Ск",

		"ua": "Ск",

		"en": "St",

		"it": "St"

	},

	"Ск - машины на складах заводов": {

		"ru": "Ск - машины на складах заводов",

		"ua": "Ск - машины на складах заводов",

		"en": "Sk - machines on plant stocks",

		"it": "Sk - machines on plant stocks"

	},

	"Скидка на запчасти": {

		"ru": "Скидка на запчасти",

		"ua": "Скидка на запчасти",

		"en": "Discount for spare parts",

		"it": "Discount for spare parts"

	},

	"Склад": {

		"ru": "Склад",

		"ua": "Склад",

		"en": "Stock",

		"it": "Stock"

	},

	"Скрывать заголовок": {

		"ru": "Скрывать заголовок",

		"ua": "Скрывать заголовок",

		"en": "Hide title",

		"it": "Hide title"

	},

	"Сменить пароль": {

		"ru": "Сменить пароль",

		"ua": "Сменить пароль",

		"en": "Change password",

		"it": "Change password"

	},

	"Соколенко Павел": {

		"ru": "Соколенко Павел",

		"ua": "Соколенко Павел",

		"en": "Sokolenko Pavel",

		"it": "Sokolenko Pavel"

	},

	"Ссылка": {

		"ru": "Ссылка",

		"ua": "Ссылка",

		"en": "Link",

		"it": "Link"

	},

	"Чевычелов Денис": {

		"ru": "Чевычелов Денис",

		"ua": "Чевычелов Денис",

		"en": "Chevychelov Denis",

		"it": "Chevychelov Denis"

	},

	"Ссылка на машину": {

		"ru": "Ссылка на машину",

		"ua": "Ссылка на машину",

		"en": "Link for machine",

		"it": "Link for machine"

	},

	"Телефон": {

		"ru": "Телефон",

		"ua": "Телефон",

		"en": "Phone",

		"it": "Phone"

	},

	"Стандартная комплектация": {

		"ru": "Стандартная комплектация",

		"ua": "Стандартная комплектация",

		"en": "Standard complectation",

		"it": "Standard complectation"

	},

	"Комплектация": {

		"ru": "Комплектация",

		"ua": "Комплектация",

		"en": "Complectation",

		"it": "Complectation"

	},

	"Цена для дилера": {

		"ru": "Цена для дилера",

		"ua": "Цена для дилера",

		"en": "Price for dealer",

		"it": "Price for dealer"

	},

	"Статистика": {

		"ru": "Статистика",

		"ua": "Статистика",

		"en": "Statistic",

		"it": "Statistic"

	},

	"Статус": {

		"ru": "Статус",

		"ua": "Статус",

		"en": "Статус",

		"it": "Статус"

	},

	"Стоимость": {

		"ru": "Стоимость",

		"ua": "Стоимость",

		"en": "Value",

		"it": "Value"

	},

	"Сумма": {

		"ru": "Сумма",

		"ua": "Сумма",

		"en": "Amount",

		"it": "Amount"

	},

	"Тел/email": {

		"ru": "Тел/email",

		"ua": "Тел/email",

		"en": "Phone/email",

		"it": "Phone/email"

	},

	"Тех характеристики": {

		"ru": "Тех характеристики",

		"ua": "Тех характеристики",

		"en": "Technical data",

		"it": "Technical data"

	},

	"Техника": {

		"ru": "Техника",

		"ua": "Техника",

		"en": "Machines",

		"it": "Machines"

	},

	"Тип": {

		"ru": "Тип",

		"ua": "Тип",

		"en": "Type",

		"it": "Type"

	},

	"Только существующие": {

		"ru": "Только существующие",

		"ua": "Только существующие",

		"en": "Only exist",

		"it": "Only exist"

	},

	"Трактор": {

		"ru": "Трактор",

		"ua": "Трактор",

		"en": "Tractor",

		"it": "Tractor"

	},

	"Тэг": {

		"ru": "Тэг",

		"ua": "Тэг",

		"en": "Tag",

		"it": "Tag"

	},

	"Удалить": {

		"ru": "Удалить",

		"ua": "Удалить",

		"en": "Remove",

		"it": "Remove"

	},

	"Удалить комплектацию": {

		"ru": "Удалить комплектацию",

		"ua": "Удалить комплектацию",

		"en": "Remove complectation",

		"it": "Remove complectation"

	},

	"Фильтр по машине": {

		"ru": "Фильтр по машине",

		"ua": "Фильтр по машине",

		"en": "Filter by machine",

		"it": "Filter by machine"

	},

	"Фото": {

		"ru": "Фото",

		"ua": "Фото",

		"en": "Pictures",

		"it": "Pictures"

	},

	"Фотографии техники": {

		"ru": "Фотографии техники",

		"ua": "Фотографии техники",

		"en": "Machines photos",

		"it": "Machines photos"

	},

	"Цена для клиента": {

		"ru": "Цена для клиента",

		"ua": "Цена для клиента",

		"en": "Price for client",

		"it": "Price for client"

	},

	"Цена клиент": {

		"ru": "Цена клиент",

		"ua": "Цена клиент",

		"en": "Price client",

		"it": "Price client"

	},

	"Цены": {

		"ru": "Цены",

		"ua": "Цены",

		"en": "Prices",

		"it": "Prices"

	},

	"Компания": {

		"ru": "Компания",

		"ua": "Компания",

		"en": "Company",

		"it": "Company"

	},

	"Ваш запрос успешно отправлен. В ближайшее время вы получите ответ по электронной почте.": {

		"ru": "Ваш запрос успешно отправлен. В ближайшее время вы получите ответ по электронной почте.",

		"ua": "Ваш запрос успешно отправлен. В ближайшее время вы получите ответ по электронной почте.",

		"en": "Your request has been sent. You will receive an answer by email shortly",

		"it": "Your request has been sent. You will receive an answer by email shortly"
	},

	"Не выбраны опции одной из позиций": {

		"ru": "Не выбраны опции одной из позиций",

		"ua": "Не выбраны опции одной из позиций",

		"en": "Options not selected for one or more items",

		"it": "Options not selected for one or more items"
	},


};
