export const isNumber = number => {
  if (number.trim() === '') {
    return true;
  }
  return !isNaN(parseInt(number, 10));
};
