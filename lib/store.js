import React, { useState, useEffect, useContext, createContext } from 'react';
import withWidth, { isWidthUp } from '@material-ui/core/withWidth';

const storeContext = createContext();

const StoreProvider = ({ children, width }) => {
  
  const cond = !isWidthUp('sm', width);
  const [appUser, setAppUser] = useState(false);
  const [uid, setUid] = useState('');
  const [token, setToken] = useState('');
  const [country, setCountry] = useState(0);
  const [customersData, setCustomersData] = useState(false); // data about all users customers
  const [customerCurOpt, setCustomerCurOpt] = useState(false); // current customer / for using inside select
  const [customerCurData, setCustomerCurData] = useState(false); // current customer / for 
  const [lang, setLang] = useState('ru');
  const [permiss, setPermiss] = useState(false);
  const [offline, setOffline] = useState(false);
  const [loading, setLoading] = useState(false);
  const [globalOpt, setGlobalOpt] = useState(false);
  const [cart, setCartHandler] = useState(false);
  const [cartOpen, setCartOpen] = useState(false);
  const [refresh, setRefresh] = useState(false);
  const [mutate, setMutate] = useState(false);

  const setCart = cart => {
    setCartHandler(cart);
    setRefresh(!refresh);
  };

  const store = {
    cond,
    appUser, setAppUser,
    uid, setUid,
    token, setToken,
    customersData, setCustomersData,
    customerCurOpt, setCustomerCurOpt,
    customerCurData, setCustomerCurData,
    country, setCountry,
    lang, setLang,
    permiss, setPermiss,
    offline, setOffline,
    loading, setLoading,
    globalOpt, setGlobalOpt,
    cart, setCart,
    cartOpen, setCartOpen,
    refresh,
    mutate, setMutate,
  };

  return  (
    <storeContext.Provider value={store}>
      {children}
    </storeContext.Provider>
  );
}

export default withWidth()(StoreProvider); 

export const useStore = () => {
  return useContext(storeContext);
};



