import React from 'react';
import useSWR from 'swr';
import Loading from 'components/loading';

const fetcher = (...args) => fetch(...args).then(res => res.json());

function fetchData(url) {
  const { data, error } = useSWR(url, fetcher);
  return {
    data: data === undefined || data.length === 0 ? [] : JSON.parse(data),
    isLoading: !error && !data,
    isError: error,
  }
}

function Error() {
  return (
    <div>
      Error...
    </div>
  );
}

function DataProvider({ render, url }) {

  const { data, isLoading, isError } = fetchData(url);

  if (isLoading) return <Loading />
  if (isError) return <Error />
  if (!data) {
    return null;
  }

  return (
    <div style={{ width: '100%', margin: '0 auto' }} id='data-container'>
      {render(data)}
    </div>  
  )
}

export default DataProvider;