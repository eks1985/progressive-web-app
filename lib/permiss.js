import acl from './access-rules';

export const calcPermiss = (user) => {
  const { roles } = user;
  const aclCopy = { ...acl };
  Object.keys(aclCopy).forEach(key => {
    const item = aclCopy[key];
    Object.keys(item).forEach(itemKey => {
      const curRoles = item[itemKey];
      let hasAccess = false;
      if (curRoles.length) {
        curRoles.forEach(role => {
          if (roles && roles.length > 0 && roles.includes(role)) {
            hasAccess = true;
          }
        });
        item[itemKey] = hasAccess;
      }
    });
  });
  return aclCopy;
};
