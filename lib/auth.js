import React, { useState, useEffect, useContext, createContext } from 'react';
import Router from 'next/router';
import { useStore } from 'lib/store';
import { calcPermiss } from 'lib/permiss';
import Auth from 'components/auth';
import axios from 'axios';
import firebase from './firebase';
import Loading from 'components/loading';

async function fetchUserMasterData(user) {
  const url = `https://webhooks.mongodb-realm.com/api/client/v2.0/app/mgd-ilrht/service/http/incoming_webhook/user_fetch?secret=L0sS3Bz1mdCm6Kuz&uid=${user.uid}`;
  const resp = await axios.get(url);
  if (resp.status === 200) {
    return JSON.parse(resp.data);
  }
  return false;
};

const authContext = createContext();

export default function AuthProvider({ children }) {

  const store = useStore();
  const auth = useFirebaseAuth(store);

  if (auth.loading) {
    return <Loading />;
  }

  if (auth.user) {
    return <authContext.Provider value={auth}>{children}</authContext.Provider>;
  }

  return <Auth auth={auth} />
}

export const useAuth = () => {
  return useContext(authContext);
};

function useFirebaseAuth(store) {

  const [user, setUser] = useState(true);
  const [loading, setLoading] = useState(false);
  const [resetLoading, setResetLoading] = useState(false);
  const [loginError, setLoginError] = useState('');
  const [resetError, setResetError] = useState('');
  const [resetSuccess, setResetSuccess] = useState(false);
  const [resetEmail, setResetEmail] = useState('');

  const handleUser = async (rawUser) => {

    if (rawUser) {
      const user = await formatUser(rawUser);
      setUser(user);
      //load user data from mongo (api key = user.uid)
      const appUser = await fetchUserMasterData(user);
      const permiss = calcPermiss(appUser);
      store.setAppUser(appUser);
      store.setPermiss(permiss);
      store.setCountry(appUser.country);
      store.setCustomersData(appUser.customersData);
      store.setCustomerCurOpt(appUser.customerCurOpt);
      store.setCustomerCurData(appUser.customerCurData);
      store.setUid(appUser._id);
      store.setToken(appUser.customerGuid);
      
      setLoading(false);
      return user;
    } else {

      setUser(false);
      setLoading(false);
      return false;

    }
  };

  const signinWithEmail = (redirect) => {
    
    const email = document.getElementById('loginLogin').value.trim();
    const password = document.getElementById('loginPassword').value.trim();
    
    setLoading(true);
    
    return firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then((response) => {
        handleUser(response.user);
        if (redirect) {
          Router.push(redirect);
        }
      })
      .catch((err) => {
        console.log('err', err);
        setLoading(false);
        setLoginError(err.toString());
      });

  };

  const resetPassword = email => {
    
    setResetLoading(true);

    return firebase
      .auth()
      .sendPasswordResetEmail(email)
      .then(() => {
        setResetLoading(false);
        setResetSuccess(true);
        setResetError(false);
        setResetEmail(email);
      })
      .catch((err) => {
        setResetLoading(false);
        setResetSuccess(false);
        setResetError(err.toString());
      });

  };

  const signout = () => {
    return firebase
      .auth()
      .signOut()
      .then(
        () => {
          handleUser(false);
          store.setPermiss(false);
        }
      );
  };

  useEffect(() => {
    const unsubscribe = firebase.auth().onIdTokenChanged(handleUser);
    return () => unsubscribe();
  }, []);

  return {
    user,
    loading,
    resetLoading,
    loginError,
    resetError,
    resetSuccess,
    resetEmail,
    signinWithEmail,
    signout,
    resetPassword,
    setResetError,
    setResetSuccess,
    setResetEmail,
  };
}

const formatUser = async (user) => {
  return {
    uid: user.uid,
    email: user.email,
    name: user.displayName,
    provider: user.providerData[0].providerId,
    photoUrl: user.photoURL,
  };
};