
export const formatNumb = number => {
  const formatCurrency = 'ru-RU';
  const formatDigits = { minimumFractionDigits: 0 };
  return number.toLocaleString(formatCurrency, formatDigits);
};