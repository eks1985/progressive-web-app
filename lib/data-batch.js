import React from 'react';
import useSWR from 'swr';
import Loading from 'components/loading';

function fetcher(...urls) {
  
  const f = u => fetch(u).then(r => r.json());

  if (urls.length > 0) {
    return Promise.all(urls.map(f));
  }
}

function fetchData(query) {

  const queryKeys = Object.keys(query);
  const { data, error, mutate } = useSWR(queryKeys.map(key => query[key]), fetcher);

  let preparedData = false;
  if (!data || data.length === 0) { 
    preparedData = false;
  } else {
    preparedData = {};
    queryKeys.forEach((key, ind) => {
      const dataItem = data[ind];
      preparedData[key] = dataItem === undefined || dataItem.length === 0 ? [] : JSON.parse(dataItem);
    });
  }

  return {
    data: preparedData,
    isLoading: !error && !data,
    isError: error,
    mutate,
  }
}

function Error() {
  return (
    <div>
      Error...
    </div>
  );
}

function DataProvider({ render, query, skipLoading }) {

  const { data, isLoading, isError, mutate } = fetchData(query);

  if (isLoading) return skipLoading ? null : <Loading />
  if (isError) return <Error />
  if (!data) {
    return null;
  }

  // console.log('data batch', data);

  return (
    <div style={{ width: '100%', margin: '0 auto' }} id='data-container'>
      {render(data, mutate)}
    </div>  
  )
}

export default DataProvider;