export const contains = (string, text) => {
  if (!string || !text) {
    return false;
  }
  return string.search(new RegExp(text, 'i')) > -1;
};

export const capit = string => {
  if (!string) {
    return '';
  }
  return string.charAt(0).toUpperCase() + string.slice(1);
};
