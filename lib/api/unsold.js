import axios from 'axios';
import { baseUrl0, secret } from 'lib/base';
import { v4 } from 'uuid';

export async function addUnsold(url_create, token, dealer, item, mutate) {
  const url = `${baseUrl0}collection_fetch_secure${secret}token=${token}&collection=unsold-mac-dealer&id=${dealer}`;
  const resp = await axios.get(url);
  if (resp.status === 200) {
    const res = JSON.parse(resp.data);
    let items = [];
    if (res.length > 0) {
      items = res[0].items;
    }
    const newItem = {
      id: v4(),
      items: item,
    };
    const newItems = [ ...items, newItem ];
    await axios.post(url_create, { collection: 'unsold-mac-dealer', id: dealer, payload: { items: newItems } });
    mutate();
  }
};

export async function removeUnsold(url_create, token, dealer, index, mutate) {
  const url = `${baseUrl0}collection_fetch_secure${secret}token=${token}&collection=unsold-mac-dealer&id=${dealer}`;
  const resp = await axios.get(url);
  if (resp.status === 200) {
    const res = JSON.parse(resp.data);
    let items = [];
    if (res.length > 0) {
      items = res[0].items;
    }
    const newItems = items.filter((_, ind) => ind !== index);
    await axios.post(url_create, { collection: 'unsold-mac-dealer', id: dealer, payload: { items: newItems } });
    mutate();
  }
};

export async function updateUnsold(url_create, token, dealer, index, qty, mutate) {
  // console.log('url_create', url_create);
  // console.log('token', token);
  // console.log('dealer', dealer);
  // console.log('index', index);
  // console.log('qty', qty);
  // console.log('mutate', mutate);
  const url = `${baseUrl0}collection_fetch_secure${secret}token=${token}&collection=unsold-mac-dealer&id=${dealer}`;
  const resp = await axios.get(url);
  if (resp.status === 200) {
    const res = JSON.parse(resp.data);
    let items = [];
    if (res.length > 0) {
      items = res[0].items;
    }
    const newItems = items.map((elem, ind) => 
      {
        if (ind !== index) {
          return elem;   
        }
        const curValue = elem.items.value;
        const newQty = parseInt(qty, 10);
        const newValue = curValue / elem.items.qty * newQty;
        const newElemItems = { ...elem.items,  qty: newQty, value: Math.ceil(newValue) };
        return { ...elem, items: newElemItems }; 
      }
    );
    await axios.post(url_create, { collection: 'unsold-mac-dealer', id: dealer, payload: { items: newItems } });
    mutate();
  }
};

