
import axios from 'axios';
import { v4 } from 'uuid';

export async function createModelCall(url_create, country, categoryId, mutate, setLoading) {
  const id = v4();
  const shape = {
    id,
    order: 999,
    descr: 'New model',
    vis: 2, // 1 - manager, 2 - manager & client
    country,
    categoryRef : categoryId,
    // picture : string,
    // subcategory : string
    // sets : [string],
  };
  setLoading(true);
  await axios.post(url_create, { collection: 'models', id, payload: shape });
  await mutate();
  setLoading(false);
};

export async function removeModelCall(url_remove, id) {
  console.log('removeModel...');
  // await axios.post(url_remove, { collection: 'categories', id });
};

export async function updateCategoryCall(url_create, item, mutate, setLoading, setCategoryMod) {
  setLoading(true);
  const { contentList, ...other } = item;
  other.content = contentList.map(elem => elem._id);
  const msg = {
    topic: 'updated-category',
    payload: other,
    country: item.country, 
  };
  await axios.post(url_create, { collection: 'categories', id: item.id, payload: other });
  await axios.post(url_create, { collection: 'msg', id: v4(), payload: msg });
  await mutate();
  setCategoryMod(false);
  setLoading(false);
};

export async function updateModelsCall(url_create, items, mutate, setModelsMod, setLoading) {
  setLoading(true);
  const toUpdate = items.filter(item => item.mod).map(item => {
    const { mod, ...other } = item;
    return other;
  });
  const promises = toUpdate.map(item => {
    return axios.post(url_create, { collection: 'models', id: item.id, payload: item });
  });
  await Promise.all(promises);
  const promisesMsg = toUpdate.map(item => {
    const msg = {
      topic: 'updated-model',
      payload: item,
      country: item.country, 
    };
    return axios.post(url_create, { collection: 'msg', id: v4(), payload: msg }); 
  });
  await Promise.all(promisesMsg);
  await mutate();
  setModelsMod(false);
  setLoading(false);
};