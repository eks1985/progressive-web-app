
import axios from 'axios';
import { v4 } from 'uuid';

export async function updateProductCall(url_create, item, mutate, setLoading, setProductMod) {
  setLoading(true);
  await axios.post(url_create, { collection: 'products', id: item.id, payload: item });
  const msg = {
    topic: 'updated-product',
    payload: item,
    country: item.country, 
  };
  await axios.post(url_create, { collection: 'msg', id: v4(), payload: msg });
  await mutate();
  setProductMod(false);
  setLoading(false);
};

export async function updateModelsCall(url_create, toUpdate, mutate, setLoading, setMod) {
  setLoading(true);
  setMod(false);
  const promises = toUpdate.map(item => {
    return axios.post(url_create, { collection: 'models', id: item.id, payload: item }); 
  });
  await Promise.all(promises);
  const promisesMsg = toUpdate.map(item => {
    const msg = {
      topic: 'updated-model',
      payload: item,
      country: item.country, 
    };
    return axios.post(url_create, { collection: 'msg', id: item.id, payload: msg }); 
  });
  await Promise.all(promisesMsg);
  await mutate();
  setLoading(false);
};

export async function sendClientOfferRequest(url_create, product, offer, country, user, setLoading, setDone) {

  setDone(false);
  setLoading(true);

  const payload = {
    topic: 'client-offer-req',
    product,
    offer,
    country,
    user,
  };

  const id = v4();

  await axios.post(url_create, { collection: 'msg', id, payload });

  setTimeout(() => {
    setDone(true);
    setLoading(false);
  }, 1000);

};

export async function getProductMock() {
  const url = 'https://webhooks.mongodb-realm.com/api/client/v2.0/app/mgd-ilrht/service/mgd/incoming_webhook/catalog?secret=L0sS3Bz1mdCm6Kuz&secret=L0sS3Bz1mdCm6Kuz&secret=L0sS3Bz1mdCm6Kuz&lists=p&filters=p{id:7ba662c1-f527-11e7-80cb-6eae8b4f0d81}&vis=2&country=0';
  const resp = await axios.get(url);
  if (resp.status === 200) {
    return JSON.parse(resp.data);
  }
};
