
import axios from 'axios';
import date from 'date-and-time';
import { baseUrl, baseUrl0, secret } from 'lib/base';
import { v4 } from 'uuid';

const url_create = `${baseUrl0}data-create${secret}`;
const url_delete = `${baseUrl}delete_document${secret}`;

export async function createRequest(store) {
  
  const { cart, appUser, country } = store;

  const id = `${cart.data.customer.value}-${date.format(new Date(), 'YYMMDDHHmmssS')}`;
  const createDate = date.format(new Date(), 'DD-MM-YYYY');
  const user = appUser;
  const token = appUser.customerGuid;

  const shape = {
    ...cart.data,
    id,
    user,
    createDate,
    country,
    token,
    v2: true,
  };
  
  shape.created = {
    date: createDate,
    by: appUser.internal ? 'manager' : 'dealer',
    user: {
      name: appUser.name,
      email: appUser.email,
    }
  };

  if (appUser.internal) {
    shape.confirmed = shape.created;
  }

  // keep initial data in case of needed comparison what was modified
  shape.paymentInitial = shape.payment;
  shape.itemsInitial = shape.items;
  shape.delivDateInitial = shape.delivDate;

  // console.log('shape', shape);

  // created {
  //   date
  //   by: dealer|manager
  //   user: name, email
  // }
  // confirmed {
  //   date
  //   user: name, email
  // }
  // processed {
  //   date
  //   user: name, email
  // }
  // events []
  //   type: created/modified/confirmed/processed/payment/shipment
  //   date
  //   user: id, email, name

  store.setLoading(true);

  const msg = {
    topic: 'mg-dealer-request',
    payload: shape,
    country, 
  };

  const msgId = `${date.format(new Date(), 'YYMMDDHHmmssS')}-${shape.customer.value}`;

  await axios.post(url_create, { collection: 'orders', id, payload: shape });
  await axios.post(url_create, { collection: 'msg', id: msgId, payload: msg });

  store.setLoading(false);
  
}

export async function updateRequest(store) {

  const { cart, mutate } = store;

  const { id, ...other } = cart.data;
  other.updated = true;

  store.setLoading(true);
  await axios.post(url_create, { collection: 'orders', id, payload: other });
  if (mutate) {
    await mutate.action();
  }
  store.setLoading(false);
  
}

export async function deleteRequest(id, store, mutate) {
  
  store.setLoading(true);
  await axios.post(url_delete, { collection: 'orders', id });
  await mutate();
  store.setLoading(false);
  
}

export async function createQuestion(store) {

  const { cart, appUser, country } = store;

  const id = `${cart.data.customer.value}-${date.format(new Date(), 'YYMMDDHHmmssS')}`;
  const createDate = date.format(new Date(), 'DD-MM-YYYY');
  const user = appUser;
  const token = appUser.customerGuid;

  const shape = {
    ...cart.data,
    id,
    user,
    createDate,
    country,
    token,
    v2: true,
  };
  
  shape.created = {
    date: createDate,
    by: appUser.internal ? 'manager' : 'dealer',
    user: {
      name: appUser.name,
      email: appUser.email,
    }
  };

  if (appUser.internal) {
    shape.confirmed = shape.created;
  }

  store.setLoading(true);

  const msg = {
    topic: 'mg-dealer-question',
    payload: shape,
    country, 
  };

  const msgId = `${date.format(new Date(), 'YYMMDDHHmmssS')}-${shape.customer.value}`;

  await axios.post(url_create, { collection: 'msg', id: msgId, payload: msg });

  store.setLoading(false);

}

export async function confirmRequest(order, store, mutate) {
  
  const { appUser } = store;
  const { id } = order;

  const confirmed = {
    date: date.format(new Date(), 'DD-MM-YYYY'),
    user: {
      name: appUser.name,
      email: appUser.email,
    }

  };

  const updatedOrder = { ...order, confirmed };

  const msg = {
    topic: 'mg-dealer-request-confirmed',
    payload: updatedOrder,
    country: order.country, 
  };

  const msgId = `${date.format(new Date(), 'YYMMDDHHmmssS')}-${order.customer.value}`;
  
  store.setLoading(true);
  await axios.post(url_create, { collection: 'orders', id, payload: updatedOrder });
  await axios.post(url_create, { collection: 'msg', id: msgId, payload: msg });
  
  await mutate();

  store.setLoading(false);
  
}

export async function createRequestUsedMachine(product, store, setRequestSent) {
  
  const { appUser, country } = store;

  store.setLoading(true);

  const payload = {
    topic: 'mg-dealer-request-used',
    country, 
    user: appUser,
    product,
  };

  await axios.post(url_create, { collection: 'msg', id: v4(), payload });

  store.setLoading(false);

  setRequestSent(true);
  
}
