
import axios from 'axios';
import { v4 } from 'uuid';

export async function createProductCall(url_create, country, model, modelId, mutate, setLoading) {
  const id = v4();
  const shape = {
    id,
    order: 999,
    title: 'New product',
    vis: 2, // 1 - manager, 2 - manager & client
    categoryRef: model.categoryRef, // if is A then accessory should have reference for category
    isA: false, // to mark price level accessory
    // details: string,
    country: country,
    // id: string,
    // modelRef: modelId,
    // options: { key: [] },
    // optionsDefault: { key: string },
    // presentation: string,
    // prices: {
    //   dealer: number,
    //   farmer: number
    // },
    // techData: string,
    //REMOVE accessory: boolean,
    //REMOVE specialOffer: false,
    //REMOVE visibility : MD
    //REMOVE showAfter : string
  };
  setLoading(true);
  const newProducts = model.sets ?  model.sets.concat(id) : [id];
  await axios.post(url_create, { collection: 'models', id: modelId, payload: { sets: newProducts } });
  await axios.post(url_create, { collection: 'products', id, payload: shape });
  let msg = {
    topic: 'updated-model',
    payload: model,
    country: model.country, 
  };
  await axios.post(url_create, { collection: 'msg', id: v4(), payload: msg });
  msg = {
    topic: 'updated-product',
    payload: shape,
    country: shape.country, 
  };
  await axios.post(url_create, { collection: 'msg', id: v4(), payload: shape });
  await mutate();
  setLoading(false);
};

export async function removeProductCall(url_remove, id) {
  console.log('remove product...');
  // await axios.post(url_remove, { collection: 'categories', id });
};

export async function updateModelCall(url_create, item, mutate, setLoading, setModelMod) {
  setLoading(true);
  await axios.post(url_create, { collection: 'models', id: item.id, payload: item });
  const msg = {
    topic: 'updated-model',
    payload: item,
    country: item.country, 
  };
  await axios.post(url_create, { collection: 'msg', id: v4(), payload: msg });
  await mutate();
  setModelMod(false);
  setLoading(false);
};

export async function updateProductsCall(url_create, items, model, modelId,  mutate, setProductsMod, setLoading) {
  setLoading(true);
  const toUpdate = items.filter(item => item.mod).map(item => {
    // eslint-disable-next-line no-unused-vars
    const { mod, ...other } = item;
    return other;
  });
  const productsIds = items.map(item => item.id);
  const promises = toUpdate.map(item => {
    return axios.post(url_create, { collection: 'products', id: item.id, payload: item }); 
  });
  await Promise.all(promises);
  await axios.post(url_create, { collection: 'models', id: modelId, payload: { sets: productsIds } });
  const promisesMsgProducts = toUpdate.map(item => {
    const msg = {
      topic: 'updated-product',
      payload: item,
      country: item.country, 
    };
    return axios.post(url_create, { collection: 'msg', id: v4(), payload: msg }); 
  });
  await Promise.all(promisesMsgProducts);
  const msg = {
    topic: 'updated-model',
    payload: { ...model, sets: productsIds },
    country: model.country, 
  };
  await axios.post(url_create, { collection: 'msg', id: v4(), payload: msg });
  await mutate();
  setProductsMod(false);
  setLoading(false);
};