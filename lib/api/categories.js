
import axios from 'axios';
import { v4 } from 'uuid';

export async function createCategory(url_create, country, mutate) {
  const id = v4();
  const shape = {
    id,
    order: 999,
    descr: 'New category',
    vis: 2, // 1 - manager, 2 - manager & client
    ext: false, // extention
    country, // country: number,
    // details: string,
    // picture: url,
  };
  const msg = {
    topic: 'updated-category',
    payload: shape,
    country, 
  };
  await axios.post(url_create, { collection: 'categories', id, payload: shape });
  await axios.post(url_create, { collection: 'msg', id: v4(), payload: msg });
  mutate();
};

export async function removeCategory(url_remove, id) {
  await axios.post(url_remove, { collection: 'categories', id });
};

export async function updateCategory(url_create, id, item, mutate, setLoading) {
  const { contentList, ...other } = item;
  if (contentList) {
    other.content = contentList.map(elem => elem._id);
  }
  const msg = {
    topic: 'updated-category',
    payload: other,
    country: other.country, 
  };
  await axios.post(url_create, { collection: 'categories', id, payload: other });
  await axios.post(url_create, { collection: 'msg', id: v4(), payload: msg });
  await mutate();
  setLoading(false);
};

export async function updateCategories(url_create, items) {
  const promises = items.map(item => {
    return axios.post(url_create, { collection: 'categories', id: item.id, payload: item }); 
  });
  await Promise.all(promises);
  const promisesMsg = items.map(item => {
    const msg = {
      topic: 'updated-category',
      payload: item,
      country: item.country, 
    };
    return axios.post(url_create, { collection: 'msg', id: v4(), payload: msg }); 
  });
  await Promise.all(promisesMsg);
};