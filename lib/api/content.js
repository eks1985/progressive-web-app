
import axios from 'axios';
import { v4 } from 'uuid';

export async function createContent(url_create, country, mutate) {
  const id = v4();
  const shape = {
    id,
    tt: 'New content',
    inl: true,
    tp: 'text',
    tag: '<no tag>',
    country,
  };
  await axios.post(url_create, { collection: 'content', id, payload: shape });
  mutate();
};

export async function updateContent(url_create, id, item) {
  await axios.post(url_create, { collection: 'content', id, payload: item });
};
