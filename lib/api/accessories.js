
import axios from 'axios';
import { v4 } from 'uuid';

export async function createAccessory(url_create, country, categoryRef, mutate) {
  const id = v4();
  const shape = {
    id,
    codes: [],
    country,
    descr: 'New accessory',
    order: 999,
    price: 0,
    vis: 2,
  };
  if (categoryRef) {
    shape.categoryRef = categoryRef;
  }
  await axios.post(url_create, { collection: 'accessories', id, payload: shape });
  mutate();
};

export async function removeAccessory(url_remove, id) {
  await axios.post(url_remove, { collection: 'accessories', id });
};

export async function updateAccessory(url_create, id, item) {
  await axios.post(url_create, { collection: 'accessories', id, payload: item });
};