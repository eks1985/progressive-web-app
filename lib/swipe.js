
export const move = (step = 1, items, item, cb) => {
  const ind = items.findIndex(elem => elem.value === item.value);
  if ((ind === 0 && step === -1) || ind === items.length - 1 && step === 1) {
    return;
  }
  const newInd = step === 1 ? ind + 1 : ind - 1;
  const newItem = items.find((_, i) => i === newInd);
  cb(newItem);
};