const optProto = {
  'Новая опция': {
    values: [
      {
        value: 'Значение 1',
        props: {
          alias: 'Наименование значения 1',
          vis: 2,
          default: true,
        },
      },
      {
        value: 'Значение 2',
        props: {
          alias: 'Наименование значения 2',
          vis: 2,
        },
      },
    ],
    vis: 2,
    order: 0,
  },
};

const valueProto = {
  value: 'Значение',
  props: {
    alias: 'Наименование значения',
    vis: 2,
  },
};

const handleEditCurVacabItem = (set, action) => {
  console.log('action', action);
  return set;
};

export const prepareNewMod = opt => {
  if (!opt) {
    return optProto;
  }
  optProto['Новая опция'].order = Object.keys(opt).length + 1;
  return { ...opt, ...optProto };
};

const updateValueProp = (value, prop, propValue) => {
  const newProps = { ...value.props, [prop]: propValue };
  return { ...value, props: newProps };
};

const updateValue = (value, newValue) => {
  return { ...value, value: newValue };
};

export const editMod = (opt, payload) => {
  const { action } = payload;
  const res = {};
  if (action === 'title') {
    const { oldTitle, newTitle } = payload;
    const keys = Object.keys(opt).filter(key => key !== oldTitle);
    res[newTitle] = { ...opt[oldTitle] };
    keys.forEach(key => {
      res[key] = { ...opt[key] };
    });
  }
  if (action === 'addValue') {
    const { title } = payload;
    const newValues = [...opt[title].values, valueProto];
    const newThisOpt = { ...opt[title], values: newValues };
    const newOpt = { ...opt, [title]: newThisOpt };
    return newOpt;
  }
  if (action === 'removeValue') {
    const { title, index } = payload;
    const newValues = opt[title].values.filter((value, ind) => ind !== index);
    const newThisOpt = { ...opt[title], values: newValues };
    const newOpt = { ...opt, [title]: newThisOpt };
    return newOpt;
  }
  if (action === 'editValueProp') {
    const { title, index, prop, propValue } = payload;
    let newValues = opt[title].values.map((value, ind) => ind !== index ? value : updateValueProp(value, prop, propValue));
    if (prop === 'default' || propValue === true) {
      newValues = newValues.map((value, ind) => ind === index ? value : updateValueProp(value, 'default', false));
    }
    const newThisOpt = { ...opt[title], values: newValues };
    const newOpt = { ...opt, [title]: newThisOpt };
    return newOpt;
  }
  if (action === 'editValue') {
    const { title, index, newValue } = payload;
    const newValues = opt[title].values.map((value, ind) => ind !== index ? value : updateValue(value, newValue));
    const newThisOpt = { ...opt[title], values: newValues };
    const newOpt = { ...opt, [title]: newThisOpt };
    return newOpt;
  }
  if (action === 'editModVis') {
    const { title, value } = payload;
    const newThisOpt = { ...opt[title], vis: value };
    const newOpt = { ...opt, [title]: newThisOpt };
    return newOpt;
  }
  return res;
};

export const removeMod = (opt, modKey) => {
  const res = {};
  const keys = Object.keys(opt).filter(key => key !== modKey);
  keys.forEach(key => {
    res[key] = opt[key];
  });
  return res;
};

export const updateComplWithOpt = (state, payload) => {
  if (payload.action === 'title') {
    const { oldTitle, newTitle } = payload;
    if (state.compl) {
      const copy = [...state.compl];
      copy.forEach(item => {
        item.options = item.options.map(option => option === oldTitle ? newTitle : oldTitle);
      });
      return copy;
    }
    return false;
  }
  return state.compl;
};