
import { replaceArrElem, moveArrayElem   } from 'lib/arrays';

const codesGroupProto = {
  options: [],
  values: [],
  materials: {
    codes: ['Код'],
    qty: [1],
  },
};

// groups
export const handleNewCodesGroup = state => {
  if (state.opt) {
    if (state.compl) {
      return state.compl.concat(codesGroupProto);
    }
    return [codesGroupProto];
  }
  // set without options
  return { codes: [], qty: [] };
};

export const handleEditCodesGroup = (state, group, payload) => {
  const current = state.compl[group];
  const { options, values } = payload;
  const newComplectation = { ...current, options, values };
  return replaceArrElem(state.compl, newComplectation, group);
};

export const handleRemoveCodesGroup = (state, group) => {
  return state.compl.filter((item, index) => index !== group);
};

// tables
export const handleNewCodesTable = (state, group) => {
  if (state.opt) {
    const current = state.compl[group];
    const newCodes = current.materials.codes.length > 0 ? current.materials.codes.concat('*').concat('Код') : current.materials.codes.concat('Код');
    const newQty = current.materials.qty.length > 0 ? current.materials.qty.concat('*').concat(1) : current.materials.qty.concat(1);
    const newMaterials = { ...current.materials, codes: newCodes, qty: newQty };
    const newComplectation = { ...current, materials: newMaterials };
    return replaceArrElem(state.compl, newComplectation, group);
  }
  // set without options
  const current = state.compl;
  const newCodes = current.codes.length > 0 ? current.codes.concat('*').concat('Код') : current.codes.concat('Код');
  const newQty = current.qty.length > 0 ? current.qty.concat('*').concat(1) : current.qty.concat(1);
  const newComplectation = { ...current, codes: newCodes, qty: newQty };
  return newComplectation;
};

export const handleMoveCodesTable = (state, group, table, step) => {
  if (state.opt) {
    const current = state.compl[group];
    const { materials } = current;
    const codesTables = calcTables(materials);
    const updatedCodesTables = moveArrayElem(codesTables, table, step);
    const newComplectation = { ...current, materials: codesTablesToMaterials(materials, updatedCodesTables) };
    return replaceArrElem(state.compl, newComplectation, group);
  }
  // set without options
  const current = state.compl;
  const codesTables = calcTables(current);
  const updatedCodesTables = moveArrayElem(codesTables, table, step);
  const newComplectation = codesTablesToMaterials(current, updatedCodesTables);
  return newComplectation;
};

export const handleRemoveCodesTable = (state, group, table) => {
  if (state.opt) {
    const current = state.compl[group];
    const { materials } = current;
    const codesTables = calcTables(materials);
    const filteredCodesTables = codesTables.filter((item, index) => index !== table);
    const newComplectation = { ...current, materials: codesTablesToMaterials(materials, filteredCodesTables) };
    return replaceArrElem(state.compl, newComplectation, group);
  }
  // set without options
  const current = state.compl;
  const codesTables = calcTables(current);
  const filteredCodesTables = codesTables.filter((item, index) => index !== table);
  const newComplectation = codesTablesToMaterials(current, filteredCodesTables);
  return newComplectation;
};

export const handleNewCodesTableRow = (state, group, table) => {
  if (state.opt) {
    const current = state.compl[group];
    const { materials } = current;
    const codesTables = calcTables(materials);
    const updatedCodesTables = codesTables.map((item, index) => index !== table ? item : item.concat({ code: 'Код', qty: 1 }));
    const newComplectation = { ...current, materials: codesTablesToMaterials(materials, updatedCodesTables) };
    return replaceArrElem(state.compl, newComplectation, group);
  }
  // set without options
  const current = state.compl;
  const codesTables = calcTables(current);
  const updatedCodesTables = codesTables.map((item, index) => index !== table ? item : item.concat({ code: 'Код', qty: 1 }));
  const newComplectation = codesTablesToMaterials(current, updatedCodesTables);
  return newComplectation;
};

export const handleEditCodesTableRow = (state, group, table, row, payload) => {
  if (state.opt) {
    const current = state.compl[group];
    const { materials } = current;
    const codesTables = calcTables(materials);
    const updatedCodesTables = codesTables.map((item, index) => index !== table ? item : item.map((codeRow, codeRowInd) => codeRowInd !== row ? codeRow : ({ code: payload.code, qty: payload.qty })));
    const newComplectation = { ...current, materials: codesTablesToMaterials(materials, updatedCodesTables) };
    return replaceArrElem(state.compl, newComplectation, group);
  }
  // set without options
  const current = state.compl;
  const codesTables = calcTables(current);
  const updatedCodesTables = codesTables.map((item, index) => index !== table ? item : item.map((codeRow, codeRowInd) => codeRowInd !== row ? codeRow : ({ code: payload.code, qty: payload.qty })));
  const newComplectation = codesTablesToMaterials(current, updatedCodesTables);
  return newComplectation;
};

export const handleRemoveCodesTableRow = (state, group, table, row) => {
  if (state.opt) {
    const current = state.compl[group];
    const { materials } = current;
    const codesTables = calcTables(materials);
    const updatedCodesTables = codesTables.map((item, index) => index !== table ? item : item.filter((codeRow, codeRowInd) => codeRowInd !== row));
    const newComplectation = { ...current, materials: codesTablesToMaterials(materials, updatedCodesTables) };
    return replaceArrElem(state.compl, newComplectation, group);
  }
  // set without options
  const current = state.compl;
  const codesTables = calcTables(current);
  const updatedCodesTables = codesTables.map((item, index) => index !== table ? item : item.filter((codeRow, codeRowInd) => codeRowInd !== row));
  const newComplectation = codesTablesToMaterials(current, updatedCodesTables);
  return newComplectation;
};

export const calcTables = node => {
  const tables = [];
  tables.push([]);
  node.codes.forEach((row, ind) => {
    const left1 = row.slice(0, 1);
    if (left1 === '*') {
      tables.push([]);
    } else {
      tables[tables.length - 1].push({ code: row, qty: node.qty[ind] });
    }
  });
  return tables;
};

export const codesTablesToMaterials = (materials, codesTables) => {
  const flatCodes = [];
  const flatQty = [];
  codesTables.forEach((codesTable, ind) => {
    console.log('codesTable', codesTable);
    codesTable.forEach(row => {
      flatCodes.push(row.code);
      flatQty.push(row.qty);
    });
    if (ind < codesTables.length - 1) {
      flatCodes.push('*');
      flatQty.push(0);
    }
  });
  return { ...materials, codes: flatCodes, qty: flatQty };
};