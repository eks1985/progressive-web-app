export const variants = {
    summary: { // 0
      title: 'Summary',
      descr: 'Summary chart data by years, final customers classification', // customers classification KFX, LLC, by qyantity of machines
      visible: true,
    },
    registrationReport: { // 1
      title: 'Registration report',
      descr: 'Registration data in table with filters',
      visible: true,
    },
    finalCustomers: { // 2
      title: 'Final customers network',
      descr: 'Final customers table with filters and lookup',
      visible: true,
    },
    finalCustomerCard: { // 3
      title: 'Customer card',
      descr: 'Single final customer review',
      visible: false,
    },
    keyClients: { // 4
      title: 'Key clients',
      descr: 'The most important customers having many machines',
      visible: true,
    },
    geoMap: { // 5
      title: 'Geo map',
      descr: 'Country map with regions',
      visible: true,
      fullScreen: true,
    },
    regionReview: { // 6
      title: 'Region review',
      descr: 'Single region review, dealers clash',
      visible: true,
      fullScreen: true,
    },
    dealerStock: { // 7
      title: 'Dealer stock',
      descr: 'Unsold machines with values',
      visible: true,
    },
    agroatlas: { // 8
      title: 'Agroatlas',
      descr: 'Agricaltural statistics by regions',
      visible: true,
      fullScreen: true,
    },
    playground: { // 0
      title: 'Playground',
      descr: 'Playground',
      visible: false,
    },
};

export const getReportKeyByIndex = index => {
  const keys = Object.keys(variants);
  return keys[index];
};
