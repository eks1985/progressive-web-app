import { groupBy, find, union } from 'lodash';

import { getStatLabel } from './report-filters';

export async function getRegProd(resource, params = undefined, fldebug = false) {

  const optionsObj = {
    resource,
    params,
  };

  if (fldebug) {
    console.log('params', params);
  }

  const options = {
    method: 'POST',
    body: JSON.stringify(optionsObj),
    headers: {
      'Content-Type': 'application/json',
    },
  };

  const res = await fetch('https://webhooks.mongodb-stitch.com/api/client/v2.0/app/mgd-ilrht/service/http/incoming_webhook/prd-reg?secret=my-api-key', options);
  let result;
  if (res.status === 200) {
    const response = await res.json();
    try {
      result = JSON.parse(response);
      if (result.res === 'ok') {
        if (fldebug) {
          console.log('result', result);
        }
        return result.data;
      }
    } catch (error) {
      console.log(error);
    }
  }

}

export const sData = (data, variantCurrent, filtersValues, startInd, portion, region) => {
  if (variantCurrent === 'summary') {
    if (data) {
      return { summary: data['reg-items-summary'] };
    }
    return false;
  }
  if (variantCurrent === 'registrationReport') {
    const hasFilters = Object.keys(filtersValues).length > 0;
    const allItemsRaw = data['reg-items-all-items'];
    const allItemsF = data['reg-items-all-items-f'] || allItemsRaw;
    const allItemsFPortion = allItemsF ? allItemsF.slice(startInd, startInd + portion) : false;
    const allItemsRawPortion = allItemsRaw ? allItemsRaw.slice(startInd, startInd + portion) : false;
    let itemsQty;
    if (hasFilters) {
      itemsQty = allItemsF ? allItemsF.length : 0;
      return { allItems: allItemsFPortion, allItemsRaw, itemsQty };
    }
    itemsQty = allItemsRaw ? allItemsRaw.length : 0;
    return { allItems: allItemsRawPortion, allItemsRaw, itemsQty };
  }
  if (variantCurrent === 'finalCustomers') {
    const hasFilters = Object.keys(filtersValues).length > 0;
    const allItemsRaw = data['final-customers-all-items'];
    const allItemsF = data['final-customers-all-items-f'] || allItemsRaw;
    const allItemsFPortion = allItemsF ? allItemsF.slice(startInd, startInd + portion) : false;
    const allItemsRawPortion = allItemsRaw ? allItemsRaw.slice(startInd, startInd + portion) : false;
    let itemsQty;
    if (hasFilters) {
      itemsQty = allItemsF ? allItemsF.length : 0;
      return { allItems: allItemsFPortion, allItemsRaw, itemsQty };
    }
    itemsQty = allItemsRaw ? allItemsRaw.length : 0;
    return { allItems: allItemsRawPortion, allItemsRaw, itemsQty };
  }
  if (variantCurrent === 'keyClients') {
    const allItemsRaw = data['final-customers-key-clients'];
    return { allItems: allItemsRaw };
  }
  if (variantCurrent === 'agroatlas') {
    const { agroatlas } = data;
    if (!agroatlas) {
      return false;
    }
    const atlCountry = agroatlas.filter(item => item.levl === 0);
    const atlDiscticts = agroatlas.filter(item => item.levl === 1);
    const atlRegions = agroatlas.filter(item => item.levl === 2);

    const districtsIndex = groupBy(atlDiscticts, 'dcde');
    const regionsIndex = groupBy(atlRegions, 'rgnp');

    return { atlCountry, districtsIndex, regionsIndex };
  }
  if (variantCurrent === 'regionReview') {
    const regions = data['region-review-regions'];
    const crops = data['region-review-crops'];
    const machines = data['region-review-machines'];
    const dealers = data['region-review-dealers'];
    const intersect = data['region-review-dealers-intersect'];
    let cropsRegion;
    let atlasRegion;
    if (region) {
      const crops2018 = find(crops, { rgnf: region.value, year: 2018 });
      const crops2019 = find(crops, { rgnf: region.value, year: 2019 });
      atlasRegion = crops2019.atls;
      const cropsNames2018 = crops2018 ? crops2018.crops.map(item => item.d) : [];
      const cropsNames2019 = crops2019 ? crops2019.crops.map(item => item.d) : [];
      const cropsNames = union(cropsNames2018, cropsNames2019);
      cropsRegion = [];
      cropsNames.forEach(cropsName => {
        const findCrop2018 = find(crops2018.crops, { d: cropsName });
        const cropVal2018 = findCrop2018 ? findCrop2018.v : 0;
        const findCrop2019 = find(crops2019.crops, { d: cropsName });
        const cropVal2019 = findCrop2019 ? findCrop2019.v : 0;
        cropsRegion.push({
          name: cropsName,
          values: [cropVal2018, cropVal2019],
        });
      });
    }
    return { regions, crops, machines, dealers, intersect, cropsRegion, atlasRegion };
  }
  if (variantCurrent === 'geoMap') {
    if (data) {
      const regItemsGeoMap = data['reg-items-geo-map'];
      if (regItemsGeoMap) {
        return { category: regItemsGeoMap.category, years: regItemsGeoMap.years };
      }
      return false;
    }
    return false;
  }
  return data;
};

export const sFiltersStructure = (variantCurrent, filtersBank, filtersValues) => {
  if (variantCurrent === 'registrationReport') {
    return [
      { name: 'Dealer', source: 'dlds', data: filtersBank.dlds || [], value: filtersValues.dlds || null },
      { name: 'Year', source: 'year', data: filtersBank.year || [], value: filtersValues.year || null },
      { name: 'Region', source: 'crgnr', data: filtersBank.crgnr || [], value: filtersValues.crgnr || null },
      { name: 'Stat', source: 'stat', data: filtersBank.stat || [], value: filtersValues.stat || null },
      { name: 'Code', source: 'pcde', data: filtersBank.pcde || [], value: filtersValues.pcde || null },
      { name: 'Model', source: 'mdes', data: filtersBank.mdes || [], value: filtersValues.mdes || null },
      { name: 'Description', source: 'pdsc', data: filtersBank.pdsc || [], value: filtersValues.pdsc || null },
      { name: 'SN', source: 'psn', data: filtersBank.psn || [], value: filtersValues.psn || null },
      { name: 'Shipment to', source: 'dest', data: filtersBank.dest || [], value: filtersValues.dest || null },
      { name: 'Tractor model', source: 'tmod', data: filtersBank.tmod || [], value: filtersValues.tmod || null },
      { name: 'Tractor power', source: 'tpow', data: filtersBank.tpow || [], value: filtersValues.tpow || null },
      { name: 'Tractor type', source: 'ttyp', data: filtersBank.ttyp || [], value: filtersValues.ttyp || null },
    ];
  }
  if (variantCurrent === 'finalCustomerCard') {
    const bankKeys = Object.keys(filtersBank);
    const result = [];
    if (bankKeys.includes('code')) {
      result.push({ name: 'Coode', source: 'code', data: filtersBank.code || [], value: filtersValues.code || null });
    }
    return result;
  }
  if (variantCurrent === 'finalCustomers') {
    const bankKeys = Object.keys(filtersBank);
    const result = [];
    if (bankKeys.includes('code')) { //
      result.push({ name: 'Code', source: 'code', data: filtersBank.code || [], value: filtersValues.code || null });
    }
    if (bankKeys.includes('catg')) { //
      result.push({ name: 'Category', source: 'catg', data: filtersBank.catg || [], value: filtersValues.catg || null });
    }
    if (bankKeys.includes('crgnc')) { //
      result.push({ name: 'Region code', source: 'crgnc', data: filtersBank.crgnc || [], value: filtersValues.crgnc || null });
    }
    if (bankKeys.includes('crgnr')) { //
      result.push({ name: 'Region name', source: 'crgnr', data: filtersBank.crgnr || [], value: filtersValues.crgnr || null });
    }
    if (bankKeys.includes('czip')) { //
      result.push({ name: 'Postal code', source: 'czip', data: filtersBank.czip || [], value: filtersValues.czip || null });
    }
    if (bankKeys.includes('dscr')) { //
      result.push({ name: 'Name', source: 'dscr', data: filtersBank.dscr || [], value: filtersValues.dscr || null });
    }
    if (bankKeys.includes('qtym')) { //
      result.push({ name: 'Qty machines', source: 'qtym', data: filtersBank.qtym || [], value: filtersValues.qtym || null });
    }
    if (bankKeys.includes('vatc')) { //
      result.push({ name: 'VAT code', source: 'vatc', data: filtersBank.vatc || [], value: filtersValues.vatc || null });
    }
    if (bankKeys.includes('wbid')) { //
      result.push({ name: 'Web id', source: 'wbid', data: filtersBank.wbid || [], value: filtersValues.wbid || null });
    }
    return result;
  }
  return false;
};

export const sFiltersValues = filtersValues => {
  const filtersKeys = Object.keys(filtersValues);
  const res = [];
  filtersKeys.forEach(key => {
    const item = filtersValues[key];
    item.forEach(fitem => {
      res.push(key === 'stat' ? getStatLabel(fitem.value) : fitem.value);
    });
  });
  const valuesLabel = res.join('; ');
  return valuesLabel;
};

