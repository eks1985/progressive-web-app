
import { replaceArrElem  } from 'lib/arrays';

const pricesGroupProto = {
  dealer: [
    {
      price: '',
      options: [],
      values: [],
    },
  ],
  farmer: [
    {
      price: '',
      options: [],
      values: [],
    },
  ],
};

const pricesRowProto = {
  price: '',
  options: [],
  values: [],
};

// groups

export const handleNewPricesGroup = state => {
  if (state.prices) {
    return { ...state.prices, farmer: state.prices.farmer.concat(pricesRowProto), dealer: state.prices.dealer.concat(pricesRowProto) };
  }
  return pricesGroupProto;
};

export const handleEditPricesGroup = (state, group, payload) => {
  const currentFarmer = state.prices.farmer[group];
  const currentDealer = state.prices.dealer[group];
  const { options, values, priceDealer, priceFarmer } = payload;
  const newFarmer = { ...currentFarmer, options: options || currentFarmer.options, values: values || currentFarmer.values, price: priceFarmer !== undefined ? priceFarmer : currentFarmer.price };
  const newDealer = { ...currentDealer, options: options || currentDealer.options, values: values || currentDealer.values, price: priceDealer !== undefined ? priceDealer : currentDealer.price };
  const newFarmerNode = replaceArrElem(state.prices.farmer, newFarmer, group);
  const newDealerNode = replaceArrElem(state.prices.dealer, newDealer, group);
  return { ...state.prices, farmer: newFarmerNode, dealer: newDealerNode };
};

export const handleRemovePricesGroup = (state, group) => {
  const newDealer = state.prices.dealer.filter((item, index) => index !== group);
  const newFarmer = state.prices.farmer.filter((item, index) => index !== group);
  return { ...state.prices, dealer: newDealer, farmer: newFarmer };
};
