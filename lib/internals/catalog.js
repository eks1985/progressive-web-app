
export const prepareSetOptions = set => {
  if (!set.opt) {
    return set;
  }
  const options = Object.keys(set.opt).reduce((res, optKey) => ({ ...res, [optKey]: set.opt[optKey].values.map(valueItem => valueItem.value) }), {});
  const optionsDefault = Object.keys(set.opt).reduce((res, optKey) => ({ ...res, [optKey]: set.opt[optKey].values.filter(valueItem => valueItem.props.default)[0].value }), {});
  return { ...set, options, optionsDefault };
};

export const getPrice = (priceType, set, opt, discount = 0) => {

  const zeroPrice = 0;
  let price = 0;
  if (!set) {
    return zeroPrice;
  }
  if (!set.prices) {
    return zeroPrice;
  }

  const priceNodeRows = set.prices[priceType];

  // if no dealer price, take farmer price and apply discount
  if (!priceNodeRows) {
    if (priceType === 'dealer') {
      if (!discount) {
        return zeroPrice;
      }
      //discount provided, calc dealer price based on farmer price
      return { price: getPrice('farmer', set, opt, discount), descr: `Client price - disc ${discount}%` };
    }
    return { price: zeroPrice, descr: 'Missing parice' };
  }

  if (opt) {
    const options = {};
    opt.forEach(optItem => {
      const f = optItem.values.find(valueItem => valueItem.active);
      options[optItem.title] = f.value;
    });
    const optionsDefault = set.opt ? Object.keys(set.opt).reduce((res, optKey) => ({ ...res, [optKey]: set.opt[optKey].values.filter(valueItem => valueItem.props.default)[0].value }), {}) : undefined;
    const testedOptions = options || optionsDefault || {};
    const optionsKeys = Object.keys(testedOptions);
    if (optionsKeys.length === 0) {
      if (priceType === 'farmer') {
        return { price: parseInt(priceNodeRows, 10) * (1 - (discount / 100)), descr: `Client price - disc ${discount}%` } ;
      }
      return { price: parseInt(priceNodeRows, 10), descr: '' };
    }
    priceNodeRows.forEach(row => {
      if (!price) {
        let ok = true;
        row.options.forEach(rowOption => {
          const optionIndex = row.options.indexOf(rowOption);
          if (optionIndex > -1 && testedOptions[rowOption] !== row.values[optionIndex]) {
            ok = false;
          }
        });
        if (ok) {
          price = row.price.toString();
        }
      }
    });
    // apply discount only for farmer price (dealer price is always net value)
    if (price && priceType === 'farmer') {
      return { price: parseInt(price, 10) * (1 - (discount / 100)), descr: `Client price - disc ${discount}%`}
    }
    // if no dealer price (net) then get farmer price and apply discount
    if (!price && priceType === 'dealer') {
      return { price: getPrice('farmer', set, opt, discount), descr: 'Dealer price' } ;
    }
    if (price) {
      return { price: parseInt(price, 10), descr: '' };
    }
  } else {
    return { price: parseInt(priceNodeRows, 10) , descr: '' };
  }
  return zeroPrice;
};



export const getProductChipsItems = (product, opt) => {

  const { properties } = product;

  let chipsItems = [];
  if (properties) {
    chipsItems = product.properties.map(item => ({ text: item, type: 'attr' }));
  }
  if (opt) {
    chipsItems.push({ text: 'варианты', type: 'variants' });
  }

  return chipsItems;

};

export const getProductPicture = product => {

  if (product && product.pictures && product.pictures.length > 0) {
    return product.pictures[0];
  }

  return false;

};