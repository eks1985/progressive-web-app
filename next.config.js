const withPWA = require('next-pwa')
const runtimeCaching = require('next-pwa/cache')
const path = require('path')

module.exports = withPWA({
  pwa: {
    dest: 'public',
    runtimeCaching,
    disable: process.env.NODE_ENV === 'development',
  },
  webpack (config) {
    config.resolve.alias['components'] = path.join(__dirname, 'components')
    config.resolve.alias['pages'] = path.join(__dirname, 'pages')
    config.resolve.alias['lib'] = path.join(__dirname, 'lib')
    config.resolve.alias['assets'] = path.join(__dirname, 'assets')
    return config
  },
  images: {
    domains: ['res.cloudinary.com'],
  }
})

// module.exports = {
//   webpack (config) {
//     config.resolve.alias['components'] = path.join(__dirname, 'components')
//     config.resolve.alias['pages'] = path.join(__dirname, 'pages')
//     config.resolve.alias['lib'] = path.join(__dirname, 'lib')
//     return config
//   }
// }
