import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Slide from '@material-ui/core/Slide';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';

const useStyles = makeStyles(theme => ({
  dialog: {
    maxWidth: '900px',
    margin: '0 auto',
  },
  appBar: {
    position: 'relative',
    display: 'flex',
    justifyContent: 'center',
  },
  toolBar: {
    display: 'flex',
    justifyContent: 'space-between',
  },
}));

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction='up' ref={ref} {...props} />;
});

const content = props => {

  const classes = useStyles();
  
  const { open, toggleOpen } = props;

  return (
    <Dialog
      className={classes.dialog}
      fullScreen
      open={open}
      onClose={toggleOpen}
      TransitionComponent={Transition}
    >
      <AppBar className={classes.appBar}>
        <Toolbar className={classes.toolBar}>
          <Typography variant='subtitle1' color='inherit'>
            Заголовок диалог
          </Typography>
          <Button
            color='inherit'
            onClick={toggleOpen}
          >
            Закрыть
          </Button>
        </Toolbar>
      </AppBar>
      <Paper elevation={0}>
        <Box mt={2} p={2}>
          {props.children}
        </Box>
      </Paper>
    </Dialog>
  );

};

export default content;