import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';

const useStyles = makeStyles(theme => ({
  container: {
    padding: '4px',
    color: props => props.color,
    fontSize: theme.typography.pxToRem(14),
    padding: theme.spacing(1),
    backgroundColor: theme.palette.background.paper,
  },
}));

const classes = useStyles({ color: 'black' });

const classes = useStyles();

<Box className={classes.container}>
  {title}
</Box>
