import React from 'react';
import DataProvider from 'lib/data';
import { useStore } from 'lib/store';

const Component = props => {

  const store = useStore();

  const { data } = props;

  return (
    <div>
      {JSON.stringify(data, null, 2)}
    </div>
  );

};

const comp = props => {

  const { url } = props;

  return (
    <DataProvider url={url} render={data => (
      <Component data={data} />
    )}/>
  );

};

comp.defaultProps = {
  url: 'https://webhooks.mongodb-realm.com/api/client/v2.0/app/mgd-ilrht/service/http/incoming_webhook/***hook_name***?secret=L0sS3Bz1mdCm6Kuz', 
};

export default comp;
