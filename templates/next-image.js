import React from 'react';
import Image from 'next/image'
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';

const useStyles = makeStyles(theme => ({
  container: {
    
  },
}));

const compName = props => {

  const src = 'https://res.cloudinary.com/mg-dealer/image/upload/v1575618867/G_CHRONO_708_fxvacm.jpg';

  return (
    <Box>
      <Box mt={2} className='img-wrap' margin='0 auto' width={300} border='1px dashed #ddd' bgcolor='#eee' p={2}>
        <Image
          className='img'
          src={src}
          alt='Picture of the author'
          layout='fill'
          objectFit='contain'
          objectPosition='center-center'
        />
      </Box>
      <Box mt={2} margin='0 auto' width={300} height={150} border='1px dashed #ddd' bgcolor='#eee' p={2} position='relative'>
        <Image
          src={src}
          alt='Picture of the author'
          layout='fill'
          objectFit='contain'
          objectPosition='center-center'
        />
      </Box>
      {/* pucture occupied full width and height as it real height */}
      <Box mt={2} margin='0 auto' className='img-wrap' width={60} height={50} position='relative'>
        <Image className='img' src={src} alt='category-picture' layout='fill' objectFit='contain' objectPosition='center center' />
      </Box>
      {/* pucture occupied full width and height as wrapper height */}
      <Box mt={2} margin='0 auto' width={60} height={50} position='relative'>
        <Image className='img' src={src} alt='category-picture' layout='fill' objectFit='contain' objectPosition='center center' />
      </Box>
    </Box>
  );

};

export default compName;
