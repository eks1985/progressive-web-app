import React from 'react';
import { baseUrl, secret } from 'lib/base';
import { useRouter } from 'next/router';
import { useStore } from 'lib/store';
import DataProviderBatch from 'lib/data-batch';
import Box from '@material-ui/core/Box';

const prepareQuery = (url, id, country) => {

  return {
    items: `${url}&id=${id}`,
  };

};

const component = props => {

  const router = useRouter(); 
  const store = useStore();

  const { country } = store;

  if (!router.query.id || !store) {
    return null;
  }

  const query = prepareQuery(props.url, router.query.id, country);

  return (
    <Box margin='0 auto' width='100%' maxWidth='900px'>
      <DataProviderBatch query={query} render={(data, mutate) => (
        <Product
          product={data.items}
        />
      )}/>
    </Box>
  );

};

component.defaultProps = {
  url: `${baseUrl}slug${secret}`,
};

export default component;