import React from 'react';
import DataProvider from 'lib/data';
import { useStore } from 'lib/store';
import Categories from 'components/categories';
import { baseUrl, secret } from 'lib/base';

const prepareUrl = (url, permiss, country) => {

  if (!permiss || !permiss.catalog.visManager) {
    return `${url}&country=${country}&vis=2`;
  }

  return `${url}&country=${country}&vis=1`;

};

const home = props => {

  const store = useStore();
  const { permiss, country } = store;
  const { url } = props;

  const ulrPrepared = prepareUrl(url, permiss, country);

  if (country === -1) {
    return null;
  }

  return (
    <DataProvider url={ulrPrepared} render={data => (
      <Categories data={data} />
    )}/>
  );

};

home.defaultProps = {
  url: `${baseUrl}catalog${secret}lists=c,m`, 
};

export default home;