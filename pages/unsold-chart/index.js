/* eslint-disable prefer-destructuring */
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { useStore } from 'lib/store';
import { orderBy } from 'lodash';
import Content from './content';

const prepareSeries = data => {
  const res = {};
  const resdd = {};
  data.forEach(item => {
    item.items.forEach(machine => {
      if (!res[machine.mdes]) {
        res[machine.mdes] = 0;
      }
      if (!resdd[machine.mdes]) {
        resdd[machine.mdes] = {};
      }
      if (!resdd[machine.mdes][machine.dlds]) {
        resdd[machine.mdes][machine.dlds] = 0;
      }
      res[machine.mdes] += 1;
      resdd[machine.mdes][machine.dlds] += 1;
    });
  });

  const resKeys = Object.keys(res);
  const chartData = resKeys.map(key => {
    return {
      name: key,
      value: res[key],
      drilldown: key,
    };
  });
  const chartDataSorted = orderBy(chartData, ['value'], ['desc']);

  const chartDD = Object.keys(resdd).map(key => {
    const item = resdd[key];
    const arr = Object.keys(item).map(item1 => {
      return { id: item1, value: resdd[key][item1] };
    });
    const arrSorted = orderBy(arr, ['value'], ['desc']);
    const dlarr = arrSorted.map(item => {
      return [item.id, item.value];
    });
    return {
      id: key,
      name: key,
      data: dlarr,
    };
  });

  return {
    chartData: chartDataSorted,
    chartDD,
  };

};

const unsoldChart = props => {

  const [data, setData] = useState(false);

  const { urlUnsold } = props;

  const store = useStore();
  const { appUser, country } = store;
  if (appUser === false || !appUser.internal) {
    return null;
  }

  useEffect(() => {
    
    async function fetchData(country, appUser) {
      
      const { customerGuid } = appUser;

      
      const resultUnsold = await axios.post(urlUnsold,
        { resource: 'unsold-machines', params: { country, custGuid: customerGuid } },
        );

      if (resultUnsold.status === 200) {
        const resultUnsoldParsed = JSON.parse(resultUnsold.data);
        if (resultUnsoldParsed.data) {
          if (resultUnsoldParsed.data.length > 0) {
            const series = prepareSeries(resultUnsoldParsed.data);
            setData(series);
          }
        }
      }
      
    }
    
    if (country !== undefined && country > -1 && appUser !== null) {
      // fetch unsold, availability, consignment, open-items, charts
      fetchData(country, appUser);
    }

  }, [country, appUser]);

  if (appUser === false) {
    return null;
  }
  const { email } = appUser;
  
  if (data) {
    return (
      <Content
        email={email}
        chartData={data.chartData}
        chartDD={data.chartDD}
      />
    );
  }

  return null;

};

unsoldChart.defaultProps = {
  urlUnsold: 'https://webhooks.mongodb-stitch.com/api/client/v2.0/app/mgd-ilrht/service/http/incoming_webhook/prd-reg?secret=my-api-key',
};

export default unsoldChart;
