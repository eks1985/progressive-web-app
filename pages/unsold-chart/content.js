import React from 'react';
import ReactHighcharts from 'react-highcharts';
// import highcharts3d from 'highcharts/highcharts-3d';
// import drilldown from 'highcharts/modules/drilldown';
import Box from '@material-ui/core/Box';
// highcharts3d(ReactHighcharts.Highcharts);
// drilldown(ReactHighcharts.Highcharts);

const getPieData = chartData => {
  if (!chartData) {
    return [];
  }
  return chartData.map(item => {
    const name = `${item.value} - ${item.name}`;
    return { name, y: item.value, drilldown: item.drilldown };
  });
};

const getPieDrillDown = chartDD => {

  return chartDD.map(item => {
    return {
      name: item.name,
      id: item.id,
      data: item.data,
      dataLabels: {
        enabled: true,
        formatter: function () {
          return `${this.y} ${this.point.name}`;
        },
      },
      legend: {
        itemStyle: {
          fontWeight: 'normal',
          fontSize: '10px',
        },
        itemWidth: 150,
        // eslint-disable-next-line
        labelFormatter: function () {
          return `${this.y} ${this.point.name}`; // eslint-disable-line
        },
      },
    };
  });

};

const StockPieChart = props => {
  const {
    chartData,
    chartDD,
  } = props;
  const data = getPieData(chartData);
  const drillDownData = getPieDrillDown(chartDD);
  const title = 'Dealers stock';
  const config = {
    colors: [
      '#ff77a9',
      '#8e99f3',
      '#ffd95b',
      '#98ee99',
      '#ffff8b',
      '#73e8ff',
      '#cfff95',
      '#6ff9ff',
      '#be9c91',
      '#64d8cb',
      '#ffa270',
      '#ffff89',
      '#80d6ff',
      '#fffd61',
      '#df78ef',
      '#ff867c',
      '#a7c0cd',
      '#b085f5',
    ],
    credits: {
      enabled: false,
    },
    legend: {
      itemStyle: {
        fontWeight: 'normal',
        fontSize: '10px',
      },
      itemWidth: 150,
      // eslint-disable-next-line
      labelFormatter: function () {
        return `${this.name}`; // eslint-disable-line
      },
    },
    chart: {
      type: 'pie',
    },
    title: {
      text: title,
    },
    plotOptions: {
      pie: {
        innerSize: 35,
        depth: 25,
        size: 150,
        showInLegend: true,
      },
      series: {
        dataLabels: {
          enabled: true,
          distance: 10,
          style: {
            fontSize: '10px',
            fontWeight: 'normal',
          },
        },
      },
    },
    series: [{
      name: 'By models',
      data,
      label: {
        enabled: false,
      },
    }],
    drilldown: {
      series: drillDownData,
      drillUpButton: {
        position: {
          y: -20,
        },
      },
      animation: false,
    },
  };

  return (
    <div>
      <ReactHighcharts config={config}></ReactHighcharts>
    </div>
  );
};

const StockPieChartContainer = props => {

  const { chartData, chartDD } = props;

  if (!chartData) {
    return null;
  }

  return (
    <Box mb={5}>
      <StockPieChart
        {...props}
        chartData={chartData}
        chartDD={chartDD}>
      </StockPieChart>
    </Box>
  );

}

export default StockPieChartContainer;
