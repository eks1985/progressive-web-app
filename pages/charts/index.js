import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useStore } from 'lib/store';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Link from 'next/link';

const useStyles = makeStyles(() => ({
  btn: {
    width: 200,
  }
}));

const Item = props => {
  const classes = useStyles();
  const { label, path, disabled } = props;
  return (
    <Box mt={2}>
      <Link href={path} >
        <Button color='primary' variant='outlined' className={classes.btn} disabled={disabled}>
          {label}   
        </Button>
      </Link>
    </Box>
  ); 
};

const charts = () => {

  const store = useStore();
  const { appUser } = store;

  if (appUser === false || !appUser.internal) {
    return null;
  }

  return (
    <Box margin='0 auto' width='100%' p={2} textAlign='center'>
      <Item label='Склад' path='/stock-chart' />
      <Item label='Продажи' path='/sales-chart' />
      <Item label='Дебиторка' path='/open-items-chart' />
      <Item label='Кредиторка' path='/intercompany-chart' disabled />
      <Item label='Деньги' path='/cash-chart' disabled />
      <Item label='Склад дилеров' path='/unsold-chart' />
    </Box>
  );

};

export default charts;
