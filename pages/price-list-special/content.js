import React from 'react';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Item from './Item';

const content = props => {
  const { data, country, requestSent, setRequestSent } = props;
  const resp = {
    ru: 'Бренд менеджер Чевычелов Денис.',
    ua: 'Логист Макаренко Максим.',
  };
  if (requestSent) {
    return (
      <Box mt={5} display='flex' justifyContent='center' alignItems='center' flexDirection='column'>
        <Typography>
          Спасибо за Ваш запрос! Мы ответим Вам в ближайшее время.
        </Typography>
        <Typography>
          {`Ответственный за обработку: ${country === 0 ? resp.ru : resp.ua}`}
        </Typography>
        <Box mt={5}>
          <Button
            color='primary'
            variant='contained'
            onClick={setRequestSent.bind(null, false)}
          >
            Закрыть
          </Button>
        </Box>
      </Box>
    );
  }
  return (
    <Box margin='0 auto' maxWidth={900} marginTop={1}>
      {data.map(item => {
        return (
          <Item key={item._id} item={item} setRequestSent={setRequestSent} />
        );
      })}
    </Box>
  );
};

export default content;
