import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useStore } from 'lib/store';
import Box from '@material-ui/core/Box';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import AccordionActions from '@material-ui/core/AccordionActions';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';
import ImageGallery from 'components/image-gallery';
import { createRequestUsedMachine } from 'lib/api/customer-request';

const useStyles = makeStyles(theme => ({
  panel: {
    margin: '0px',
  },
  panelDetails: {
    padding: '4px',
    borderTop: '1px solid #ccc',
    display: 'flex',
    flexDirection: 'column',
  },
  summary: {
    fontSize: theme.typography.pxToRem(14),
    padding: theme.spacing(1),
  },
  list: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
}));

const Panel = props => {
  const store = useStore();
  const { title, descr, location, snPrint, work, year, price, priceSpecial, config, photo } = props.item;
  const { setRequestSent } = props;
  const photoPrepared = photo.map(item => ({ width: 360, height: 260, src: item.path, alt: item.title }));
  const classes = useStyles();
  return (
    <Box mb={1}>
      <Accordion square className={classes.panel}>
        <AccordionSummary
          className={classes.summary}
          expandIcon={<ExpandMoreIcon />}
          aria-label='Expand'
          aria-controls='additional-actions1-content'
          id='additional-actions1-header'
        >
          <Typography variant='body2'>
            {title}
          </Typography>
        </AccordionSummary>
        <AccordionDetails className={classes.panelDetails}>
          {descr &&
            <Typography variant='caption' style={{ padding: '6px', textAlign: 'left', lineHeight: '1.3' }}>
              {descr}
            </Typography>
          }
          <List className={classes.root}>
            {year &&
              <ListItem>
                <ListItemText primary={year} secondary='Год производства' />
              </ListItem>
            }
            {config &&
              <ListItem>
                <ListItemText primary={config} secondary='Конфигурация' />
              </ListItem>
            }
            {work &&
              <ListItem>
                <ListItemText primary={work} secondary='Наработка' />
              </ListItem>
            }
            {snPrint &&
              <ListItem>
                <ListItemText primary={snPrint} secondary='Серийный номер' />
              </ListItem>
            }
            {location &&
              <ListItem>
                <ListItemText primary={location} secondary='Местонахождения' />
              </ListItem>
            }
            {price && parseInt(price, 10) > 0 &&
              <ListItem>
                <ListItemText primary={price} secondary='Цена по прайс-листу' />
              </ListItem>
            }
            {priceSpecial && parseInt(priceSpecial, 10) > 0 &&
              <ListItem>
                <ListItemText primary={priceSpecial} secondary='Специальная цена' />
              </ListItem>
            }
          </List>
          <ImageGallery photos={photoPrepared} />
        </AccordionDetails>
        <AccordionActions>
          <Box mb={1}>
            <Button
              color='primary'
              variant='contained'
              onClick={
                () => {
                  createRequestUsedMachine(props.item, store, setRequestSent);
                }
              }
            >
              Заказать
            </Button>
          </Box>
        </AccordionActions>
      </Accordion>
    </Box>
  );
};

export default Panel;
