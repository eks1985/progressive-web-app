import React, { useState } from 'react';
import { useStore } from 'lib/store';
import DataProviderBatch from 'lib/data-batch';
import { baseUrl, secret } from 'lib/base';
import Box from '@material-ui/core/Box';
import Content from './content';

const prepareQuery = country => {
  const cntr = country === 0 ? 'ru' : 'ua';
  return {
    list: `${baseUrl}price_list_special${secret}&cntr=${cntr}`,
  };
};

const priceListSpecial = props => {

  const [requestSent, setRequestSent] = useState(false);
  const store = useStore();
  const { country } = store;

  if (country === -1) {
    return null;
  }

  const query = prepareQuery(country);

  return (
    <Box margin='0 auto' width='100%' maxWidth='600px'>
      <DataProviderBatch query={query} render={(data, mutate) => {
        if (!data.list || data.list.length === 0) {
          return null;
        }
        return (
          <Content
            data={data.list}
            country={country}
            requestSent={requestSent}
            setRequestSent={setRequestSent}
          />
        );

      }}/>
    </Box>
  );

};

export default priceListSpecial;
