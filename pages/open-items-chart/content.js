import React from 'react';
import ReactHighcharts from 'react-highcharts';
import Box from '@material-ui/core/Box';

const getCategories = data => {
  if (!data) {
    return [];
  }
  return Object.keys(data);
};

const getValues = (data, resource) => {
  if (!data) {
    return [];
  }
  return Object.keys(data).map(key => {
    const val = Math.round(data[key][resource] / 1000000, 0);
    return resource === 'prepayment' ? -val : val;
  });
};

const generatePlotLine = (data, i) => {
  const debt = Math.round(data.debt / 1000000, 0);
  const overdue = Math.round(data.overdue / 1000000, 0);
  const prepayment = Math.round(data.prepayment / 1000000, 0);
  return {
    color: '#ccc',
    width: 1,
    value: i,
    label: {
      y: 0,
      text: `${debt}-${overdue}-${prepayment}`,
      style: {
        fontSize: 12,
        zIndex: 9999,
      },
    },
  };
};

const getPlotLines = data => {
  const plotLines = [];
  const dataKeys = Object.keys(data);
  const lastKey = dataKeys[dataKeys.length - 1];
  const q = lastKey.split('-')[1];
  dataKeys.forEach((key, i) => {
    const curQ = key.split('-')[1];
    if (curQ === q) {
      plotLines.push(generatePlotLine(data[key], i));
    }
  });
  return plotLines;
};

const getMax = data => {
  return data.reduce((res, item) => item > res ? item : res, 0) * 1.2;
};

const getMin = data => {
  return data.reduce((res, item) => item < res ? item : res, 0) * 1.2;
};

export class Charts extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    const { data } = props;
    const plotLines = getPlotLines(data);
    const categories = getCategories(data);
    const dataDebt = getValues(data, 'debt');
    const dataOverdue = getValues(data, 'overdue');
    const dataPrepayment = getValues(data, 'prepayment');
    this.state = {
      configLine: {
        credits: {
          enabled: false,
        },
        title: {
          text: 'Customers open items',
        },
        xAxis: {
          categories,
          labels: {
            rotation: -90,
          },
          plotLines,
        },
        yAxis: {
          title: null,
          min: getMin(dataPrepayment),
          max: getMax(dataDebt),
          tickInterval: 50,
        },
        plotOptions: {
          series: {
            label: {
              connectorAllowed: false,
            },
            marker: {
              symbol: 'circle',
              radius: 3,
            },
          },
        },
        series: [
          {
            name: 'Debt',
            data: dataDebt,
            type: 'areaspline',
            color: '#2196F3',
          },
          {
            name: 'Overdue',
            data: dataOverdue,
            type: 'areaspline',
            color: '#FF5722',
          },
          {
            name: 'Adv payment',
            data: dataPrepayment,
            type: 'areaspline',
            color: '#8BC34A',
          },
        ],
      },
    };
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.data && !this.props.data) {
      const { data } = nextProps;
      const stateCopy = { ...this.state };
      const plotLines = getPlotLines(data);
      const categories = getCategories(data);
      const dataDebt = getValues(data, 'debt');
      const dataOverdue = getValues(data, 'overdue');
      const dataPrepayment = getValues(data, 'prepayment');
      stateCopy.configLine.xAxis.categories = categories;
      stateCopy.configLine.xAxis.plotLines = plotLines;
      stateCopy.configLine.series[0].data = dataDebt;
      stateCopy.configLine.series[1].data = dataOverdue;
      stateCopy.configLine.series[2].data = dataPrepayment;
      const { configLine } = stateCopy;
      this.setState({ configLine });
    }
  }
  render() {
    return (
      <Box>
        <ReactHighcharts config={this.state.configLine}></ReactHighcharts>
      </Box>
    );
  }
}

export default Charts;
