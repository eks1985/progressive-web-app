
import React, { useState, useEffect } from 'react';
import Box from '@material-ui/core/Box';
import qs from 'qs';
import { useStore } from 'lib/store';
import { useRouter } from 'next/router';
import { filterData } from 'lib/internals/product-reg/report-filters';
import { getRegProd, sData, sFiltersStructure, sFiltersValues } from 'lib/internals/product-reg';
import { getReportKeyByIndex } from 'lib/internals/product-reg/report-variants';
import { getParsedFilters } from 'lib/internals/product-reg/report-filters';
import Variants from 'components/product-reg/Variants';
import Filters from 'components/product-reg/Filters';
import Content from 'components/product-reg/Content';

async function load(args) {

  const {
    data,
    loadSuccess,
    variantCurrent,
    // filtersValues,
    // startInd,
    // portion,
    // region,
    country,
    customerGuid,
    setLoading,
    attachFiltersHandler,
    applyFiltersHandler,
    // applyFiltersTriggerHandler
  } = args;

  // console.log('variantCurrent', variantCurrent);

  let resource;
  let dataNew;
  if (variantCurrent === 'summary') {
    setLoading(true);
    resource = 'reg-items-summary';
    dataNew = await getRegProd(resource, { country, custGuid: customerGuid });
    const res = dataNew.reduce((res, item) => ({ ...res, [item._id]: item }), {});
    loadSuccess(resource, res);
  }
  if (variantCurrent === 'registrationReport') {
    if (!data.hasOwnProperty('reg-items-all-items')) {
      setLoading(true);
      resource = 'reg-items-all-items';
      dataNew = await getRegProd(resource, { country, custGuid: customerGuid });
      const { items, ...other } = dataNew;
      loadSuccess(resource, items);
      attachFiltersHandler(other);
      // applyFiltersHandler();
      // applyFiltersTriggerHandler();
    }
  }
  if (variantCurrent === 'geoMap') {
    setLoading(true);
    resource = 'reg-items-geo-map';
    dataNew = await getRegProd(resource, { country, custGuid: customerGuid });
    const category = data.category.map(item => item._id);
    const years = data.years.map(item => item._id);
    loadSuccess(resource, { category, years });
  }
  if (variantCurrent === 'regionReview') {
    setLoading(true);
    resource = 'region-review-regions';
    const regions = await getRegProd( resource, { country, custGuid: customerGuid });
    const regionsFlat = regions.map(region => {
      const { ranktotal, total, mactotal, rankprec, prec, macprecision } = region.rank;
      return { ...region, ranktotal, total, mactotal, rankprec, prec, macprecision };
    });
    loadSuccess(resource, regionsFlat);
    resource = 'region-review-crops';
    const crops = await getRegProd(resource, { country, custGuid: customerGuid });
    loadSuccess(resource, crops);
  }
  if (variantCurrent === 'finalCustomers') {
    setLoading(true);
    resource = 'final-customers-all-items';
    dataNew = await getRegProd(resource, { country, custGuid: customerGuid });
    const { items, ...other } = dataNew;
    attachFiltersHandler(other);
    loadSuccess(resource, items);
  }
  if (variantCurrent === 'keyClients') {
    setLoading(true);
    resource = 'final-customers-key-clients';
    dataNew = await getRegProd(resource, { country, custGuid: customerGuid });
    loadSuccess(resource, dataNew);
  }
  if (variantCurrent === 'agroatlas') {
    setLoading(true);
    resource = 'agroatlas';
    dataNew = await getRegProd(resource, { country, custGuid: customerGuid });
    loadSuccess(resource, data);
  }

}

const productReg = () => {

  const [variantOpen, setVariantOpen] = useState(false);
  const [variantCurrent, setVariantCurrent] = useState('summary');
  const [filtersValues, setFiltersValues] = useState({});
  const [filtersBank, setFiltersBank] = useState({});
  const [startInd, setStartInd] = useState(0);
  const [portion, setPortion] = useState(10);
  const [singleItem, setSingleItem] = useState(false);
  const [region, setRegion] = useState(false);
  const [summaryViewType, setSummaryViewType] = useState('total');
  const [mapCategory, setMapCategory] = useState(false);
  const [mapYear, setMapYear] = useState(false);
  const [data, setData] = useState({});
  const [dataFiltered, setDataFiltered] = useState({});
  const [loading, setLoading] = useState(false);
  // const [loaded, setLoaded] = useState(false);
  
  const router = useRouter();
  const store = useStore();
  const { country, appUser} = store;

  const selectVariantHandler = variant => {
    setVariantCurrent(variant);
  };

  const loadSuccess = (resource, result) => {
    const uData = { ...data, [resource]: result };
    setData(uData);
    setLoading(false);
    // console.log('load success');
    // if (resource === 'reg-items-all-items') {
    //   applyFiltersHandler();
    // }
  };

  const applyFiltersHandler = () => {

    
    if (!data || !data['reg-items-all-items']) {
      return;
    }
    // console.log('applyFiltersHandler', data);
    
    const filters = sFiltersStructure(variantCurrent, filtersBank, filtersValues);
    const dataCurrent = sData(data, variantCurrent, filtersValues, startInd, portion, region);
    if (variantCurrent === 'registrationReport') {
      const dataRaw = dataCurrent.allItemsRaw;
      // console.log('dataRaw', dataRaw);
      console.log('filters', filters);
      // console.log('data', data);
      const filtered = filterData(dataRaw, filters);
      console.log('filtered', filtered);
      const uData = { ...data, ['reg-items-all-items-f']: filtered };
      setData(uData);
    }
    if (variantCurrent === 'finalCustomers') {
      const dataRaw = data.allItemsRaw;
      const uData = { ...data, ['final-customers-all-items-f']: filterData(dataRaw, filters) };
      setData(uData);
    }

  };

  const toggleVariantHandler = () => {
    setVariantOpen(!variantOpen);
  };

  const editFiltersHandler = (name, values) => {
    console.log('values', values);
    let _fltersValues;
    if (name === 'clear') {
      _fltersValues = {};
    } else if (name === 'multiple') {
      _fltersValues = { ...filtersValues };
      values.forEach(item => {
        _fltersValues[item.name] = item.values;
      });
    } else {
      _fltersValues = { ...filtersValues, [name]: values };
    }
    setFiltersValues(_fltersValues);
  };

  const attachFiltersHandler = filters => {
    setFiltersBank(filters);
  };

  const nextPortionHandler = (step, qty) => {
    let newStartInd;
    if (startInd === 0 && step === -1) {
      return;
    }
    newStartInd = startInd + portion * step;
    if (newStartInd > qty) {
      return;
    }
    setStartInd(newStartInd);
  };

  const toggleSingleItemHandler = () => {
    setSingleItem(!singleItem);
  };

  const setRegionHandler = region => {
    setRegion(region);
  };

  const setSummaryTotal = () => {
    setSummaryViewType('total');
  };

  const setSummaryDealers = () => {
    setSummaryViewType('bydealers');
  };

  const setGeoMapFilter = (name, value) => {
    console.log(name, value);
  };

  const customerGuid = appUser ? appUser.customerGuid : '';

  let search = '';
  if (router.asPath.includes('?')) {
    search = router.asPath.split('?')[1];
  } 
  useEffect(() => {

    if (search !== '') {
      const searchObj = qs.parse(search, { ignoreQueryPrefix: true });
      if (searchObj.report) {
        selectVariantHandler(getReportKeyByIndex(searchObj.report));
      }
      const parsedFilters = getParsedFilters(searchObj);
      const { bank, values } = parsedFilters;
      if (bank) {
        attachFiltersHandler(bank);
      }
      if (values.length > 0) {
        editFiltersHandler('multiple', values);
        // applyFiltersHandler();
      }
      if (searchObj.single) {
        toggleSingleItemHandler();
      }
    }
    if (country > -1 && customerGuid) {
      load({
        variantOpen,
        variantCurrent,
        filtersValues,
        filtersBank,
        startInd,
        portion,
        singleItem,
        region,
        summaryViewType,
        mapCategory,
        mapYear,
        dataFiltered,
        loading,
        customerGuid,
        country,
        data,
        setVariantOpen,
        setVariantCurrent,
        setFiltersValues,
        setFiltersBank,
        setStartInd,
        setPortion,
        setSingleItem,
        setRegion,
        setSummaryViewType,
        setMapCategory,
        setMapYear,
        setData,
        setDataFiltered,
        setLoading,
        loadSuccess,
        selectVariantHandler,
        applyFiltersHandler,
        toggleVariantHandler,
        editFiltersHandler,
        attachFiltersHandler,
        nextPortionHandler,
        toggleSingleItemHandler,
        setRegionHandler,
        setSummaryTotal,
        setSummaryDealers,
        setGeoMapFilter,
      });
    }

  }, [appUser, search, variantCurrent]);

  useEffect(() => {
    if (variantCurrent === 'registrationReport') {
      applyFiltersHandler();
    }
  }, [data['reg-items-all-items'], variantCurrent, filtersValues, filters]);

  if (!appUser) {
    return null;
  }

  const settings = {
    variantOpen,
    variantCurrent,
    filtersValues,
    filtersBank,
    startInd,
    portion,
    singleItem,
    region,
    summaryViewType,
    mapCategory,
    mapYear, 
  };

  const filters = sFiltersStructure(variantCurrent, filtersBank, filtersValues);
  const filtersLabel = sFiltersValues(filtersValues);
  const actualData = sData(data, variantCurrent, filtersValues, startInd, portion, region);

  // console.log('data', data);

  return (
    <Box margin='0 auto' width='100%' maxWidth='900px'>
      {appUser.internal &&
        <Variants
          settings={settings}
          toggleVariant={toggleVariantHandler}
          selectVariant={selectVariantHandler}
          editFilters={editFiltersHandler}
        />
      }
      <Filters
        filters={filters}
        settings={settings}
        editFilters={editFiltersHandler}
        applyFiltersTrigger={applyFiltersHandler}
        filtersLabel={filtersLabel}
      />
      <Content
        data={actualData}
        settings={settings}
        nextPortion={nextPortionHandler}
        selectVariant={selectVariantHandler}
        editFilters={editFiltersHandler}
        applyFiltersTrigger={applyFiltersHandler}
        setRegion={setRegion}
        region={region}
        // geoMapFilters={geoMapFiltersH}
        setSummaryTotal={setSummaryTotal}
        setSummaryDealers={setSummaryDealers}
        setGeoMapFilter={setGeoMapFilter}
        email={appUser.email}
        isInternalUser={appUser.internal}
      />
    </Box>
  );

};

export default productReg;