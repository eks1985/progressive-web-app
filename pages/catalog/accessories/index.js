import React, { useState } from 'react';
import { baseUrl, secret } from 'lib/base';
import { useStore } from 'lib/store';
import DataProviderBatch from 'lib/data-batch';
import Box from '@material-ui/core/Box';
import List from './components/list';
import CategoryFilter from './components/category-filter';

const prepareQuery = (props, country) => {

  const { url_accessories, url_categories } = props;

  return {
    accessories: `${url_accessories}&country=${country}`,
    categories: `${url_categories}&country=${country}`,
  };

};

const accessoriesCMS = props => {

  const [category, selectCategory] = useState(false);
  const store = useStore();
  const { country } = store;

  const query = prepareQuery(props, country);

  return (
    <Box margin='0 auto' width='100%' maxWidth='900px'>
      <DataProviderBatch query={query} render={(data, mutate) => (
        <React.Fragment>
          <CategoryFilter
            key='category_filter'
            data={data}
            category={category}
            selectCategory={selectCategory}
          />
          <List
            key='accessory_list'
            data={data}
            category={category}
            mutate={mutate}
          />
        </React.Fragment>
      )}/>
    </Box>
  );

};

accessoriesCMS.defaultProps = {
  url_accessories: `${baseUrl}accessories${secret}`,
  url_categories: `${baseUrl}catalog${secret}lists=c&vis=2`, 
};

export default accessoriesCMS;