import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 300,
    width: '100%',
  },
  selectEmpty: {
    marginTop: theme.spacing(1) * 2,
  },
}));

const assign = props => {

  const { categoryRef, categories, updateItem } = props;

  const classes = useStyles();

  return (
    <form className={classes.root} autoComplete='off'>
      <FormControl className={classes.formControl}>
        <InputLabel htmlFor='category-simple'>Категория</InputLabel>
        <Select
          value={categoryRef || ''}
          onChange={
            e => {
              updateItem({ prop: 'categoryRef', value: e.target.value })
            }
          }
          inputProps={{
            name: 'category',
            id: 'category-simple',
          }}
        >
          {
            categories.map(category => (
              <MenuItem
                key={category.id}
                value={category.id}
              >
                {category.descr}
              </MenuItem>
            ))
          }
        </Select>
      </FormControl>
    </form>
  );
};

export default assign;


