import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    overflowX: 'auto',
  },
  table: {
    width: '100%',
  },
}));

const codesRender = props => {

  const classes = useStyles();

  const { data } = props;
  
  return (
    <Paper className={classes.root} elevation={0}>
      <Table className={classes.table}>
        <TableBody>
          {data.map((row, ind) => {
            return (
              <TableRow
                key={`${row.code}-${ind}`}
              >
                <TableCell component='th' scope='row'>
                  {row.code}
                </TableCell>
                <TableCell align='right'>
                  {row.qty}
                </TableCell>
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    </Paper>
  );
};

export default codesRender;

