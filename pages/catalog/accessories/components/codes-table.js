import React from 'react';
import { capit } from 'lib/strings';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import IconAddCode from '@material-ui/icons/AddCircleOutline';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    overflowX: 'auto',
    marginTop: '16px',
    marginBottom: '16px',
    border: '1px solid #eee'
  },
  table: {
    width: '100%',
  },
  iconAdd: {
    marginRight: theme.spacing(2),
  }
}));

const CodesTable = props => {

  const classes = useStyles();

  const { codes, addCode, setCodeInd } = props;

  return (
    <Paper className={classes.root} elevation={0}>
      <Box display='flex' ml={3} my={1}>
        <Button
          color='primary'
          size='small'
          onClick={
            () => {
              addCode();
            }
          }
        >
          <IconAddCode className={classes.iconAdd} />
          Добавить код
        </Button>
      </Box>
      <Table className={classes.table}>
        <TableBody>
          {codes.map((row, ind) => {
            return (
              <TableRow
                key={`${row.code}-${ind}`}
                onClick={
                  () => {
                    setCodeInd(ind);
                  }
                }
              >
                <TableCell
                  component='th'
                  scope='row'
                >
                  {row.code}
                </TableCell>
                <TableCell
                  align='right'
                >
                  {row.qty}
                </TableCell>
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    </Paper>
  );
};

const CodeEditDialog = props => {
  
  const { codes, codeInd, setCodeInd, editCode, removeCode } = props;

  const code = codeInd > -1 ? codes[codeInd].code : '';
  const qty = codeInd > -1 ? codes[codeInd].qty : '';
  
  return (
    <div>
      <Dialog
        open={codeInd > -1}
        onClose={
          () => {
            setCodeInd(-1);
          }
        }
        aria-labelledby='alert-dialog-title'
        aria-describedby='alert-dialog-description'
      >
        <DialogTitle id='alert-dialog-title'>Код и количество</DialogTitle>
        <DialogContent>
          <FormControl fullWidth aria-describedby='code-value'>
            <InputLabel htmlFor='code-value'>Код</InputLabel>
            <Input
              style={{ fontSize: '20px' }}
              id='сode-value'
              label='Код'
              value={code}
              onChange={
                e => {
                  editCode({ prop: 'code', value: e.target.value })
                }
              }
              inputProps={{
                style: { textTransform: 'uppercase' },
              }}
            />
          </FormControl>
          <FormControl fullWidth aria-describedby='qty-value' className='mt-3'>
            <InputLabel htmlFor='qty-value'>Количeство</InputLabel>
            <Input
              style={{ fontSize: '20px' }}
              id='qty-value'
              label='Количество'
              value={qty}
              onChange={
                e => {
                  const numb = parseInt(e.target.value, 10);
                  if (isNaN(numb)) {
                    editCode({ prop: 'qty', value: 0 });
                  } else {
                    editCode({ prop: 'qty', value: numb })
                  }
                }
              }
            />
          </FormControl>
        </DialogContent>
        <DialogActions>
          {codeInd > -1 &&
            <Button
              onClick={removeCode}
            >
              Удалить
            </Button>
          }
          <Button
            onClick={
              () => {
                // removeCode();
                setCodeInd(-1);
              } 
            }
            color='primary' variant='contained' autoFocus
          >
            Ок
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

const codesTableContainer = props => { 

  return [
    <CodesTable
      key='codes'
      {...props}
    />,
    <CodeEditDialog
      key='edit-code'
      {...props}
    />,
  ];

};

export default codesTableContainer;
