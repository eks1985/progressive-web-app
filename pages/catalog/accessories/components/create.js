import React from 'react';
import { useStore } from 'lib/store';
import { makeStyles } from '@material-ui/core/styles';
import AddIcon from '@material-ui/icons/Add';
import Fab from '@material-ui/core/Fab';
import { createAccessory } from 'lib/api/accessories';

const useStyles = makeStyles(theme => ({
  root: {
    position: 'fixed',
    top: 40,
    right: 3,
    zIndex: '9999',
  },
}));

const create = props => {

  const classes = useStyles();
  const { mutate, url_create, categoryRef } = props;
  const store = useStore();
  const { country } = store;

  const create = () => {
    createAccessory(url_create, country, categoryRef, mutate);
  }

  return (
    <div className={classes.root}>
      <Fab
        aria-label='create'
        size='small'
        onClick={create}
      >
        <AddIcon />
      </Fab>
    </div>
  );
};

create.defaultProps = {
  url_create: 'https://webhooks.mongodb-realm.com/api/client/v2.0/app/mgd-ilrht/service/http/incoming_webhook/data-create?secret=L0sS3Bz1mdCm6Kuz',
};

export default create;

