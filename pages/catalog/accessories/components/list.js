import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Loading from 'components/loading';
import Picture from './picture';
import ItemDialog from './item-dialog';
import ItemDialogContent from './item-dialog-content';
import CodesRender from './codes-render';
import Create from './create';
import { removeAccessory, updateAccessory } from 'lib/api/accessories';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    fontSize: theme.typography.pxToRem(13),
    position: 'relative',
    marginTop: theme.spacing(1),
  },
  head: {
    fontSize: theme.typography.pxToRem(12),
    padding: '4px',
    paddingLeft: '0px',
    paddingRight: theme.spacing(6),
    color: theme.palette.primary.main,
  },
  category: {
    color: '#555',
  },
}));

const Item = props => {

  const [item, setItem] = useState(props.item); // open accessory dialog
  const [open, setOpen] = useState(false); // open accessory dialog
  const [code, setCode] = useState(''); // state for currently edit code
  const [codeInd, setCodeInd] = useState(-1); // state for currently edit code
  const [qty, setQty] = useState(1); // state for currently edit qty
  const [mod, setMod] = useState(false); // mark that current item is mod now
  const [progress, setProgress] = useState(false);
 
  const {
    categories,
    setOpenRoot,
    url_create, url_delete, mutate,
  } = props;

  const { descr, picture, price, manualPrice, codes, categoryRef } = item;

  const addCode = () => {
    const codes = [...item.codes];
    const newCode = {
      code: 'Code',
      qty: 1,
    }
    codes.push(newCode);
    updateItem({ prop: 'codes', value: codes });
  };

  const editCode = props => {
    const { prop, value } = props;
    const { codes } = item;
    const code = { ...codes[codeInd] };
    const updatedCode = { ...code, [prop]: value };
    const updatedCodes = codes.map((item, ind) => {
      if (ind === codeInd) {
        return updatedCode;
      }
      return item;
    });
    updateItem({ prop: 'codes', value: updatedCodes });
  };

  const removeCode = () => {
    const codes = item.codes.filter((item, index) => index !== codeInd);
    setCodeInd(-1);
    updateItem({ prop: 'codes', value: codes });
  };

  const deleteAccess = () => {
    setOpen(false);
    setProgress(true);
    removeAccessory(url_delete, item.id);
    setTimeout(() => {
      setMod(false);
      setProgress(false);
      mutate();
      setOpenRoot(false);
    }, 1000);
  };

  const updateItem = props => {
    const { prop, value } = props;
    const updated = { ...item, [prop]: value };
    setItem(updated);
    setMod(true);
  };

  const saveAccess = () => {
    setProgress(true);
    updateAccessory(url_create, item.id, item);
    setTimeout(() => {
      setMod(false);
      setProgress(false);
      mutate();
    }, 1000);
  };

  const classes = useStyles();

  const preparedPrice = manualPrice || price;
  const catFilter = categories.filter(cat => cat.id === categoryRef);
  const categoryTitle = catFilter.length > 0 ? catFilter[0].descr : '';

  if (progress) {
    return <Loading />;
  }

  return (
    <Accordion>
      <AccordionSummary expandIcon={<ExpandMoreIcon />}>
        <div>
          <div>
            <Typography className={classes.head}>{descr}</Typography>
          </div>
          {categoryRef &&
            <div>
              <Typography className={classes.category} variant='body1'>
                {categoryTitle}
              </Typography>
            </div>
          }
        </div>
      </AccordionSummary>
      <AccordionDetails>
        <Box width='100%' display='flex' flexDirection='column'>
          {price > 0 &&
            <Typography variant='subtitle1'>{`${preparedPrice.toLocaleString('ru-RU', { minimumFractionDigits: 0 })}€`}</Typography>
          }
          {picture &&
            <div>
              <Picture picture={picture} progress={progress} />
            </div>
          }
          <CodesRender data={codes} />
          <Box display='flex' alignItems='center' justifyContent='flex-end'>
            <ItemDialog setOpenRoot={setOpenRoot} open={open} setOpen={setOpen} mod={mod} save={saveAccess}>
              <ItemDialogContent
                item={item}
                categories={categories}
                open={open}
                setOpen={setOpen}
                setOpenRoot={setOpenRoot}
                progress={progress}
                setProgress={setProgress}
                addCode={addCode}
                editCode={editCode}
                removeCode={removeCode}
                deleteAccess={deleteAccess}
                code={code}
                setCode={setCode}
                qty={qty}
                setQty={setQty}
                codeInd={codeInd}
                setCodeInd={setCodeInd}
                saveAccess={saveAccess}
                mod={mod}
                setMod={setMod}
                updateItem={updateItem}
              />
            </ItemDialog>
          </Box>
        </Box>
      </AccordionDetails>
    </Accordion>
  );
};

const list = props => {
  
  const { data, mutate, category } = props;

  const [openRoot, setOpenRoot] = useState(false); // open accessory dialog

  const classes = useStyles();

  const listPrepared = category ? data.accessories.filter(item => item.categoryRef === category.value) : data.accessories;

  return (
    <Box className={classes.root}>
      {!openRoot &&
        <Create mutate={mutate} categoryRef={category ? category.value : false} />
      }
      {
        listPrepared.map((item, ind) => (
          <Item key={item.id} ind={ind} item={item} {...props} setOpenRoot={setOpenRoot} categories={data.categories.categories} />
        ))
      }
    </Box>
  );
};

list.defaultProps = {
  url_delete: 'https://webhooks.mongodb-realm.com/api/client/v2.0/app/mgd-ilrht/service/mgd/incoming_webhook/delete_document?secret=L0sS3Bz1mdCm6Kuz',
  url_create: 'https://webhooks.mongodb-realm.com/api/client/v2.0/app/mgd-ilrht/service/http/incoming_webhook/data-create?secret=L0sS3Bz1mdCm6Kuz',
};

export default list;

