import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Slide from '@material-ui/core/Slide';

const useStyles = makeStyles(theme => ({
  appBar: {
    position: 'relative',
  },
  flex: {
    flex: 1,
  },
}));

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const itemDialog = props => {

  const { open, setOpen, setOpenRoot, mod, save } = props;

  const classes = useStyles();

  return (
    <Box display='flex' flexDirection='column' justifyContent='flex-end' mt={3}>
      <Box display='flex'>
        {mod &&
          <Button
          color='primary'
          onClick={
            () => {
              save();
            }
          }
        >
          Сохранить
        </Button>
        }
        <Box ml={2}>
          <Button
            // variant='outlined'
            onClick={
              () => {
                setOpen(true);
                setOpenRoot(true);
              }
            }
          >
            Изменить
          </Button>
        </Box>
      </Box>
      {open &&
        <Dialog
          style={{ maxWidth: '900px', margin: '0 auto' }}
          fullScreen
          open={open}
          onClose={
            () => {
              setOpen(false);
            }
          }
          TransitionComponent={Transition}
        >
          <AppBar className={classes.appBar}>
            <Toolbar>
              <Typography variant='subtitle1' color='inherit' className={classes.flex}>
                Редактирование аксессуара
              </Typography>
              <Button
                color='inherit'
                onClick={
                  () => {
                    setOpen(false);
                    setOpenRoot(false);
                  }
                }
              >
                Ok
              </Button>
            </Toolbar>
          </AppBar>
          <Paper square>
            <Box mt={2} p={2}>
              {props.children}
            </Box>
          </Paper>
        </Dialog>
      }
    </Box>
  );

};

export default itemDialog;

