import React from 'react';
import Select from 'components/select';
import Box from '@material-ui/core/Box';

const categoryFilter = props => {

  const { data, category, selectCategory } = props;

  if (!data || !data.categories) {
    return null;
  }

  const options = data.categories.categories.map(item => ({ value: item._id, label: item.descr })); 

  return (
    <Box margin='0 auto' width='100%' maxWidth='900px' p={2}>
      <Select placeholder='Категория' options={options} onSelectHandler={selectCategory} category={category} />
    </Box>
  );

};

export default categoryFilter;
