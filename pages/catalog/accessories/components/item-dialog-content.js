import React from 'react';
import FileUpload from 'components/cdn-upload';
import Box from '@material-ui/core/Box';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Button from '@material-ui/core/Button';
import Loading from 'components/loading';
import Picture from './picture';
import Assign from './assign';
import CodesTable from './codes-table';

const itemDialogContent = props => {

  const handleAddPicture = value => {
    updateItem({ prop: 'picture', value });  
  }
  
  const {
    item: { descr, picture, categoryRef, codes, hasManualPrice, manualPrice },
    categories,
    progress, setProgress,
    addCode,
    removeCode,
    deleteAccess,
    updateItem,
    codeInd, setCodeInd,
    editCode,
  } = props;
  
  return (
    <Box>
      <div>
        <TextField
          fullWidth
          multiline
          id='tt'
          label='Наименование'
          margin='dense'
          variant='outlined'
          value={descr}
          onChange={
            e => {
              updateItem({ prop: 'descr', value: e.target.value });
            }
          }
        />
      </div>
      <Assign categoryRef={categoryRef} categories={categories} updateItem={updateItem} />
      <Box ml={1} mt={2} display='flex' alignItems='center'>
        <FormControlLabel
          control={
            <Checkbox
              checked={hasManualPrice || false}
              color='primary'
              onChange={
                e => {
                  updateItem({ prop: 'hasManualPrice', value: e.target.checked });
                }
              }
              name='Ручная цена'
            />
          }
          label='Ручная цена'
        />
        {hasManualPrice &&
          <Box ml={1}>
            <TextField
              fullWidth
              multiline
              id='pr'
              label='Цена €'
              margin='dense'
              variant='outlined'
              value={manualPrice || 0}
              onChange={
                e => {
                  const numb = parseInt(e.target.value, 10);
                  if (!isNaN(numb)) {
                    updateItem({ prop: 'manualPrice', value: numb });
                  } else {
                    updateItem({ prop: 'manualPrice', value: 0 });
                  }
                }
              }
            />
          </Box>
        }
      </Box>
      <CodesTable
        codes={codes}
        addCode={addCode}
        removeCode={removeCode}
        setCodeInd={setCodeInd}
        codeInd={codeInd}
        editCode={editCode}
      />
      {progress &&
        <Loading />
      }
      {!progress && picture &&
        <Picture
          picture={picture}
        />
      }
      <FileUpload {...props} setUploading={setProgress} handleAddPicture={handleAddPicture} />
      <Box my={5} display='flex' justifyContent='center'>
        <Button
          onClick={deleteAccess}
        >
          Удалить аксессуар
        </Button>
      </Box>
    </Box>
  );
};

export default itemDialogContent;
