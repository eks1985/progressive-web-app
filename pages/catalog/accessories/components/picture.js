import React from 'react';
import Box from '@material-ui/core/Box';

const Picture = ({ url }) => {

  const containerStyle = {
    marginTop: '4px',
    justifyContent: 'center',
    flex: '0 0 100%',
    minHeight: '160px',
    backgroundImage: `url("${url}")`,
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'contain',
    borderRadius: '4px',
    backgroundPosition: 'center center',
  };
  return (
    <div className='mb-2'>
      <div className='d-flex'>
        <div
          style={containerStyle}
          onClick={
            () => {
              const link = document.createElement('a');
              document.body.appendChild(link);
              link.href = url;
              link.target = '_blank';
              link.click();
            }
          }
        />
      </div>
    </div>
  );

};

const acessPicture = props => {
  
  const { picture } = props;
  
  return (
    <Box mt={2} border='1px solid #eee' borderRadius='8px'>
      {picture &&
        <Picture url={picture} />
      }
    </Box>
  );
};

export default acessPicture;
