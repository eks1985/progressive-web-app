import React, { useEffect, useState } from 'react';
import Image from 'next/image'
import { useRouter } from 'next/router';
import { useStore } from 'lib/store';
import { makeStyles } from '@material-ui/core/styles';
import classNames from 'classnames';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import IconVis1 from '@material-ui/icons/Done';
import IconVis2 from '@material-ui/icons/DoneAll';
import IconVis0 from '@material-ui/icons/NotInterested';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import CatalogItemMenu from 'components/catalog-item-menu';
import { updateCategory } from 'lib/api/categories';
import { shiftArrayElem } from 'lib/arrays';

const useStyles = makeStyles(theme => ({
  list: {
    width: '100%',
  },
  listItem: {
    padding: '12px 20px',
    borderTop: '1px solid #eee',
  },
  listItemText: {
    fontSize: '14px',
    paddingRight: '20px',
  },
  gray: {
    color: '#999',
  },
  icon: {
    color: theme.palette.secondary.main,
  },
}));

const Picture = props => {

  const { url, descr } = props;

  return (
    <Image
      src={url}
      alt={descr}
      width={60}
      height={30}
    />
  );

};

const item = props => {

  const classes = useStyles();
  const router = useRouter();
  const { categories, url_create, mutate, index, setCategories, setRootMod } = props;
  const store = useStore();

  const [item, setItem] = useState(props.category);
  const [mod, setMod] = useState(false);

  useEffect(() => {
    setItem(props.category);
  }, [categories]);

  const edit = (index, updated) => {
    setMod(true);
    setItem(updated);
  };

  const shift = step => {
    const newCat = shiftArrayElem(categories, index, step);
    setCategories(newCat);
    setRootMod(true);
  };

  const saveCategory = () => {
    store.setLoading(true);
    updateCategory(url_create, item.id, item, mutate, store.setLoading);
    // store.setLoading(false);
    // mutate();
    setMod(false);
  };

  return (
    <ListItem
      className={classes.listItem}
      button
      onClick={
        () => {
          router.push(`/catalog/categories/${item.id}`);
        }
      }
    >
      {item.vis === 1 &&
        <ListItemIcon className={classes.icon}>
          <IconVis1 />
        </ListItemIcon>
      }
      {item.vis === 2 &&
        <ListItemIcon className={classes.icon}>
          <IconVis2 />
        </ListItemIcon>
      }
      {(item.vis === 0 || (!item.vis)) &&
        <ListItemIcon className={classes.icon}>
          <IconVis0 />
        </ListItemIcon>
      }
      {item.picture &&
        <Box mr={2}>
          <Picture url={item.picture} descr={item.descr} />
        </Box>
      }
      {mod &&
        <Box mr={2}>
          <Button
            variant='outlined'
            size='small'
            color='primary'
            onClick={
              e => {
                e.preventDefault();
                e.stopPropagation();
                saveCategory();
              }
            }
          >
            Сохранить
          </Button>
        </Box>
      }
      <ListItemText
        disableTypography
        className={classNames(classes.listItemText, { [classes.gray]: !item.vis })}
        primary={<div>{item.descr}</div>}
      />
      <ListItemSecondaryAction>
        <CatalogItemMenu
          data={categories}
          index={index}
          updateData={edit}
          shiftData={shift}
        />
      </ListItemSecondaryAction>
    </ListItem>
  );

};

item.defaultProps = {
  url_create: 'https://webhooks.mongodb-realm.com/api/client/v2.0/app/mgd-ilrht/service/http/incoming_webhook/data-create?secret=L0sS3Bz1mdCm6Kuz',
};

export default item;


