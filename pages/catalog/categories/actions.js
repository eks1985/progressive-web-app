import React from 'react';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

const actions = props => {
  const { updateAll, setRootMod, updated, toggleUpdated } = props;
  return (
    <Box display='flex' justifyContent='center' my={3}>
      <Button
        variant='contained'
        color='primary'
        onClick={updateAll}
      >
        Сохранить
      </Button>
      <Box ml={2}>
        <Button
          variant='outlined'
          onClick={
            () => {
              setRootMod(false);
            }
          }
        >
          Отменить
        </Button>
      </Box>
      <Snackbar
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        open={updated}
        autoHideDuration={2000}
        onClose={toggleUpdated}
        ContentProps={{
          'aria-describedby': 'message-id',
        }}
        message={<span id='mesage-id'>Данные обновлены</span>}
        action={[
          <IconButton
            key='close'
            aria-label='close'
            color='inherit'
            onClick={toggleUpdated}
          >
            <CloseIcon />
          </IconButton>,
        ]}
      />
    </Box>
  );
};

export default actions;
