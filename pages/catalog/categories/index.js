import React from 'react';
import { baseUrl, secret } from 'lib/base';
import { useStore } from 'lib/store';
import DataProviderBatch from 'lib/data-batch';
import Box from '@material-ui/core/Box';
import Content from './content';

const prepareQuery = (props, country) => {

  const { url_categories } = props;

  return {
    categories: `${url_categories}&country=${country}`,
  };

};

const categories = props => {

  const store = useStore();
  const { country } = store;

  const query = prepareQuery(props, country);

  return (
    <Box margin='0 auto' width='100%' maxWidth='900px'>
      <DataProviderBatch query={query} render={(data, mutate) => (
        <Content
          data={data.categories.categories}
          mutate={mutate}
        />
      )}/>
    </Box>
  );

};

categories.defaultProps = {
  url_categories: `${baseUrl}catalog${secret}lists=c&vis=0`, 
};

export default categories;