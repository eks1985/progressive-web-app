import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import Create from './create';
import Item from './item';

const useStyles = makeStyles(theme => ({
  list: {
    width: '100%',
  },
  listItem: {
    padding: '12px 20px',
    borderTop: '1px solid #eee',
  },
  listItemText: {
    fontSize: '14px',
    paddingRight: '20px',
  },
  gray: {
    color: '#999',
  },
  icon: {
    marginRight: '8px',
    color: theme.palette.secondary.main,
  },
}));

const list = props => {
  
  const classes = useStyles();
  const { categories, setCategories, setRootMod, mutate } = props;

  return [
    <List key='list' className={classes.list}>
      {
        categories.map((category, index) => {
          return (
            <Item
              key={index}
              index={index}
              setCategories={setCategories}
              categories={categories}
              category={category}
              setRootMod={setRootMod}
              mutate={mutate}
            />
          );
        })
      }
    </List>,
    <Create key='create' mutate={mutate} />,
  ];

};

export default list;


