import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import Create from './create-model';
import Item from './model';
import Actions from './actions-models';

const useStyles = makeStyles(theme => ({
  list: {
    width: '100%',
  },
  // listItem: {
  //   padding: '12px 20px',
  //   borderTop: '1px solid #eee',
  // },
  // listItemText: {
  //   fontSize: '14px',
  //   paddingRight: '20px',
  // },
  // gray: {
  //   color: '#999',
  // },
  // icon: {
  //   marginRight: '8px',
  //   color: theme.palette.secondary.main,
  // },
}));

const list = props => {
  
  const classes = useStyles();
  const { models, modelsMod, createModel, editModel, shiftModel, updateModels, setModelsMod } = props;

  return [
    <Actions
      key='actions'
      modelsMod={modelsMod}
      updateModels={updateModels}
      setModelsMod={setModelsMod}
    />,
    <List key='list' className={classes.list}>
      {
        models.map((model, index) => {
          return (
            <Item
              key={index}
              index={index}
              models={models}
              model={model}
              modelsMod={modelsMod}
              editModel={editModel}
              shiftModel={shiftModel}
              updateModels={updateModels}
            />
          );
        })
      }
    </List>,
    <Create
      key='create'
      createModel={createModel}
    />,
  ];

};

export default list;


