import React, { useState } from 'react';
import { useRouter } from 'next/router';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import IconMoreVert from '@material-ui/icons/MoreVert';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import CategorySelect from './category-select';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  grow: {
    flexGrow: 1,
    fontSize: '14px',
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
  toolbar: {
    paddingRight: '0px',
    display: 'flex',
    justifyContent: 'space-between',
  },
}));

const AppBarMenu = props => {

  const { anchorEl, open, handleMenu, handleClose } = props;
  const router = useRouter();

  return (
    <div>
      <IconButton
        aria-owns={open ? 'menu-appbar' : null}
        aria-haspopup='true'
        onClick={handleMenu}
        color='inherit'
      >
        <IconMoreVert />
      </IconButton>
      <Menu
        id='menu-appbar'
        anchorEl={anchorEl}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        open={open}
        onClose={handleClose}
      >
        <MenuItem
          onClick={
            () => {
              handleClose();
              router.push('/catalog/categories');
            }
          }
        >
          Все категории
        </MenuItem>
      </Menu>
    </div>
  );

};

const menuAppBar = props => {

  const { categories, category } = props;

  const classes = useStyles();

  const [anchorEl, setAnchorEl] = useState();

  const handleMenu = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);

  return (
    <div className={classes.root}>
      <AppBar position='static'>
        <Toolbar className={classes.toolbar}>
          <CategorySelect
            text={`Категория: ${category.descr}`}
            categories={categories}
          />
          <AppBarMenu
            anchorEl={anchorEl}
            open={open}
            handleMenu={handleMenu}
            handleClose={handleClose}
          />
        </Toolbar>
      </AppBar>
    </div>
  );

};

export default menuAppBar;
