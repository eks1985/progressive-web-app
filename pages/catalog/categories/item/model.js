import React, { useEffect, useState } from 'react';
import Image from 'next/image'
import { useRouter } from 'next/router';
import { makeStyles } from '@material-ui/core/styles';
import classNames from 'classnames';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import IconVis1 from '@material-ui/icons/Done';
import IconVis2 from '@material-ui/icons/DoneAll';
import IconVis0 from '@material-ui/icons/NotInterested';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import CatalogItemMenu from 'components/catalog-item-menu';

const useStyles = makeStyles(theme => ({
  list: {
    width: '100%',
  },
  listItem: {
    padding: '12px 20px',
    borderTop: '1px solid #eee',
  },
  listItemText: {
    fontSize: '14px',
    paddingRight: '20px',
  },
  gray: {
    color: '#999',
  },
  icon: {
    color: theme.palette.secondary.main,
  },
}));

const Picture = props => {

  const { url, descr } = props;

  return (
    <Image
      src={url}
      alt={descr}
      width={60}
      height={30}
    />
  );

};

const item = props => {

  const classes = useStyles();
  const router = useRouter();
  const { models, model, modelsMod, index, editModel, shiftModel, updateModels } = props;

  return (
    <ListItem
      className={classes.listItem}
      button
      onClick={
        () => {
          router.push(`/catalog/models/${model.id}`);
        }
      }
    >
      {model.vis === 1 &&
        <ListItemIcon className={classes.icon}>
          <IconVis1 />
        </ListItemIcon>
      }
      {model.vis === 2 &&
        <ListItemIcon className={classes.icon}>
          <IconVis2 />
        </ListItemIcon>
      }
      {(model.vis === 0 || (!model.vis)) &&
        <ListItemIcon className={classes.icon}>
          <IconVis0 />
        </ListItemIcon>
      }
      {model.picture &&
        <Box mr={2}>
          <Picture url={model.picture} descr={model.descr} />
        </Box>
      }
      {model.mod && !modelsMod && // if all models mod then show separete Save btn on top of list
        <Box mr={2}>
          <Button
            variant='outlined'
            size='small'
            color='primary'
            onClick={
              e => {
                e.preventDefault();
                e.stopPropagation();
                updateModels();
              }
            }
          >
            Сохранить
          </Button>
        </Box>
      }
      <ListItemText
        disableTypography
        className={classNames(classes.listItemText, { [classes.gray]: !model.vis })}
        primary={<div>{model.descr}</div>}
      />
      <ListItemSecondaryAction>
        <CatalogItemMenu
          index={index}
          data={models}
          shiftData={shiftModel}
          updateData={editModel}
        />
      </ListItemSecondaryAction>
    </ListItem>
  );

};

item.defaultProps = {
  url_create: 'https://webhooks.mongodb-realm.com/api/client/v2.0/app/mgd-ilrht/service/http/incoming_webhook/data-create?secret=L0sS3Bz1mdCm6Kuz',
};

export default item;


