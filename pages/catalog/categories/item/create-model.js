import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AddIcon from '@material-ui/icons/Add';
import Fab from '@material-ui/core/Fab';

const useStyles = makeStyles(theme => ({
  root: {
    position: 'fixed',
    top: 40,
    right: 3,
    zIndex: '9999',
  },
}));

const create = props => {

  const classes = useStyles();
  const { createModel } = props;

  return (
    <div className={classes.root}>
      <Fab
        aria-label='create'
        size='small'
        onClick={createModel}
      >
        <AddIcon />
      </Fab>
    </div>
  );
};

export default create;

