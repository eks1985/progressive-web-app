import React, { useState, useEffect } from 'react';
import { useStore } from 'lib/store';
import { makeStyles } from '@material-ui/core/styles';
import { useRouter } from 'next/router';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Header from './header';
import Category from './category';
import Models from './models';
import Actions from './actions';
import { updateCategoryCall, updateModelsCall, createModelCall } from 'lib/api/categories-item';
import { shifObjArrayElem, shiftArrayElem } from 'lib/arrays';

const useStyles = makeStyles(theme => ({
  btn: {
    minWidth: '200px',
  },
}));

const content = props => {

  const store = useStore();
  const router = useRouter();
  const classes = useStyles();
  const { categories, categoryId, mutate, url_create } = props;

  const [category, setCategory] = useState(props.singleCat.length > 0 ? props.singleCat[0] : false);
  const [models, setModels] = useState(props.models);
  const [categoryMod, setCategoryMod] = useState(false);
  const [modelsMod, setModelsMod] = useState(false);
  const [updated, toggleUpdated] = useState(false);

  useEffect(() => {
    setModels(props.models);
    const newCategory = props.singleCat.length > 0 ? props.singleCat[0] : false;
    setCategory(newCategory);
  }, [props]);

  const updateCategory = () => {
    updateCategoryCall(url_create, category, mutate, store.setLoading, setCategoryMod);
  };

  const edit = (prop, value) => {
    setCategory(category => {
      return { ...category, [prop]: value };
    });

    setCategoryMod(true);
  };

  const selectContent = selected => {
    edit('contentList', category.contentList.concat(selected));
  }

  const shiftContent = (ind, step) => {
    const newContentList = shifObjArrayElem(category.contentList, ind, step);
    edit('contentList', newContentList);
  };

  const removeContent = ind => {
    const newContentList = category.contentList.filter((item, curInd) => curInd !== ind);
    edit('contentList', newContentList);
  };

  const updateModels = () => {
    updateModelsCall(url_create, models, mutate, setModelsMod, store.setLoading);
  };

  const shiftModel = (step, index) => {
    setModelsMod(true);
    const newModels = shiftArrayElem(models, index, step).map(item => {
      return { ...item, mod: true };
    });
    setModels(newModels);
  };

  const editModel = (index, newModel) => {
    const newModels = models.map((item, ind) => {
      if (ind === index) {
        return newModel;
      }
      return item;
    });
    setModels(newModels);
  };

  const createModel = () => {
    createModelCall(url_create, store.country, categoryId, mutate, store.setLoading);
  };

  if (!category) {
    return null;
  }

  return (
    <Box maxWidth='900px' width='100%' margin='0 auto'>
      <Header
        mutate={mutate}
        category={category}
        categories={categories}
      />
      {categoryMod &&
        <Actions
          updateCategory={updateCategory}
          updated={updated}
          toggleUpdated={toggleUpdated}
          setCategoryMod={setCategoryMod}
        />
      }
      <Category
        category={category}
        edit={edit}
        selectContent={selectContent}
        shiftContent={shiftContent}
        removeContent={removeContent}
      />
      <Models
        models={models}
        modelsMod={modelsMod}
        setModelsMod={setModelsMod}
        createModel={createModel}
        editModel={editModel}
        shiftModel={shiftModel}
        updateModels={updateModels}
      />
      <Box my={3} display='flex' justifyContent='center'>
        <Button
          className={classes.btn}
          variant='outlined'
          onClick={
            () => {
              router.push('/catalog/accessories');
            }
          }
        >
          Аксессуары
        </Button>
      </Box>
      <Box my={3} display='flex' justifyContent='center'>
        <Button
          className={classes.btn}
          variant='outlined'
          onClick={
            () => {
              router.push('/content');
            }
          }
        >
          Контент
        </Button>
      </Box>
    </Box>
  );

};

content.defaultProps = {
  url_create: 'https://webhooks.mongodb-realm.com/api/client/v2.0/app/mgd-ilrht/service/http/incoming_webhook/data-create?secret=L0sS3Bz1mdCm6Kuz',
  categories: [],
  models: [],
};

export default content;
