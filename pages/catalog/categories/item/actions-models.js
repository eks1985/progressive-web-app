import React from 'react';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';

const actions = props => {

  const { modelsMod, updateModels, setModelsMod } = props;

  // console.log('actions update models');

  if (!modelsMod) {
    return null;
  }

  return (
    <Box margin='0 auto' width='100%' display='flex' justifyContent='center' mt={2}>
      <Box>
        <Button
          variant='contained'
          color='primary'
          onClick={updateModels}
        >
          Сохранить
        </Button>
      </Box>
      <Box ml={2}>
        <Button
          onClick={setModelsMod.bind(null, false)}>
          Отмена
        </Button>    
      </Box>
    </Box>
  );

};

export default actions;
