import React from 'react';
import Image from 'next/image';
import Box from '@material-ui/core/Box';
import FileUpload from 'components/cdn-upload';

const Picture = ({ url }) => {
  const containerStyle = {
    marginTop: '4px',
    justifyContent: 'center',
    flex: '0 0 100%',
    minHeight: '80px',
    backgroundImage: `url("${url}")`,
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'contain',
    borderRadius: '4px',
  };
  return (
    <Box mb={2}>
      <Box display='flex'>
        <Box
          style={containerStyle}
          onClick={
            () => {
              const link = document.createElement('a');
              document.body.appendChild(link);
              link.href = url;
              link.target = '_blank';
              link.click();
            }
          }
        />
      </Box>
    </Box>
  );
};

const categoryPicture = props => {
  
  const { category } = props;
  
  const handleAddPicture = picture => {
   
    const { edit } = props;
    console.log('picture', picture);
    edit('picture', picture);

  };
  
  return (
    <Box id='pict-container' display='flex' flexDirection='column' alignItems='center' width='100%'>
      {category.picture &&
        <Box className='img-wrap' width={100} mt={2} position='relative'>
          <Image className='img' src={category.picture} alt='category-picture' layout='fill' objectFit='contain' objectPosition='center center' />
        </Box>
      }
      <FileUpload {...props} handleAddPicture={handleAddPicture} />
    </Box>
  );
  
};

export default categoryPicture;



