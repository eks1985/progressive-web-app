import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import FormLabel from '@material-ui/core/FormLabel';
import Box from '@material-ui/core/Box';
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Title from './title';
import Picture from './picture';
import Properties from './properties';
import ContentList from './content-list';
import HtmlContent from '../../../../content';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    fontSize: theme.typography.pxToRem(13),
    marginTop: theme.spacing(2),
  },
  head: {
    fontSize: theme.typography.pxToRem(15),
    padding: '6px',
    paddingRight: '50px',
    color: theme.palette.primary.main,
  },
  modelsDetails: {
    padding: 0,
  },
  details: {
    display: 'flex',
    flexDirection: 'column',
  },
  translations: {
    marginTop: theme.spacing(2),
  },
}));

const category = props => {

  const classes = useStyles();

  const { category, edit, selectContent, shiftContent, removeContent } = props;

  // console.log('category', category);

  return (
    <Box className={classes.root}>

      {/* title  */}
      <Accordion square>

        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
          <Typography className={classes.head}>Заголовок</Typography>
        </AccordionSummary>

        <AccordionDetails className={classes.details}>

          <Title category={category} edit={edit} lang='ru' />

          <Accordion square className={classes.translations}>
            <AccordionSummary expandIcon={<ExpandMoreIcon />}>
              <Typography className={classes.head}>Ukranian</Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Title category={category} edit={edit} lang='ua' />
            </AccordionDetails>
          </Accordion>

          <Accordion square>
            <AccordionSummary expandIcon={<ExpandMoreIcon />}>
              <Typography className={classes.head}>English</Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Title category={category} edit={edit} lang='en' />
            </AccordionDetails>
          </Accordion>

          <Accordion square>
            <AccordionSummary expandIcon={<ExpandMoreIcon />}>
              <Typography className={classes.head}>Italian</Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Title category={category} edit={edit} lang='it' />
            </AccordionDetails>
          </Accordion>

        </AccordionDetails>

      </Accordion>

      {/* picture  */}
      <Accordion square>

        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
          <Typography className={classes.head}>Картинка</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Picture category={category} edit={edit} />
        </AccordionDetails>

      </Accordion>

      {/* props  */}
      <Accordion square>

        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
          <Typography className={classes.head}>Свойства</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Properties category={category} edit={edit} />
        </AccordionDetails>

      </Accordion>

      {/* content  */}
      <Accordion square>

        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
          <Typography className={classes.head}>Контент</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Box>
            <Box>
              <FormLabel>
                Выбранный контент
              </FormLabel>
              <ContentList content={category.contentList} shiftContent={shiftContent} removeContent={removeContent} />
            </Box>
            <Box mt={3}>
              <FormLabel>
                Выбор контента
              </FormLabel>
              <HtmlContent selectHandler={selectContent} sMode />
            </Box>
          </Box>
        </AccordionDetails>

      </Accordion>

    </Box>
  );
};

export default category;

