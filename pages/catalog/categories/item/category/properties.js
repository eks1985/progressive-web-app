import React from 'react';
import Switch from '@material-ui/core/Switch';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';

const properties = props => {

  const { category, edit } = props;
  const handleChange = event => {
    edit('ext', event.target.checked);
  };

  return (
    <FormGroup>
      <FormControlLabel
        control={
          <Switch
            checked={category.ext}
            onChange={handleChange}
            value='checkedB'
            color='secondary'
          />
        }
        label='Вспомогательная'
      />
    </FormGroup>
  );
};

export default properties;

