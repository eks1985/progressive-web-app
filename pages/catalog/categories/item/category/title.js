import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles(theme => ({
  textField: {
    marginLeft: '2px',
    marginRight: '2px',
    width: '100%',
  },
}));

const getPropAlias = lang => {
  switch (lang) {
    case 'ru':
      return 'descr';
    case 'ua':
      return 'descr-ua';
    case 'en':
      return 'descr-en';
    case 'it':
      return 'descr-it';
    default:
      return 'descr';
  }
};

const title = props => {
  
  const classes = useStyles();
  const { category, lang, edit } = props;
  const alias = getPropAlias(lang);
  const handleChange = e => {
    edit(alias, e.target.value);
  };
  
  return (
    <TextField
      id='category-title'
      multiline
      rowsMax='2'
      className={classes.textField}
      value={category ? category[alias] : ''}
      onChange={handleChange}
      margin='none'
    />
  );
};

export default title;


