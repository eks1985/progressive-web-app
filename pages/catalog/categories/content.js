import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useRouter } from 'next/router';
import { useStore } from 'lib/store';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Header from './header';
import List from './list';
import Actions from './actions';
import { updateCategories } from 'lib/api/categories';

const useStyles = makeStyles(theme => ({
  btn: {
    minWidth: '200px',
  },
}));

const content = props => {

  const router = useRouter();
  const classes = useStyles();
  const { mutate, url_create } = props;
  
  const [categories, setCategories] = useState(props.data);
  const [rootMod, setRootMod] = useState(false);
  const [updated, toggleUpdated] = useState(false);

  const store = useStore();

  useEffect(() => {
    setCategories(props.data);
  }, [props.data]);

  const updateAll = () => {
    store.setLoading(true);
    updateCategories(url_create, categories);
    setTimeout(() => {
      mutate();
      store.setLoading(false); 
      setRootMod(false);
    }, 500);
  };

  return (
    <Box maxWidth='900px' width='100%' margin='0 auto'>
      <Header />
      {rootMod &&
        <Actions
          updateAll={updateAll}
          setRootMod={setRootMod}
          updated={updated}
          toggleUpdated={toggleUpdated}
        />
      }
      <List
        categories={categories}
        setCategories={setCategories}
        setRootMod={setRootMod}
        mutate={mutate}
      />
      <Box my={3} display='flex' justifyContent='center'>
        <Button
          className={classes.btn}
          variant='outlined'
          onClick={
            () => {
              router.push('/accessory-edit');
            }
          }
        >
          Аксессуары
        </Button>
      </Box>
      <Box my={3} display='flex' justifyContent='center'>
        <Button
          className={classes.btn}
          variant='outlined'
          onClick={
            () => {
              router.push('/content');
            }
          }
        >
          Контент
        </Button>
      </Box>
    </Box>
  );

};

content.defaultProps = {
  url_create: 'https://webhooks.mongodb-realm.com/api/client/v2.0/app/mgd-ilrht/service/http/incoming_webhook/data-create?secret=L0sS3Bz1mdCm6Kuz',
};

export default content;
