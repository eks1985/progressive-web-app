import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  toolbar: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  grow: {
    flexGrow: 1,
    fontSize: '14px',
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
}));

const appBar = () => {

  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar position='static'>
        <Toolbar className={classes.toolbar}>
          <Typography variant='h6' color='inherit' className={classes.grow}>
            Все категории
          </Typography>
        </Toolbar>
      </AppBar>
    </div>
  );

};

export default appBar;
