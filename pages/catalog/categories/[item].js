import React from 'react';
import { useRouter } from 'next/router';
import { baseUrl, secret } from 'lib/base';
import { useStore } from 'lib/store';
import DataProviderBatch from 'lib/data-batch';
import Box from '@material-ui/core/Box';
import Content from './item/content';

const prepareQuery = (props, country, id) => {

  const { url_categories_models, url_category } = props;

  return {
    catalog: `${url_categories_models}&country=${country}&filters=m{c:${id}}`,
    category: `${url_category}&country=${country}&filters=c{id:${id}}`,
  };

};

const category = props => {

  const store = useStore();
  const { country } = store;
  const router = useRouter();

  const query = prepareQuery(props, country, router.query.item);

  return (
    <Box margin='0 auto' width='100%' maxWidth='900px'>
      <DataProviderBatch query={query} render={(data, mutate) => (
        <Content
          {...data.catalog}
          singleCat={data.category.categories}
          categoryId={router.query.item}
          mutate={mutate}
        />
      )}/>
    </Box>
  );

};

category.defaultProps = {
  url_categories_models: `${baseUrl}catalog${secret}lists=c,m&vis=0`,
  url_category: `${baseUrl}catalog${secret}lists=c&vis=0`, 
};

export default category;