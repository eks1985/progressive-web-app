import React from 'react';
import { useStore } from 'lib/store';
import Categories from './categories';

const catalog = () => {

  const store = useStore();
  const { appUser } = store;
  if (appUser === false || !appUser.internal) {
    return null;
  }

  return <Categories />;

};

export default catalog;
