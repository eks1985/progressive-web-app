import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Image from 'next/image';
import Box from '@material-ui/core/Box';
import IconTop from '@material-ui/icons/VerticalAlignTop';
import IconBottom from '@material-ui/icons/VerticalAlignBottom';
import IconDelete from '@material-ui/icons/DeleteForever';
import IconButton from '@material-ui/core/IconButton';
import { moveArrayElem } from 'lib/arrays';
import FileUpload from 'components/cdn-upload';

const useStyles = makeStyles(theme => ({
  icon: {
    color: '#555',
  },
  actions: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
  },
}));

const Picture = props => {

  const classes = useStyles();

  const { url, remove, mTop, mBottom } = props;

  return (
    <Box mb={2}>
      <hr></hr>
      <Box display='flex'>
        <Box
          mt={1}
          margin='0 auto'
          width='100%'
          height={240}
          position='relative'
          onClick={
            () => {
              const link = document.createElement('a');
              document.body.appendChild(link);
              link.href = url;
              link.target = '_blank';
              link.click();
            }
          }
        >
         <Image className='img' src={url} alt='category-picture' layout='fill' objectFit='contain' objectPosition='center center' />
        </Box>
        <div className={classes.actions}>
          <IconButton onClick={mTop}>
            <IconTop className={classes.icon} />
          </IconButton>
          <IconButton onClick={remove}>
            <IconDelete className={classes.icon} />
          </IconButton>
          <IconButton onClick={mBottom}>
            <IconBottom className={classes.icon} />
          </IconButton>
        </div>
      </Box>
    </Box>
  );
};

const Pictures = props => {

  const { pictures, add, remove, mTop, mBottom } = props;

  return (
    <Box width='100%'>
      {pictures.map((item, index) => {
        return (
          <Picture
            key={item}
            url={item}
            remove={remove(index)}
            mTop={mTop(index)}
            mBottom={mBottom(index)}
          />
        );
      })}
      <hr></hr>
      <FileUpload
        handleAddPicture={add}
      />
    </Box>
  );
};

const picturesContainer = props => {

  const [pictures, setPictures] = useState(props.product.pictures || []);
  const { product, edit } = props;

  const pushChanges = newPictures => {
    edit('pictures', newPictures);
  }

  const handleAddPicture = url => {
    const newPictures = [...pictures, url];
    setPictures(newPictures);
    pushChanges(newPictures);
  };

  const handleRemovePicture = index => () => {
    const newPictures = pictures.filter((elem, ind) => ind !== index);
    setPictures(newPictures);
    pushChanges(newPictures);
  };

  const handleMoveTop = index => () => {
    const newPictures = moveArrayElem(pictures, index, -1);
    setPictures(newPictures);
    pushChanges(newPictures);
  };
  
  const handleMoveBottom = (index) => () => {
    const newPictures = moveArrayElem(pictures, index, 1);
    setPictures(newPictures);
    pushChanges(newPictures);
  };

  const actions = {
    add: handleAddPicture,
    remove: handleRemovePicture,
    mTop: handleMoveTop,
    mBottom: handleMoveBottom,
  };
  return (
    <Pictures
      pictures={pictures}
      product={product}
      {...actions}
    />
  );

};

export default picturesContainer;
