import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles(theme => ({
  textField: {
    marginLeft: '2px',
    marginRight: '2px',
    width: '100%',
  },
}));

const getPropAlias = lang => {
  switch (lang) {
    case 'ru':
      return 'title';
    case 'ua':
      return 'title-ua';
    case 'en':
      return 'title-en';
    case 'it':
      return 'title-it';
    default:
      return 'title';
  }
};

const title = props => {
  
  const classes = useStyles();
  const { product, lang, edit } = props;
  const alias = getPropAlias(lang);
  
  const handleChange = e => {
    edit(alias, e.target.value);
  };
  
  return (
    <TextField
      id='product-title'
      multiline
      rowsMax='2'
      className={classes.textField}
      value={product ? product[alias] : ''}
      onChange={handleChange}
      margin='none'
    />
  );
};

export default title;


