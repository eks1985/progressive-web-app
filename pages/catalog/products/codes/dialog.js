import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Slide from '@material-ui/core/Slide';

const useStyles = makeStyles(theme => ({
  dialog: {
    maxWidth: '900px',
    margin: '0 auto',
  },
  appBar: {
    position: 'relative',
  },
  flex: {
    flex: 1,
    lineHeight: '1.3',
  },
  chdlContainer: {
    paddingBottom: '60px',
  },
}));

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction='up' ref={ref} {...props} />;
});

const dialog = props => {

  const classes = useStyles();
  const { open, handleClose, children } = props;

  return (
    <div className='codes-dialog'>
      <Dialog
        className={classes.dialog}
        fullScreen
        open={open}
        onClose={handleClose}
        TransitionComponent={Transition}
      >
        <AppBar className={classes.appBar}>
          <Toolbar>
            <Typography variant='subtitle1' color='inherit' className={classes.flex}>
              Редактирование кодов
            </Typography>
            <Button
              color='inherit'
              onClick={handleClose}>
              Ок
            </Button>
          </Toolbar>
        </AppBar>
        <div className={classes.chdlContainer}>
          {children}
        </div>
      </Dialog>
    </div>
  );

};

export default dialog;

