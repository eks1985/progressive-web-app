import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Typography from '@material-ui/core/Typography';
import Dialog from './dialog';
import AddTable from './add-table';
import CodesTable from './codes-table';
import ComplectationTitle from './compl-title';
import ComplectationRemove from './compl-remove';
import SelectOptionsValues from '../select-options-values';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    maxWidth: '900px',
    margin: '0 auto',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
    color: '#f44336',
    display: 'flex',
    alignItems: 'center',
  },
  itemWrap: {
    cursor: 'pointer',
  },
  panelDetails: {
    padding: '0px',
  },
}));

const CodesGroup = props => {

  const {
    group,
    table,
    data,
    editCodesTableRow,
    removeCodesTableRow,
    addCodesTableRow,
    removeCodesTable,
    moveCodesTable,
    length,
  } = props;

  const classes = useStyles();

  return (
    <Accordion defaultExpanded={table === 0}>
      <AccordionSummary expandIcon={<ExpandMoreIcon />}>
        <Typography className={classes.heading}>{`#${table + 1}`}</Typography>
      </AccordionSummary>
      <AccordionDetails className={classes.panelDetails}>
        <CodesTable
          group={group}
          table={table}
          length={length}
          data={data}
          removeCodesTable={
            () => {
              removeCodesTable({ group, table });
            }
          }
          moveCodesTable={moveCodesTable}
          editCodesTableRow={editCodesTableRow}
          removeCodesTableRow={removeCodesTableRow}
          addCodesTableRow={
            () => {
              addCodesTableRow({ group, table });
            }
          }
        />
      </AccordionDetails>
    </Accordion>
  );

};

const Item = props => {
  
  const {
    withOptions,
    index,
    title,
    item,
    group,
    codesTables,
    options,
    
    handleClickOpen,
    handleClose,

    removeCodesGroup,
    
    editCodesGroup,
    
    addCodesTable,
    moveCodesTable,
    removeCodesTable,

    addCodesTableRow,
    editCodesTableRow,
    removeCodesTableRow,
    
    ...other
  } = props;

  const classes = useStyles();
  
  return (
    <Box width='100%' className={classes.itemWrap}>
      <ComplectationTitle
        withOptions={withOptions}
        handleClickOpen={handleClickOpen}
        handleClose={handleClose}
        title={title}
      />
      <Dialog handleClose={handleClose} {...other}>
        {withOptions &&
          <Box p={3}>
            <SelectOptionsValues
              item={item}
              data={options}
              group={group}
              edit={editCodesGroup}
              {...other}
            />
          </Box>
        }
        {codesTables.map((table, ind) => {
          return (
            <CodesGroup
              key={ind}
              group={group}
              table={ind}
              data={table}
              removeCodesTable={removeCodesTable}
              addCodesTableRow={addCodesTableRow}
              editCodesTableRow={editCodesTableRow}
              removeCodesTableRow={removeCodesTableRow}
              moveCodesTable={moveCodesTable}
              length={codesTables.length}
            />
          );
        })}
        <AddTable group={index} addCodesTable={addCodesTable} />
        {withOptions &&
          <ComplectationRemove
            withOptions={withOptions}
            group={index}
            remove={removeCodesGroup}
            handleClose={handleClose}
          />
        }
      </Dialog>
    </Box>
  );
};

const compl = props => {

  const [open, setOpen] = useState(false);

  const handleClickOpen = () => {
    const { setCurrentOpt, index } = props;
    setCurrentOpt(index);
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <Item
      {...props}
      open={open}
      handleClose={handleClose}
      handleClickOpen={handleClickOpen}
    />
  );

};

export default compl;
