import React from 'react';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';

const remove = props => {

  const { group, remove, handleClose } = props;

  return (
    <Box mt={5} mb={3} display='flex' justifyContent='center'>
      <Button
        variant='outlined'
        color='primary'
        onClick={
          () => {
            remove(group);
            handleClose();
          }
        }
      >
        Удалить комплектацию
      </Button>
    </Box>
  );

};

export default remove;
