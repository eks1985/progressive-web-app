import React from 'react';
import Box from '@material-ui/core/Box';
import Complectation from './compl';
import AddComplectation from './compl-add';
import {
  calcTables,
  handleNewCodesGroup,
  handleEditCodesGroup,
  handleRemoveCodesGroup,
  handleNewCodesTable,
  handleRemoveCodesTable,
  handleMoveCodesTable,
  handleNewCodesTableRow,
  handleEditCodesTableRow,
  handleRemoveCodesTableRow,
} from 'lib/internals/catalog-products-codes';

const getOptionTitle = (option, optionValue, options) => {

  const optionNode = options[option];

  const { alias } = optionNode.values.filter(item => item.value === optionValue)[0].props;

  return alias;
};

const complTitle = (compl, options) => {

  const title = compl.options.map((option, index) => getOptionTitle(option, compl.values[index], options));

  return title;
  
};

const selectCodesTables = (product, currentOpt) => {

  if (!product.compl || currentOpt === -1) {
    return [];
  }
  if (product.opt) {
    // Kirill
    return calcTables(product.compl[currentOpt].materials);
  }
  return calcTables(product.compl);

};

const codes = props => {

  const { product, edit, currentOpt, setCurrentOpt } = props;

  const codesTables = selectCodesTables(product, currentOpt);

  // codes groups

  const addCodesGroup = () => {

    const compl = handleNewCodesGroup(product);
    edit('compl', compl);

  };

  const editCodesGroup = (action) => {

    const { group, payload } = action;
    const compl = handleEditCodesGroup(product, group, payload);
    edit('compl', compl);

  };

  const removeCodesGroup = group => {

    const compl = handleRemoveCodesGroup(product, group);
    setCurrentOpt(-1);
    edit('compl', compl);

  };

  // codes tables

  const addCodesTable = group => {

    const compl = handleNewCodesTable(product, group);
    edit('compl', compl);

  };
  
  const moveCodesTable = action => {

    const { group, table, step } = action;
    const compl = handleMoveCodesTable(product, group, table, step);
    edit('compl', compl);

  };

  const removeCodesTable = action => {

    const { group, table } = action;
    
    const compl = handleRemoveCodesTable(product, group, table);
    edit('compl', compl);

  };

  // codes

  const addCodesTableRow = action => {

    const { group, table } = action;
    const compl = handleNewCodesTableRow(product, group, table);
    edit('compl', compl);

  };

  const editCodesTableRow = action => {

    const { group, table, rowInd, payload } = action;
    const compl = handleEditCodesTableRow(product, group, table, rowInd, payload);
    edit('compl', compl);

  };

  const removeCodesTableRow = action => {

    const { group, table, rowInd } = action;
    const compl = handleRemoveCodesTableRow(product, group, table, rowInd);
    edit('compl', compl);

  };

  const actions = {
    addCodesGroup,
    addCodesTable,
    addCodesTableRow,
    editCodesGroup,
    editCodesTableRow,
    moveCodesTable,
    removeCodesTableRow,
    removeCodesTable,
    removeCodesGroup,
  };

  const showAdd = product.opt || !product.compl;

  return (
    <Box width='100%' position='relative'>
      {
        product.compl && product.opt && product.compl &&
          product.compl.map((item, ind) => {  
            return (
              <Complectation
                key={ind}
                index={ind}
                withOptions
                title={complTitle(item, product.opt)}
                item={item}
                options={product.opt}
                codesTables={codesTables}
                setCurrentOpt={setCurrentOpt}
                group={currentOpt}
                {...actions}
              />
            );
          }
        )
      }
      {product.compl && !product.opt &&
        <Complectation
          compl={product.compl}
          {...actions}
          codesTables={codesTables}
          setCurrentOpt={setCurrentOpt}
          group={currentOpt}
        />
      }
      {showAdd &&
        <AddComplectation {...actions} />
      }
    </Box>
  );
};

export default codes;
