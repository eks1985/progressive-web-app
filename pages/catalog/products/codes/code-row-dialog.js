import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';

const codeRowDialog = props => {

  const { open, handleClose, handleChange, handleDelete, code, qty, rowInd } = props;

  return (
    <div>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby='alert-dialog-title'
        aria-describedby='alert-dialog-description'
      >
        <DialogTitle id='alert-dialog-title'>Код и количество</DialogTitle>
        <DialogContent>
          <FormControl fullWidth aria-describedby='code-value'>
            <InputLabel htmlFor='code-value'>Код</InputLabel>
            <Input
              style={{ fontSize: '20px' }}
              id='сode-value'
              label='Код'
              value={code}
              onChange={handleChange('code')}
              inputProps={{
                style: { textTransform: 'uppercase' },
              }}
            />
          </FormControl>
          <FormControl fullWidth aria-describedby='qty-value' className='mt-3'>
            <InputLabel htmlFor='qty-value'>Количество</InputLabel>
            <Input
              style={{ fontSize: '20px' }}
              id='qty-value'
              label='Количество'
              value={qty}
              onChange={handleChange('qty')}
            />
          </FormControl>
        </DialogContent>
        <DialogActions>
          {rowInd > 0 &&
            <Button onClick={handleDelete} color='primary' variant='outlined'>
              Удалить
            </Button>
          }
          <Button onClick={handleClose} color='primary' variant='contained' autoFocus>
            Ок
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );

};

export default codeRowDialog;

