import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Paper from '@material-ui/core/Paper';
import IconAddCode from '@material-ui/icons/AddCircleOutline';
import IconUp from '@material-ui/icons/ArrowUpward';
import IconDown from '@material-ui/icons/ArrowDownward';
import IconRemoveTable from '@material-ui/icons/DeleteSweep';
import { capit } from 'lib/strings';
import CodeRowDialog from './code-row-dialog';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    overflowX: 'auto',
  },
  table: {
    width: '100%',
  },
}));

const CodesTableContent = props => {

  const { handleClickOpen, addCodesTableRow, removeCodesTable, moveCodesTable, data, group, table, length } = props;
  const classes = useStyles();

  return (
    <Paper className={classes.root} square>
      <Box display='flex' justifyContent='space-between' my={1} ml={1}>
        <Button
          color='primary'
          onClick={
            () => {
              addCodesTableRow({ group, table });
            }  
          }
        >
          <Box display='flex' alignItems='center'>
            <Box mr={1} display='flex' alignItems='center'>
              <IconAddCode/>
            </Box>
            <span>
              Добавить код
            </span>
          </Box>
        </Button>
        {length > 1 &&
          <Box display='flex'>
            <IconButton
              color='primary'
              onClick={
                () => {
                  moveCodesTable({ group, table, step: -1 });
                }
              }
            >
              <IconUp />
            </IconButton>
            <IconButton
              color='primary'
              onClick={
                () => {
                  moveCodesTable({ group, table, step: 1 });
                }
              }
            >
              <IconDown />
            </IconButton>
          </Box>
        }
      </Box>
      <Table className={classes.table}>
        <TableBody>
          {data.map((row, ind) => {
            return (
              <TableRow
                key={`${row.code}-${ind}`}
                onClick={handleClickOpen(row, ind)}
              >
                <TableCell component='th' scope='row'>
                  {row.code}
                </TableCell>
                <TableCell align='right'>{row.qty}</TableCell>
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
      {table > 0 &&
        <Box my={3} ml={1}>
          <Button
            color='primary'
            onClick={
              () => {
                removeCodesTable({ group, table });
              }
            }
          >
            <Box display='flex' alignItems='center'>
              <Box mr={1} display='flex' alignItems='center'>
                <IconRemoveTable />
              </Box>
              <span>
                Удалить таблицу
              </span>
            </Box>
          </Button>
        </Box>
      }
    </Paper>
  );
};

const codesTable = props => {

  const [open, setOpen] = useState(false);
  const [code, setCode] = useState('');
  const [qty, setQty] = useState(1);
  const [rowInd, setRowInd] = useState(-1);

  const handleClickOpen = (row, rowInd) => () => {
    const { code, qty } = row;
    setOpen(true);
    setCode(code);
    setQty(qty);
    setRowInd(rowInd);
  };

  const handleClose = () => {
    const { editCodesTableRow, group, table } = props;
    setOpen(false);
    const payload = { code, qty };
    editCodesTableRow({ group, table, rowInd, payload });
  };

  const handleDelete = () => {
    const { removeCodesTableRow, group, table } = props;
    setOpen(false);
    removeCodesTableRow({ group, table, rowInd });
  };

  const parseString = string => {

    if (string === '') {
      return 0;
    }
    try {
      return parseInt(string, 10);
    } catch (e) {
      return 0;
    }
  };

  const handleChange = prop => e => {

    if (prop === 'qty') {
      const newQty = parseString(e.target.value);
      setQty(newQty);
    }

    if (prop === 'code') {
      setCode(capit(e.target.value));
    }

  };

  return (
    <React.Fragment>
      <CodesTableContent
        key='codes'
        {...props}
        open={open}
        handleClose={handleClose}
        handleClickOpen={handleClickOpen}
      />
      <CodeRowDialog
        key='edit-code'
        rowInd={rowInd}
        open={open}
        code={code}
        qty={qty}
        handleClose={handleClose}
        handleChange={handleChange}
        handleDelete={handleDelete}
        />
    </React.Fragment>
  ) 

}

export default codesTable;
