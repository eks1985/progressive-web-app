import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles(theme => ({
  title: {
    border: '1px solid #ddd',
    borderRadius: '4px',
  }
}));

const Title = props => {

  const classes = useStyles();
  const { title } = props;

  if (!title) {
    return null;
  }

  return (
    <Box p={2} mb={2} className={classes.title}>
      {
        title.map(item => (
          <Typography key={item} variant='body1'>
            {item}
          </Typography>
        ))
      }
      {title.length === 0 &&
        <Typography key='new' variant='body1'>
          Новая комплектация
        </Typography>
      }
    </Box>
  );
};

const complTitle = props => {

  const { withOptions, handleClickOpen, title } = props;

  return (
    <div
      onClick={handleClickOpen}
    >
      {withOptions
        ?
        <Title title={title} />
        :
        <Box display='flex' justifyContent='center'>
          <Button>
            Изменить
          </Button>
        </Box>
      }
    </div>
  );
};

export default complTitle;
