import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    height: '50px',
  },
  textField: {
    marginLeft: '2px',
    marginRight: '2px',
    width: '100%',
  },
}));

const description = props => {

  const classes = useStyles();
  
  const { product, edit } = props;

  const handleChange = e => {
    edit('details', e.target.value );
  };

  return (
    <TextField
      id='setDescr'
      multiline
      className={classes.textField}
      value={product ? product.details : ''}
      onChange={handleChange}
      margin='none'
    />
  );
};

export default description;

