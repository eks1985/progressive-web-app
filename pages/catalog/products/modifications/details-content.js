import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import AddValue from './add-value';
import RemoveValue from './remove-value';
import ValueVisibility from './value-visibility';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    maxWidth: '900px',
    margin: '0 auto',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
    color: theme.palette.primary.main,
  },
}));

const Value = props => {

  const { title, index, value, editMod } = props;

  return (
    <TextField
      fullWidth
      id='standard-name'
      label='Значение'
      margin='dense'
      value={value.value}
      onChange={
        e => {
          editMod({ action: 'editValue', title, index, newValue: e.target.value });
        }
      }
    />
  );

};

const Description = props => {

  const { title, index, value, editMod } = props;

  return (
    <TextField
      fullWidth
      id='standard-name'
      label='Наименование'
      margin='dense'
      value={value.props.alias}
      onChange={
        e => {
          editMod({ action: 'editValueProp', title, index, prop: 'alias', propValue: e.target.value });
        }
      }
    />
  );

};

const IsDefault = props => {

  const { title, index, value, editMod } = props;

  return (
    <FormControlLabel
      control={
        <Switch
          value='checkedB'
          color='secondary'
          checked={value.props.default || false}
          onChange={
            e => {
              if (e.target.checked) {
                editMod({ action: 'editValueProp', title, index, prop: 'default', propValue: e.target.checked });
              }
            }
          }
        />
      }
      label='Опция по-умолчанию'
    />
  );

};

const AddValueWrapper = props => {
  
  const { title, editMod } = props;

  return (
    <Box display='flex' justifyContent='center' mt={3}>
      <AddValue title={title} editMod={editMod} />
    </Box>
  );

};

const OptionValue = props => {

  const { title, value, index, editMod, qty } = props;

  const classes = useStyles();

  return (
    <Accordion square>
      <AccordionSummary expandIcon={<ExpandMoreIcon />}>
        <Typography className={classes.heading}>{`#${index + 1}`}</Typography>
      </AccordionSummary>
      <AccordionDetails>
        <Box display='flex' flexDirection='column' flex='1 0 auto'>
          <Value title={title} index={index} value={value} editMod={editMod} />
          <Description title={title} index={index} value={value} editMod={editMod} />
          <Box mt={2}>
            <IsDefault title={title} index={index} value={value} editMod={editMod} />
          </Box>
          <Box mt={2}>
            <ValueVisibility title={title} value={value} index={index} editMod={editMod} />
          </Box>
          {qty > 2 &&
            <div>
              <RemoveValue title={title} index={index} editMod={editMod} />
            </div>
          }
        </Box>
      </AccordionDetails>
    </Accordion>
  );

};

const List = props => {
  
  const { data: { values }, title, editMod } = props;

  const classes = useStyles();

  return (
    <div>
      {values.map((value, index) => {
        return <OptionValue key={index} editMod={editMod} title={title} index={index} classes={classes} value={value} qty={values.length} />
      })}
    </div>
  );

} 

const detailsContent = props => {
  return [
    <List key='values-list' {...props} />,
    <AddValueWrapper key='values-add' {...props} />,
  ];
};

export default detailsContent;
