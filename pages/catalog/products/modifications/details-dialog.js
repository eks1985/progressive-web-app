import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Slide from '@material-ui/core/Slide';

const useStyles = makeStyles(theme => ({
  appBar: {
    position: 'relative',
  },
  flex: {
    flex: 1,
    lineHeight: '1.3',
  },
  dialog: {
    maxWidth: '900px',
    margin: '0 auto',
    paddingBottom: '90px',
  },
}));

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const detailsDialog = props => {

  const classes = useStyles();

  const { open, handleClose, children, title } = props; 

  return (
    <div>
      <Dialog
        fullScreen
        className={classes.dialog}
        open={open}
        onClose={handleClose}
        TransitionComponent={Transition}
      >
        <AppBar className={classes.appBar}>
          <Toolbar>
            <Typography variant='subtitle1' color='inherit' className={classes.flex}>
              {title}
            </Typography>
            <Button color='inherit' onClick={handleClose}>
              ОК
            </Button>
          </Toolbar>
        </AppBar>
        {children}
      </Dialog>
    </div>
  );

};
export default detailsDialog;



