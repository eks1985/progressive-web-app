import React from 'react';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';

const removeValue = props => {

  const { editMod, title, index } = props;

  return (
    <Box mt={2}>
      <Button
        color='primary'
        variant='outlined'
        onClick={
          () => {
            editMod({ action: 'removeValue', title, index });
          }
        }
      >
        Удалить
      </Button>
    </Box>
  );

};

export default removeValue;
