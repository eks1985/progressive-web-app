import React from 'react';
import { orderBy } from 'lodash';
import Box from '@material-ui/core/Box';
import Modificator from './modificator';
import Add from './add';
import { prepareNewMod } from 'lib/internals/catalog-products-modifications';

const modifications = props => {

  const { product, product: { opt }, edit } = props;

  const handleModAdd = () => {
    const newOpt = prepareNewMod(opt);
    const compl = false;
    const prices = false;
    edit('opt', newOpt);
    edit('compl', compl);
    edit('prices', prices);
  };

  if (!opt) {
    return <Add addMod={handleModAdd} />;
  }

  const list = Object.keys(opt).map(key => ({ ...opt[key], title: key }));

  return (
    <Box display='flex' flexDirection='column' position='relative' flex='1 0 auto'>
      {
        orderBy(list, ['order'], ['asc']).map(item => {
          return <Modificator key={item.title} edit={edit} title={item.title} product={product} data={opt[item.title]} />;
        })
      }
      <Add addMod={handleModAdd} />
    </Box>
  );
};

export default modifications;
