import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AddIcon from '@material-ui/icons/Add';
import Fab from '@material-ui/core/Fab';

const useStyles = makeStyles(theme => ({
  root: {
    textAlign: 'center',
    margin: '0 auto',
    width: '100%',
    marginTop: theme.spacing(2),
  },
}));

const add = props => {

  const { addMod } = props;
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Fab
        aria-label='create'
        size='small'
        onClick={addMod}
      >
        <AddIcon />
      </Fab>
    </div>
  );

};

export default add;
