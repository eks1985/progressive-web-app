import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Radio from '@material-ui/core/Radio';
import FormLabel from '@material-ui/core/FormLabel';

const useStyles = makeStyles(theme => ({
  root: {
    minHeight: '120px',
  },
  radio: {
    height: '40px',
  },
}));

const visibility = props => {

  const classes = useStyles();
  const { title, data: { vis }, editMod } = props;

  return (
    <Box display='flex' flexDirection='column' className={classes.root}>
      <div>
        <Radio
          color='secondary'
          className={classes.radio}
          checked={vis === 2}
          onChange={
            () => {
              editMod({ action: 'editModVis', title, value: 2 });
            }
          }
          value='0'
          name='vis'
          aria-label='0'
        />
        <FormLabel>Для менеджера и клиента</FormLabel>
      </div>
      <div>
        <Radio
          color='secondary'
          className={classes.radio}
          checked={vis === 1}
          onChange={
            () => {
              editMod({ action: 'editModVis', title, value: 1 });
            }
          }
          value='1'
          name='vis'
          aria-label='1'
        />
        <FormLabel>Для менеджера</FormLabel>
      </div>
      <div>
        <Radio
          color='secondary'
          className={classes.radio}
          checked={vis === 0}
          onChange={
            () => {
              editMod({ action: 'editModVis', title, value: 0 });
            }
          }
          value='2'
          name='vis'
          aria-label='2'
        />
        <FormLabel>Не отображать</FormLabel>
      </div>
    </Box>
  );

};

export default visibility;


