import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AddIcon from '@material-ui/icons/Add';
import Fab from '@material-ui/core/Fab';

const useStyles = makeStyles(theme => ({
  root: {
    position: 'fixed',
    top: 40,
    right: 3,
    zIndex: '9999',
  },
}));

const addValue = props => {

  const { editMod, title } = props;
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Fab
        aria-label='create'
        size='small'
        onClick={
          () => {
            editMod({ action: 'addValue', title });
          }
        }
      >
        <AddIcon />
      </Fab>
    </div>
  );

};

export default addValue;
