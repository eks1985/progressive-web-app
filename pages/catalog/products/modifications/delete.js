import React from 'react';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';

const deleteComp = props => {

  const { removeMod, title } = props;
  
  return (
    <Box display='flex' justifyContent='center' my={3} alignItems='flex-end' flex='1 0 auto'>
      <Button
        onClick={
          () => {
            removeMod(title);
          }
        }
      >
        Удалить опцию
      </Button>
    </Box>
  );
};

export default deleteComp;

