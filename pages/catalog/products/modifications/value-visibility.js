import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Radio from '@material-ui/core/Radio';
import FormLabel from '@material-ui/core/FormLabel';

const useStyles = makeStyles(theme => ({
  radio: {
    height: '40px',
  },
}));

const visibility = props => {

  const { title, index, value, editMod } = props;
  const classes = useStyles();

  return (
    <Box display='flex' flexDirection='column'>
      <div>
        <Radio
          color='secondary'
          className={classes.radio}
          checked={value.props.vis === 2}
          onChange={
            () => {
              editMod({ action: 'editValueProp', title, index, prop: 'vis', propValue: 2 });
            }
          }
          value='0'
          name='vis'
          aria-label='0'
        />
        <FormLabel>Для менеджера и клиента</FormLabel>
      </div>
      <div>
        <Radio
          color='secondary'
          className={classes.radio}
          checked={value.props.vis === 1}
          onChange={
            () => {
              editMod({ action: 'editValueProp', title, index, prop: 'vis', propValue: 1 });
            }
          }
          value='1'
          name='vis'
          aria-label='1'
        />
        <FormLabel>Для менеджера</FormLabel>
      </div>
      <div>
        <Radio
          color='secondary'
          className={classes.radio}
          checked={value.props.vis === 0}
          onChange={
            () => {
              editMod({ action: 'editValueProp', title, index, prop: 'vis', propValue: 0 });
            }
          }
          value='2'
          name='vis'
          aria-label='2'
        />
        <FormLabel>Не отображать</FormLabel>
      </div>
    </Box>
  );

};

export default visibility;



