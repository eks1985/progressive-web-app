import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import IconVisibility from '@material-ui/icons/Visibility';
import IconDone from '@material-ui/icons/Done';
import DetailsDialog from './details-dialog';
import DetailsContent from './details-content';
import Delete from './delete';
import OptionVisibility from './option-visibility';
import { editMod, removeMod, updateComplWithOpt } from 'lib/internals/catalog-products-modifications';

const useStyles = makeStyles(theme => ({
  dense: {
    marginTop: 16,
  },
}));

const Modificator = props => {

  const { title, mod, handleClickOpen, handleTitleChange, handleModEdit, handleModRemove, handleTitleSave, ...other } = props;

  const classes = useStyles();
  
  return (
    <Box display='flex' alignItems='center'>
      <TextField
        fullWidth
        id='mod'
        label='Опция'
        className={classes.dense}
        margin='dense'
        variant='outlined'
        value={title}
        onChange={
          e => {
            handleTitleChange(e.target.value);
          }
        }
      />
      {mod &&
        <IconButton
          className={classes.button}
          aria-label='save'
          color='secondary'
          onClick={handleTitleSave}
        >
          <IconDone />
        </IconButton>
      }
      {!mod &&
        <IconButton
          className={classes.button}
          aria-label='visibility'
          color='secondary'
          onClick={handleClickOpen}
        >
          <IconVisibility />
        </IconButton>
      }
      <DetailsDialog {...other} title={title}>
        <DetailsContent {...other} title={title} editMod={handleModEdit} />
        <OptionVisibility title={title} data={props.data} editMod={handleModEdit} />
        <Delete title={title} removeMod={handleModRemove} />
      </DetailsDialog>
    </Box>
  );
};

const modificatorContainer = props => {

  const { edit, product } = props;

  const [open, setOpen] = useState(false);
  const [title, setTitle] = useState(props.title);
  const [mod, setMod] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleTitleChange = title => {
    setTitle(title);
    setMod(true);
  };

  const handleModEdit = action => {
    const opt = editMod(product.opt, action);
    const compl = updateComplWithOpt(product, action);
    const prices= false;
    edit('opt', opt);
    edit('compl', compl);
    edit('prices', prices);
    setMod(false);
  };

  const handleModRemove = modKey => {
    const opt = removeMod(product.opt, modKey);
    const compl = false;
    const prices= false;
    edit('opt', opt);
    edit('compl', compl);
    edit('prices', prices);
    setMod(false);
  };

  const handleTitleSave = () => {
    const payload = { action: 'title', oldTitle: props.title, newTitle: title };
    const opt = editMod(product.opt, payload);
    const compl = updateComplWithOpt(product, payload);
    const prices= false;
    edit('opt', opt);
    edit('compl', compl);
    edit('prices', prices);
    setMod(false);
  };

  return (
    <Modificator
      {...props}
      open={open}
      title={title}
      mod={mod}
      handleClose={handleClose}
      handleClickOpen={handleClickOpen}
      handleTitleChange={handleTitleChange}
      handleTitleSave={handleTitleSave}
      handleModEdit={handleModEdit}
      handleModRemove={handleModRemove}
    />
  );

};

export default modificatorContainer;


