import React, { useState, useEffect } from 'react';
import { useStore } from 'lib/store';
import { makeStyles } from '@material-ui/core/styles';
import { useRouter } from 'next/router';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import { shifObjArrayElem } from 'lib/arrays';
import { updateProductCall } from 'lib/api/products';
import Header from './header';
import Actions from './actions';
import Product from './product';

const useStyles = makeStyles(theme => ({
  btn: {
    minWidth: '200px',
  },
}));

const product = props => {

  const store = useStore();
  const router = useRouter();
  const classes = useStyles();
  const { mutate, url_create } = props;

  const [product, setProduct] = useState(props.product.length > 0 ? props.product[0] : false);
  const [productMod, setProductMod] = useState(false);
  const [updated, toggleUpdated] = useState(false);
  const [currentOpt, setCurrentOpt] = useState(-1);
  const [currentOptPrices, setCurrentOptPrices] = useState(-1);
  
  useEffect(() => {
    const newProduct = props.product.length > 0 ? props.product[0] : false;
    setProduct(newProduct);
  }, [props]);

  const updateProduct = () => {
    setCurrentOpt(-1);
    setCurrentOptPrices(-1);
    const { contentList, ...other } = product;
    updateProductCall(url_create, other, mutate, store.setLoading, setProductMod);
  };

  const edit = (prop, value) => {

    setProduct(product => {
      return { ...product, [prop]: value };
    });

    setProductMod(true);

  };

  const selectContent = selected => {
    const newContentList = product.contentList.concat(selected);
    edit('contentList', newContentList);
    edit('content', newContentList.map(item => item.id));
  }

  const shiftContent = (ind, step) => {
    console.log('shiftContent');
    const newContentList = shifObjArrayElem(product.contentList, ind, step);
    edit('contentList', newContentList);
    edit('content', newContentList.map(item => item.id));
  };

  const removeContent = ind => {
    console.log('removeContent');
    const newContentList = product.contentList.filter((item, curInd) => curInd !== ind);
    edit('contentList', newContentList);
    edit('content', newContentList.map(item => item.id));
  };

  if (!product) {
    return null;
  }

  return (
    <Box maxWidth='900px' width='100%' margin='0 auto'>
      <Header
        mutate={mutate}
        product={product}
        modelId={router.query.model}
      />
      {productMod &&
        <Actions
          updateProduct={updateProduct}
          updated={updated}
          toggleUpdated={toggleUpdated}
          setProductMod={setProductMod}
        />
      }
      <Product
        product={product}
        edit={edit}
        currentOpt={currentOpt}
        setCurrentOpt={setCurrentOpt}
        currentOptPrices={currentOptPrices}
        setCurrentOptPrices={setCurrentOptPrices}
        selectContent={selectContent}
        shiftContent={shiftContent}
        removeContent={removeContent}
      />
      <Box my={3} display='flex' justifyContent='center'>
        <Button
          className={classes.btn}
          variant='outlined'
          onClick={
            () => {
              router.push('/catalog/accessories');
            }
          }
        >
          Аксессуары
        </Button>
      </Box>
      <Box my={3} display='flex' justifyContent='center'>
        <Button
          className={classes.btn}
          variant='outlined'
          onClick={
            () => {
              router.push('/content');
            }
          }
        >
          Контент
        </Button>
      </Box>
    </Box>
  );

};

product.defaultProps = {
  url_create: 'https://webhooks.mongodb-realm.com/api/client/v2.0/app/mgd-ilrht/service/http/incoming_webhook/data-create?secret=L0sS3Bz1mdCm6Kuz',
};

export default product;
