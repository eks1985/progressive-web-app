import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Slide from '@material-ui/core/Slide';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

const useStyles = makeStyles(theme => ({
  appBar: {
    position: 'relative',
  },
  flex: {
    flex: 1,
    lineHeight: '1.3',
  },
  dialog: {
    maxWidth: '900px',
    margin: '0 auto',
  },
}));

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction='up' ref={ref} {...props} />;
});

const dialog = props => {

  const { open, handleClose, handleSave, children, title } = props;

  const classes = useStyles();
  
  return (
    <div>
      <Dialog
        fullScreen
        className={classes.dialog}
        open={open}
        TransitionComponent={Transition}
      >
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton color='inherit' onClick={handleClose} aria-label='Close'>
              <CloseIcon />
            </IconButton>
            <Typography variant='h6' color='inherit' className={classes.flex}>
              Редактирование
            </Typography>
            <Typography variant='subtitle1' color='inherit' className={classes.flex}>
              {title}
            </Typography>
            <Button color='inherit' onClick={handleSave}>
              ОК
            </Button>
          </Toolbar>
        </AppBar>
        {children}
      </Dialog>
    </div>
  );

};

export default dialog;

