import React, { useState } from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import Dialog from './dialog';
import DialogContent from './dialog-content';

const Item = props => {

  const {
    ind,
    title,
    id,
    gdRef,
    
    open, handleOpen, handleClose,

    editOffer,
    removeOffer,
    handleChange,
    handleSave,
  } = props;

  return (
    [
      <ListItem
        key='list-item'
        button
        onClick={handleOpen}
      >
        <ListItemText
          primary={title}
        />
        <ListItemSecondaryAction>
          <IconButton
            aria-label='Delete'
            onClick={
              () => {
                removeOffer(ind);
                handleClose();
              }
            }
          >
            <DeleteIcon />
          </IconButton>
        </ListItemSecondaryAction>
      </ListItem>,
      <Dialog
        key='dialog'
        open={open}
        title={title}
        handleClose={handleClose}
        handleSave={handleSave}
      >
        <DialogContent
          ind={ind}
          title={title}
          id={id}
          gdRef={gdRef}
          editOffer={editOffer}
          handleChange={handleChange}
        />
      </Dialog>,
    ]
  );

};

const ItemContainer = props => {

  const [open, setOpen] = useState(false);
  const [title, setTitle] = useState(props.item.title);
  const [id, setId] = useState(props.item.id);
  const [gdRef, setGdRef] = useState(props.item.gdRef);

  const handleOpen = () => {
    setOpen(true);
  }
  
  const handleClose = () => {
    setOpen(false);
  }
  
  const handleChange = prop => e => {
    if (prop === 'title') {
      setTitle(e.target.value);
    }
    if (prop === 'id') {
      setId(e.target.value);
    }
    if (prop === 'gdRef') {
      setGdRef(e.target.value);
    }
  }
  
  const handleSave = () => {
    const { editOffer, ind } = props;
    setOpen(false);
    editOffer({ ind, prop: 'title', value: title });
    editOffer({ ind, prop: 'id', value: id });
    editOffer({ ind, prop: 'gdRef', value: gdRef });
  }

  return (
    <Item
      {...props}
      open={open}
      title={title}
      id={id}
      gdRef={gdRef}
      handleOpen={handleOpen}
      handleChange={handleChange}
      handleClose={handleClose}
      handleSave={handleSave}
    />
  );

};

const list = props => {

  const { offer, removeOffer, editOffer } = props;

  return (
    <div>
      <List>
        {
          offer.map((item, ind) => (
            <ItemContainer key={`${item.title}${ind}`} item={item} ind={ind} editOffer={editOffer} removeOffer={removeOffer} />
          ))
        }
      </List>
    </div>
  );
};

export default list;
