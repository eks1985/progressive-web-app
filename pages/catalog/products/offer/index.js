import React from 'react';
import Box from '@material-ui/core/Box';
import List from './list';
import Add from './add';

const offer = props => {

  const { product, edit } = props;

  const addOffer = () => {
    const offerShape = { title: 'Новое предложение', id: '', gdRef };
    const updated = product.offer ? product.offer.concat(offerShape) : [offerShape];
    edit('offer', updated);
  };

  const removeOffer = ind => {
    edit('offer', product.offer.filter((item, i) => i !== ind));
  };

  const editOffer = action => {
    const { ind, prop, value } = action;
    const updated = product.offer.map((item, i) => {
      if (i === ind) {
        const updatedItem = { ...item, [prop]: value };
        return updatedItem;
      }
      return item;
    });
    edit('offer', updated);
  };

  return (
    <Box display='flex' flexDirection='column' position='relative' flex='1 0 auto'>
      {product.offer &&
        <List
          offer={product.offer}
          editOffer={editOffer}
          removeOffer={removeOffer}
        />
      }
      <Add
        addOffer={addOffer}
      />
    </Box>
  );
};

export default offer;


