import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    maxWidth: '900px',
    margin: '0 auto',
  },
}));

const dialogContent = props => {

  const classes = useStyles();

  const { handleChange, title, id, gdRef } = props;

  return (
    <div className={classes.root}>
      <Box p={2}>
        <TextField
          fullWidth
          id='standard-title'
          label='Заголовок'
          margin='dense'
          value={title || ''}
          onChange={handleChange('title')}
        />
      </Box>
      <Box p={2}>
        <TextField
          fullWidth
          id='standard-id'
          label='Имя файла'
          margin='dense'
          value={id || ''}
          onChange={handleChange('id')}
        />
      </Box>
      <Box p={2}>
        <TextField
          fullWidth
          id='standard-gdref'
          label='Google drive ссылка'
          margin='dense'
          value={gdRef || ''}
          onChange={handleChange('gdRef')}
        />
      </Box>
    </div>
  );
};

export default dialogContent;


