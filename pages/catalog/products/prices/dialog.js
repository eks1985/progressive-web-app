import React from 'react';
import Box from '@material-ui/core/Box';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Slide from '@material-ui/core/Slide';

const useStyles = makeStyles(theme => ({
  appBar: {
    position: 'relative',
  },
  flex: {
    flex: 1,
    lineHeight: '1.3',
  },
  dialog: {
    maxWidth: '900px',
    margin: '0 auto',
  },
}));

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction='up' ref={ref} {...props} />;
});

const dialog = props => {

  const { open, handleClose, children } = props;

  const classes = useStyles();
  
  return (
    <Box className='codes-dialog'>
      <Dialog
        className={classes.dialog}
        fullScreen
        open={open}
        onClose={handleClose}
        TransitionComponent={Transition}
      >
        <AppBar className={classes.appBar}>
          <Toolbar>
            <Typography variant='subtitle1' color='inherit' className={classes.flex}>
              Редактирование цен
            </Typography>
            <Button color='inherit' onClick={handleClose}>
              Ок
            </Button>
          </Toolbar>
        </AppBar>
        {children}
      </Dialog>
    </Box>
  );

};

export default dialog; 

