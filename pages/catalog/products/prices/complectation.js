import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import SelectOptionsValues from '../select-options-values';
import Dialog from './dialog';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    maxWidth: '900px',
    margin: '0 auto',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
    color: theme.palette.primary.main,
    display: 'flex',
    alignItems: 'center',
  },
  itemContainer: {
    width: '100%',
    cursor: 'pointer',
  },
}));

const Remove = props => {

  const { group, remove, handleClose } = props;

  return (
    <Box mt={5} mb={3} display='flex' justifyContent='center'>
      <Button
        variant='outlined'
        color='primary'
        onClick={
          () => {
            remove(group);
            handleClose();
          }
        }
      >
        Удалить комплектацию
      </Button>
    </Box>    
  );

};

const PricesGroup = props => {

  const { group, priceDealer, priceFarmer, edit } = props;

  return (
    <Box p={3}>
      <FormControl fullWidth aria-describedby='farmer-value'>
        <InputLabel htmlFor='farmer-value'>Цена для клиента</InputLabel>
        <Input
          id='farmer-value'
          label='Цена для клиента'
          value={priceFarmer}
          onChange={
            e => {
              edit({ group, payload: { priceFarmer: e.target.value }});
            }
          }
        />
      </FormControl>
      <FormControl fullWidth aria-describedby='dealer-value'>
        <InputLabel htmlFor='dealer-value'>Цена для дилера</InputLabel>
        <Input
          id='dealer-value'
          label='Цена для дилера'
          value={priceDealer}
          onChange={
            e => {
              edit({ group, payload: { priceDealer: e.target.value }});
            }
          }
        />
      </FormControl>
    </Box>
  );
}

const Title = props => {

  const { title } = props;
  
  return (
    <Box p={2} mb={2} border='1px solid #ddd' borderRadius='4px'>
      {
        title.map(item => (
          <Typography key={item} variant='body1'>
            {item}
          </Typography>
        ))
      }
      {title.length === 0 &&
        <Typography key='new' variant='body1'>
          Новая комплектация
        </Typography>
      }
    </Box>
  );

};

const Item = props => {
  
  const {
    index,
    title,
    item,
    group,
    options,
    
    handleClickOpen,
    handleClose, 
    
    editPricesGroup,
    removePricesGroup,

    priceFarmer,
    priceDealer,
    
    ...other
  } = props;

  const classes = useStyles();

  return (
    <Box className={classes.itemContainer}>
      <div
        onClick={handleClickOpen}
      >
        <Title title={title} />
      </div>
      <Dialog handleClose={handleClose} {...other}>
        <Box p={3}>
          <SelectOptionsValues
            item={item}
            data={options}
            group={group}
            edit={editPricesGroup}
            {...other}
          />
        </Box>
        <PricesGroup
          group={group}
          editPricesGroup={editPricesGroup}
          priceFarmer={priceFarmer}
          priceDealer={priceDealer}
          edit={editPricesGroup}
        />
        <Remove
          group={group}
          remove={removePricesGroup}
          handleClose={handleClose}
        />
      </Dialog>
    </Box>
  );
};

const itemContainer = props => {

  const [open, setOpen] = useState(false);

  const handleClickOpen = () => {

    const { setCurrentOpt, index } = props;

    setCurrentOpt(index);
    
    setOpen(true);
    
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <Item
      {...props}
      open={open}
      setOpen={setOpen}
      handleClose={handleClose}
      handleClickOpen={handleClickOpen}
    />
  );

}

export default itemContainer;
