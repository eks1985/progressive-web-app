import React from 'react';
import Box from '@material-ui/core/Box';
import Complectation from './complectation';
import AddComplectation from './add-complectation';
import InlinePrices from './inline-prices';
import { handleNewPricesGroup, handleEditPricesGroup, handleRemovePricesGroup } from 'lib/internals/catalog-products-prices';

const getOptionTitle = (option, optionValue, options) => {

  const optionNode = options[option];
  const { alias } = optionNode.values.filter(item => item.value === optionValue)[0].props;
  return alias;

};

const pricelTitle = (item, options) => {

  const title = item.options.map((option, index) => getOptionTitle(option, item.values[index], options));
  return title;

};

const prices = props => {

  const { product, edit, currentOpt, setCurrentOpt } = props;

  // codes groups

  const addPricesGroup = () => {

    const prices = handleNewPricesGroup(product);
    edit('prices', prices);

  };

  const editPricesGroup = action => {

    const { group, payload } = action;
    const prices = handleEditPricesGroup(product, group, payload);
    edit('prices', prices);

  };

  const removePricesGroup = group => {

    const prices = handleRemovePricesGroup(product, group);
    setCurrentOpt(-1);
    edit('prices', prices);

  };

  const actions = {
    addPricesGroup,
    editPricesGroup,
    removePricesGroup,
  };

  return (
    <Box width='100%' position='relative'>
      {
        product.prices && product.opt && product.prices.farmer.map((item, ind) => {
          const priceFarmer = item.price;
          const priceDealer = product.prices.dealer[ind].price;
          return (
            <Complectation
              key={ind}
              variant={0}
              index={ind}
              title={pricelTitle(item, product.opt)}
              item={item}
              group={currentOpt}
              options={product.opt}
              priceFarmer={priceFarmer}
              priceDealer={priceDealer}
              currentOpt={currentOpt}
              setCurrentOpt={setCurrentOpt}
              {...actions}
            />
          );
        })
      }
      {!product.opt &&
        <InlinePrices
          product={product}
          {...actions}
        />
      }
      <AddComplectation {...actions} />
    </Box>
  );
};

export default prices;
