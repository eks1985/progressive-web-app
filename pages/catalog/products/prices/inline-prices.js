import React from 'react';
import Box from '@material-ui/core/Box';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';

const inlinePrices = props => {

  const { product, edit } = props;

  const farmer = product.prices ? product.prices.farmer : '';
  const dealer = product.prices ? product.prices.dealer : '';
  const cost   = product.prices ? product.prices.cost : '';

  return (
    <Box p={3}>
      <FormControl fullWidth aria-describedby='farmer-value'>
        <InputLabel htmlFor='farmer-value'>Цена для клиента</InputLabel>
        <Input
          id='farmer-value'
          label='Цена для клиента'
          value={farmer}
          onChange={
            e => {
              const prices = product.prices || {};
              const updatedPrices = { ...prices, farmer: e.target.value };
              edit({ ...product, prices: updatedPrices });
            }
          }
        />
      </FormControl>
      <FormControl fullWidth aria-describedby='dealer-value'>
        <InputLabel htmlFor='dealer-value'>Цена для дилера</InputLabel>
        <Input
          id='dealer-value'
          label='Цена для дилера'
          value={dealer}
          onChange={
            e => {
              const prices = product.prices || {};
              const updatedPrices = { ...prices, dealer: e.target.value };
              edit({ ...product, prices: updatedPrices });
            }
          }
          inputProps={{
            style: { textTransform: 'uppercase' },
          }}
        />
      </FormControl>
      <FormControl fullWidth aria-describedby='cost-value'>
        <InputLabel htmlFor='cost-value'>Цена входящая</InputLabel>
        <Input
          id='cost-value'
          label='Цена входящая'
          value={cost}
          onChange={
            e => {
              const prices = product.prices || {};
              const updatedPrices = { ...prices, cost: e.target.value };
              edit({ ...product, prices: updatedPrices });
            }
          }
          inputProps={{
            style: { textTransform: 'uppercase' },
          }}
        />
      </FormControl>
    </Box>
  );
};

export default inlinePrices;
