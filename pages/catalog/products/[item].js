import React from 'react';
import { useRouter } from 'next/router';
import { baseUrl, secret } from 'lib/base';
import { useStore } from 'lib/store';
import DataProviderBatch from 'lib/data-batch';
import Box from '@material-ui/core/Box';
import Content from './content';

const prepareQuery = (props, country, id) => {

  const { url_product } = props;

  return {
    catalogProduct: `${url_product}&country=${country}&filters=p{id:${id}}`,
  };

};

const product = props => {

  const store = useStore();
  const { country } = store;
  const router = useRouter();

  if (!router.query.item) {
    return null;
  }

  const query = prepareQuery(props, country, router.query.item);

  return (
    <Box margin='0 auto' width='100%' maxWidth='900px'>
      <DataProviderBatch query={query} render={(data, mutate) => (
        <Content
          productId={router.query.item}
          product={data.catalogProduct.products}
          mutate={mutate}
        />
      )}/>
    </Box>
  );

};

product.defaultProps = {
  url_product: `${baseUrl}catalog${secret}lists=p&vis=0`,
};

export default product;