import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles(theme => ({
  textField: {
    marginLeft: '2px',
    marginRight: '2px',
    marginTop: '4px',
    width: '100%',
  },
}));

const medialib = props => {

  const classes = useStyles();
  const { product, edit } = props;

  const handleChangePictures = e => {
    edit('mpict', e.target.value);
  };

  const handleChangeVideos = e => {
    edit('mvid', e.target.value);
  };

  const handleChangeOffers = e => {
    edit('moffers', e.target.value);
  };

  const handleChangeLeaflets = e => {
    edit('mleafl', e.target.value);
  };

  return (
    <Box width='100%'>
      <TextField
        id='mPict'
        label='Фото'
        multiline
        rowsMax='2'
        className={classes.textField}
        value={product.mpict || ''}
        onChange={handleChangePictures}
        margin='none'
      />
      <TextField
        id='mVid'
        label='Видео'
        multiline
        rowsMax='2'
        className={classes.textField}
        value={product.mvid || ''}
        onChange={handleChangeVideos}
        margin='none'
      />
      <TextField
        id='mOffers'
        label='Коммерческие предложения'
        multiline
        rowsMax='2'
        className={classes.textField}
        value={product.moffers || ''}
        onChange={handleChangeOffers}
        margin='none'
      />
      <TextField
        id='mLeaflets'
        label='Каталоги'
        multiline
        rowsMax='2'
        className={classes.textField}
        value={product.mleafl || ''}
        onChange={handleChangeLeaflets}
        margin='none'
      />
    </Box>
  );
};

export default medialib;

