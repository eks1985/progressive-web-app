import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles(theme => ({
  textField: {
    marginLeft: '2px',
    marginRight: '2px',
    width: '100%',
  },
}));

const video = props => {

  const { product, edit } = props;
  
  const handleChange = e => {
    edit({ ...product, video: e.target.value });
  };

  const classes = useStyles();
  
  return (
    <TextField
      id='setTitle'
      multiline
      rowsMax='2'
      className={classes.textField}
      value={set ? set.video : ''}
      onChange={handleChange}
      margin='none'
    />
  );
};

export default video;

