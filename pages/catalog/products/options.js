import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import TextField from '@material-ui/core/TextField';
import Chip from '@material-ui/core/Chip';
import IconAdd from '@material-ui/icons/AddCircle';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    flexWrap: 'wrap',
    padding: theme.spacing(1) / 2,
  },
  colorPrimary: {
    background: '#aaa !important',
    color: 'white',
  },
  deleteIconColorPrimary: {
    color: '#aaa',
  },
  chip: {
    margin: theme.spacing(1) / 2,
  },
  textField: {
    marginLeft: '2px',
    marginRight: '2px',
    width: '100%',
    marginTop: '8px',
  },
}));

const prepareChipData = props => {
  const { product } = props;
  const add = { key: -999, label: 'добавить' };
  if (!product || !product.properties) {
    return [add];
  }
  return product.properties.map((prop, i) => ({ key: i, label: prop })).concat(add);
};

const EditDescr = props => {

  const classes = useStyles();
  const { handleEditDescr, chipData, active } = props;
  
  const handleChange = e => {
    handleEditDescr(e.target.value);
  };

  return (
    <TextField
      id='optionTitle'
      autoFocus
      multiline
      className={classes.textField}
      value={chipData[active].label}
      onChange={handleChange}
      margin='none'
    />
  );
};

const options = props => {

  const classes = useStyles();

  const [chipData, setChipData] = useState(prepareChipData(props));
  const [active, setActive] = useState(-1);
  const [mod, setMod] = useState(false);

  const handleDelete = data => () => {
    chipDataNew = chipData.filter((item, ind) => ind !== data);
    chipDataNew.forEach((item, ind) => {
      if (item.key !== -999) {
        item.key = ind;
      }
    });
    setChipData(chipDataNew);
    setMod(true);
  };

  const handleSetActive = active => () => {
    setActive(active);
    setMod(true);
  };

  const handleEditDescr = descr => {
    const newChipData = [...chipData];
    newChipData[active].label = descr;
    setChipData(newChipData);
    setMod(true);
  };

  const handleAddOption = () => {
    const { length } = chipData;
    const last = chipData.pop();
    const newChipData = chipData.concat({ key: length - 1, label: 'опция' });
    newChipData.push(last);
    setChipData(newChipData);
    setMod(true);
    setActive(length - 1);
  };

  const handleResetState = () => {
    setChipData(prepareChipData(props));
    setMod(false);
    setActive(-1);
  }

  const handleSaveChanges = () => {
    const { edit, product } = props;
    const properties = chipData.reduce((res, item) => item.key === -999 ? res : res.concat(item.label), []);
    edit('properties', properties);
    setMod(false);
    setActive(-1);
  }

  return (
    <div>
      <div>
        {chipData.map(data => {
          if (data.label === 'добавить') {
            return (
              <Chip
                variant='outlined'
                deleteIcon={<IconAdd />}
                key={data.key}
                label={data.label}
                onDelete={handleAddOption}
                className={classes.chip}
                onClick={handleAddOption}
              />
            );
          }
          return (
            <Chip
              color={data.key === active ? 'primary' : 'default'}
              key={data.key}
              label={data.label.slice(0, 32)}
              onDelete={handleDelete(data)}
              onClick={handleSetActive(data.key)}
              classes={{
                root: classes.chip,
                colorPrimary: classes.colorPrimary,
                deleteIconColorPrimary: classes.deleteIconColorPrimary,
              }}
            />
          );
        })}
      </div>
      {active >= 0 &&
        <EditDescr active={active} chipData={chipData} mod={mod} handleEditDescr={handleEditDescr} />
      }
      {mod &&
        <Box mt={3} display='flex' justifyContent='center'>
          <Button
            onClick={handleSaveChanges}
            color='primary'
            variant='contained'
          >
            ОК
          </Button>
          <Button
            onClick={handleResetState}
          >
            Отменить
          </Button>
        </Box>
      }
    </div>
  );

};

export default options;





