import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Select from 'react-select';

const useStyles = makeStyles(theme => ({
  groupStyles: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  groupBadgeStyles: {
    backgroundColor: '#EBECF0',
    borderRadius: '2em',
    color: '#172B4D',
    display: 'inline-block',
    fontSize: 12,
    fontWeight: 'normal',
    lineHeight: '1',
    minWidth: 1,
    padding: '0.16666666666667em 0.5em',
    textAlign: 'center',
  },
  select: {
    zIndex: 9999,
  },
}));

const prepareGroupedOptions = data => {

  const rootKeys = Object.keys(data);
  
  const options = rootKeys.map(rootKey => {
    return { label: rootKey, options: data[rootKey].values.map(item => ({ value: `${rootKey}@@@${item.value}`, label: item.props.alias })) };
  });

  return options;

};

const getPropLabel = (data, option, value) => {

  return data[option].values.filter(item => item.value === value)[0].props.alias;

};

const calcCurrentValue = (data, item) => {

  return item.options.map((option, index) => {
    return {
      value: `${option}@@@${item.values[index]}`,
      label: getPropLabel(data, option, item.values[index])
    };
  });

};

const selectOptionsValues = props => {

  const { data, group, edit, item } = props;

  const groupedOptions = prepareGroupedOptions(data);

  const value = calcCurrentValue(data, item);

  const classes = useStyles();


  return (
    <Select
      className={classes.select}
      fullWidth
      isMulti
      value={value}
      options={groupedOptions}
      onChange={
        selected => {
          const options = [];
          const values = [];
          selected.forEach(row => {
            const parts = row.value.split('@@@');
            options.push(parts[0]);
            values.push(parts[1]);
          });
          const payload = { options, values };
          edit({group, payload});
        }
      }
    />
  );
};

export default selectOptionsValues;
