import React, { useState } from 'react';
import { useRouter } from 'next/router';
import { makeStyles } from '@material-ui/core/styles';
import DataProviderBatch from 'lib/data-batch';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import IconMoreVert from '@material-ui/icons/MoreVert';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import { baseUrl, secret } from 'lib/base';
import ProductSelect from './product-select';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  toolbar: {
    paddingRight: '0px',
    display: 'flex',
    justifyContent: 'space-between',
  },
}));

const AppBarMenu = props => {

  const { modelId, anchorEl, open, handleMenu, handleClose } = props;
  const router = useRouter();

  if (!modelId) {
    return null;
  }

  return (
    <div>
      <IconButton
        aria-owns={open ? 'menu-appbar' : null}
        aria-haspopup='true'
        onClick={handleMenu}
        color='inherit'
      >
        <IconMoreVert />
      </IconButton>
      <Menu
        id='menu-appbar'
        anchorEl={anchorEl}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        open={open}
        onClose={handleClose}
      >
        <MenuItem
          onClick={
            () => {
              handleClose();
              router.push(`/catalog/models/${modelId}`);
            }
          }
        >
          Все машины
        </MenuItem>
      </Menu>
    </div>
  );

};

const menuAppBar = props => {

  const { product, modelId } = props;

  const classes = useStyles();

  const [anchorEl, setAnchorEl] = useState();

  const handleMenu = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);

  const prepareQuery = () => {

    const { url_products } = props;
  
    return {
      catalogProduct: `${url_products}&filters=p{p:${product.id}}`, //get siblings
    };
  
  };

  const query = prepareQuery();

  return (
    <div className={classes.root}>
      <AppBar position='static'>
        <Toolbar className={classes.toolbar}>
          <DataProviderBatch query={query} render={data => (
            <ProductSelect
              text={`Машина: ${product.title}`}
              products={data.catalogProduct.products}
            />
          )}/>
          <AppBarMenu
            modelId={modelId}
            anchorEl={anchorEl}
            open={open}
            handleMenu={handleMenu}
            handleClose={handleClose}
          />
        </Toolbar>
      </AppBar>
    </div>
  );

};

menuAppBar.defaultProps = { 
  products: [],
  url_products: `${baseUrl}catalog${secret}lists=p`, 
};

export default menuAppBar;
