import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Select from 'components/select';

const useStyles = makeStyles(theme => ({
  textField: {
    marginLeft: '2px',
    marginRight: '2px',
    marginTop: '4px',
    width: '100%',
  },
  container: {
    width: '100%',
  },
}));

const LangInput = props => {

  const classes = useStyles();

  const { lang, text, editText } = props;

  return (
    <TextField
      id={`word-${lang}`}
      label={lang}
      multiline
      rowsMax='2'
      className={classes.textField}
      value={text}
      onChange={
        e => {
          editText(lang, e.target.value);
        }
      }
      margin='none'
    />
  );
};

const calcSetVacab = product => {

  const { complectation } = product;
  
  let res = [];
  if (complectation !== undefined) {
    res.push(...complectation);
  }
  res = res.map(item => {
    return { value: item, label: item };
  });
  return res;
};

const local = props => {
  
  const classes = useStyles();

  const { product } = props;
  
  const editText = (lang, text) => {
    console.log('lang', lang);
    console.log('text', text);
  };
  
  const vacab = calcSetVacab(set);
  
  return (
    <div className={classes.container}>
      <Select options={vacab} />
      <LangInput lang='ru' editText={editText} />
      <LangInput lang='ua' editText={editText} />
      <LangInput lang='en' editText={editText} />
      <LangInput lang='it' editText={editText} />
    </div>
  );
};

export default local;



