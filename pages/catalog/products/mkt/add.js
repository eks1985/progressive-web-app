import React from 'react';
import Box from '@material-ui/core/Box';
import AddIcon from '@material-ui/icons/Add';
import Fab from '@material-ui/core/Fab';

const addMkt = props => {

  const { addMkt } = props;

  return (
    <Box width='100%' margin='0 auto' textAlign='center' mt={2}>
      <Fab
        aria-label='create'
        size='small'
        onClick={addMkt}
      >
        <AddIcon />
      </Fab>
    </Box>
  );

};

export default addMkt;

