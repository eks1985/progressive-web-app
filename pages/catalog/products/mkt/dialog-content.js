import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    maxWidth: '900px',
    margin: '0 auto',
  },
}));

const dialogContent = props => {

  const classes = useStyles();

  const { handleChange, title, path } = props;

  return (
    <div className={classes.root}>
      <Box p={2}>
        <TextField
          fullWidth
          id='standard-title'
          label='Заголовок'
          margin='dense'
          value={title || ''}
          onChange={handleChange('title')}
        />
      </Box>
      <Box p={2}>
        <TextField
          fullWidth
          id='standard-path'
          label='Ссылка'
          margin='dense'
          value={path || ''}
          onChange={handleChange('path')}
        />
      </Box>
    </div>
  );
};

export default dialogContent;


