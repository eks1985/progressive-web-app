import React from 'react';
import Box from '@material-ui/core/Box';
import List from './list';
import Add from './add';

const mkt = props => {

  const { product, edit } = props;

  const addMkt = () => {
    const mktShape = { title: 'Новый материал', path: '' };
    const updated = product.mkt ? product.mkt.concat(mktShape) : [mktShape];
    edit('mkt', updated);
  };

  const removeMkt = ind => {
    edit('mkt', product.mkt.filter((item, i) => i !== ind));
  };

  const editMkt = action => {
    // console.log('editMkt', action);
    const { ind, prop, value } = action;
    const updated = product.mkt.map((item, i) => {
      if (i === ind) {
        const updatedItem = { ...item, [prop]: value };
        return updatedItem;
      }
      return item;
    });
    edit('mkt', updated);
  };

  return (
    <Box display='flex' flexDirection='column' position='relative' flex='1 0 auto'>
      {product.mkt &&
        <List
          mkt={product.mkt}
          editMkt={editMkt}
          removeMkt={removeMkt}
        />
      }
      <Add
        addMkt={addMkt}
      />
    </Box>
  );
};

export default mkt;


