import React, { useState } from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import Dialog from './dialog';
import DialogContent from './dialog-content';

const Item = props => {

  const {
    ind,
    title,
    path,
    
    open, handleOpen, handleClose,

    editMkt,
    removeMkt,
    handleChange,
    handleSave,
  } = props;

  // console.log('hc', handleChange);

  return (
    [
      <ListItem
        key='list-item'
        button
        onClick={handleOpen}
      >
        <ListItemText
          primary={title}
        />
        <ListItemSecondaryAction>
          <IconButton
            aria-label='Delete'
            onClick={
              () => {
                removeMkt(ind);
                handleClose();
              }
            }
          >
            <DeleteIcon />
          </IconButton>
        </ListItemSecondaryAction>
      </ListItem>,
      <Dialog
        key='dialog'
        open={open}
        title={title}
        handleClose={handleClose}
        handleSave={handleSave}
      >
        <DialogContent
          ind={ind}
          title={title}
          path={path}
          editMkt={editMkt}
          handleChange={handleChange}
        />
      </Dialog>,
    ]
  );

};

const ItemContainer = props => {

  const [open, setOpen] = useState(false);
  const [title, setTitle] = useState(props.item.title);
  const [path, setPath] = useState(props.item.path);

  const handleOpen = () => {
    setOpen(true);
  }
  
  const handleClose = () => {
    setOpen(false);
  }
  
  const handleChange = prop => e => {
    if (prop === 'title') {
      setTitle(e.target.value);
    }
    if (prop === 'path') {
      setPath(e.target.value);
    }
  }
  
  const handleSave = () => {
    const { editMkt, ind } = props;
    setOpen(false);
    editMkt({ ind, prop: 'title', value: title });
    editMkt({ ind, prop: 'path', value: path });
  }

  return (
    <Item
      {...props}
      open={open}
      title={title}
      path={path}
      handleOpen={handleOpen}
      handleChange={handleChange}
      handleClose={handleClose}
      handleSave={handleSave}
    />
  );

};

const list = props => {

  const { mkt, removeMkt, editMkt } = props;

  return (
    <div>
      <List>
        {
          mkt.map((item, ind) => (
            <ItemContainer key={`${item.title}${ind}`} item={item} ind={ind} editMkt={editMkt} removeMkt={removeMkt} />
          ))
        }
      </List>
    </div>
  );
};

export default list;
