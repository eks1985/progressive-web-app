import React from 'react';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';

const actions = props => {

  const { updateProduct, setProductMod } = props;

  return (
    <Box margin='0 auto' width='100%' display='flex' justifyContent='center' mt={2}>
      <Box>
        <Button onClick={updateProduct} variant='contained' color='primary'>
          Сохранить
        </Button>
      </Box>
      <Box ml={2}>
        <Button
          onClick={setProductMod.bind(null, false)}>
          Отмена
        </Button>    
      </Box>
    </Box>
  );

};

export default actions;
