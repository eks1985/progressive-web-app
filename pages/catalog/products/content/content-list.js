import React, { useState } from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import ItemDialog from './dialog';
import ContentData from '../../../content/item';

const ContentItem = props => {

  const { item, title, ind, removeContent, shiftContent, setShowContent } = props;

  return (
    <ListItem
      onClick={
        () => {
          setShowContent(item);
        }
      }
    >
      <ListItemText
        primary={title}
      />
      <ListItemSecondaryAction>
        <IconButton
          aria-label='shift-forward'
          onClick={
            () => {
              shiftContent(ind, 1);
            }
          }
        >
          <ArrowDownwardIcon />
        </IconButton>
        <IconButton
          aria-label='shift-back'
          onClick={
            () => {
              shiftContent(ind, -1);
            }
          }
        >
          <ArrowUpwardIcon />
        </IconButton>
        <IconButton
          aria-label='shift-back'
          onClick={
            () => {
              removeContent(ind);
            }
          }
        >
          <DeleteIcon />
        </IconButton>
      </ListItemSecondaryAction>
    </ListItem>
  );

};

const contentList = props => {

  const { content, ...other } = props;
  const [showContent, setShowContent] = useState(false);
  
  return (
    <div>
      <ItemDialog content={showContent} setOpen={setShowContent}>
        <ContentData inline item={showContent} />
      </ItemDialog>
      <List>
        {
          content.map((item, ind) => (
            <ContentItem
              key={item.id}
              title={item.tt}
              item={item}
              ind={ind}
              setShowContent={setShowContent}
              {...other}
            />
          ))
        }
      </List>
    </div>
  );
};

export default contentList;
