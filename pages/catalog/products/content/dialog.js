import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Slide from '@material-ui/core/Slide';

const useStyles = makeStyles(theme => ({
  appBar: {
    position: 'relative',
  },
  toolBar: {
    display: 'flex',
    justifyContent: 'space-between',
  },
}));

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const itemEditDialog = props => {

  const classes = useStyles();

  const { setOpen, content } = props;

  const title = content ? content.tt : '';

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <Box display='flex' justifyContent='flex-end' mt={3}>
      <Dialog
        fullScreen
        open={open}
        onClose={handleClose}
        open={Boolean(content)}
        TransitionComponent={Transition}
      >
        <AppBar className={classes.appBar}>
          <Toolbar className={classes.toolBar}>
            <Typography variant='subtitle1' color='inherit'>
              {title}
            </Typography>
            <Button color='inherit' onClick={handleClose}>
              Ok
            </Button>
          </Toolbar>
        </AppBar>
        <Paper square>
          <Box p={2}>
            {props.children}
          </Box>
        </Paper>
      </Dialog>
    </Box>
  );

};

export default itemEditDialog;


