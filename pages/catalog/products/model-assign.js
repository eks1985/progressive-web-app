import React, { useEffect, useState } from 'react';
import { baseUrl, secret } from 'lib/base';
import { makeStyles } from '@material-ui/core/styles';
import DataProviderBatch from 'lib/data-batch';
import Loading from 'components/loading/indicator'
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Input from '@material-ui/core/Input';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Chip from '@material-ui/core/Chip';
import { updateModelsCall } from 'lib/api/products';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    flex: '1 0 auto',
  },
  select: {
    width: '100%',
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 200,
    width: '100%',
  },
  chips: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  chip: {
    margin: theme.spacing(1) / 4,
  },
}));

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;

const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

const ModelAssign = props => {

  const { belongsToModels , categoryModels, product, mutate, url_create } = props;
  const [current, setCurrent] = useState(belongsToModels.map(model => model.descr));
  const [mod, setMod] = useState(false);
  const [toUpdate, setToUpdate] = useState(false);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setCurrent(props.belongsToModels.map(model => model.descr))
  }, [belongsToModels]);

  const handleChange = event => {

    setCurrent(event.target.value);

    const selectedIds = event.target.value.map(descr => categoryModels.find(model => model.descr === descr).id);

    const toUpdate = [];

    categoryModels.forEach(model => {
      
      if (selectedIds.includes(model.id)) {
        const modelSets = model.sets || [];
        if (!modelSets.includes(product.id)){
          toUpdate.push({ ...model, sets: modelSets.concat(product.id), mod: true });
        }
      } else {
        const modelSets = model.sets || [];
        if (modelSets.includes(product.id)){
          toUpdate.push({ ...model, sets: modelSets.filter(id => id !== product.id), mod: true });
        }
      }

    });

    setMod(true);
    setToUpdate(toUpdate);

  };

  const classes = useStyles();

  return (
    <div className={classes.root}>
      <FormControl fullWidth className={classes.formControl}>
        <Select
          className={classes.select}
          fullWidth
          variant='filled'
          // multiple={product.isA}
          multiple
          value={current}
          onChange={handleChange}
          input={<Input id='select-multiple-chip' />}
          renderValue={selected => {
            return (
              <div className={classes.chips}>
                {selected.map(value => (
                  <Chip key={value} label={value} className={classes.chip} />
                ))}
              </div>
            );
          }}
          MenuProps={MenuProps}
        >
          {categoryModels && categoryModels.map(model => (
            <MenuItem
              key={model.id}
              value={model.descr}
            >
              {model.descr}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
      <Box mt={2}>
        {loading &&
          <Loading />
        }
        {mod &&
          <Button
            disabled={loading}
            color='primary'
            variant='contained'
            onClick={
              () => {
                updateModelsCall(url_create, toUpdate, mutate, setLoading, setMod);
              }
            }
          >
            Сохранить
          </Button>
        }
      </Box>
    </div>
  );

};

const prepareQuery = (props, categoryRef, productId) => {

  const { url_category_models, url_set_belongs_to_models } = props;

  return {
    categoryModels: `${url_category_models}&filters=m{c:${categoryRef}}`,
    setBelongsToModels: `${url_set_belongs_to_models}&filters=m{p:${productId}}`,
  };

};

const modelAssignContainer = props => {

  const { product } = props;
  const query = prepareQuery(props, product.categoryRef, product.id);

  return (
    <DataProviderBatch query={query} render={(data, mutate) => (
      <ModelAssign
        product={product}
        categoryModels={data.categoryModels.models}
        belongsToModels={data.setBelongsToModels.models}
        mutate={mutate}
      />
    )}/>
  );

};

modelAssignContainer.defaultProps = { 
  url_category_models: `${baseUrl}catalog${secret}lists=m`, 
  url_set_belongs_to_models: `${baseUrl}catalog${secret}lists=m`, 
};

ModelAssign.defaultProps = {
  url_create: 'https://webhooks.mongodb-realm.com/api/client/v2.0/app/mgd-ilrht/service/http/incoming_webhook/data-create?secret=L0sS3Bz1mdCm6Kuz',
};

export default modelAssignContainer;



