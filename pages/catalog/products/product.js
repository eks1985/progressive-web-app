import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import FormLabel from '@material-ui/core/FormLabel';
import Title from './title';
import Description from './description';
import IsAccessory from './is-accessory';
import Assign from './model-assign';
import Options from './options';
import Standard from './standard';
import TechData from './tech-data';
import Pictures from './pictures';
import Medialib from './medialib';
import Modifications from './modifications';
import Codes from './codes';
import Prices from './prices';
import Mkt from './mkt';
import Offer from './offer';
import ContentList from './content/content-list';
import HtmlContent from '../../content';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    fontSize: theme.typography.pxToRem(13),
    marginTop: theme.spacing(2),
  },
  head: {
    fontSize: theme.typography.pxToRem(15),
    padding: '6px',
    paddingRight: '50px',
    color: theme.palette.primary.main,
  },
  details: {
    display: 'flex',
    flexDirection: 'column',
  },
  translations: {
    marginTop: theme.spacing(2),
  },
}));

const product = props => {

  const classes = useStyles();

  const {
      product,
      currentOpt, setCurrentOpt,
      currentOptPrices, setCurrentOptPrices,
      edit,
      selectContent,
      shiftContent,
      removeContent
  } = props;

  // console.log('product', product);

  return (
    <Box className={classes.root}>

      {/* Заголовок  */}
      <Accordion square>

        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
          <Typography className={classes.head}>Заголовок</Typography>
        </AccordionSummary>

        <AccordionDetails className={classes.details}>

          <Title product={product} edit={edit} lang='ru' />

          <Accordion className={classes.translations}>
            <AccordionSummary expandIcon={<ExpandMoreIcon />}>
              <Typography className={classes.head}>Ukranian</Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Title product={product} edit={edit} lang='ua' />
            </AccordionDetails>
          </Accordion>

          <Accordion>
            <AccordionSummary expandIcon={<ExpandMoreIcon />}>
              <Typography className={classes.head}>English</Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Title product={product} edit={edit} lang='en' />
            </AccordionDetails>
          </Accordion>

          <Accordion>
            <AccordionSummary expandIcon={<ExpandMoreIcon />}>
              <Typography className={classes.head}>Italian</Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Title product={product} edit={edit} lang='it' />
            </AccordionDetails>
          </Accordion>

        </AccordionDetails>

      </Accordion>

      {/* Описание */}
      <Accordion square>
        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
          <Typography className={classes.head}>Описание</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Description product={product} edit={edit} />
        </AccordionDetails>
      </Accordion>

      {/* Аксессуар */}
      <Accordion square>
        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
          <Typography className={classes.head}>Аксессуар</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <IsAccessory product={product} edit={edit} />
        </AccordionDetails>
      </Accordion>

      {/* Модели */}
      <Accordion square>
        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
          <Typography className={classes.head}>Модели</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Assign product={product} edit={edit} />
        </AccordionDetails>
      </Accordion>

      {/* Опции */}
      <Accordion square>
        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
          <Typography className={classes.head}>Опции</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Options product={product} edit={edit} />
        </AccordionDetails>
      </Accordion>    

      {/* Стандартная комплектация */}
      <Accordion square>
        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
          <Typography className={classes.head}>Стандартная комплектация</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Standard product={product} edit={edit} />
        </AccordionDetails>
      </Accordion>    

      {/* Тех характеристики */}
      <Accordion square>
        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
          <Typography className={classes.head}>Тех характеристики</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <TechData product={product} edit={edit} />
        </AccordionDetails>
      </Accordion>    

      {/* Картинки */}
      <Accordion square>
        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
          <Typography className={classes.head}>Картинки</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Pictures product={product} edit={edit} />
        </AccordionDetails>
      </Accordion> 

      {/* Медиа */}
      <Accordion square>
        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
          <Typography className={classes.head}>Медиа</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Medialib product={product} edit={edit} />
        </AccordionDetails>
      </Accordion>

      {/* Модификации */}
      <Accordion square>
        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
          <Typography className={classes.head}>Модификации</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Modifications product={product} edit={edit} />
        </AccordionDetails>
      </Accordion> 

      {/* Коды */}
      <Accordion square>
        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
          <Typography className={classes.head}>Коды</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Codes
            product={product}
            currentOpt={currentOpt}
            setCurrentOpt={setCurrentOpt}
            edit={edit}
          />
        </AccordionDetails>
      </Accordion>

      {/* Цены */}
      <Accordion square>
        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
          <Typography className={classes.head}>Цены</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Prices
            product={product}
            currentOpt={currentOptPrices}
            setCurrentOpt={setCurrentOptPrices}
            edit={edit}
          />
        </AccordionDetails>
      </Accordion>   

      {/* content  */}
      <Accordion square>

        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
          <Typography className={classes.head}>Контент</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Box>
            <Box>
              <FormLabel>
                Выбранный контент
              </FormLabel>
              <ContentList
                content={product.contentList}
                shiftContent={shiftContent}
                removeContent={removeContent}
              />
            </Box>
            <Box mt={3}>
              <FormLabel>
                Выбор контента
              </FormLabel>
              <HtmlContent
                selectHandler={selectContent}
                sMode
              />
            </Box>
          </Box>
        </AccordionDetails>

      </Accordion> 

      {/* Маркетинг */}
      <Accordion square>
        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
          <Typography className={classes.head}>Маркетинг</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Mkt product={product} edit={edit}  />
        </AccordionDetails>
      </Accordion>

      {/* Предложение клиенту */}
      <Accordion square>
        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
          <Typography className={classes.head}>Предложение клиенту</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Offer product={product} edit={edit}  />
        </AccordionDetails>
      </Accordion>

    </Box>
  );
};

export default product;

