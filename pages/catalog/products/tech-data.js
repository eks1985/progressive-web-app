import React, { useState } from 'react';
import Box from '@material-ui/core/Box';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import TechDataTable from './tech-data-table';

const useStyles = makeStyles(theme => ({
  textField: {
    marginLeft: '2px',
    marginRight: '2px',
    width: '100%',
  },
  dialogContent: {
    padding: 0,
  }
}));

const TechData = props => {

  const classes = useStyles();

  const { product, edit, handleClickOpen, handleClose, open } = props;

  const handleChange = e => {
    edit('tech', e.target.value);
  };

  return (
    <Box width='100%'>
      <TextField
        fullWidth
        key='data'
        id='setTechData'
        multiline
        className={classes.textField}
        value={product ? product.tech : ''}
        onChange={handleChange}
        margin='none'
      />
      {product.tech &&
        <Box mt={2}>
          <Button
            color='primary'
            variant='outlined'
            onClick={handleClickOpen}
          >
            Показать
          </Button>
        </Box>
      }
      <Dialog
        fullScreen
        open={open}
        onClose={handleClose}
        aria-labelledby='responsive-dialog-title'
      >
        <DialogTitle id='responsive-dialog-title'>Технические характеристики</DialogTitle>
        <DialogActions>
          <Button onClick={handleClose} color='primary' autoFocus>
            Закрыть
          </Button>
        </DialogActions>
        <DialogContent className={classes.dialogContent}>
          <TechDataTable tech={product.tech} />
        </DialogContent>
      </Dialog>
    </Box>
  );
};

const techDataContainer = props => {

  const [open, setOpen] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <TechData
      {...props}
      open={open}
      handleClickOpen={handleClickOpen}
      handleClose={handleClose}
    />
  );

};

export default techDataContainer;
