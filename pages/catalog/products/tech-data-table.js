import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles(theme => ({
  root: {
    overflowX: 'auto',
  },
  textField: {
    marginLeft: '2px',
    marginRight: '2px',
    width: '100%',
  },
  cell: {
    textAlign: 'center',
    padding: '4px 12px',
    minWidth: '50px',
  },
}));

const prepareTable = tech => {
  const res = {
    header: [],
    body: [],
  };
  try {
    const struct = tech.split('===');
    const hRows = struct[0].split('---');
    const bRows = struct[1].split('---');
    hRows.forEach(row => {
      res.header.push([]);
      const rowCols = row.split('\n');
      rowCols.forEach(rowCol => {
        const colParts = rowCol.split('>');
        if (colParts[0]) {
          const colPropParts = colParts[1].split(':');
          res.header[res.header.length - 1].push({ text: colParts[0], rowSpan: colPropParts[0], colSpan: colPropParts[1] });
        }
      });
    });
    bRows.forEach(row => {
      res.body.push([]);
      const rowCols = row.split('\n');
      rowCols.forEach(rowCol => {
        const colParts = rowCol.split('>');
        if (colParts[0]) {
          const colPropParts = colParts[1].split(':');
          res.body[res.body.length - 1].push({ text: colParts[0], rowSpan: colPropParts[0], colSpan: colPropParts[1] });
        }
      });
    });
  } catch (e) {
    console.log(e);
  }
  return res;
};

const techDataTable = props => {

  const { tech } = props;
  const table = prepareTable(tech);
  const classes = useStyles();

  return (
    <Paper square className={classes.root} elevation={0}>
      <Table>
        <TableHead>
          {
            table.header.map((row, index) => {
              return (
                <TableRow key={`th${index}`}>
                  {
                    row.map((col, ind) => {
                      return (
                        <TableCell
                          padding='none'
                          key={`${col.text}-${ind}`}
                          rowSpan={col.rowSpan} 
                          colSpan={col.colSpan} 
                          className={classes.cell}
                        >
                          {col.text}
                        </TableCell>
                      );
                    })
                  }
                </TableRow>
              );
            })
          }
        </TableHead>
        <TableBody>
          {
            table.body.map((row, index) => {
              return (
                <TableRow key={`th${index}`}>
                  {
                    row.map((col, ind) => {
                      return (
                        <TableCell
                          padding='none'
                          key={`${col.text}-${ind}`}
                          rowSpan={col.rowSpan}
                          colSpan={col.colSpan} 
                          className={classes.cell}
                        >
                          {col.text}
                        </TableCell>
                      );
                    })
                  }
                </TableRow>
              );
            })
          }
        </TableBody>
      </Table>
    </Paper>
  );

};

export default techDataTable;


