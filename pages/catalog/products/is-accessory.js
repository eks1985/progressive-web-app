import React from 'react';
import Switch from '@material-ui/core/Switch';

const isAccessory = props => {

  const { product, edit } = props;

  const handleChange = event => {
    edit('isA', event.target.checked);
  };

  return (
    <div>
      <Switch
        checked={product.isA}
        onChange={handleChange}
        value='checkedB'
        color='secondary'
      />
    </div>
  );
}

export default isAccessory;
