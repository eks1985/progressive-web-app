import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useRouter } from 'next/router';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    maxWidth: 360,
  },
  title: {
    fontSize: '14px',
    color: '#fff',
  },
  menuItem: {
    fontSize: '14px',
    padding: '6px 12px',
  },
}));

const  Title = props => {

  const { text } = props; 

  const classes = useStyles();

  return (
    <Typography variant='h6' color='inherit' className={classes.title}>
      {text}
    </Typography>
  );
}

const modelSelect = props => {

  const { models, text } = props;

  const router = useRouter();

  const [anchorEl, setAnchorEl] = useState(null);

  const classes = useStyles();

  const handleClickListItem = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleMenuItemClick = (event, index, models) => {
    setAnchorEl(null);
    router.push(`/catalog/models/${models[index].id}`);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div className={classes.root}>
      <List component='nav'>
        <ListItem
          button
          aria-haspopup='true'
          aria-controls='lock-menu'
          aria-label='When device is locked'
          onClick={handleClickListItem}
        >
          <ListItemText
            primary={<Title text={text} />}
          />
        </ListItem>
      </List>
      <Menu
        id='lock-menu'
        anchorEl={anchorEl}
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        {
          models && models.map((model, index) => (
            <MenuItem
              className={classes.menuItem}
              key={index}
              onClick={event => handleMenuItemClick(event, index, models)}
            >
              {model.descr}
            </MenuItem>
          ))
        }
      </Menu>
    </div>
  );

};

export default modelSelect;


