import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import Create from './create-product';
import Item from './product';
import Actions from './actions-products';

const useStyles = makeStyles(theme => ({
  list: {
    width: '100%',
  },
}));

const list = props => {

  const classes = useStyles();
  const {
    modelId,
    products,
    productsMod,
    createProduct,
    editProduct,
    shiftProduct,
    updateProducts,
    setProductsMod
  } = props;

  return [
    <Actions
      key='actions'
      productsMod={productsMod}
      updateProducts={updateProducts}
      setProductsMod={setProductsMod}
    />,
    <List key='list' className={classes.list}>
      {
        products.map((product, index) => {
          return (
            <Item
              key={index}
              index={index}
              modelId={modelId}
              models={products}
              product={product}
              products={products}
              productsMod={productsMod}
              editProduct={editProduct}
              shiftProduct={shiftProduct}
              updateProducts={updateProducts}
            />
          );
        })
      }
    </List>,
    <Create
      key='create'
      createProduct={createProduct}
    />,
  ];

};

list.defaultProps = {
  products: [],
};

export default list;


