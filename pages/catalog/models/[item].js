import React from 'react';
import { useRouter } from 'next/router';
import { baseUrl, secret } from 'lib/base';
import { useStore } from 'lib/store';
import DataProviderBatch from 'lib/data-batch';
import Box from '@material-ui/core/Box';
import Content from './content';

const prepareQuery = (props, country, id) => {

  const { url_model, url_products, url_categories } = props;

  return {
    catalogModel: `${url_model}&country=${country}&filters=m{id:${id}}`,
    catalogProducts: `${url_products}&country=${country}&filters=p{m:${id}}`,
    catalogCategories: `${url_categories}&country=${country}`,
  };

};

const model = props => {

  const store = useStore();
  const { country } = store;
  const router = useRouter();

  if (!router.query.item) {
    return null;
  }

  const query = prepareQuery(props, country, router.query.item);

  return (
    <Box margin='0 auto' width='100%' maxWidth='900px'>
      <DataProviderBatch query={query} render={(data, mutate) => (
        <Content
          modelId={router.query.item}
          models={data.catalogModel.models}
          products={data.catalogProducts.products}
          categories={data.catalogCategories.categories}
          mutate={mutate}
        />
      )}/>
    </Box>
  );

};

model.defaultProps = {
  url_model: `${baseUrl}catalog${secret}lists=m&vis=0`, 
  url_products: `${baseUrl}catalog${secret}lists=p&vis=0`,
  url_categories: `${baseUrl}catalog${secret}lists=c&vis=0`, 
};

export default model;