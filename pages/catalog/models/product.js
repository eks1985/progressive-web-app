import React from 'react';
import Image from 'next/image'
import { useRouter } from 'next/router';
import { makeStyles } from '@material-ui/core/styles';
import classNames from 'classnames';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import IconVis1 from '@material-ui/icons/Done';
import IconVis2 from '@material-ui/icons/DoneAll';
import IconVis0 from '@material-ui/icons/NotInterested';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import CatalogItemMenu from 'components/catalog-item-menu';

const useStyles = makeStyles(theme => ({
  list: {
    width: '100%',
  },
  listItem: {
    borderTop: '1px solid #eee',
  },
  listItemText: {
    fontSize: '14px',
    paddingRight: '20px',
  },
  gray: {
    color: '#999',
  },
  icon: {
    color: theme.palette.secondary.main,
  },
}));

const item = props => {

  const classes = useStyles();
  const router = useRouter();
  const { modelId, products, product, productsMod, index, editProduct, shiftProduct, updateProducts } = props;

  return (
    <ListItem
      className={classes.listItem}
      button
      onClick={
        () => {
          router.push(`/catalog/products/${product.id}?model=${modelId}`);
        }
      }
    >
      {product.vis === 1 &&
        <ListItemIcon className={classes.icon}>
          <IconVis1 />
        </ListItemIcon>
      }
      {product.vis === 2 &&
        <ListItemIcon className={classes.icon}>
          <IconVis2 />
        </ListItemIcon>
      }
      {(product.vis === 0 || (!product.vis)) &&
        <ListItemIcon className={classes.icon}>
          <IconVis0 />
        </ListItemIcon>
      }
      <Box mr={2}>
        <Box width={60} height={50} position='relative'>
          {product.pictures && product.pictures.length > 0 &&
            <Image className='img' src={product.pictures[0]} alt='category-picture' layout='fill' objectFit='contain' objectPosition='center center' />
          }
        </Box>
      </Box>
      {product.mod && !productsMod && // if all products mod then show separete Save btn on top of list
        <Box mr={2}>
          <Button
            variant='outlined'
            size='small'
            color='primary'
            onClick={
              e => {
                e.preventDefault();
                e.stopPropagation();
                updateProducts();
              }
            }
          >
            Сохранить
          </Button>
        </Box>
      }
      <ListItemText
        disableTypography
        className={classNames(classes.listItemText, { [classes.gray]: !product.vis })}
        primary={<div>{product.title}</div>}
      />
      <ListItemSecondaryAction>
        <CatalogItemMenu
          index={index}
          data={products}
          shiftData={shiftProduct}
          updateData={editProduct}
        />
      </ListItemSecondaryAction>
    </ListItem>
  );

};

item.defaultProps = {
  url_create: 'https://webhooks.mongodb-realm.com/api/client/v2.0/app/mgd-ilrht/service/http/incoming_webhook/data-create?secret=L0sS3Bz1mdCm6Kuz',
};

export default item;


