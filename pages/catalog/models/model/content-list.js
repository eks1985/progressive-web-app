import React from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';

const ContentItem = props => {

  const { title, ind, removeContent, shiftContent } = props;

  return (
    <ListItem>
      <ListItemText
        primary={title}
      />
      <ListItemSecondaryAction>
        <IconButton
          aria-label='shift-forward'
          onClick={
            () => {
              shiftContent(ind, 1);
            }
          }
        >
          <ArrowDownwardIcon />
        </IconButton>
        <IconButton
          aria-label='shift-back'
          onClick={
            () => {
              shiftContent(ind, -1);
            }
          }
        >
          <ArrowUpwardIcon />
        </IconButton>
        <IconButton
          aria-label='shift-back'
          onClick={
            () => {
              removeContent(ind);
            }
          }
        >
          <DeleteIcon />
        </IconButton>
      </ListItemSecondaryAction>
    </ListItem>
  );

};

const contentList = props => {

  const { content, ...other } = props;
  
  return (
    <div>
      <List>
        {
          content.map((item, ind) => (
            <ContentItem key={item.id} title={item.tt} ind={ind} {...other} />
          ))
        }
      </List>
    </div>
  );
};

export default contentList;
