import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Title from './title';
import Subcategory from './subcategory';
import Picture from './picture';
import Properties from './properties';
import CategoryAssign from './category-assign';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    fontSize: theme.typography.pxToRem(13),
    marginTop: theme.spacing(2),
  },
  head: {
    fontSize: theme.typography.pxToRem(15),
    padding: '6px',
    paddingRight: '50px',
    color: theme.palette.primary.main,
  },
  modelsDetails: {
    padding: 0,
  },
  details: {
    display: 'flex',
    flexDirection: 'column',
  },
  translations: {
    marginTop: theme.spacing(2),
  },
}));

const model = props => {

  const classes = useStyles();

  const { model, categories, edit } = props;

  return (
    <Box className={classes.root}>

      {/* title  */}
      <Accordion square>

        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
          <Typography className={classes.head}>Заголовок</Typography>
        </AccordionSummary>

        <AccordionDetails className={classes.details}>

          <Title model={model} edit={edit} lang='ru' />

          <Accordion square className={classes.translations}>
            <AccordionSummary expandIcon={<ExpandMoreIcon />}>
              <Typography className={classes.head}>Ukranian</Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Title model={model} edit={edit} lang='ua' />
            </AccordionDetails>
          </Accordion>

          <Accordion square>
            <AccordionSummary expandIcon={<ExpandMoreIcon />}>
              <Typography className={classes.head}>English</Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Title model={model} edit={edit} lang='en' />
            </AccordionDetails>
          </Accordion>

          <Accordion square>
            <AccordionSummary expandIcon={<ExpandMoreIcon />}>
              <Typography className={classes.head}>Italian</Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Title model={model} edit={edit} lang='it' />
            </AccordionDetails>
          </Accordion>

        </AccordionDetails>

      </Accordion>


      {/* subcategory  */}
      <Accordion square>

      <AccordionSummary expandIcon={<ExpandMoreIcon />}>
        <Typography className={classes.head}>Подкатегория</Typography>
      </AccordionSummary>

      <AccordionDetails className={classes.details}>

        <Subcategory model={model} edit={edit} lang='ru' />

        <Accordion square className={classes.translations}>
          <AccordionSummary expandIcon={<ExpandMoreIcon />}>
            <Typography className={classes.head}>Ukranian</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Subcategory model={model} edit={edit} lang='ua' />
          </AccordionDetails>
        </Accordion>

        <Accordion square>
          <AccordionSummary expandIcon={<ExpandMoreIcon />}>
            <Typography className={classes.head}>English</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Subcategory model={model} edit={edit} lang='en' />
          </AccordionDetails>
        </Accordion>

        <Accordion square>
          <AccordionSummary expandIcon={<ExpandMoreIcon />}>
            <Typography className={classes.head}>Italian</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Title model={model} edit={edit} lang='it' />
          </AccordionDetails>
        </Accordion>

      </AccordionDetails>

      </Accordion >

      {/* picture  */}
      <Accordion square>

        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
          <Typography className={classes.head}>Картинка</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Picture model={model} edit={edit} />
        </AccordionDetails>

      </Accordion>

      {/* category assign  */}
      <Accordion square>

        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
          <Typography className={classes.head}>Свойства</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <CategoryAssign categories={categories} model={model} edit={edit} />
        </AccordionDetails>

      </Accordion>

    </Box>
  );
};

export default model;

