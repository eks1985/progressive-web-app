import React from 'react';
import Image from 'next/image';
import Box from '@material-ui/core/Box';
import FileUpload from 'components/cdn-upload';

const modelPicture = props => {
  
  const { model } = props;
  
  const handleAddPicture = picture => {
   
    const { edit } = props;
    edit('picture', picture);

  };
  
  return (
    <Box id='pict-container' display='flex' flexDirection='column' alignItems='center' width='100%'>
      {model.picture &&
        <Box className='img-wrap' width='100%' mt={2} position='relative'>
          <Image className='img' src={model.picture} alt='category-picture' layout='fill' objectFit='contain' objectPosition='center center' />
        </Box>
      }
      <FileUpload {...props} handleAddPicture={handleAddPicture} />
    </Box>
  );
  
};

export default modelPicture;



