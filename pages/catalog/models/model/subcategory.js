import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles(theme => ({
  textField: {
    marginLeft: '2px',
    marginRight: '2px',
    width: '100%',
  },
}));

const getPropAlias = lang => {
  switch (lang) {
    case 'ru':
      return 'subcategory';
    case 'ua':
      return 'subcategory-ua';
    case 'en':
      return 'subcategory-en';
    case 'it':
      return 'subcategory-it';
    default:
      return 'subcategory';
  }
};

const title = props => {
  
  const classes = useStyles();
  const { model, lang, edit } = props;
  const alias = getPropAlias(lang);
  const handleChange = e => {
    edit(alias, e.target.value);
  };
  
  return (
    <TextField
      id='subcategory'
      multiline
      rowsMax='2'
      className={classes.textField}
      value={model ? model[alias] : ''}
      onChange={handleChange}
      margin='none'
    />
  );
};

export default title;


