import React, { useState, useEffect } from 'react';
import { useStore } from 'lib/store';
import { makeStyles } from '@material-ui/core/styles';
import { useRouter } from 'next/router';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Header from './header';
import Model from './model';
import Products from './products';
import Actions from './actions';
import { updateModelCall, updateProductsCall, createProductCall } from 'lib/api/models';
import { shiftArrayElem } from 'lib/arrays';

const useStyles = makeStyles(theme => ({
  btn: {
    minWidth: '200px',
  },
}));

const content = props => {

  const store = useStore();
  const router = useRouter();
  const classes = useStyles();
  const { modelId, mutate, url_create, categories } = props;

  const [model, setModel] = useState(props.models.length > 0 ? props.models[0] : false);
  const [products, setProducts] = useState(props.products);
  const [modelMod, setModelMod] = useState(false);
  const [productsMod, setProductsMod] = useState(false);
  const [updated, toggleUpdated] = useState(false);
  
  useEffect(() => {
    setProducts(props.products);
    const newModel = props.models.length > 0 ? props.models[0] : false;
    // setModel(newModel);
  }, [props]);

  const updateModel = () => {
    updateModelCall(url_create, model, mutate, store.setLoading, setModelMod);
  };

  const edit = (prop, value) => {
    const updated = { ...model, [prop]: value };
    setModel(updated); 
    setModelMod(true);
  };

  const updateProducts = () => {
    updateProductsCall(url_create, products, model, modelId, mutate, setProductsMod, store.setLoading);
  };

  const shiftProduct = (step, index) => {
    setProductsMod(true);
    const newProducts = shiftArrayElem(products, index, step).map(item => {
      return { ...item, mod: true };
    });
    setProducts(newProducts);
  };

  const editProduct = (index, newProduct) => {
    const newProducts = products.map((item, ind) => {
      if (ind === index) {
        return newProduct;
      }
      return item;
    });
    setProducts(newProducts);
  };

  const createProduct = () => {
    createProductCall(url_create, store.country, model, modelId, mutate, store.setLoading);
  };

  if (!model) {
    return null;
  }

  return (
    <Box maxWidth='900px' width='100%' margin='0 auto'>
      <Header
        mutate={mutate}
        model={model}
      />
      {modelMod &&
        <Actions
          updateModel={updateModel}
          updated={updated}
          toggleUpdated={toggleUpdated}
          setModelMod={setModelMod}
        />
      }
      <Model
        categories={categories}
        model={model}
        edit={edit}
      />
      <Products
        modelId={modelId}
        products={products}
        productsMod={productsMod}
        setProductsMod={setProductsMod}
        createProduct={createProduct}
        editProduct={editProduct}
        shiftProduct={shiftProduct}
        updateProducts={updateProducts}
      />
      <Box my={3} display='flex' justifyContent='center'>
        <Button
          className={classes.btn}
          variant='outlined'
          onClick={
            () => {
              router.push('/catalog/accessories');
            }
          }
        >
          Аксессуары
        </Button>
      </Box>
      <Box my={3} display='flex' justifyContent='center'>
        <Button
          className={classes.btn}
          variant='outlined'
          onClick={
            () => {
              router.push('/content');
            }
          }
        >
          Контент
        </Button>
      </Box>
    </Box>
  );

};

content.defaultProps = {
  url_create: 'https://webhooks.mongodb-realm.com/api/client/v2.0/app/mgd-ilrht/service/http/incoming_webhook/data-create?secret=L0sS3Bz1mdCm6Kuz',
};

export default content;
