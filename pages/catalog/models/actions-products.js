import React from 'react';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';

const actions = props => {

  const { productsMod, updateProducts, setProductsMod } = props;

  if (!productsMod) {
    return null;
  }

  return (
    <Box margin='0 auto' width='100%' display='flex' justifyContent='center' mt={2}>
      <Box>
        <Button
          variant='contained'
          color='primary'
          onClick={updateProducts}
        >
          Сохранить
        </Button>
      </Box>
      <Box ml={2}>
        <Button
          onClick={setProductsMod.bind(null, false)}>
          Отмена
        </Button>    
      </Box>
    </Box>
  );

};

export default actions;
