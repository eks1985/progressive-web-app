import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useStore } from 'lib/store';
import { useRouter } from 'next/router';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles(theme => ({
  container: {
    
  },
}));

const generateLabel = (options, values, opt) => {

  const label = [];
  options.forEach((option, i) => {
    const value = values[i];
    const optCurrent = opt[option];
    optCurrent.values.forEach(item => {
      if (item.value === value) {
        label.push(item.props.alias);
      }
    });
  });
  return label;

};

const options = props => {

  const store = useStore();
  const router = useRouter();
  const classes = useStyles();

  const { priceItem, opt } = props;
  const { options, values } = priceItem;
  const label = generateLabel(options, values, opt);

  return (
    <Box mt={1} margin='0 auto' width='100%' textAlign='center'>
      {label.map(item => {
        return (
          <Box key={item}>
            <Typography variant='caption' color='primary'>
              {item}
            </Typography>
          </Box>
        );
      })}
    </Box>
  );

};

export default options;
