import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useStore } from 'lib/store';
import { useRouter } from 'next/router';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Product from './product';

const useStyles = makeStyles(theme => ({
  container: {
    fontSize: theme.typography.pxToRem(13), 
  },
}));

const model = props => {

  const store = useStore();
  const router = useRouter();
  const classes = useStyles();
  const { model, filtered } = props;

  return (
    <Box margin='0 auto' width='100%' mt={1}>
      {!filtered &&
        <Box textAlign='center'>
          <Typography variant='subtitle2' color='primary' className={classes.container}>
            {model.descr}
          </Typography>
        </Box>
      }
      {model.sets &&
        <Box width='100%'>
          {model.sets.map(product => {
            return <Product key={product} id={product} />;
          })}
        </Box>
      }
    </Box>
  );

};

export default model;
