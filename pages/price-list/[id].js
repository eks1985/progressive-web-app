import React from 'react';
import { baseUrl, secret } from 'lib/base';
import { useRouter } from 'next/router';
import { useStore } from 'lib/store';
import DataProviderBatch from 'lib/data-batch';
import Box from '@material-ui/core/Box';
import Product from 'components/product';

const prepareQuery = (url, id, country) => {

  return {
    catalog: `${url}&filters=p{id:${id}}&country=${country}`,
  };

};

const product = props => {

  const router = useRouter(); 
  const store = useStore();

  const { country } = store;

  if (!router.query.id || !store) {
    return null;
  }

  const query = prepareQuery(props.url, router.query.id, country);

  return (
    <Box margin='0 auto' width='100%' maxWidth='900px'>
      <DataProviderBatch query={query} render={(data, mutate) => (
        <Product
          product={data.catalog.products}
        />
      )}/>
    </Box>
  );

};

product.defaultProps = {
  url: `${baseUrl}catalog${secret}lists=p`,
};

export default product;