import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Image from 'next/image';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Box from '@material-ui/core/Box';

const useStyles = makeStyles(theme => ({
  container: {
    fontSize: theme.typography.pxToRem(14), 
  },
}));

const category = props => {
  const classes = useStyles();
  const { category, children } = props;
  const { descr } = category;

  const [expanded, setExpanded] = useState(false);

  const handleChange = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  return (
    <Accordion square expanded={expanded === 'panel1'} onChange={handleChange('panel1')}>
      <AccordionSummary expandIcon={<ExpandMoreIcon />}>
        <Box height={40} display='flex' alignItems='center'>
          {category.picture &&
            <Box width={60} minWidth={60} height={40} position='relative'>
              <Image className='img' src={category.picture} alt={descr} layout='fill' objectFit='contain' objectPosition='center center' />
            </Box>
          }
          <Box ml={2}>
            <Typography variant='body1' className={classes.container}>
              {descr}
            </Typography>
          </Box>
        </Box>
      </AccordionSummary>
      <AccordionDetails>
        {expanded === 'panel1' &&
          children
        }
      </AccordionDetails>
    </Accordion>
  );
};

export default category;
