import React, { useState } from 'react';
import { useStore } from 'lib/store';
import Box from '@material-ui/core/Box';
import DataProviderBatch from 'lib/data-batch';
import { baseUrl, secret } from 'lib/base';
import Category from './category';
import Model from './model';
import ModelSelect from './model-select';

const prepareQuery = (url, country, permiss) => {

  let vis = 1;
  if (!permiss || !permiss.catalog.visManager) {
    vis++;
  }
 
  return {
    catalog: `${url}&country=${country}&vis=${vis}`,
  };

};

const priceList = props => {

  const [model, setModel] = useState(false);

  const store = useStore();
  const { appUser, country, permiss, cond } = store;
  if (appUser === false) {
    return null;
  }
  const query = prepareQuery(props.url, country, permiss);

  if (!permiss) {
    return null;
  }

  if (!permiss.atoms.priceClient && !permiss.atoms.priceDealer && !permiss.catalog.showClientPrice && !permiss.catalog.showDealerPrice) {
    return null;
  } 

  return (
    <Box margin='0 auto' width='100%' maxWidth='600px' mb={1}>
      <DataProviderBatch query={query} render={(data, mutate) => {
        return (
          <>
            <Box p={cond ? 1 : 0} mb={1}>
              <ModelSelect model={model} setModel={setModel} models={data.catalog.models} />
            </Box>
            {model
              ?
              <Box p={1}>
                <Model filtered model={data.catalog.models.find(item => item._id === model.value)} />
              </Box>
              :
              <Box>
                {data.catalog.categories.map(category => {
                  const categoryModels = data.catalog.models.filter(model => model.categoryRef === category._id); 
                  return (
                    <Category key={category.id} category={category}>
                      <Box display='flex' flexDirection='column' width='100%'>
                        {categoryModels.map(model => {
                          return (
                            <Model key={model._id} model={model} />
                          );
                        })}
                      </Box>
                    </Category>  
                  );
                })}  
              </Box>
            }
          </>
        );

      }}/>
    </Box>
  );

};

priceList.defaultProps = {
  url: `${baseUrl}catalog${secret}lists=c,m,p`, 
};

export default priceList;
