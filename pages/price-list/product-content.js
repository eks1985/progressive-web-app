import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useStore } from 'lib/store';
import { useRouter } from 'next/router';
import Box from '@material-ui/core/Box';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import IconSend from '@material-ui/icons/Send';
import IconEdit from '@material-ui/icons/Edit';
import Cart from 'lib/cart';
import PriceItem from './price-item';
import Options from './options';

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 600,
    marginTop: theme.spacing(1),
    width: '100%',
    border: '1px solid #ddd',
  },
  productProps: {
    color: '#777',
    lineHeight: '1em',
  },
  content: {
    padding: theme.spacing(3)/2,
  }
}));

const productContent = props => {
  const classes = useStyles();

  const { product, permiss } = props;
  const { title } = product;

  const store = useStore();
  const { appUser } = store;

  const router = useRouter();

  if (!product.prices || !product.prices.farmer) {
    return null;
  }

  const handleAddToCart = () => {
    let cart = store.cart;
    const { globalOpt, setCart, setCartOpen } = store;
    if (!cart) {
      cart = new Cart();
    }
    cart.addProduct(product, globalOpt);
    if (appUser) {
      cart.update({ event: 'changeProp', prop: 'customer', value:  appUser.customerCurOpt });   
    }
    setCart(cart);
    setCartOpen(true);
  };

  const editProduct = () => {
    router.push(`catalog/products/${product.id}`);  
  };

  return (
    <Card className={classes.root} elevation={0}>
      <CardContent className={classes.content}>
        <Box>
          <Typography variant='subtitle1' color='primary'>
            {title}
          </Typography>
        </Box>
        {product.properties &&
          <Box mt={1} lineHeight='1em'>
            <Typography variant='caption' className={classes.productProps}>
              {product.properties.join(', ')}
            </Typography>
          </Box>
        }
        {product.prices && Array.isArray(product.prices.farmer) && 
          <Box mt={2}>
            {product.prices.farmer.map((priceItem, index) => {
              return (
                <Box key={index}>
                  <Options priceItem={priceItem} opt={product.opt} />
                  <PriceItem permiss={permiss} dealer={product.prices.dealer[index].price} farmer={priceItem.price} />
                </Box>
              );    
            })}  
          </Box>
        }
        {product.prices && !Array.isArray(product.prices.farmer) && 
          <Box mt={2}>
            <PriceItem permiss={permiss} dealer={product.prices.dealer} farmer={product.prices.farmer} />
          </Box>
        }
      </CardContent>
      <CardActions disableSpacing>
        <Box display='flex' justifyContent='flex-end' width='100%'>
        {permiss.orders.placeOrders || permiss.atoms.ordersSend || permiss.atoms.requestSend &&
          <IconButton
            aria-label='request'
            onClick={handleAddToCart}
          >
            <IconSend />
          </IconButton>
        }
          {appUser && appUser.internal &&
            <IconButton
              aria-label='edit'
              onClick={editProduct}
            >
              <IconEdit />
            </IconButton>
          }
        </Box>
      </CardActions>
    </Card>
  );
}

export default productContent;
