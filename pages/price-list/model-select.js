import React from 'react';
import Box from '@material-ui/core/Box';
import Select from 'components/select';

const modelSelect = props => {

  const { model, setModel, models } = props;
  const options = models.map(model => ({ value: model._id, label: model.descr }));

  return (
    <Select
      width='100%'
      options={options}
      placeholder='Модель'
      option={model}
      isClearable
      onSelectHandler={
        val => {
          setModel(val);
        }
      }
    />
  );

};

export default modelSelect;
