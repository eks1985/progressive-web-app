import React from 'react';
import { baseUrl, secret } from 'lib/base';
import { useStore } from 'lib/store';
import DataProviderBatch from 'lib/data-batch';
import Box from '@material-ui/core/Box';
import ProductContent from './product-content';

const prepareQuery = (url, id, country, permiss) => {

  let vis = 1;
  if (!permiss || !permiss.catalog.visManager) {
    vis++;
  }

  return {
    catalog: `${url}&filters=p{id:${id}}&country=${country}&vis=${vis}`,
  };

};

const product = props => {

  const store = useStore();
  const { country } = store;
  const { id } = props;

  if (!id || !store) {
    return null;
  }

  const { permiss } = store;

  const query = prepareQuery(props.url, id, country, permiss);

  return (
    <Box margin='0 auto' width='100%' maxWidth='600px'>
      <DataProviderBatch query={query} render={(data, mutate) => {
        if (data.catalog.products && data.catalog.products.length > 0) {
          return <ProductContent permiss={permiss} product={data.catalog.products[0]} />
        }
      }}/>
    </Box>
  );

};

product.defaultProps = {
  url: `${baseUrl}catalog${secret}lists=p`,
};

export default product;