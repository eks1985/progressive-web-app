import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { formatNumb } from 'lib/format';

const useStyles = makeStyles(theme => ({
  container: {
    
  },
}));

const priceItem = props => {

  const { farmer, dealer, permiss } = props;

  if (!farmer) {
    return null;
  }

  let farmerPrepared = '';
  const farmerParsed = parseInt(farmer);
  if (Number.isInteger(farmerParsed)) {
    farmerPrepared = `${formatNumb(farmerParsed)} €`;
  } else {
    farmerPrepared = '-';
  }

  let dealerPrepared = '';
  const dealerParsed = parseInt(dealer);
  if (Number.isInteger(dealerParsed)) {
    dealerPrepared = `${formatNumb(dealerParsed)} €`;
  } else {
    dealerPrepared = '-';
  }
  
  return (
    <Box display='flex' justifyContent='space-around'>
      <Box textAlign='center'>
        <Box>
          <Typography variant='caption'>
            Цена клиент
          </Typography>
        </Box>
        <Box>
          <Typography>
            {farmerPrepared}    
          </Typography>
        </Box>
      </Box>
      {(permiss.atoms.priceDealer || permiss.catalog.showDealerPrice) &&
        <Box textAlign='center'>
          <Box>
            <Typography variant='caption'>
              Цена дилер
            </Typography>
          </Box>
          <Box>
            {dealer
              ?
              <Typography>
                {dealerPrepared}    
              </Typography>
              :
              <Typography>
                Скидка дилера    
              </Typography>
            }
          </Box>
        </Box>
      }
    </Box>
  );

};

export default priceItem;
