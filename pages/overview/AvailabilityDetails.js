import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { green } from '@material-ui/core/colors';
import { blue } from '@material-ui/core/colors';
import { orange } from '@material-ui/core/colors';
import { deepPurple } from '@material-ui/core/colors';
import { grey } from '@material-ui/core/colors';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Tooltip from '@material-ui/core/Tooltip';

const useStyles = makeStyles(theme =>
  (
    {
      row: {
        height: '16px',
        overflow: 'hidden',
        display: 'flex',
        alignItems: 'flex-start',
      },
      cell: {
        fontSize: '10px',
      },
      cellSn: {
        fontSize: '10px',
        color: props => (props.qual && props.qual !== 'New') ? '#E1001A' : blue[500],
      },
      cellConfDate: {
        fontSize: '10px',
        color: props => props.conf ? 'initial' : '#E1001A',
      },
      cellDDate: {
        fontSize: '10px',
        color: props => props.expired ? '#E1001A' : 'initial',
      },
      cellPrdDescr: {
        fontSize: '10px',
        fontStyle: 'italic',
      },
      stat: {
        width: '100%',
        paddingLeft: '3px',
        fontSize: '10px',
        justifyContent: 'center',
        color: 'white',
        backgroundColor: props => props.colorStat,
      },
      ord: {
        width: '100%',
        paddingLeft: '3px',
        fontSize: '10px',
        justifyContent: 'center',
        color: 'white',
        backgroundColor: props => props.colorOrd,
      },
    }
  ));

const caclStatColor = stat => {

  if (stat === 'Склад') {
    return blue[500];
  }

  if (stat === 'В пути') {
    return orange[500];
  }

  if (stat === 'В заказе поставщику') {
    return deepPurple[500];
  }

  return green[500];

};

const caclOrdColor = t => {

  if (t === 'Заказ') {
    return green[500];
  }

  if (t === 'КП') {
    return grey[500];
  }

  if (t === 'Внутр заказ') {
    return '#795548';
  }

  return green[500];

};

const Row = props => {

  const { item } = props;

  const [openSn, setOpenSn] = React.useState(false);

  const handleTooltipClose = () => {
    setOpenSn(false);
  };

  const handleTooltipOpen = () => {
    setOpenSn(true);
  };

  // console.log('item', item);

  const colorStat = caclStatColor(item.stat1);
  const colorOrd = caclOrdColor(item.ordt2);

  const classes = useStyles({ colorStat, colorOrd, qual: item.qual1, conf: item.ddate1, expired: item.expired });

  return (
    <Box width='100%' display='flex' borderTop='1px dashed #aaa'>
      <Box width='50%' borderRight='1px solid #eee'>
        <Box width='100%' display='flex' className={classes.row}>
          <Box width='50%' className={classes.row}>
            <Typography variant='caption' className={classes.cell}>
              {item.code1}
            </Typography>
          </Box>
          <Box width='50%' className={classes.row}>
            <Typography variant='caption' className={classes.stat}>
              {item.stat1}
            </Typography>
          </Box>
        </Box>
        <Box width='100%' display='flex' className={classes.row}>
          <Typography variant='caption' className={classes.cellPrdDescr}>
            {item.dscr1}
          </Typography>
        </Box>
        <Box width='100%' display='flex' className={classes.row}>
          <Box width='50%' className={classes.row}>
            {item.stat1 === 'Склад' &&
              <ClickAwayListener onClickAway={handleTooltipClose}>
                <Tooltip
                  PopperProps={{
                    disablePortal: true,
                  }}
                  onClose={handleTooltipClose}
                  open={openSn}
                  disableFocusListener
                  disableHoverListener
                  disableTouchListener
                  title={item.qual1}
                >
                  <Typography variant='caption' className={classes.cellSn} onClick={handleTooltipOpen}>
                    {item.sn1}
                  </Typography>
                </Tooltip>
              </ClickAwayListener>
            }
            {item.stat1 === 'В пути' &&
              <Typography variant='caption' className={classes.cell}>
                {item.ddate1}
              </Typography>
            }
            {item.stat1 === 'В заказе поставщику' &&
              <Typography variant='caption' className={classes.cellConfDate}>
                {item.ddate1 || 'not confirmed'}
              </Typography>
            }
          </Box>
          <Box width='50%' className={classes.row}>
            {item.stat1 === 'Склад' &&
              <Typography variant='caption' className={classes.cell}>
                {item.det1}
              </Typography>
            }
            {item.stat1 === 'В пути' &&
              <Typography variant='caption' className={classes.cell}>
                {`>${item.sdate1}`}
              </Typography>
            }
            {item.stat1 === 'В заказе поставщику' && item.sdate1 &&
              <Typography variant='caption' className={classes.cell}>
                {`>${item.sdate1}`}
              </Typography>
            }
          </Box>
        </Box>
      </Box>
      <Box width='50%' style={{ paddingLeft: '4px' }}>
        <Box width='100%' display='flex' className={classes.row}>
          <Box width='50%' className={classes.row}>
            <Typography variant='caption' className={classes.cell}>
              {item.code2}
            </Typography>
          </Box>
          <Box width='50%' className={classes.row}>
            <Typography variant='caption' className={classes.ord}>
              {item.cstds}
            </Typography>
          </Box>
        </Box>
        <Box width='100%' display='flex' className={classes.row}>
          <Typography variant='caption' className={classes.cellPrdDescr}>
            {item.dscr2}
          </Typography>
        </Box>
        <Box width='100%' display='flex' className={classes.row}>
          <Box width='50%' className={classes.row}>
            <Typography variant='caption' className={classes.cell}>
              {item.ordnr2}
            </Typography>
          </Box>
          <Box width='50%' className={classes.row}>
            <Typography variant='caption' className={classes.cellDDate}>
              {item.ddate2}
            </Typography>
          </Box>
        </Box>
      </Box>
    </Box>
  );

};

const Details = props => {

  const { items } = props;

  return (
    <Box width='100%' mt={2} p={1} maxWidth='600px' margin='0 auto' style={{ paddingBottom: '120px' }}>
      <Box mt={2}>
        <Box width='100%' display='flex'>
          <Box textAlign='center' width='50%'>
            <Typography variant='body2'>
              Наличие
            </Typography>
          </Box>
          <Box textAlign='center' width='50%'>
            <Typography variant='body2'>
              Клиенты
            </Typography>
          </Box>
        </Box>
        {items.map((item, i) => {
          return <Row key={`row-${i}`} item={item} />;
        })}
      </Box>
    </Box>
  );
};

export default Details;
