import React, { useState, useEffect } from 'react';
import { useStore } from 'lib/store';
import { useRouter } from 'next/router';
import axios from 'axios';
import { orderBy } from 'lodash';
import Loading from 'components/loading';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import Content from './Content';

const overview = props => {

  const router = useRouter();
  const store = useStore();
  const { country, token, appUser, appUser: { email }} = store;
  
  const { urlUnsold, urlOpenItems, urlCons, urlAvailab, urlStockChart } = props;
  
  const [loading, setLoading] = useState(false);
  const [pages, setPages] = useState([]);
  const [models, setModels] = useState([]);
  const [page, setPage] = useState(false);
  const [model, setModel] = useState(false);
  const [ind, setInd] = useState([]);
  const [data, setData] = useState(false);
  const [details, setDetails] = useState(false);
  const [stockBranchValue, setStockBranchValue] = useState(0);
  const [unsoldValue, setUnsoldValue] = useState(0);
  
  const swiped = (e, deltaX) => {
    if (deltaX > 50) {
      move();
    } else if (deltaX < -50) {
      move(-1);
    }
  };

  const move = (step = 1) => {
    const qty = pages.length;
    const ind = pages.reduce((res, filter, i) => filter.value === page.value ? i : res, 0);
    let newInd = step === 1 ? ind + 1 : ind - 1;
    if (ind === 0 && step === -1) {
      newInd = qty - 1;
    }
    if (ind === qty - 1 && step === 1) {
      newInd = 0;
    }
    const newPageValue = pages.reduce((res, filter, i) => i === newInd ? filter : res, 0);
    setPage(newPageValue);
  };

  useEffect(() => {

    async function fetchData(country, token) {

      const ind = [];
      const res = {};

      res.mg = {
        id: 'mg',
        title: 'MASCHIO GASPARDO',
      };

      ind.push('mg');

      res.cons = {
        id: 'cons',
        title: 'CONSIGNMENT',
      };

      ind.push('cons');

      setPage({ value: 'mg', label: 'MASCHIO GASPARDO' });

      // availability

      const resultAvailab = await axios.post(urlAvailab,
        { cntr: country === 0 ? 'ru' : 'ua', custGuid: token },
      );

      if (resultAvailab.status === 200) {
        const resDataAvailab = JSON.parse(resultAvailab.data);
        if (resDataAvailab.data) {
          res.mg.data = resDataAvailab.data;
        }
      }

      // unsold

      let totalUnsold = 0;
      const resultUnsold = await axios.post(urlUnsold,
        { resource: 'unsold-machines', params: { country, custGuid: token } },
      );


      if (resultUnsold.status === 200) {
        const resDataUnsold = JSON.parse(resultUnsold.data);
        resDataUnsold.data.forEach(elem => {
          if (!ind.includes(elem._id)) {
            ind.push(elem._id);
            if (!res[elem._id]) {
              res[elem._id] = { title: elem.items[0].dlds };
            }
            res[elem._id].unsold = elem;
            elem.items.forEach(item => {
              const curEur = item.valr / item.exch;
              totalUnsold += isNaN(curEur) ? 0 : curEur;
            });
          }
        });
      }

      if (totalUnsold) {
        setUnsoldValue(Math.ceil(totalUnsold));
      }

      // open-items

      const resultOpenItems = await axios.post(urlOpenItems,
        { cntr: country === 0 ? 'ru' : 'ua', custGuid: token },
      );

      if (resultOpenItems.status === 200) {
        const resDataOpenItems = JSON.parse(resultOpenItems.data);
        if (resDataOpenItems.data) {
          resDataOpenItems.data.forEach(elem => {
            if (elem._id !== 'total') {
              if (!ind.includes(elem._id)) {
                ind.push(elem._id);
              }
              if (!res[elem._id]) {
                res[elem._id] = { title: elem.props.name }; // title: elem.props.name
              }
              res[elem._id].openItems = elem;
            }
          });
        }
      }

      // consignment

      const resultCons = await axios.post(urlCons,
        { cntr: country === 0 ? 'ru' : 'ua', custGuid: token },
      );

      if (resultCons.status === 200) {
        const resDataCons = JSON.parse(resultCons.data);
        if (resDataCons.data) {
          res.cons.data = resDataCons.data;
          resDataCons.data.forEach(elem => {
            if (!ind.includes(elem._id)) {
              ind.push(elem._id);
            }
            if (!res[elem._id]) {
              res[elem._id] = { title: elem.dscr }; // title: elem.props.name
            }
            res[elem._id].cons = elem;
          });
        }
      }

      // stock chart (to calc total mg stock value)
      let stockBranch = 0;
      const resultChartData = await axios.post(urlStockChart,
        { cntr: country === 0 ? 'ru' : 'ua', custGuid: token },
      );

      if (resultChartData.status === 200) {
        const resultChartDataParsed = JSON.parse(resultChartData.data);
        if (resultChartDataParsed.data) {
          if (resultChartDataParsed.data.length > 0) {
            const chartData = resultChartDataParsed.data[0];

            const keys = Object.keys(chartData);
            const curData = keys.reduce((res, item) => {
              if (chartData[item].dettypes) {
                return chartData[item];
              }
              return res;
            }, false);

            stockBranch = curData.value;

          }
        }
      }

      if (stockBranch) {
        setStockBranchValue(Math.ceil((stockBranch / 90) * 1000000));
      }

      // dealers select index

      if (ind) {
        const newPages = ind.map(item => {
          return { value: item, label: res[item].title };
        });
        setPages(newPages);
      }

      // model index

      const modelsInd = [];
      if (ind) {
        ind.forEach(index => {
          const cur = res[index];
          if (cur.unsold) {
            cur.unsold.items.forEach(elem => {
              if (!modelsInd.includes(elem.mdes)) {
                modelsInd.push(elem.mdes);
              }
            });
          }
          if (index === 'mg') {
            cur.data.forEach(elem => {
              if (!modelsInd.includes(elem._id)) {
                modelsInd.push(elem._id);
              }
            });
          }
          if (cur.cons) {
            cur.cons.items.forEach(elem => {
              if (!modelsInd.includes(elem.model)) {
                modelsInd.push(elem.model);
              }
            });
          }
        });
      }

      const newModels = modelsInd.sort().map(elem => ({ value: elem, label: elem }));
      setModels(newModels);

      setInd(ind);
      setData(res);

      const unsorted = ind.map(item => {
        let qty = 0;
        if (res[item].unsold) {
          qty += res[item].unsold.items.length;
        }
        if (res[item].openItems) {
          qty += res[item].openItems.items.length;
        }
        let rate = qty;
        if (item === 'mg') {
          rate = 1000;
        }
        if (item === 'cons') {
          rate = 900;
        }
        return { id: item, qty: rate }; // mg item always has high priority
      });

      const sortedInd = orderBy(unsorted, 'qty', 'desc');

      const sortedIndArray = sortedInd.map(item => item.id);

      setInd(sortedIndArray);

      if (ind) {
        const newPages = sortedIndArray.map(item => {
          return { value: item, label: res[item].title };
        });
        setPages(newPages);
      }

      setLoading(false);

    }

    if (country !== undefined && country > -1 && token !== '') {

      // fetch unsold, availability, consignment, open-items, charts
      setLoading(true);
      fetchData(country, token);

    }
  }, [country, token]);

  if (loading) {
    return <Loading />;
  }

  if (appUser === false || !appUser.internal) {
    return null;
  }

  return (
    <>
      <Content
        country={country}
        pages={pages}
        page={page}
        setPage={setPage}
        models={models}
        model={model}
        setModel={setModel}
        swiped={swiped}
        ind={ind}
        data={data}
        details={details}
        setDetails={setDetails}
        email={email}
        stockBranchValue={stockBranchValue}
        unsoldValue={unsoldValue}
      />
      <Box mt={2} p={1}>
        <Button
          size='small'
          onClick={
            () => {
              router.push('/machines-stock-old');
            }
          }
        >
          Старый отчет
        </Button>
      </Box>
    </>
  );

};

overview.defaultProps = {
  urlUnsold: 'https://webhooks.mongodb-stitch.com/api/client/v2.0/app/mgd-ilrht/service/http/incoming_webhook/prd-reg?secret=my-api-key',
  urlAvailab: 'https://webhooks.mongodb-stitch.com/api/client/v2.0/app/mgd-ilrht/service/http/incoming_webhook/availability?secret=L0sS3Bz1mdCm6Kuz',
  urlOpenItems: 'https://webhooks.mongodb-stitch.com/api/client/v2.0/app/mgd-ilrht/service/http/incoming_webhook/open_items?secret=L0sS3Bz1mdCm6Kuz',
  urlCons: 'https://webhooks.mongodb-stitch.com/api/client/v2.0/app/mgd-ilrht/service/http/incoming_webhook/consignment?secret=L0sS3Bz1mdCm6Kuz',
  urlStockChart: 'https://webhooks.mongodb-realm.com/api/client/v2.0/app/mgd-ilrht/service/http/incoming_webhook/stock_chart?secret=L0sS3Bz1mdCm6Kuz',
};

export default overview;
