import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Divider from '@material-ui/core/Divider';
import Box from '@material-ui/core/Box';
import blue from '@material-ui/core/colors/blue';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.primary,
  },
  icon: {
    verticalAlign: 'bottom',
    height: 20,
    width: 20,
  },
  details: {
    alignItems: 'center',
    padding: '4px',
  },
  pad: {
    marginRight: 20,
  },
}));

const Item = props => {
  const { item } = props;
  return (
    <Box width='100%' display='flex' borderTop='1px dashed #ccc'>
      <Box display='flex' flex='1 0 auto'>
        <Box>
          <Box>
            <Typography variant='caption'>
              {`${item.code} ${item.descr.slice(0, 25)}`}
            </Typography>
          </Box>
          <Box>
            <Typography variant='caption' style={{ color: blue[500] }}>
              {`sn ${item.sn}`}
            </Typography>
          </Box>
        </Box>
      </Box>
      <Box width={100} textAlign='right'>
        <Box>
          <Typography variant='caption'>
            {item.days}
          </Typography>
        </Box>
        <Box>
          <Typography variant='caption'>
            days
          </Typography>
        </Box>
      </Box>
    </Box>
  );
};

const Consignment = props => {

  const { data, model } = props;

  const res = {};

  data.forEach(stock => {
    stock.items.forEach(item => {
      if (item.model === model.value) {
        if (res[stock.dscr] === undefined) {
          res[stock.dscr] = [];
        }
        res[stock.dscr].push(item);
      }
    });
  });

  const resLength = Object.keys(res).length;

  if (resLength === 0) {
    return null;
  }

  const classes = useStyles();

  return (
    <div className={classes.root}>

      <Accordion square expanded>
        <AccordionSummary
          aria-controls='panel1c-content'
          id='panel1c-header'
        >
          <div className={classes.pad}>
            <ExpandMoreIcon />
          </div>
          <Typography className={classes.secondaryHeading}>Хранение</Typography>
        </AccordionSummary>
        <AccordionDetails className={classes.details}>
          <Box width='100%'>

            {Object.keys(res).map(key => {
              return (
                <Box key={key}>
                  <Box>
                    <Typography variant='subtitle1' color='primary'>
                      {key}
                    </Typography>
                  </Box>
                  <Box>
                    {res[key].map((item, i) => {
                      return (
                        <Item item={item} key={i} />
                      );
                    })}
                  </Box>
                </Box>
              );
            })}

          </Box>
        </AccordionDetails>
        <Divider />
      </Accordion>

    </div>
  );

};

export default Consignment;
