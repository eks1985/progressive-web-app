import React from 'react';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import blue from '@material-ui/core/colors/blue';

const Consignment = props => {

  const { items } = props;

  return (
    <Box width='100%'>
      {items.map((item, i) => {
        return (
          <Box width='100%' display='flex' key={i}>
            <Box display='flex' flex='1 0 auto'>
              <Box>
                <Box>
                  <Typography variant='caption'>
                    {`${item.code} ${item.descr.slice(0, 35)}`}
                  </Typography>
                </Box>
                <Box>
                  <Typography variant='caption' style={{ color: blue[500] }}>
                    {`sn ${item.sn}`}
                  </Typography>
                </Box>
              </Box>
            </Box>
            <Box width={100} textAlign='center'>
              <Box>
                <Typography variant='caption'>
                  {item.days}
                </Typography>
              </Box>
              <Box>
                <Typography variant='caption'>
                  days
                </Typography>
              </Box>
            </Box>
          </Box>
        );
      })}
    </Box>
  );

};

export default Consignment;
