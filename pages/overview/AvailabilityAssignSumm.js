import React from 'react';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';

const Summary = props => {

  const { custOrd, purch, purchFree, stock, stockFree, total, totalFree, way, wayFree } = props;

  return (
    <Box maxWidth='320px' width='100%' mt={1} margin='0 auto'>
      <Box width='100%' display='flex'>
        <Box width='50%'>

        </Box>
        <Box width='25%' textAlign='center'>
          <Typography variant='caption'>
            Кол
          </Typography>
        </Box>
        <Box width='25%' textAlign='center'>
          <Typography variant='caption'>
            Своб
          </Typography>
        </Box>
      </Box>
      <Box width='100%' display='flex' borderTop='1px solid #eee'>
        <Box width='50%'>
          <Typography variant='caption'>
            Склад
          </Typography>
        </Box>
        <Box width='25%' textAlign='center'>
          <Typography variant='caption'>
            {stock}
          </Typography>
        </Box>
        <Box width='25%' textAlign='center'>
          <Typography variant='caption'>
            {stockFree}
          </Typography>
        </Box>
      </Box>
      <Box width='100%' display='flex'>
        <Box width='50%'>
          <Typography variant='caption'>
            В Пути
          </Typography>
        </Box>
        <Box width='25%' textAlign='center'>
          <Typography variant='caption'>
            {way}
          </Typography>
        </Box>
        <Box width='25%' textAlign='center'>
          <Typography variant='caption'>
            {wayFree}
          </Typography>
        </Box>
      </Box>
      <Box width='100%' display='flex'>
        <Box width='50%'>
          <Typography variant='caption'>
            В заказе поставщику
          </Typography>
        </Box>
        <Box width='25%' textAlign='center'>
          <Typography variant='caption'>
            {purch}
          </Typography>
        </Box>
        <Box width='25%' textAlign='center'>
          <Typography variant='caption'>
            {purchFree}
          </Typography>
        </Box>
      </Box>
      <Box width='100%' display='flex' borderTop='1px solid #eee'>
        <Box width='50%' fontWeight='bold'>
          <Typography variant='caption' style={{ fontWeight: 'bold' }}>
            Итого
          </Typography>
        </Box>
        <Box width='25%' textAlign='center'>
          <Typography variant='caption' style={{ fontWeight: 'bold' }}>
            {total}
          </Typography>
        </Box>
        <Box width='25%' textAlign='center'>
          <Typography variant='caption' style={{ fontWeight: 'bold' }}>
            {totalFree}
          </Typography>
        </Box>
      </Box>
      <Box width='100%' display='flex' borderTop='1px solid #eee'>
        <Box width='50%'>
          <Typography variant='caption'>
            В заказе клиентов
          </Typography>
        </Box>
        <Box width='50%' textAlign='center'>
          <Typography variant='caption'>
            {custOrd}
          </Typography>
        </Box>
      </Box>
    </Box>
  );

};

export default Summary;
