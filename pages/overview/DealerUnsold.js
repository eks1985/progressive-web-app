/* eslint-disable no-restricted-globals */
import React from 'react';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import UnsoldItem from './UnsoldItem';

const UnsoldLabel = props => {

  const { items } = props;
  const vale = items.reduce((res, item) => {
    const cur = Math.ceil(item.valr / item.exch);
    if (!isNaN(cur)) {
      res += cur;
    } else {
      console.log(item.exch, item.valr);
    }
    return res;
  }, 0);
  const valef = vale.toLocaleString('ru-RU', { minimumFractionDigits: 0 });

  const machines = items.length;

  return (
    <Box mb={2} display='flex' color='#E1001A' justifyContent='center' alignItems='center'>
      <Box display='flex' alignItems='center'>
        <Typography>
          {`${machines} машин`}
        </Typography>
        <Typography style={{ marginLeft: '4px' }}>
          {`на сумму ${valef} €`}
        </Typography>
      </Box>
    </Box>
  );

};

const Unsold = props => {

  const { items, email } = props;

  return (
    <Box width='100%'>
      <UnsoldLabel items={items} />
      {items.map((item, i) => {

        return (
          <UnsoldItem key={i} item={item} email={email} useStockConfirm />
        );
      })}
    </Box>
  );

};

export default Unsold;
