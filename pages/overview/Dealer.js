import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Divider from '@material-ui/core/Divider';
import DealerConsignment from './DealerConsignment';
import DealerUnsold from './DealerUnsold';
import DealerOpenItems from './DealerOpenItems';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.primary,
  },
  icon: {
    verticalAlign: 'bottom',
    height: 20,
    width: 20,
  },
  details: {
    alignItems: 'center',
    padding: '4px',
  },
  pad: {
    marginRight: 20,
  },
}));

const Dealer = props => {

  const { data, email, unsoldValue, country } = props;

  const classes = useStyles();

  const [expandedCons, setExpandedCons] = React.useState(false);
  const [expandedUnsold, setExpandedUnsold] = React.useState(false);
  const [expandedOpenItems, setExpandedOpenItems] = React.useState(false);

  const handleChangeCons = () => {
    setExpandedCons(!expandedCons);
  };

  const handleChangeUnsold = () => {
    setExpandedUnsold(!expandedUnsold);
  };

  const handleChangeOpenItems = () => {
    setExpandedOpenItems(!expandedOpenItems);
  };

  // console.log('data', data);

  return (
    <div className={classes.root}>
      {data.cons &&
        <Accordion square expanded={expandedCons} onChange={handleChangeCons}>
          <AccordionSummary
            aria-controls='panel1c-content'
            id='panel1c-header'
          >
            <div className={classes.pad}>
              <ExpandMoreIcon />
            </div>
            <Typography className={classes.secondaryHeading}>Хранение</Typography>
          </AccordionSummary>
          <AccordionDetails className={classes.details}>
            <DealerConsignment items={data.cons.items} />
          </AccordionDetails>
          <Divider />
        </Accordion>
      }
      {data.unsold &&
        <Accordion square expanded={expandedUnsold} onChange={handleChangeUnsold}>
          <AccordionSummary
            aria-controls='panel1c-content'
            id='panel1c-header'
          >
            <div className={classes.pad}>
              <ExpandMoreIcon />
            </div>
            <Typography className={classes.secondaryHeading}>Не проданные машины</Typography>
          </AccordionSummary>
          <AccordionDetails className={classes.details}>
            <DealerUnsold items={data.unsold.items} email={email} />
          </AccordionDetails>
          <Divider />
        </Accordion>
      }
      {data.openItems &&
        <Accordion square expanded={expandedOpenItems} onChange={handleChangeOpenItems}>
          <AccordionSummary
            aria-controls='panel1c-content'
            id='panel1c-header'
          >
            <div className={classes.pad}>
              <ExpandMoreIcon />
            </div>
            <Typography className={classes.secondaryHeading}>Дебиторка</Typography>
          </AccordionSummary>
          <AccordionDetails className={classes.details}>
            <DealerOpenItems openItems={data.openItems} unsold={data.unsold} unsoldValue={unsoldValue} country={country} />
          </AccordionDetails>
          <Divider />
        </Accordion>
      }
    </div>
  );

};

export default Dealer;
