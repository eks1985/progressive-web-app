import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Divider from '@material-ui/core/Divider';
import Box from '@material-ui/core/Box';
import UnsoldItem from './UnsoldItem';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.primary,
  },
  icon: {
    verticalAlign: 'bottom',
    height: 20,
    width: 20,
  },
  details: {
    alignItems: 'center',
    padding: '4px',
  },
  pad: {
    marginRight: 20,
  },
}));

const Unsold = props => {

  const { data, model } = props;

  const res = {};

  Object.keys(data).forEach(key => {
    if (data[key].unsold) {
      data[key].unsold.items.forEach(item => {
        if (item.mdes === model.value) {
          const dealer = data[key].title;
          if (res[dealer] === undefined) {
            res[dealer] = [];
          }
          res[dealer].push(item);
        }
      });
    }
  });

  const resLength = Object.keys(res).length;

  if (resLength === 0) {
    return null;
  }

  const classes = useStyles();

  return (
    <div className={classes.root}>

      <Accordion square expanded>
        <AccordionSummary
          aria-controls='panel1c-content'
          id='panel1c-header'
        >
          <div className={classes.pad}>
            <ExpandMoreIcon />
          </div>
          <Typography className={classes.secondaryHeading}>Не проданные</Typography>
        </AccordionSummary>
        <AccordionDetails className={classes.details}>
          <Box width='100%'>

            {Object.keys(res).map(key => {
              return (
                <Box key={key}>
                  <Box>
                    <Typography variant='subtitle1' color='primary'>
                      {key}
                    </Typography>
                  </Box>
                  <Box>
                    {res[key].map((item, i) => {
                      return (
                        <UnsoldItem key={i} item={item} useStockConfirm={false} />
                      );
                    })}
                  </Box>
                </Box>
              );
            })}

          </Box>
        </AccordionDetails>
        <Divider />
      </Accordion>

    </div>
  );

};

export default Unsold;
