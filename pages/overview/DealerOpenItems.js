import React from 'react';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';

const calcCountryCurrencyLabel = country => {
  return country === 0 ? 'EUR' : 'EUR';
};

const calcExchRateByCountry = country => {
  return country === 0 ? 90 : 33;
};

const OpenItems = props => {
  const { openItems, unsold, country } = props;
  const { items } = openItems;
  const preparedItems = items.filter(item => item.descr !== '' && item.orderSummary.debtLocal !== 0);

  let unsoldValue = 0;
  if (unsold && unsold.items) {
    unsold.items.forEach(item => {
      const curEur = item.valr / item.exch;
      unsoldValue += isNaN(curEur) ? 0 : curEur;
    });
  }

  const currencyLabel = calcCountryCurrencyLabel(country);
  const exchaRate = calcExchRateByCountry(country);

  const debtSpareParts = preparedItems.reduce((res, item) => {
    if (item.descr === '') {
      res += item.orderSummary.debtLocal;
      return res;
    }
    return res;
  }, 0);

  const overdueSpareParts = preparedItems.reduce((res, item) => {
    if (item.descr === '') {
      res += item.orderSummary.overdueLocal;
      return res;
    }
    return res;
  }, 0);

  const debtTotal = preparedItems.reduce((res, item) => {
    if (item.orderSummary.debtLocal > 0) {
      res += item.orderSummary.debtLocal;
    }
    return res;
  }, 0);

  const debtTotalEur = Math.ceil(debtTotal / exchaRate);

  let ratio = 0;
  if (unsoldValue > 0) {
    ratio = Math.round(((debtTotal / 1.2 - debtSpareParts / 1.2) / (unsoldValue * exchaRate)) * 100);
    ratio = `${ratio.toLocaleString('ru-RU', { minimumFractionDigits: 0 })} %`;
  }

  const debtSparePartsFormatted = debtSpareParts ? debtSpareParts.toLocaleString('ru-RU', { minimumFractionDigits: 0 }) : '0';
  const overdueSparePartsFormatted = overdueSpareParts ? overdueSpareParts.toLocaleString('ru-RU', { minimumFractionDigits: 0 }) : '0';
  const hasDebt = (items && items.length > 0) || (debtSpareParts !== 0 && debtSpareParts !== undefined);
  if (!hasDebt) {
    return null;
  }
  return (
    <Box width='100%'>
      <Box p={1} color='#E1001A' width='100%' textAlign='center'>
        <Typography>
          {`Долг: ${debtTotalEur.toLocaleString('ru-RU', { minimumFractionDigits: 0 })} ${currencyLabel} (${ratio} от склада)`}
        </Typography>
      </Box>
      {debtSpareParts !== 0 && debtSpareParts !== undefined &&
        <Box display='flex' p={1} style={{ fontSize: '11px' }}>
          <Box width='50%'>
            <Typography variant='caption'>
              Запчасти
            </Typography>
          </Box>
          <Box width='50%' display='flex' justifyContent='flex-end'>
            <Typography variant='caption'>
              <span>{`${debtSparePartsFormatted} / `}</span>
              <span style={{ color: '#E1001A', marginLeft: '2px' }}>{` ${overdueSparePartsFormatted} `}</span>
              <span style={{ marginLeft: '2px' }}>{currencyLabel}</span>
            </Typography>
          </Box>
        </Box>
      }
      {items &&
        <Box>
          {
            preparedItems.map((item, ind) => {
              const { orderSummary: { debtLocal, overdueLocal } } = item;
              const debtFormatted = debtLocal ? debtLocal.toLocaleString('ru-RU', { minimumFractionDigits: 0 }) : '0';
              const overdueFormatted = overdueLocal ? overdueLocal.toLocaleString('ru-RU', { minimumFractionDigits: 0 }) : '0';
              return (
                <Box display='flex' key={ind} borderBottom='1px dashed #ddd' alignItems='center' p={1}>
                  <Box width='50%'>
                    <Typography variant='caption'>
                      {item.descr}
                    </Typography>
                  </Box>
                  <Box width='50%' display='flex' justifyContent='flex-end'>
                    <Typography variant='caption'>
                      <span style={{ textDecoration: 'underline', cursor: 'pointer' }}>{`${debtFormatted}`}</span>
                      {debtLocal > 0 &&
                        <span style={{ color: '#f44336', marginLeft: '2px' }}>{` / ${overdueFormatted} `}</span>
                      }
                      {debtLocal > 0 &&
                        <span style={{ marginLeft: '2px' }}>{item.orderSummary.currency}</span>
                      }
                    </Typography>
                  </Box>
                </Box>
              );
            })
          }
        </Box>
      }
    </Box>
  );
};

export default OpenItems;
