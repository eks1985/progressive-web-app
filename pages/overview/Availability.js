import React from 'react';
import Box from '@material-ui/core/Box';
import AvailabilityModel from './AvailabilityModel';

const Availability = props => {

  const { data, details, setDetails, model } = props;

  if (!data || !data.data || data.data.length === 0) {
    return null;
  }

  if (model) {

    const prepared = data.data.filter(item => item._id === model.value);

    return (
      <Box>
        {
         prepared.map(item => {
            return (
              <AvailabilityModel title='Наличие' key={item._id} data={item} details={details} setDetails={setDetails} />
            );
          })
        }
      </Box>
    );
  }

  return (
    <Box>
      {
        data.data.map(item => {
          return (
            <AvailabilityModel key={item._id} data={item} details={details} setDetails={setDetails} />
          );
        })
      }
    </Box>
  );

};

export default Availability;
