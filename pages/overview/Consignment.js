import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Divider from '@material-ui/core/Divider';
// import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import blue from '@material-ui/core/colors/blue';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.primary,
  },
  icon: {
    verticalAlign: 'bottom',
    height: 20,
    width: 20,
  },
  details: {
    alignItems: 'center',
    padding: '4px',
  },
  pad: {
    marginRight: 20,
  },
}));

const Consignment = props => {

  const { data } = props;

  const classes = useStyles();

  return (
    <div className={classes.root}>

      {data.map(stock => {
        return (
          <Accordion square expanded key={stock.id}>
            <AccordionSummary
              aria-controls='panel1c-content'
              id='panel1c-header'
            >
              <div className={classes.pad}>
                <ExpandMoreIcon />
              </div>
              <Typography className={classes.secondaryHeading}>{stock.dscr}</Typography>
            </AccordionSummary>
            <AccordionDetails className={classes.details}>
              <Box width='100%'>
                {stock.items.map((item, i) => {
                  return (
                    <Box width='100%' display='flex' key={i} borderTop='1px dashed #ccc'>
                      <Box display='flex' flex='1 0 auto'>
                        <Box>
                          <Box>
                            <Box display='flex'>
                              <Typography variant='caption'>
                                {item.code}
                              </Typography>
                              <Typography variant='caption' style={{ fontWeight: 'bold', marginLeft: '8px' }}>
                                {item.descr.slice(0, 25)}
                              </Typography>
                            </Box>
                          </Box>
                          <Box>
                            <Typography variant='caption' style={{ color: blue[500] }}>
                              {`sn ${item.sn}`}
                            </Typography>
                          </Box>
                        </Box>
                      </Box>
                      <Box width={100} minWidth={100} textAlign='center'>
                        <Box>
                          <Typography variant='caption' style={{ fontWeight: 'bold' }}>
                            {item.days}
                          </Typography>
                        </Box>
                        <Box>
                          <Typography variant='caption'>
                            days
                          </Typography>
                        </Box>
                      </Box>
                    </Box>
                  );
                })}
              </Box>
            </AccordionDetails>
            <Divider />
          </Accordion>
        );
      })}

    </div>
  );

};

export default Consignment;
