/* eslint-disable no-restricted-globals */
import React from 'react';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import blue from '@material-ui/core/colors/blue';
import { daysBetweenToday2 } from 'lib/dates';
import StockConfirm from './StockConfirm';

const UnsoldItem = props => {

  const { item, useStockConfirm, email } = props;

  const diff = item.shpd ? daysBetweenToday2(item.shpd) : '';

  const vale = item.valr && item.exch ? Math.ceil(item.valr / item.exch) : '';
  const valef = vale.toLocaleString('ru-RU', { minimumFractionDigits: 0 });
  return (
    <Box width='100%' borderBottom='1px dashed #ddd'>
      <Box width='100%' display='flex'>
        <Box width='70%'>
          <Box>
            <Box>
              <Typography variant='caption'>
                {item.pcde}
              </Typography>
              <Typography variant='caption' style={{ fontWeight: 'bold', marginLeft: '3px' }}>
                {item.pdsc.slice(0, 20)}
              </Typography>
            </Box>
            <Box>
              <Typography variant='caption' style={{ color: blue[500] }}>
                {`sn ${item.psn}`}
              </Typography>
            </Box>
          </Box>
        </Box>
        <Box width='15%' textAlign='center'>
          <Box>
            <Typography variant='caption' style={{ fontWeight: 'bold' }}>
              {valef}
            </Typography>
          </Box>
          <Box>
            <Typography variant='caption'>
              €
            </Typography>
          </Box>
        </Box>
        <Box width='15%' textAlign='center'>
          <Box>
            <Typography variant='caption' style={{ fontWeight: 'bold' }}>
              {diff}
            </Typography>
          </Box>
          <Box>
            <Typography variant='caption'>
              days
            </Typography>
          </Box>
        </Box>
      </Box>
      {useStockConfirm &&
        <StockConfirm record={item} email={email} />
      }
    </Box>
  );

};

export default UnsoldItem;
