import React, { useState} from 'react';
import Swipeable from 'react-swipeable';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import ChartIcon from '@material-ui/icons/Assessment';
import LeftIcon from '@material-ui/icons/ArrowLeft';
import RightIcon from '@material-ui/icons/ArrowRight';
import Select from 'components/select';
import Loading from 'components/loading';
import Availability from './Availability';
import AvailabilityDetailsDialog from './AvailabilityDetailsDialog';
import Dealer from './Dealer';
import ChartDialog from './ChartDialog';
import Consignment from './Consignment';
import ConsignmentModel from './ConsignmentModel';
import UnsoldModel from './UnsoldModel';

const useStyles = makeStyles(() => ({
  iconButton: {
    margin: 0,
  },
}));

const Content = props => {

  const {
    pages,
    page,
    setPage,
    models,
    model,
    setModel,
    swiped,
    data,
    details,
    setDetails,
    email,
    stockBranchValue,
    unsoldValue,
    country,
  } = props;

  const [openCharts, setOpenCharts] = useState(false);

  const classes = useStyles();

  return (
    <Box maxWidth='600px' margin='0 auto' width='100%'>
      <AvailabilityDetailsDialog open={Boolean(details)} details={details} setDetails={setDetails} />
      <ChartDialog open={openCharts} setOpen={setOpenCharts} />
      <Box p={1} width='100%' minWidth='350px'>
        {pages.length === 0 &&
          <Loading big={false} />
        }
        {pages.length > 0 && !model &&
          <Box display='flex' alignItems='center' width='100%' flex='1 0 auto'>
            <IconButton
              aria-label='nav'
              className={classes.iconButton}
              onClick={
                () => {
                  swiped(undefined, -100);
                }
              }
            >
              <LeftIcon fontSize='small' />
            </IconButton>
            <Select
              width='100%'
              options={pages}
              placeholder='Location'
              option={page}
              onSelectHandler={
                val => {
                  setPage(val);
                }
              }
            />
            <IconButton
              aria-label='nav'
              className={classes.iconButton}
              onClick={
                () => {
                  swiped(undefined, 100);
                }
              }
            >
              <RightIcon fontSize='small' />
            </IconButton>
          </Box>
        }
      </Box>
      <Box p={1} width='100%' minWidth='350px'>
        {models.length === 0 &&
          <Loading big={false} />
        }
        {models.length > 0 &&
          <Select
            options={models}
            placeholder='Model'
            isClearable
            option={model}
            onSelectHandler={
              val => {
                setModel(val);
              }
            }
          />
        }
      </Box>
      {!model &&
        <Swipeable
          flickThreshold={5}
          onSwiped={swiped}
          style={{ flex: '1 0 auto', minHeight: '100vh' }}
        >
          {data && page.value &&
            <Box minHeight='100px' width='100%'>
              {page.value === 'mg' &&
                <Box p={1} display='flex' alignItems='center'>
                  <Box>
                    <Box>
                      <Typography>
                        {`Склад филиала ${stockBranchValue.toLocaleString('ru-RU', { minimumFractionDigits: 0 })} €`}
                      </Typography>
                    </Box>
                    <Box>
                      <Typography>
                        {`Склады дилеров ${unsoldValue.toLocaleString('ru-RU', { minimumFractionDigits: 0 })} €`}
                      </Typography>
                    </Box>
                  </Box>
                  <Box ml={2} display='flex' alignItems='center'>
                    <IconButton
                      aria-label='open-chart'
                      color='primary'
                      onClick={
                        () => {
                          setOpenCharts(true);
                        }
                      }
                    >
                      <ChartIcon />
                    </IconButton>
                  </Box>
                </Box>
              }
              {page.value === 'mg' &&
                <Availability data={data.mg} details={details} setDetails={setDetails} />
              }
              {page.value === 'cons' &&
                <Consignment data={data.cons.data} />
              }
              {page.value !== 'mg' && page.value !== 'cons' &&
                <Dealer data={data[page.value]} email={email} unsoldValue={unsoldValue} country={country} />
              }
            </Box>
          }
        </Swipeable>
      }
      {model &&
        <Availability data={data.mg} details={details} setDetails={setDetails} model={model} />
      }
      {model &&
        <ConsignmentModel data={data.cons.data} model={model} />
      }
      {model &&
        <UnsoldModel data={data} model={model} />
      }
    </Box>
  );
};

export default Content;
