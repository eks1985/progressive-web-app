import React from 'react';
import Box from '@material-ui/core/Box';
import EuroIcon from '@material-ui/icons/Euro';
import { green } from '@material-ui/core/colors';
import { blue } from '@material-ui/core/colors';
import { orange } from '@material-ui/core/colors';
import { deepPurple } from '@material-ui/core/colors';

const Brick = props => {

  const { status } = props;

  if (status === 'stock') {
    return (
      <EuroIcon style={{ color: blue[500], fontSize: '14px' }} />
    );
  }

  if (status === 'way') {
    return (
      <EuroIcon style={{ color: orange[500], fontSize: '14px' }} />
    );
  }

  if (status === 'po') {
    return (
      <EuroIcon style={{ color: deepPurple[500], fontSize: '14px' }} />
    );
  }

  if (status === 'so') {
    return (
      <EuroIcon style={{ color: green[500], fontSize: '14px' }} />
    );
  }

  console.log('status', status);

  return null;

};

const Content = props => {

  const { summary } = props;

  const bricksStock = [];
  if (summary.blockStock) {
    for (let index = 0; index < summary.blockStock; index++) {
      bricksStock.push(index);
    }
  }

  const bricksWay = [];
  if (summary.blockWay) {
    for (let index = 0; index < summary.blockWay; index++) {
      bricksWay.push(index);
    }
  }

  const bricksPO = [];
  if (summary.blockPO) {
    for (let index = 0; index < summary.blockPO; index++) {
      bricksPO.push(index);
    }
  }

  const bricksSO = [];
  if (summary.blockSO) {
    for (let index = 0; index < summary.blockSO; index++) {
      bricksSO.push(index);
    }
  }

  return (
    <Box width='100%' display='flex' alignItems='flex-start'>
      <Box width='50%' borderRight='1px solid #ccc' display='flex' flexWrap='wrap' pr={2}>
        {
          bricksStock.map(ind => {
            return <Brick key={ind} status='stock' />;
          })
        }
        {
          bricksWay.map(ind => {
            return <Brick key={ind} status='way' />;
          })
        }
        {
          bricksPO.map(ind => {
            return <Brick key={ind} status='po' />;
          })
        }
      </Box>
      <Box width='50%' display='flex' flexWrap='wrap' pl={2}>
        {
          bricksSO.map(ind => {
            return <Brick key={ind} status='so' />;
          })
        }
      </Box>
    </Box>
  );
};

export default Content;
