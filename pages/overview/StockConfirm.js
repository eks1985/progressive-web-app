/* eslint-disable no-restricted-globals */
import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import axios from 'axios';
import date from 'date-and-time';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import Loading from 'components/loading';
import StockConfirmHistory from './StockConfirmHistory';

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
}));


const StockConfirm = props => {

  const { record, url, email } = props;
  const [progress, setProgress] = useState(false);
  const [cnfhist, setCnfhist] = useState(record.cnfhist || '');
  const [historyOpen, setHistoryOpen] = useState(false);
  const [toDelete, setToDelete] = useState(false);

  const classes = useStyles();

  const lastCnfDate = cnfhist && cnfhist.length > 0 ? cnfhist[cnfhist.length - 1].d : false;
  const lastCnfUser = cnfhist && cnfhist.length > 0 ? cnfhist[cnfhist.length - 1].u : false;

  let stockConfirmed = false;
  if (lastCnfDate) {
    const d = new Date(lastCnfDate);
    const today = new Date();
    const diff = date.subtract(today, d).toDays();
    stockConfirmed = lastCnfDate && diff < 30;
  }

  async function removeConfirmation(toDelete) {

    const { _id, ...other } = record;
    other.cnfhist = other.cnfhist ? other.cnfhist.filter((item, ind) => item.d !== toDelete) : [];
    other.cnfdate = other.cnfhist.length > 0 ? other.cnfhist[other.cnfhist.length - 1].d : null;
    const result = await axios.post(url,
      { collection: 'prd-reg-ru', id: _id, payload: other },
    );
    if (result.status === 200) {
      setToDelete(false);
      setCnfhist(other.cnfhist);
      setProgress(false);
    }
  }
  useEffect(() => {
    if (toDelete !== false) {
      setProgress(true);
      removeConfirmation(toDelete);
    }
  }, [toDelete]);


  return (
    <Box display='flex' flexDirection='column' className={classes.root}>
      {progress && <Loading />}
      {!stockConfirmed &&
        <Box>
          <Box
            size='small'
            color='primary'
            onClick={
              () => {

                setProgress(true);

                async function updateProduct() {

                  const { _id } = record;
                  const cnfhistCopy = cnfhist ? [...cnfhist] : [];
                  const today = new Date();
                  const newCnfDate = date.format(today, 'YYYY-MM-DD');
                  cnfhistCopy.push({ d: newCnfDate, u: email });
                  const result = await axios.post(url,
                    { collection: 'prd-reg-ru', id: _id, payload: { cnfhist: cnfhistCopy, cnfdate: newCnfDate } },
                  );
                  if (result.status === 200) {
                    setProgress(false);
                    setCnfhist(cnfhistCopy);
                  }

                }

                updateProduct();

              }
            }
          >
            <Typography color='primary' variant='caption' style={{ cursor: 'pointer' }}>
              Нет подтверждения
            </Typography>
          </Box>
        </Box>
      }
      {cnfhist && cnfhist.length > 0 &&
        <Box className={classes.root} display='flex'>
          <Typography variant='caption'>
            <Link
              href='#'
              onClick={
                () => {
                  setToDelete(false);
                  setHistoryOpen(true);
                }
              }
            >
              {`Остатки подтверждены на ${lastCnfDate} / ${lastCnfUser}`}
            </Link>
          </Typography>
        </Box>
      }
      <StockConfirmHistory
        open={historyOpen}
        setOpen={setHistoryOpen}
        cnfhist={cnfhist}
        setProgress={setProgress}
        setToDelete={setToDelete}
        progress={progress}
      />
    </Box>
  );

};

StockConfirm.defaultProps = {
  url: 'https://webhooks.mongodb-realm.com/api/client/v2.0/app/mgd-ilrht/service/http/incoming_webhook/data-create?secret=L0sS3Bz1mdCm6Kuz',
};

export default StockConfirm;
