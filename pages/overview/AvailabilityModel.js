import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
// import { green } from '@material-ui/core/colors';
import AvailabilityChart from './AvailabilityModelChart';
import AvailabilityAssignSumm from './AvailabilityAssignSumm';
// import AvailabilityDetails from './AvailabilityDetails';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.primary,
  },
  icon: {
    verticalAlign: 'bottom',
    height: 20,
    width: 20,
  },
  details: {
    alignItems: 'center',
    padding: '4px',
  },
  pad: {
    marginRight: 20,
  },
}));

const AvailabilityModel = props => {

  const { data, setDetails, title } = props;
  const { _id, summary, items } = data;

  const [expanded, setExpanded] = React.useState(false);

  const handleChange = () => {
    setExpanded(!expanded);
  };

  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Accordion square expanded={expanded} onChange={handleChange}>
        <AccordionSummary
          aria-controls='panel1c-content'
          id='panel1c-header'
        >
          <div className={classes.pad}>
            <ExpandMoreIcon />
          </div>
          <Typography className={classes.secondaryHeading}>{title || _id}</Typography>
        </AccordionSummary>
        <AccordionDetails className={classes.details}>
          <Box display='flex' flexDirection='column' width='100%'>
            {expanded &&
              <AvailabilityChart summary={summary} />
            }
            {expanded && items.map(item => {
              return (
                <Box key={item.title} mt={3} width='100%'>
                  <Box pl={2}>
                    <Typography style={{ color: '#E1001A' }}>
                      {item.title}
                    </Typography>
                  </Box>
                  <AvailabilityAssignSumm {...item.summary} />
                  <Box width='100%' margin='0 auto' textAlign='center'>
                    <Button
                      size='small'
                      variant='outlined'
                      onClick={
                        () => {
                          setDetails(item.details);
                        }
                      }
                    >
                      Детально
                    </Button>
                  </Box>
                  {/* <AvailabilityDetails items={item.details} /> */}
                </Box>
              );
            })}
          </Box>
        </AccordionDetails>
        <Divider />
      </Accordion>
    </div>
  );

};

export default AvailabilityModel;
