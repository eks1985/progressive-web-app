import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useStore } from 'lib/store';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import IconPhoto from '@material-ui/icons/Photo';
import IconVideo from '@material-ui/icons/Subscriptions';
import IconOffer from '@material-ui/icons/ContactMail';
import IconLeaflet from '@material-ui/icons/ChromeReaderMode';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';

const useStyles = makeStyles(theme => ({
  heading: {
    fontSize: theme.typography.pxToRem(16),
    fontWeight: theme.typography.fontWeightRegular,
    height: '48px',
    display: 'flex',
    alignItems: 'center',
    paddingLeft: '12px',
    lineHeight: '1.2',
  },
  body: {
    fontSize: theme.typography.pxToRem(14),
    fontWeight: theme.typography.fontWeightRegular,
    color: '#555',
    marginBottom: '8px',
    lineHeight: '1.2',
  },
  summary: {
    paddingLeft: '8px',
    height: '72px',   
  },
  avatar: {
    backgroundColor: '#E1001A',
  }
}));

const getOffersPathByCountry = country => {
  if (country === 0) {
    return 'https://drive.google.com/drive/folders/1fLEMTIQKFzG2Spdrdo7HFaf8x4dkvCtc?usp=sharing';
  }
  if (country === 1) {
    return 'https://drive.google.com/drive/folders/1FUufFZlkI6P_E6UnCwzFkIgr_wnoZmEO?usp=sharing';
  }
  if (country === 2) {
    return 'https://drive.google.com/drive/folders/1cy5L3eIxP-SQxG__N49E1hmMmfVgxmPe?usp=sharing';
  }
  if (country === 3) {
    return 'https://drive.google.com/open?id=1236TlCGrWbDdpgOOB84-ntVApVGhXdKX';
  }
  return '';
};

const medialib = () => {
  const classes = useStyles();
  const store = useStore();
  const { country } = store;
  return (
    <Box maxWidth={600} margin='0 auto'>
      <Accordion square expanded>
        <AccordionSummary expandIcon={<ExpandMoreIcon />} className={classes.summary}>
          <Avatar className={classes.avatar}>
            <IconPhoto />
          </Avatar>
          <Typography className={classes.heading}>Фотографии техники</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Button
            variant='contained'
            color='primary'
            onClick={
              () => {
                window.open('https://drive.google.com/drive/folders/1uIWpZuyG2VmuAY2K6dgxtwVxH2a00PwW?usp=sharing');
              }
            }
          >
            Перейти
          </Button>
        </AccordionDetails>
      </Accordion>
      <Accordion expanded>
        <AccordionSummary expandIcon={<ExpandMoreIcon />} className={classes.summary}>
          <Avatar className={classes.avatar}>
            <IconVideo />
          </Avatar>
          <Typography className={classes.heading}>Видео техники в работе</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Button
            variant='contained'
            color='primary'
            onClick={
              () => {
                window.open('https://drive.google.com/drive/folders/1vu7Zp8NWfucvecx4HPG3GBSR30gLO8Dz?usp=sharing');
              }
            }
          >
            Перейти
          </Button>
        </AccordionDetails>
      </Accordion>
      <Accordion expanded>
        <AccordionSummary expandIcon={<ExpandMoreIcon />} className={classes.summary}>
          <Avatar className={classes.avatar}>
            <IconOffer />
          </Avatar>
          <Typography className={classes.heading}>Коммерческие предложения клиентам</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Box>
            <Typography variant='body2'>
              Все предложения представлены в pdf и word вариантах
            </Typography>
            <Typography variant='body2'>
              Вы можете сразу отправлять клиентам предложение в pdf версии, либо скачать word файл и модифицировать по своему усмотрению
            </Typography>
            <Box mt={2}>
              <Button
                variant='contained'
                color='primary'
                onClick={
                  () => {
                    window.open(getOffersPathByCountry(country));
                  }
                }
              >
                Перейти
              </Button>
            </Box>
          </Box>
        </AccordionDetails>
      </Accordion>
      <Accordion expanded>
        <AccordionSummary expandIcon={<ExpandMoreIcon />} className={classes.summary}>
          <Avatar className={classes.avatar}>
            <IconLeaflet />
          </Avatar>
          <Typography className={classes.heading}>Рекламные каталоги</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Button
            variant='contained'
            color='primary'
            onClick={
              () => {
                window.open('https://drive.google.com/drive/folders/1toxqIvK-SRlN8e3uALP-goZUk8KYtEEu?usp=sharing');
              }
            }
          >
            Перейти
          </Button>
        </AccordionDetails>
      </Accordion>
      <Paper square>
        <Box p={2}>
          <Typography variant='body2'>
            Вы также можете сделать любые файлы доступными оффлайн на ваших мобильных устройствах, воспользуйтесь функцией оффлайн-доступ (у вас должно быть установлено мобильное приложение Google Drive)
          </Typography>
        </Box>
      </Paper>
    </Box>
  );
};

export default medialib;
