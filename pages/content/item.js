import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import ReactMarkdown from 'react-markdown';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Picture from './picture';
import AssignContent from './assign-content';
import VideoNative from 'components/video-native';

const useStyles = makeStyles(theme => ({
  heading: {
    fontSize: theme.typography.pxToRem(14),
    fontWeight: theme.typography.fontWeightRegular,
    color: theme.palette.primary.main,
  },
  html: {
    color: '#555',
    fontSize: '12px',
    textAlign: 'justify',
  },
}));

const item = props => {

  const classes = useStyles();

  const { item, sMode, inline, renderClient, setItem, setOpen, update, selectHandler } = props;
  // id
  // inl   - инлайн
  // nt    - примечание
  // tt    - заголовок
  // tx    - текст
  // pc    - картинка
  // ph    - высота картинки
  // vd    - видео линк
  // txtp  - сначала картинка, потом текст
  // tp    - тип элемента: видео / text
  // mt    - margin top
  // tag   - таг
  
  const { id, inl, mt, tt, tp, pc, tx, vd, txtp, tag, mod } = item;

  const content = [];
  if (tp === 'video') {
    // content.push(
    //   <VideoNative key='video' d={{ videoId: vd, mt, tx }} />
    // );
    return <VideoNative key='video' d={{ videoId: vd, mt, tx }} />;
  }
  if (tp === 'text') {
    content.push(
      <Picture key='pict' picture={pc} id={id} />
    );
  }
  content.push(
    <div key='txt' className={classes.html}>
      <ReactMarkdown>
        {tx}
      </ReactMarkdown>
    </div>
  );
  if (txtp) {
    content.reverse();
  }
  if (tag && !renderClient) {
    content.push(
      <Box key='tag'>
        <Typography variant='subtitle1'>
          {`#${tag}`}
        </Typography>
      </Box>
    );
  }

  if (inline) {
    return (
      <Box width='100%' display='flex' flexDirection='column' justifyContent='center' alignItems='center'>
        {content}
      </Box>
    );
  } 

  if (renderClient) {
    return (
      <Box width='100%' display='flex' flexDirection='column' justifyContent='center' alignItems='center'>
        <Typography className={classes.heading}>
          {tt}
        </Typography>
        {content}
      </Box>
    );
  }

  return (
    <Accordion>
      <AccordionSummary expandIcon={<ExpandMoreIcon />}>
        <Typography className={classes.heading}>
          {tt}
        </Typography>
      </AccordionSummary>
      <AccordionDetails>
        <Box width='100%' display='flex' flexDirection='column' justifyContent='center' alignItems='center'>
          {content}
          <Box display='flex' alignItems='center' justifyContent='flex-end'>
            {mod &&
              <Button
                onClick={update}
                variant='contained'
                color='primary'
              >
                Сохранить
              </Button>
            }
            {sMode && !mod &&
              <AssignContent content={item} selectHandler={selectHandler} />
            }
          </Box>
          <Box my={2}>
            <Button
              variant='outlined'
              onClick={
                () => {
                  setItem(item);
                  setOpen(true);
                }
              }>
                Редактировать
              </Button>
          </Box>
        </Box>
      </AccordionDetails>
    </Accordion>
  );
};

export default item;


