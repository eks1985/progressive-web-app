import React from 'react';
import Box from '@material-ui/core/Box';
import Select from 'components/select';

const titleSelect = props => {
  
  const { titlesIndex, title, setTitle } = props;

  const options = titlesIndex.map(item => ({ value: item, label: item }));

  return (
    <Box p={1}>
      <Select
        options={options}
        placeholder='Заголовок'
        value={title}
        isClearable
        onSelectHandler={
          sel => {
            setTitle(sel);
          }
        }
      />
    </Box>
  );

};

export default titleSelect;
