import React, { useEffect, useState } from 'react';
import { useStore } from 'lib/store';
import Box from '@material-ui/core/Box';
import List from './list';
import TagSelect from './tag-select';
import TitleSelect from './title-select';
import Create from './create';
import ItemEditDialog from './item-edit-dialog';
import ItemEdit from './item-edit';
import { updateContent } from 'lib/api/content';

const content = props => {
  
  const { data: { tagsIndex, titlesIndex }, sMode, url_create, mutate, selectHandler } = props;

  const store = useStore();

  const [items, setItems] = useState(props.data.items);
  const [tag, setTag] = useState(undefined);
  const [title, setTitle] = useState(undefined);
  const [item, setItem] = useState(false);
  const [dialogOpen, setDialogOpen] = useState(false);
  
  useEffect(() => {
    setItems(props.data.items);
  }, [props.data]);

  const edit = (prop, value) => {
    const elem = items.filter(elem => elem.id === item.id)[0];
    const elemUpdated = { ...elem, [prop]: value, mod: true };
    const updated = items.map(elem => {
      if (elem.id === item.id) {
        return elemUpdated;
      } 
      return elem;
    });
    setItem(elemUpdated);
    setItems(updated);
  };

  const update = () => {
    store.setLoading(true);
    const { mod, ...other } = item;
    updateContent(url_create, item.id, other);
    setTimeout(() => {
      store.setLoading(false);
      mutate();
    }, 500);
  };

  return (
    <Box width='100%' margin='0 auto'>
      {sMode &&
        <Create mutate={mutate} sMode={sMode}/>
      }
      <TagSelect
        tagsIndex={tagsIndex}
        tag={tag}
        setTag={setTag}
      />
      <TitleSelect
        titlesIndex={titlesIndex}
        title={title}
        setTitle={setTitle}
      />
      <List
        tag={tag}
        title={title}
        items={items}
        sMode={sMode}
        edit={edit}
        update={update}
        setItem={setItem}
        setOpen={setDialogOpen}
        selectHandler={selectHandler}
      />
      {!sMode && !dialogOpen &&
        <Create mutate={mutate} sMode={sMode}/>
      }
      <ItemEditDialog open={dialogOpen} setOpen={setDialogOpen}>
        <ItemEdit
          item={item}
          edit={edit}
        />
      </ItemEditDialog>
    </Box>
  );
};

content.defaultProps = {
  url_create: 'https://webhooks.mongodb-realm.com/api/client/v2.0/app/mgd-ilrht/service/http/incoming_webhook/data-create?secret=L0sS3Bz1mdCm6Kuz',
};

export default content;
