import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Radio from '@material-ui/core/Radio';

const useStyles = makeStyles(theme => ({
  label: {
    color: '#555',  
  }
}));

const contentType = props => {
  
  const { tp, edit } = props;

  const classes = useStyles();
  
  return (
    <Box display='flex' alignItems='center' flexWrap='wrap'>
      <Box display='flex' alignItems='center'>
        <Radio
          checked={tp === 'text'}
          onChange={
            () => {
              edit('tp', 'text');
            }
          }
          value='contentTypeText'
          name='contentTypeText'
          aria-label='contentTypeText'
        />
        <div className={classes.label}>Текст</div>
      </Box>
      <Box display='flex' alignItems='center'>
        <Radio
          checked={tp === 'video'}
          onChange={
            () => {
              edit('tp', 'video');
            }
          }
          value='contentTypeVideo'
          name='contentTypeVideo'
          aria-label='contentTypeVideo'
        />
        <div className={classes.label}>Видео</div>
      </Box>
    </Box>
  );
};

export default contentType;

