import React from 'react';
import Box from '@material-ui/core/Box';
import Switch from '@material-ui/core/Switch';
import FormLabel from '@material-ui/core/FormLabel';

const toggleTxtPrior= props => {

  const { txtp, edit } = props;
  
  return (
    <Box display='flex' alignItems='center'>
      <Switch
        color='secondary'
        checked={txtp || false}
        value='txtp'
        onChange={
          e => {
            edit('txtp', e.target.checked);
          }
        }
      />
      <FormLabel>
        Текст -> Фото
      </FormLabel>
    </Box>
  );

};

export default toggleTxtPrior;



