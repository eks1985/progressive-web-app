import React from 'react';
import Item from './item';

const list = props => {

  const { items, tag, title, ...other } = props;

  let list = [...items];
  if (tag) {
    list = list.filter(item => item.tag === tag);
  }
  if (title) {
    list = list.filter(item => item.tt === title);
  }

  return (
    <div>
      {
        list.map(item => {
          return <Item key={item.id} item={item} {...other}/>;
        })
      }
    </div>
  );
};

export default list;

