import React from 'react';
import { baseUrl, secret } from 'lib/base';
import { useStore } from 'lib/store';
import DataProviderBatch from 'lib/data-batch';
import Box from '@material-ui/core/Box';
import Content from './content';

const prepareQuery = (props, country) => {
  const { ulr_content } = props;
  return {
    content: `${ulr_content}&country=${country}`,
  };
};

const content = props => {

  const store = useStore();
  const { appUser, country } = store;
  if (appUser === false || !appUser.internal) {
    return null;
  }

  const query = prepareQuery(props, country);

  return (
    <Box margin='0 auto' width='100%' maxWidth='900px'>
      <DataProviderBatch query={query} render={(data, mutate) => (
        <Content
          data={data.content}
          mutate={mutate}
          {...props}
        />
      )}/>
    </Box>
  );

};

content.defaultProps = {
  ulr_content: `${baseUrl}content${secret}`, 
};

export default content;