import React from 'react';
import Box from '@material-ui/core/Box';
import Select from 'components/select';

const tagSelect = props => {
  
  const { tagsIndex, tag, setTag } = props;

  const options = tagsIndex.map(item => ({ value: item, label: item }));

  return (
    <Box p={1}>
      <Select
        options={options}
        placeholder='Таг'
        value={tag}
        isClearable
        onSelectHandler={
          sel => {
            setTag(sel);
          }
        }
      />
    </Box>
  );

};

export default tagSelect;
