import React from 'react';
import { useStore } from 'lib/store';
import { makeStyles } from '@material-ui/core/styles';
import AddIcon from '@material-ui/icons/Add';
import Fab from '@material-ui/core/Fab';
import { createContent } from 'lib/api/content';

const useStyles = makeStyles(theme => ({
  root: {
    position: props=> props.sMode ? 'initial' : 'fixed',
    top: 40,
    right: 3,
    zIndex: '9999',
    textAlign: props=> props.sMode ? 'center' : 'initial',
    marginBottom: props=> props.sMode ? theme.spacing(2) : 'initial',
  },
}));

const create = props => {

  const { mutate, url_create, sMode } = props;
  const classes = useStyles({ sMode });
  const store = useStore();
  const { country } = store;

  const create = () => {
    createContent(url_create, country, mutate);
  }

  return (
    <div className={classes.root}>
      <Fab
        aria-label='create'
        size='small'
        onClick={create}
      >
        <AddIcon />
      </Fab>
    </div>
  );
};

create.defaultProps = {
  url_create: 'https://webhooks.mongodb-realm.com/api/client/v2.0/app/mgd-ilrht/service/http/incoming_webhook/data-create?secret=L0sS3Bz1mdCm6Kuz',
};

export default create;

