import React from 'react';
import Box from '@material-ui/core/Box';
import TextField from '@material-ui/core/TextField';

const tag = props => {

  const { tag, edit } = props;

  console.log('tag', tag);
  
  return (
    <Box display='flex' alignItems='center' justifyContent='center'>
      <TextField
        fullWidth
        id='tag'
        label='Тэг'
        margin='none'
        value={tag || ''}
        onChange={
          e => {
            edit('tag', e.target.value);
          }
        }
      />
    </Box>
  ) 

};

export default tag;

