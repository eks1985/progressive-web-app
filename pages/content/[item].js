import React from 'react';
import { useRouter } from 'next/router';
import { useStore } from 'lib/store';
import { baseUrl, secret } from 'lib/base';
import DataProviderBatch from 'lib/data-batch';
import Box from '@material-ui/core/Box';
import Item from './item';

const prepareQuery = (props, id) => {
  const { ulr_content } = props;
  return {
    content: `${ulr_content}&id=${id}`,
  };
};

const item = props => {

  const router = useRouter();
  const store = useStore();
  const { appUser } = store;
  if (appUser === false || !appUser.internal) {
    return null;
  }

  const query = prepareQuery(props, router.query.item);

  return (
    <Box margin='0 auto' width='100%' maxWidth='900px'>
      <DataProviderBatch query={query} render={(data, mutate) => (
        <Item
          item={data}
          mutate={mutate}
        />
      )}/>
    </Box>
  );

};

item.defaultProps = {
  url_content: `${baseUrl}content${secret}`, 
};

export default item;