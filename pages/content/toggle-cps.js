import React from 'react';
import Box from '@material-ui/core/Box';
import Switch from '@material-ui/core/Switch';
import FormLabel from '@material-ui/core/FormLabel';

const toggleCps = props => {

  const { cps, edit } = props;
  
  return (
    <Box display='flex' alignItems='center'>
      <Switch
        color='secondary'
        checked={cps || false}
        value='cps'
        onChange={
          e => {
            edit('cps', e.target.checked);
          }
        }
      />
      <FormLabel>
        Свернуто
      </FormLabel>
    </Box>
  );
};

export default toggleCps;
