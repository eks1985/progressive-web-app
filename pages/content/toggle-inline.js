import React from 'react';
import Box from '@material-ui/core/Box';
import Switch from '@material-ui/core/Switch';
import FormLabel from '@material-ui/core/FormLabel';

const toggleInl= props => {

  const { inl, edit } = props;
  
  return (
    <Box display='flex' alignItems='center'>
      <Switch
        color='secondary'
        checked={inl || false}
        value='inl'
        onChange={
          e => {
            edit('inl', e.target.checked);
          }
        }
      />
      <FormLabel>
        Инлайн
      </FormLabel>
    </Box>
  );

};

export default toggleInl;



