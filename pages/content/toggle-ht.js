import React from 'react';
import Box from '@material-ui/core/Box';
import Switch from '@material-ui/core/Switch';
import FormLabel from '@material-ui/core/FormLabel';

const toggleHtt= props => {

  const { htt, edit } = props;
  
  return (
    <Box display='flex' alignItems='center'>
      <Switch
        color='secondary'
        checked={htt || false}
        value='htt'
        onChange={
          e => {
            edit('htt', e.target.checked);
          }
        }
      />
      <FormLabel>
        Скрывать заголовок
      </FormLabel>
    </Box>
  );

};

export default toggleHtt;

