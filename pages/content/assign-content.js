import React from 'react';
import Button from '@material-ui/core/Button';

const assignContent = props => {
  const { selectHandler, content } = props;
  return (
    <Button
      variant='contained'
      color='primary'
      onClick={
        () => {
          selectHandler(content);
        }
      }
    >
      Выбрать
    </Button>
  );
};

export default assignContent;
