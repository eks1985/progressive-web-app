import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Picture from './picture';
import ToggleHt from './toggle-ht';
import ToggleInline from './toggle-inline';
import ToggleCps from './toggle-cps';
import ToggleTextPriority from './toggle-text-priority';
import Tag from './tag';
import ContentType from './content-type';

const itemEdit = props => {
  
  const { item: { id, tt, htt, tx, pc, ph, inl, cps, nt, tag, vd, tp, txtp }, edit } = props;

  return (
    <div style={{ paddingBottom: '56px' }}>
      <div>
        <Typography style={{ color: '#777' }} variant='caption' gutterBottom align='justify'>
          <a target='_blank' href={`https://console.firebase.google.com/project/mg-dealer/database/mg-dealer/data/content/${id}`}>
            {id}
          </a>
        </Typography>
      </div>
      <div>
        <ContentType edit={edit} tp={tp} id={id} vd={vd} />
      </div>
      <div>
        <TextField
          fullWidth
          multiline
          id='tt'
          label='Заголовок'
          InputProps={{
            style: { fontSize: '12px' },
          }}
          margin='dense'
          variant='outlined'
          value={tt}
          onChange={
            e => {
              edit('tt', e.target.value);
            }
          }
        />
      </div>
      {tp === 'video' &&
        <div className='mt-2'>
          Текст будет использован в качестве заголовка видео
        </div>
      }
      <div>
        <TextField
          fullWidth
          multiline
          rows={8}
          id='tt'
          label='Текст'
          InputProps={{
            style: { fontSize: '12px' },
          }}
          margin='dense'
          variant='outlined'
          value={tx}
          onChange={
            e => {
              edit('tx', e.target.value);
            }
          }
        />
      </div>
      {tp === 'text' &&
        <div>
          <ToggleHt id={id} htt={htt} edit={edit} />
        </div>
      }
      {tp === 'text' &&
        <div>
          <ToggleInline id={id} inl={inl} edit={edit} />
        </div>
      }
      {tp === 'text' &&
        <div>
          <ToggleTextPriority id={id} txtp={txtp} edit={edit} />
        </div>
      }
      {tp === 'text' &&
        <div>
          <ToggleCps id={id} cps={cps} edit={edit} />
        </div>
      }
      {tp === 'text' &&
        <Picture picture={pc} editMode id={id} edit={edit} />
      }
      {tp === 'text' &&
        <div>
          <TextField
            fullWidth
            id='ph'
            label='Высота картинки'
            margin='dense'
            value={ph || ''}
            onChange={
              e => {
                edit('ph', e.target.value);
              }
            }
          />
        </div>
      }
      {tp === 'text' &&
        <div>
          <TextField
            fullWidth
            id='ntc'
            label='Комментарий'
            margin='dense'
            value={nt || ''}
            onChange={
              e => {
                edit('nt', e.target.value);
              }
            }
          />
        </div>
      }
      {tp === 'video' &&
        <div>
          <TextField
            fullWidth
            id='vd'
            label='Youtube video id'
            margin='dense'
            value={vd || ''}
            onChange={
              e => {
                edit('vd', e.target.value);
              }
            }
          />
        </div>
      }
      <div>
        <Tag id={id} tag={tag} edit={edit} />
      </div>
    </div>
  );
};

export default itemEdit;

