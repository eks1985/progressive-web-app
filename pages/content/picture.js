import React from 'react';
import Box from '@material-ui/core/Box';
import CdnUpload from 'components/cdn-upload';

const Picture = ({ url }) => {
  const containerStyle = {
    marginTop: '4px',
    justifyContent: 'center',
    flex: '0 0 100%',
    minHeight: '200px',
    backgroundImage: `url("${url}")`,
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'contain',
    borderRadius: '4px',
    backgroundPosition: 'center',
  };
  return (
    <Box mb={2}>
      <Box display='flex'>
        <Box
          style={containerStyle}
          onClick={
            () => {
              const link = document.createElement('a');
              document.body.appendChild(link);
              link.href = url;
              link.target = '_blank';
              link.click();
            }
          }
        />
      </Box>
    </Box>
  );
};

const pictureComponent = props => {

  const { picture, editMode } = props;

  const handleAddPicture = picture => {
    const { edit } = props;
    edit('pc', picture);
  };

  return (
    <Box mt={2} width='100%'>
      {picture &&
        <Picture url={picture} />
      }
      {editMode &&
        [
          <hr key='separate'></hr>,
          <CdnUpload
            key='upload'
            {...props}
            handleAddPicture={handleAddPicture}
          />,
        ]
      }
    </Box>
  );
};

export default pictureComponent;

