import React from 'react';
import { useStore } from 'lib/store';
import { useRouter } from 'next/router';
import { makeStyles } from '@material-ui/core/styles';
import { translate } from 'lib/vacab';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import Box from '@material-ui/core/Box';
import OpenItemsIcon from 'components/icons/open-items.js';
import ProductionIcon from 'components/icons/efficiency.js';
import OverviewIcon from 'components/icons/planter.js';
import ChartIcon from 'components/icons/bar-chart.js';
import CompareIcon from 'components/icons/compare.js';
import PulseIcon from 'components/icons/heartbeat.js';
import PriceIcon from 'components/icons/price-tag.js';
import SalesIcon from 'components/icons/revenue.js';
import DealersIcon from 'components/icons/collaboration.js';
import POIcon from 'components/icons/po.js';
import ProductRegistrationIcon from 'components/icons/database1.js';
import WarrantyIcon from '@material-ui/icons/Build';
import CatalogIcon from '@material-ui/icons/Dashboard';
import CompetitorsIcon from '@material-ui/icons/CompareArrows';
import MediaIcon from '@material-ui/icons/Wallpaper';
import UsersIcon from '@material-ui/icons/People';

const useStyles = makeStyles(theme => ({
  container: {
    backgroundColor: theme.palette.background.paper,
  },
  list: {
    width: '100%',
  },
  item: {
    height: '56px',
    cursor: 'pointer',
  },
  iconColor: {
    backgroundColor: theme.palette.primary.main,
  },
  iconDisabledColor: {
    backgroundColor: '#999',
  },
}));

const Icon = props => {

  const { active, path, size } = props;
  const classes = useStyles();

  return (
    <ListItemIcon>
      <Avatar
        className={active ? classes.iconColor: classes.iconDisabledColor}
      >
        {path === '/pulse' &&
          <PulseIcon width={size} heigth={size} />  
        }
        {path === '/years-comparison' &&
          <CompareIcon width={size} heigth={size} />  
        }
        {path === '/charts' &&
          <ChartIcon width={size} heigth={size} />  
        }
        {path === '/price-list' &&
          <PriceIcon width={size} heigth={size} />  
        }
        {path === '/price-list-special' &&
          <PriceIcon width={size} heigth={size} />  
        }
        {path === '/dealers' &&
          <DealersIcon width={size} heigth={size} />  
        }
        {path === '/overview' &&
          <OverviewIcon width={size} heigth={size} />  
        }
        {path === '/sales' &&
          <SalesIcon width={size} heigth={size} />  
        }
        {path === '/open-items' &&
          <OpenItemsIcon width={size} heigth={size} />  
        }
        {path === '/production-plan' &&
          <ProductionIcon width={size} heigth={size} />  
        }
        {path === '/purchase-orders' &&
          <POIcon width={size} heigth={size} />  
        }
        {path === '/product-reg' &&
          <ProductRegistrationIcon width={size} heigth={size} />  
        }
        {path === '/warranty' &&
          <WarrantyIcon width={size} heigth={size} />  
        }
        {path === '/catalog' &&
          <CatalogIcon width={size} heigth={size} />  
        }
        {path === '/competitors' &&
          <CompetitorsIcon width={size} heigth={size} />  
        }
        {path === '/medialib' &&
          <MediaIcon width={size} heigth={size} />  
        }
        {path === '/users' &&
          <UsersIcon width={size} heigth={size} />  
        }
        {path === '/customer' &&
          <DealersIcon width={size} heigth={size} />  
        }
      </Avatar>
    </ListItemIcon>
  );
};

const Item = props => {

  const classes = useStyles();
  const { text, path, active, push } = props;
  
  return (
    <ListItem
      classes={{
        root: classes.item,
      }}
      onClick={
        () => {
          if (path === '/customer') {
            window.location = 'https://cis-maschio.ru/customer';
          } else if (active) {
            push(path);
          }
        }
      }
    >
      <Icon {...props} />
      <ListItemText primary={text} />
    </ListItem>
  );
};

Item.defaultProps = {
  active: true,
  size: 24,
};

const company = () => {

  const store = useStore();
  const router = useRouter();
  const classes = useStyles();
  const { lang } = store;
  const { push } = router;
  const { appUser } = store;
  if (appUser === false || !appUser.internal) {
    return null;
  }

  return (
    <Box margin='0 auto' mt={2} maxWidth='900px' display='flex' alignItems='center' flexDirection='column' width='100%' className={classes.container}>
      <List className={classes.list}>
        <Item
          text={translate('Пульс компании', lang)}
          path='/pulse'
          active={false}
          size={30}
          push={push}
        />
        <Item
          text={translate('Сравнение по годам', lang)}
          path='/years-comparison'
          active={false}
          size={26}
          push={push}
        />
        <Item
          text={translate('Графики', lang)}
          path='/charts'
          active
          size={24}
          push={push}
        />
        <Item
          text={translate('Прайс-лист', lang)}
          path='/price-list'
          active
          size={32}
          push={push}
        />
        <Item
          text={translate('Прайс-лист машин с наработкой', lang)}
          path='/price-list-special'
          active
          size={32}
          push={push}
        />
        <Item
          text={translate('Дилеры', lang)}
          path='/dealers'
          active
          size={30}
          push={push}
        />
        <Item
          text={translate('Машины на складах', lang)}
          path={'/overview'}
          active
          size={32}
          push={push}
        />
        <Item
          text={translate('Продажи', lang)}
          path='/sales'
          active={false}
          size={24}
          push={push}
        />
        <Item
          text={translate('Дебиторская задолженность', lang)}
          path='/open-items'
          size={30}
          push={push}
        />
        <Item
          text={translate('План производства', lang)}
          path='/production-plan'
          active
          size={30}
          push={push}
        />
        <Item
          text={translate('Заказы поставщику', lang)}
          path='/purchase-orders'
          active
          size={24}
          push={push}
        />
        <Item
          text={translate('База регистрации машин', lang)}
          path='/product-reg'
          active
          size={24}
          push={push}
        />
        <Item
          text={translate('Гарантия', lang)}
          path='/warranty'
          service
          active
          size={24}
          push={push}
        />
        <Item
          text={translate('Каталог', lang)}
          path='/catalog'
          catalog
          active
          size={24}
          push={push}
        />
        <Item
          text={translate('Конкуренты', lang)}
          path='/competitors'
          competitors
          active
          size={24}
          push={push}
        />
        <Item
          text={translate('Медиа библиотека', lang)}
          path='/medialib'
          medialib
          active
          size={24}
          push={push}
        />
        <Item
          text={translate('Пользователи', lang)}
          path='/users'
          users
          active
          size={24}
          push={push}
        />
        <Item
          text={translate('Сервисы 360', lang)}
          path='/customer'
          users
          active
          size={24}
          push={push}
        />
      </List>
    </Box>
  );

};

export default company;

