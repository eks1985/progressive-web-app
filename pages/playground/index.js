import React, { useState } from 'react';
import Filters from 'components/filters';
import Box from '@material-ui/core/Box';

const FILTERS = [
  {
    name: 'foo',
    label: 'Foo',
    items: [{ value: 1, label: 'one'}, { value: 2, label: 'two'}, { value: 3, label: 'three'}],
    value: [],
    mult: true,
  },
  {
    name: 'bar',
    label: 'Bar',
    items: [{ value: 1, label: 'one'}, { value: 2, label: 'two'}, { value: 3, label: 'three'}],
    value: [],
    mult: true,
  },
  {
    name: 'baz',
    label: 'Baz',
    items: [{ value: 1, label: 'one'}, { value: 2, label: 'two'}, { value: 3, label: 'three'}],
    value: [],
    mult: true,
  }
];

const comp = () => {

  const [filters, setFilters] = useState(FILTERS);

  const filtersEdit = action => {
    const {  index, value } = action;
    const copy = [...filters];
    copy[index].value = value;
    setFilters(copy);
  };

  const filtersApply = () => {
    console.log('---filtersApply');
    filtersReset();
  };

  const filtersReset = () => {
    console.log('---filtersReset');
  };

  const actions={
    filtersEdit,
    filtersApply,
    filtersReset, 
  }
  
  console.log('render');

  return (
    <Box maxWidth={600} margin='0 auto'>
      <Filters filters={FILTERS} {...actions} />
    </Box>
  );
};

export default comp;