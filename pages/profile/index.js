import React from 'react';
import { useAuth } from 'lib/auth';
import { useStore } from 'lib/store';
import { useRouter } from 'next/router';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Profile from 'components/profile';

const profile = () => {

  const auth = useAuth();
  const store = useStore();
  const router = useRouter();

  const { appUser } = store;
  
  if (!appUser) {
    return null;
  }

  const { email, name } = appUser;

  return (
    <Box margin='0 auto' mt={5} width='100%'>
      <Box textAlign='center'>
        <Box>
          <Typography variant='subtitle1'>
            {name}
          </Typography>
        </Box>
        <Box>
          <Typography variant='subtitle1'>
            {email}
          </Typography>
        </Box>
      </Box>
      <Box mt={2} textAlign='center'>
        <Button
          variant='contained'
          color='primary'
          onClick={
            () => {
              auth.signout();
              router.push('/');
            }
          }
        >
          Выйти
        </Button>
      </Box>
      <Profile user={appUser} />
    </Box>
  );

};

export default profile;