import React from 'react';
import { useRouter } from 'next/router';
import { baseUrl, secret } from 'lib/base';
import { useStore } from 'lib/store';
import DataProviderBatch from 'lib/data-batch';
import Box from '@material-ui/core/Box';
import Content from './content';

const prepareQuery = (url, model, permiss, country) => {
  const vis = `&vis${permiss.catalog.visManager ? '>0' : '=2'}`;
  return {
    catalog: `${url}&filters=p{m:${model}}${vis}&country=${country}`,
  };
};

const products = props => {

  const router = useRouter();
  const store = useStore();
  const { permiss, country } = store;

  if (!permiss || !store) {
    return null;
  }

  const query = prepareQuery(props.url, router.query.model, permiss, country);

  return (
    <Box margin='0 auto' width='100%' maxWidth='1200px'>
      <DataProviderBatch query={query} render={(data, mutate) => (
        <Content
          products={data.catalog.products}
          mutate={mutate}
        />
      )}/>
    </Box>
  );

};

products.defaultProps = {
  url: `${baseUrl}catalog${secret}lists=p`,
};

export default products;