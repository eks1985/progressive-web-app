import React from 'react';
import Box from '@material-ui/core/Box';
import ProductCard from 'components/product-card';

const content = props => {

  const { products } = props;

  if (!products || products.length === 0) {
    return null;
  }

  return (
    <Box maxWidth='1200px' margin='0 auto' display='flex' flexWrap='wrap' justifyContent='center' alignItems='flex-start'>
      {products.map((item, ind) => <ProductCard key={ind} product={item} />)}
    </Box>
  );

};

export default content;