import React from 'react';
import { useStore } from 'lib/store';
import Box from '@material-ui/core/Box';
import Users from 'components/users';
import DataProviderBatch from 'lib/data-batch';
import { baseUrl, secret } from 'lib/base';

const prepareQuery = (url, customerGuid, country) => {
  return {
    items: `${url}token=${customerGuid}&country=${country}`,
  };
};

const users = props => {

  const store = useStore();
  const { appUser, country } = store;
  if (appUser === false || !appUser.internal) {
    return null;
  }
  const { customerGuid } = appUser;
  const query = prepareQuery(props.url, customerGuid, country);

  return (
    <Box margin='0 auto' width='100%' maxWidth='900px'>
      <DataProviderBatch query={query} render={data => {
        if (!data || data.items.length === 0) {
          return null;
        }
        // console.log('users', data.items);
        return <Users items={data.items} />;
      }}/>
    </Box>
  );

};

users.defaultProps = {
  url: `${baseUrl}users${secret}`,
};

export default users;
