import React, { useState } from 'react';
import { useStore } from 'lib/store';
import Box from '@material-ui/core/Box';
import DataProviderBatch from 'lib/data-batch';
import { baseUrl0, secret } from 'lib/base';
import Dealer from './dealer';
import SoldMachineSelectContainer from './sold-machine-select-container';
import UnsoldContainer from './unsold-container';
import Dialog from './dialog';
import { addUnsold, updateUnsold } from 'lib/api/unsold';
import Edit from './edit-qty';

const prepareQuery = (url, customerGuid, dealer) => {
  return {
    items: `${url}token=${customerGuid}&collection=customers`,
    sold: `${url}token=${customerGuid}&collection=sold-mac-dealer&id=${dealer}`,
    unsold: `${url}token=${customerGuid}&collection=unsold-mac-dealer&id=${dealer}`,
  };
};

const machinesStockOld = props => {

  const [dealer, setDealer] = useState(false);
  const [itemEdit, setItemEdit] = useState(-1);
  const [qty, setQty] = useState(1);

  const store = useStore();
  const { appUser, country } = store;
  if (appUser === false) {
    return null;
  }
  const { customerGuid } = appUser;
  const query = prepareQuery(props.url, customerGuid, dealer.value);

  const close = () => {
    setItemEdit(-1);
    setQty(0);
  }

  return (
    <Box margin='0 auto' width='100%' maxWidth='900px'>
      <DataProviderBatch query={query} render={(data, mutate) => {
        return (
          <>
            <Dialog open={itemEdit > - 1} close={close} >
              <Edit qty={qty} setQty={setQty} updateUnsold={updateUnsold} index={itemEdit} mutate={mutate} token={customerGuid} dealer={dealer.value} setItemEdit={setItemEdit} />
            </Dialog>
            <Dealer items={data.items} dealer={dealer} setDealer={setDealer} />
            {dealer && data.sold && data.sold.length > 0 && 
              <SoldMachineSelectContainer sold={data.sold[0].items} dealer={dealer.value} mutate={mutate} addUnsold={addUnsold} customerGuid={customerGuid} />
            }
            {dealer && data.unsold && data.unsold.length > 0 &&
              <UnsoldContainer unsold={data.unsold[0].items} customerGuid={customerGuid} dealer={dealer.value} mutate={mutate} setItemEdit={setItemEdit} setQty={setQty} />
            }
          </>
        );
      }}/>
      
    </Box>
  );

};

machinesStockOld.defaultProps = {
  url: `${baseUrl0}collection_fetch_secure${secret}`,
  url_create: 'https://webhooks.mongodb-realm.com/api/client/v2.0/app/mgd-ilrht/service/http/incoming_webhook/data-create?secret=L0sS3Bz1mdCm6Kuz',
};

export default machinesStockOld;
