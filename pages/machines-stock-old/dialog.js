import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Slide from '@material-ui/core/Slide';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';

const useStyles = makeStyles(theme => ({
  dialog: {
    maxWidth: '600px',
    margin: '0 auto',
    background: '#fafafa',
  },
  appBar: {
    position: 'relative',
    display: 'flex',
    justifyContent: 'center',
  },
  toolBar: {
    display: 'flex',
    justifyContent: 'space-between',
  },
}));

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction='up' ref={ref} {...props} />;
});

const content = props => {

  const classes = useStyles();
  
  const { title, open, close } = props;

  return (
    <Dialog
      className={classes.dialog}
      fullScreen
      open={open}
      onClose={close}
      TransitionComponent={Transition}
    >
      <AppBar className={classes.appBar}>
        <Toolbar className={classes.toolBar}>
          <Typography variant='subtitle1' color='inherit'>
            {title}
          </Typography>
          <Button
            color='inherit'
            onClick={close}
            variant='outlined'
          >
            Закрыть
          </Button>
        </Toolbar>
      </AppBar>
      <Paper elevation={0} style={{ background: '#fafafa' }}>
        <Box p={2} width='100%' margin='0 auto' textAlign='center' pb={10}>
          {props.children}
        </Box>
      </Paper>
    </Dialog>
  );

};

export default content;