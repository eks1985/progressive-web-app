import React from 'react';
import Box from '@material-ui/core/Box';
import { baseUrl0, secret } from 'lib/base';
import SoldMachineSelect from './sold-machine-select';

const machinesStockOld = props => {

  const { dealer, customerGuid, sold, mutate, addUnsold, setQty } = props;

  return (
    <Box margin='0 auto' width='100%' maxWidth='900px'>
      <SoldMachineSelect sm={sold} customerGuid={customerGuid} dealer={dealer} mutate={mutate} addUnsold={addUnsold} setQty={setQty} />
    </Box>
  );

};

machinesStockOld.defaultProps = {
  url: `${baseUrl0}collection_fetch_secure${secret}`,
};

export default machinesStockOld;
