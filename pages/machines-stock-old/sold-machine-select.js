import React from 'react';
import Select from 'react-select';
import Box from '@material-ui/core/Box';

const OptionLabel = ({ cde, dcr, qty, dte }) => {
  return (
    <div className='d-flex flex-column' style={{ borderTop: '1px solid #eee' }}>
      <div style={{ textAlign: 'left', fontSize: '12px' }}>{`${cde} - ${dcr}`}</div>
      <div style={{ textAlign: 'center' }}>{`${qty}шт:  ${dte}`}</div>
    </div>
  );
};

const prepareOptions = list => {
  return Object.keys(list).map(itemKey => {
    const item = list[itemKey];
    // console.log('item', item);
    const { cde, dcr, qty, dte, val, mde } = item;
    return {
      label: <OptionLabel {...item} />,
      value: `${cde}@@@${dcr}@@@${qty}@@@${dte}@@@${val}@@@${mde}`,
    };
  });
};

const customStyles = {
  option: base => ({
    ...base,
    fontSize: '13px',
    padding: '3px 12px',
  }),
  control: base => ({
    ...base,
    fontSize: '12px',
  }),
};

const MachineSelect = props => {
  const { sm, customerGuid, dealer, mutate, addUnsold } = props;
  const options = prepareOptions(sm);
  return (
    <Box mt={1}>
      <Select
        placeholder='Машины проданные дилеру'
        isSearchable
        styles={customStyles}
        options={options}
        onChange={
          selected => {
            const selectedParts = selected.value.split('@@@');
            const item = { code: selectedParts[0], dscr: selectedParts[1], qty: parseInt(selectedParts[2], 10), date: selectedParts[3], value: parseInt(selectedParts[4], 10), model: selectedParts[5] };
            // console.log('customerGuid', customerGuid);
            addUnsold(props.url_create, customerGuid, dealer, item, mutate);
          }
        }
      />
    </Box>
  );
};

MachineSelect.defaultProps = {
  url_create: 'https://webhooks.mongodb-realm.com/api/client/v2.0/app/mgd-ilrht/service/http/incoming_webhook/data-create?secret=L0sS3Bz1mdCm6Kuz',
};

export default MachineSelect;
