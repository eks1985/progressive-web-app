import React from 'react';
import Box from '@material-ui/core/Box';
import { baseUrl0, secret } from 'lib/base';
import Unsold from './unsold';

const unsoldContainer = props => {

  const { unsold, dealer, mutate, customerGuid, setItemEdit, setQty } = props;
  // console.log('setQty', setQty);
  return (
    <Box margin='0 auto' width='100%' maxWidth='900px'>
      <Unsold list={unsold} dealer={dealer} token={customerGuid} mutate={mutate} setItemEdit={setItemEdit} setQty={setQty} />
    </Box>
  );

};

unsoldContainer.defaultProps = {
  url: `${baseUrl0}collection_fetch_secure${secret}`,
};

export default unsoldContainer;
