import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Select from 'components/select';

const dealer = props => {

  const { items, dealer, setDealer } = props;
  const options = items.map(item => ({ value: item.id, label: item.dcr }));

  return (
    <Box display='flex' alignItems='center' width='100%' flex='1 0 auto'>
      <Select
        width='100%'
        options={options}
        placeholder='Dealer'
        option={dealer}
        onSelectHandler={
          val => {
            setDealer(val);
          }
        }
      />
    </Box>
  );
};

export default dealer;