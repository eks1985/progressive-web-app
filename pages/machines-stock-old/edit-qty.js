import React from 'react';
import Box from '@material-ui/core/Box';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

const edit = props => {
  const { qty, setQty, updateUnsold, index, mutate, token, dealer, url_create, setItemEdit } = props;
  return (
    <Box>
      <Box mt={1} mb={1}>
        <TextField
          value={qty}
          onChange={
            e => {
              setQty(e.target.value);
            }
          }
        />
      </Box>
      <Box mt={2}>
        <Button
          size='small'
          variant='contained'
          color='primary'
          onClick={
            () => {
              updateUnsold(url_create, token, dealer, index, qty, mutate);
              setItemEdit(-1);
            }
          }
        >
          Сохранить
        </Button>    
      </Box>
    </Box>    
  );
};

edit.defaultProps = {
  url_create: 'https://webhooks.mongodb-realm.com/api/client/v2.0/app/mgd-ilrht/service/http/incoming_webhook/data-create?secret=L0sS3Bz1mdCm6Kuz',
};

export default edit;