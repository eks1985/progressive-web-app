import React from 'react';
import Box from '@material-ui/core/Box';
import IconButton from '@material-ui/core/IconButton';
import IconClear from '@material-ui/icons/Clear';
import IconEdit from '@material-ui/icons/Edit';
import Typography from '@material-ui/core/Typography';
import { formatNumb } from 'lib/format';
import { removeUnsold } from 'lib/api/unsold';

const unsold = props => {

  const { list, url_create, token, dealer, mutate, setItemEdit, setQty } = props;
  return (
    <Box p={1}>
      {list.map((item, index) => {
        const { items } = item;
        const { value, qty, model, dscr, date, code } = items;
        return (
          <Box key={index} mt={1} display='flex' alignItems='center' borderTop={index > 0 ? '1px dashed #ccc': 'initial'}>
            <Box width='10%'>
              <IconButton
                size='small'
                onClick={
                  () => {
                    removeUnsold(url_create, token, dealer, index, mutate);
                  }
                }
              >
                <IconClear />
              </IconButton>
            </Box>
            <Box width='10%'>
              <IconButton
                size='small'
                onClick={
                  () => {
                    setItemEdit(index);
                    setQty(qty);
                  }
                }
              >
                <IconEdit />
              </IconButton>
            </Box>
            <Box flex='1 0 auto' pl={1} width='80%'>
              <Box display='flex'>
                <Box>
                  <Typography variant='caption'>
                    {code}
                  </Typography>
                </Box>
                <Box ml={2}>
                  <Typography variant='caption'>
                    {dscr.slice(0, 25)}
                  </Typography>
                </Box>
              </Box>
              <Box display='flex' justifyContent='space-between'>
                <Box>
                  <Typography variant='caption'>
                    {qty}
                  </Typography>
                </Box>
                <Box>
                  <Typography variant='caption'>
                    {`€ ${formatNumb(value)}`}
                  </Typography>
                </Box>
              </Box>
            </Box>
          </Box>        
        );
      })}
    </Box>
  );

};

unsold.defaultProps = {
  url_create: 'https://webhooks.mongodb-realm.com/api/client/v2.0/app/mgd-ilrht/service/http/incoming_webhook/data-create?secret=L0sS3Bz1mdCm6Kuz',
};

export default unsold;