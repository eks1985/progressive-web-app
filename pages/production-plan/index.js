import React from 'react';
import { useStore } from 'lib/store';
import Box from '@material-ui/core/Box';
import DataProviderBatch from 'lib/data-batch';
import { baseUrl0, secret } from 'lib/base';
import ProductionPlan from 'components/production-plan';
import filtersHoc from 'components/filters-hoc';

const productionPlan = props => {

  const store = useStore();
  const { appUser } = store;
  if (appUser === false || !appUser.internal) {
    return null;
  }

  const { url } = props;

  return (
    <Box margin='0 auto' width='100%' maxWidth='600px'>
      <DataProviderBatch query={{ items: url }} render={data => {
        if (!data || data.items.length === 0) {
          return null;
        }
        const options = {
          filtersLegend: [
            { name: 'code' },
            { name: 'descr' },
          ],
        };
        const Enhanced = filtersHoc(ProductionPlan, data.items, options);
        return <Enhanced />;
      }}/>
    </Box>
  );

};

productionPlan.defaultProps = {
  url: `${baseUrl0}collection_fetch${secret}collection=prod-plan-corp`,
};

export default productionPlan;
