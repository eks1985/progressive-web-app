import Head from 'next/head';
import { ThemeProvider } from '@material-ui/core/styles';
import AuthProvider from 'lib/auth';
import StoreProvider from 'lib/store';
import CssBaseline from '@material-ui/core/CssBaseline';
import Layout from 'components/layout';
import theme from 'lib/theme';
import '../styles/globals.css';

export default function App({ Component, pageProps }) {
  return (
    <>
      <Head>
        <meta charSet='utf-8' />
        <meta httpEquiv='X-UA-Compatible' content='IE=edge' />
        <meta
          name='viewport'
          content='width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no'
        />
        <meta name='description' content='Description' />
        <meta name='keywords' content='Keywords' />
        <title>MG Dealer</title>

        <link rel='manifest' href='/manifest.json' />
        <link
          href='/icons/favicon-16x16.png'
          rel='icon'
          type='image/png'
          sizes='16x16'
        />
        <link
          href='/icons/favicon-32x32.png'
          rel='icon'
          type='image/png'
          sizes='32x32'
        />
        <link rel='apple-touch-icon' href='/apple-icon.png'></link>
        <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap' />
        <meta name='theme-color' content='#317EFB' />
      </Head>
      <ThemeProvider key='theme-prov' theme={theme}>
        <StoreProvider>
          <AuthProvider>
            <Layout>
              <CssBaseline />
              <Component {...pageProps} />
            </Layout>
          </AuthProvider>
        </StoreProvider>
      </ThemeProvider>
    </>
  )
}
