import React from 'react';
import { baseUrl, secret } from 'lib/base';
import { useRouter } from 'next/router';
import { useStore } from 'lib/store';
import DataProviderBatch from 'lib/data-batch';
import Box from '@material-ui/core/Box';
import Order from 'components/order/item';

const prepareQuery = (url, id, token) => {

  return {
    order: `${url}&id=${id}&token=${token}`,
  };

};

const order = props => {

  const router = useRouter(); 
  const store = useStore();

  const { appUser: { customerGuid } } = store;

  if (!router.query.id || !store) {
    return null;
  }

  const query = prepareQuery(props.url, router.query.id, customerGuid);

  return (
    <Box margin='0 auto' width='100%' maxWidth='600px'>
      <DataProviderBatch query={query} render={(data, mutate) => {
        if (!data.order || data.order.length === 0) {
          return null;
        }
        return <Order order={data.order[0]} mutate={mutate} />;
      }}/>
    </Box>
  );

};

order.defaultProps = {
  url: `${baseUrl}orders${secret}`,
};

export default order;