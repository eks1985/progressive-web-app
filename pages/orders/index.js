import React from 'react';
import { useStore } from 'lib/store';
import Box from '@material-ui/core/Box';
import List from 'components/order/list';
import DataProviderBatch from 'lib/data-batch';
import { baseUrl, secret } from 'lib/base';

const prepareQuery = (url, customerGuid, country) => {
  return {
    orders: `${url}token=${customerGuid}&country=${country}&filters=true`,
  };
};

const orders = props => {

  const store = useStore();
  const { appUser, country } = store;
  if (appUser === false) {
    return null;
  }
  const { customerGuid } = appUser;
  const query = prepareQuery(props.url, customerGuid, country);

  return (
    <Box margin='0 auto' width='100%' maxWidth='900px'>
      <DataProviderBatch query={query} render={(data, mutate) => {
        return (
          <List
            items={data.orders.items}
            filters={data.orders.filters}
            mutate={mutate}
          />
        );
      }}/>
    </Box>
  );

};

orders.defaultProps = {
  url: `${baseUrl}orders${secret}`,
};

export default orders;
