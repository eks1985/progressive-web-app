import React from 'react';
import { useStore } from 'lib/store';
import Box from '@material-ui/core/Box';
import List from 'components/purchase-order/list';
import DataProviderBatch from 'lib/data-batch';
import { baseUrl, secret } from 'lib/base';
import { getWeekData } from 'components/purchase-order/internals';
import moment from 'moment';

const prepareQuery = (url, customerGuid, country) => {
  return {
    orders: `${url}token=${customerGuid}&country=${country}`,
  };
};

const orders = props => {

  const store = useStore();
  const { appUser, country } = store;
  if (appUser === false || !appUser.internal) {
    return null;
  }
  const { customerGuid } = appUser;
  const query = prepareQuery(props.url, customerGuid, country);

  return (
    <Box margin='0 auto' width='100%' maxWidth='900px'>
      <DataProviderBatch query={query} render={data => {
        if (!data || data.length === 0) {
          return null;
        }
        const orders = data.orders[0].items.map(week => {
          return { ...week, weekData: getWeekData(week.week) }
        });
        const expired = orders.reduce((res, item) => {
          if (item.week !== 'way') {
            if (parseInt(item.weekData.weekNr) < moment().week() && parseInt(item.weekData.year) <= moment().year()) {
              return res += item.value;
            }
            return res;
          }
          return res;
        }, 0);
        
        const value = orders.reduce((res, item) => {
          if (item.week !== 'way') {
            return res + item.value;
          }
          return res;
        }, 0);
        return (
          <List
            orders={orders}
            value={Math.ceil(value)}
            expired={Math.ceil(expired)}
          />
        );
      }}/>
    </Box>
  );

};

orders.defaultProps = {
  url: `${baseUrl}purchase_orders${secret}`,
};

export default orders;
