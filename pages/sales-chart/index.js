/* eslint-disable prefer-destructuring */
import React from 'react';
import { useStore } from 'lib/store';
import DataProviderBatch from 'lib/data-batch';
import { baseUrl, secret } from 'lib/base';
import Box from '@material-ui/core/Box';
import Content from './content';

const prepareQuery = (custGuid, country) => {
  const cntr = country === 0 ? 'ru' : 'ua';
  return {
    chart: `${baseUrl}sales_chart${secret}token=${custGuid}&cntr=${cntr}`,
  };
};

const salesChart = () => {

  const store = useStore();
  const { appUser, country } = store;
  if (appUser === false || !appUser.internal) {
    return null;
  }
  const { customerGuid, email } = appUser;
  const query = prepareQuery(customerGuid, country);

  return (
    <Box margin='0 auto' width='100%' maxWidth='900px'>
      <DataProviderBatch query={query} render={(data, mutate) => {

        if (!data.chart || data.chart.length === 0) {
          return null;
        }

        return (
          <Content
            email={email}
            data={data.chart[0]}
            country={country}
          />
        );

      }}/>
    </Box>
  );

};

export default salesChart;