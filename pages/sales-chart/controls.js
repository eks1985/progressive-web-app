import React from 'react';
import Select from 'react-select';
import Box from '@material-ui/core/Box';

const currencyOptionsRu = [
  { value: 'RUB', label: 'RUB' },
  { value: 'EUR', label: 'EUR' },
];

const currencyOptionsUa = [
  { value: 'UAH', label: 'UAH' },
  { value: 'EUR', label: 'EUR' },
];

const prepareCurrencyOptions = country => {
  if (country === 0) {
    return currencyOptionsRu;
  }
  return currencyOptionsUa;
};

const materialTypeOptions = [
  { value: 'total', label: 'total' },
  { value: 'machines', label: 'machines' },
  { value: 'spare parts', label: 'spare parts' },
];

const сontrols = props => {
  const { currency, materialType, setCurrency, setMaterialType, country } = props;
  return (
    <Box display='flex'>
      <Box minWidth={180} m={1}>
        <Select
          menuContainerStyle={{ zIndex: 9999 }}
          menuStyle={{ zIndex: 9999 }}
          options={materialTypeOptions}
          value={materialType}
          isSearchable={false}
          onChange={
            selected => {
              setMaterialType(selected);
            }
          }
        />
      </Box>
      <Box minWidth={100} m={1}>
        <Select
          menuContainerStyle={{ zIndex: 9999 }}
          menuStyle={{ zIndex: 9999 }}
          options={prepareCurrencyOptions(country)}
          value={currency}
          isSearchable={false}
          onChange={
            selected => {
              setCurrency(selected);
            }
          }
        />
      </Box>
    </Box>
  );
};

export default сontrols;
