import React from 'react';
import ReactHighcharts from 'react-highcharts';
import Controls from './controls';

const colors = ['#9C27B0', '#2962FF', '#FF6F00', '#C6FF00'];

const getCategories = data => {
  if (!data) {
    return [];
  }
  return ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
};

const prepareSeriesTurnover = (data, currency, materialType) => {
  if (!data) {
    return [];
  }
  const allSeries = data[materialType.value];
  const filteresSeries = [];
  let count = -1;
  let max = 0;
  allSeries.forEach(item => {
    if (item.currency === currency.value && item.type === 'line') {
      count += 1;
      const valuesLength = item.values.length;
      max = Math.max(max, valuesLength > 0 ? item.values[valuesLength - 1].y : 0);
      filteresSeries.push(
        { name: item.name, data: item.values, type: item.type, color: colors[count] }
      );
    }
  });
  return { series: filteresSeries, max };
};

const prepareSeriesProfit = (data, currency, materialType) => {
  if (!data) {
    return [];
  }
  const allSeries = data[materialType.value];
  const filteresSeries = [];
  let count = -1;
  let max = 0;
  allSeries.forEach(item => {
    if (item.currency === currency.value && item.type === 'column') {
      count += 1;
      const valuesLength = item.values.length;
      max = Math.max(max, valuesLength > 0 ? item.values[valuesLength - 1].y : 0);
      filteresSeries.push(
        { name: item.name, data: item.values, type: item.type, color: colors[count] }
      );
    }
  });
  return { series: filteresSeries, max };
};

export class Charts extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    const { data, country } = props;
    const currencyByCountry = country === 0 ? 'RUB' : 'UAH';
    const currency = { value: currencyByCountry, label: currencyByCountry };
    const materialType = { value: 'total', label: 'total' };
    const categories = getCategories(data);
    const turnoverData = prepareSeriesTurnover(data, currency, materialType);
    const profitData = prepareSeriesProfit(data, currency, materialType);
    this.state = {
      currency,
      materialType,
      configTurnover: {
        chart: {
          height: 300,
        },
        credits: {
          enabled: false,
        },
        title: {
          text: 'Turnover',
          style: {
            color: '#000',
            font: 'bold 14px Trebuchet MS, Verdana, sans-serif',
          },
        },
        plotOptions: {
          series: {
            label: {
              connectorAllowed: false,
            },
            marker: {
              symbol: 'circle',
              radius: 3,
            },
          },
        },
        xAxis: {
          categories,
          labels: {
            rotation: -90,
          },
        },
        yAxis: {
          title: null,
          min: 0,
          max: turnoverData.max + 100,
          tickInterval: currency.value === 'RUB' ? 50 : 1,
        },
        series: turnoverData.series,
      },
      configProfit: {
        chart: {
          height: 220,
        },
        credits: {
          enabled: false,
        },
        title: {
          text: 'Profit',
          style: {
            color: '#000',
            font: 'bold 14px Trebuchet MS, Verdana, sans-serif',
          },
        },
        plotOptions: {
          series: {
            label: {
              connectorAllowed: false,
            },
            marker: {
              symbol: 'circle',
              radius: 3,
            },
            pointWidth: 4.5,
          },
        },
        xAxis: {
          categories,
          labels: {
            rotation: -90,
          },
        },
        yAxis: {
          title: null,
          min: 0,
          max: profitData.max + 10,
          tickInterval: 10,
        },
        series: profitData.series,
      },
    };
  }
  setCurrency = currency => {
    this.setState({ currency }, () => {
      this.refreshChart();
    });
  }
  setMaterialType = materialType => {
    this.setState({ materialType }, () => {
      this.refreshChart();
    });
  }
  refreshChart = () => {
    const { data } = this.props;
    const stateCopy = { ...this.state };
    const categories = getCategories(data);
    const { currency, materialType } = this.state;
    const turnoverData = prepareSeriesTurnover(data, currency, materialType);
    const turnoverProfitData = prepareSeriesProfit(data, currency, materialType);
    stateCopy.configTurnover.xAxis.categories = categories;
    stateCopy.configTurnover.series = turnoverData.series;
    stateCopy.configTurnover.yAxis.max = turnoverData.max;
    stateCopy.configTurnover.yAxis.tickInterval = stateCopy.currency.value === 'RUB' ? 50 : 1;
    stateCopy.configProfit.xAxis.categories = categories;
    stateCopy.configProfit.series = turnoverProfitData.series;
    stateCopy.configProfit.yAxis.max = turnoverProfitData.max;
    const { configTurnover, configProfit } = stateCopy;
    this.setState({ configTurnover, configProfit });
  }
  render() {
    const { country } = this.props;
    const { currency, materialType } = this.state;
    return (
      <div className='mt-3' style={{ width: '95%', margin: '0 auto', maxWidth: '900px' }}>
        <Controls
          currency={currency}
          materialType={materialType}
          setCurrency={this.setCurrency}
          setMaterialType={this.setMaterialType}
          country={country}
        />
        <ReactHighcharts config={this.state.configTurnover}></ReactHighcharts>
        <ReactHighcharts config={this.state.configProfit}></ReactHighcharts>
      </div>

    );
  }
}

export default Charts;
