import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useStore } from 'lib/store';
import { useRouter } from 'next/router';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles(theme => ({
  container: {
    
  },
}));

const competitors = props => {

  const router = useRouter();
  const classes = useStyles();
  const store = useStore();
  const { appUser } = store;
  if (appUser === false || !appUser.internal) {
    return null;
  }

  return (
    <Box margin='0 auto' width='100%' textAlign='center'>
      Отчет на стадии доработки
    </Box>
  );

};

export default competitors;
