import React from 'react';
import Box from '@material-ui/core/Box';
import ReactHighcharts from 'react-highcharts';
import highcharts3d from 'highcharts/highcharts-3d';
import drilldown from 'highcharts/modules/drilldown';
// highcharts3d(ReactHighcharts.Highcharts);
// drilldown(ReactHighcharts.Highcharts);

const getPieData = (data, total) => {
  const { details } = data;
  return details.map(item => {
    const title = item.title === 'Others' ? item.title : item.title.slice(4);
    const name = `${Math.round((item.value / total) * 100)}% ${title}`;
    return { name, drilldown: name, y: item.value };
  });
};

const getPieDrillDown = (data, total, country) => {
  const currency = country === 0 ? 'rub' : 'uah';
  const { details, orders } = data;
  return details.map(item => {
    const { title, value } = item;
    const titlePrepared = title === 'Others' ? title : title.slice(4);
    const name = `${Math.round((item.value / total) * 100)}% ${titlePrepared}`;
    const covered = orders[title] || 0;
    const notCovered = value >= covered ? value - covered : 0;
    return {
      name,
      id: name,
      data: [['Покрыто заказами', covered], ['Не покрыто заказами', notCovered]],
      dataLabels: {
        enabled: true,
        distance: -10,
        crop: false,
        // eslint-disable-next-line
        formatter: function () {
          return `${this.y} m ${currency} - ${Math.round(this.percentage, 0)}%`;
        },
      },
    };
  });
};

const StockPieChart = props => {

  const { chartData, country } = props;

  const keys = Object.keys(chartData);
  const piedata = keys.reduce((res, item) => {
    if (chartData[item].dettypes) {
      return chartData[item];
    }
    return res;
  }, false);

  const total = piedata.value;
  const data = getPieData(piedata, total);
  const drillDownData = getPieDrillDown(piedata, total, country);
  const title = `Value on current date: ${total}`;
  const config = {
    colors: [
      '#ff77a9',
      '#8e99f3',
      '#ffd95b',
      '#98ee99',
      '#ffff8b',
      '#73e8ff',
      '#cfff95',
      '#6ff9ff',
      '#be9c91',
      '#64d8cb',
      '#ffa270',
      '#ffff89',
      '#80d6ff',
      '#fffd61',
      '#df78ef',
      '#ff867c',
      '#a7c0cd',
      '#b085f5',
    ],
    credits: {
      enabled: false,
    },
    legend: {
      itemStyle: {
        fontWeight: 'normal',
        fontSize: '10px',
      },
      itemWidth: 150,
      // eslint-disable-next-line
      labelFormatter: function () {
        return `${this.y} - ${this.name}`; // eslint-disable-line
      },
    },
    chart: {
      type: 'pie',
      options3d: {
        enabled: true,
        alpha: 25,
      },
      height: 350,
    },
    title: {
      text: title,
    },
    plotOptions: {
      pie: {
        innerSize: 35,
        depth: 25,
        size: 180,
        showInLegend: true,
      },
      series: {
        dataLabels: {
          enabled: true,
          distance: 10,
          style: {
            fontSize: '10px',
            fontWeight: 'normal',
          },
        },
      },
    },
    series: [{
      name: 'By categories',
      data,
      label: {
        enabled: false,
      },
    }],
    drilldown: {
      series: drillDownData,
      animation: false,
    },
  };
  return (
    <Box width='100%'>
      <ReactHighcharts config={config}></ReactHighcharts>
    </Box>
  );
};

export default StockPieChart;

