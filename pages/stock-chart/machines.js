import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import IconExpand from '@material-ui/icons/ExpandMore';
import MachinesByDays from './machines-by-dates';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    maxWidth: '900px',
    margin: '0 auto',
  },
  textMachine: {
    fontSize: theme.typography.pxToRem(11),
    color: '#333',
  },
  heading: {
    fontSize: theme.typography.pxToRem(13),
    fontWeight: theme.typography.fontWeightRegular,
    color: '#f44336',
  },
  headingBold: {
    fontSize: theme.typography.pxToRem(13),
    fontWeight: 'bold',
    color: '#f44336',
  },
}));

const Machine = props => {
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const { machine, country } = props;
  const { t, q, v, id } = machine;
  return (
    <React.Fragment>
      <Box display='flex' alignItems='center' key='machine'>
        <Box width={25}>
          {q > 0 &&
            <IconButton size='small' onClick={setOpen.bind(null, true)}>
              <IconExpand />
            </IconButton>
          }
        </Box>
        <Typography style={{ width: '25px', marginRight: '4px', textAlign: 'right' }} className={classes.textMachine}>{q || ''}</Typography>
        <Typography style={{ width: '70px', marginRight: '8px', textAlign: 'right', fontWeight: 'bold' }} className={classes.textMachine}>{v > 0 ? v.toLocaleString('ru-RU', { minimumFractionDigits: 0 }) : ''}</Typography>
        <Typography style={{ width: '200px', marginRight: '2px' }} className={classes.textMachine}>{t.slice(0, 28)}</Typography>
      </Box>
      {open &&
        <MachinesByDays model={t} country={country} id={id} />
      }
    </React.Fragment>
  );
};

const M = props => {
  const { detmac, t, country } = props;
  const machines = detmac.filter(mac => mac.c === t);
  return (
    <div>
      {machines.map((machine, i) => {
        return <Machine key={`${machine.t}${i}`} machine={machine} country={country} />;
      })}
    </div>
  );
};

const T = props => {
  const classes = useStyles();
  const { tp, detmac, country } = props;
  const { t, v } = tp;
  return (
    <Accordion square>
      <AccordionSummary expandIcon={<ExpandMoreIcon />}>
        <Typography style={{ marginRight: '2px', width: '80px' }} className={classes.headingBold}>{v.toLocaleString('ru-RU', { minimumFractionDigits: 0 })}</Typography>
        <Typography className={classes.heading}>{t.slice(0, 50)}</Typography>
      </AccordionSummary>
      <AccordionDetails style={{ padding: '4px', paddingBottom: '6px' }}>
        <M detmac={detmac} t={t} country={country} />
      </AccordionDetails>
    </Accordion>
  );
};

const Machines = props => {
  const { chartStock, country } = props;
  const keys = Object.keys(chartStock);
  const keysL = keys.length;
  const lastKey = keys[keysL - 1];
  const det = chartStock[lastKey];
  const { detmac, dettypes } = det;
  return (
    <Box mt={2} mnb={2}>
      {dettypes.map(t => {
        return (
          <T key={t.t} tp={t} detmac={detmac} country={country} />
        );
      })}
    </Box>
  );
};


export default Machines;
