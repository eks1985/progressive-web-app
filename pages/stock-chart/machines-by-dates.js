import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import DataProviderBatch from 'lib/data-batch';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    maxWidth: '900px',
    margin: '0 auto',
  },
  textMachine: {
    fontSize: theme.typography.pxToRem(11),
    color: '#333',
  },
  heading: {
    fontSize: theme.typography.pxToRem(13),
    fontWeight: theme.typography.fontWeightRegular,
    color: '#f44336',
  },
  headingBold: {
    fontSize: theme.typography.pxToRem(13),
    fontWeight: 'bold',
    color: '#f44336',
  },
}));

const prepareQuery = (url, country, id) => {

  const reqUrl = `${url}&country=${country === 0 ? 'ru' : 'ua'}&id=${id}`;

  return {
    items: reqUrl,
  };

};

const List = props => {

  const classes = useStyles();

  const { items} = props;

  return (
    <Box>
      {items.map((item, i) => {
        const { qty, days, value } = item;
        return (
          <Box display='flex' alignItems='center' key={i}>
            <Box width={25} />
            <Typography style={{ color: '#3d5afe', width: '25px', marginRight: '4px', textAlign: 'right' }} className={classes.textMachine}>{qty || ''}</Typography>
            <Typography style={{ color: '#3d5afe', width: '70px', marginRight: '8px', textAlign: 'right', fontWeight: 'bold' }} className={classes.textMachine}>{value > 0 ? value.toLocaleString('ru-RU', { minimumFractionDigits: 0 }) : ''}</Typography>
            <Typography style={{ color: '#3d5afe', width: '200px', marginRight: '2px' }} className={classes.textMachine}>{`${days} дней`}</Typography>
          </Box>
        );
      })}
    </Box>
  );

};

const MachinesByDays = props => {

  const { url, id, country } = props;

  const query = prepareQuery(url, country, id);

  return (
    <Box>
      <DataProviderBatch
        query={query}
        render={(data, mutate) => (
          <List
            items={data.items && data.items.length > 0 ? data.items[0].items : []}
            mutate={mutate}
          />
        )}
      />
    </Box>
  );

};

MachinesByDays.defaultProps = {
  url: 'https://webhooks.mongodb-realm.com/api/client/v2.0/app/mgd-ilrht/service/http/incoming_webhook/models_cost_fetch?secret=L0sS3Bz1mdCm6Kuz',
};

export default MachinesByDays;
