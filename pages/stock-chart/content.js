import React, { useState, useEffect } from 'react';
import ReactHighcharts from 'react-highcharts';
import highcharts3d from 'highcharts/highcharts-3d';
import StockPieChart from './stock-pie-chart';
import Machines from './Machines';
// highcharts3d(ReactHighcharts.Highcharts);

const getCategories = data => {
  if (!data) {
    return [];
  }
  return Object.keys(data).filter(item => item !== '_id');
};

const getChartData = (data, spareParts = false) => {
  if (!data) {
    return [];
  }
  return Object.keys(data).filter(item => item !== '_id').map(key => {
    return spareParts ? data[key].valueSpareParts : data[key].value;
  });
};

const Content = props => {

    const { data, country } = props;

    console.log('data.chart', data);

    const [config, setConfig] = useState(false);

    useEffect(() => {

      const categories = getCategories(data);
      const dataAll = getChartData(data);
      const dataSpareParts = getChartData(data, true);

      const conf = {
        credits: {
          enabled: false,
        },
        title: {
          text: 'Stock',
        },
        xAxis: {
          categories,
          labels: {
            rotation: -90,
          },
        },
        yAxis: {
          title: null,
          min: 0,
          tickInterval: 50,
        },
        plotOptions: {
          series: {
            label: {
              connectorAllowed: false,
            },
          },
        },
        series: [
          {
            name: 'All',
            data: dataAll,
          },
          {
            name: 'Spare parts',
            data: dataSpareParts,
          },
        ],
      };

      conf.xAxis.categories = categories;
      conf.yAxis.max = country === 1 ? 300 : 800;
      conf.series[0].data = dataAll;
      conf.series[1].data = dataSpareParts;

      setConfig(conf);

    }, [data, country]);

    if (!config) {
      return null;
    }

    return (
      <div className='mt-4' style={{ width: '95%', margin: '0 auto', maxWidth: '900px' }}>
        <ReactHighcharts config={config}></ReactHighcharts>
        <StockPieChart chartData={data} country={country} />
        <Machines chartStock={data} country={country} />
      </div>
    );

};

export default Content;
