import React from 'react';
import { useStore } from 'lib/store';
import Box from '@material-ui/core/Box';
import DataProviderBatch from 'lib/data-batch';
import { baseUrl, secret } from 'lib/base';
import OpenItems from 'components/open-items';

const prepareQuery = (url, customerGuid, country) => {
  return {
    openItems: `${url}token=${customerGuid}&country=${country}`,
  };
};

const openItems = props => {

  const store = useStore();
  const { appUser, country } = store;
  if (appUser === false || !appUser.internal) {
    return null;
  }
  const { customerGuid } = appUser;
  const query = prepareQuery(props.url, customerGuid, country);

  return (
    <Box margin='0 auto' width='100%' maxWidth='900px'>
      <DataProviderBatch query={query} render={data => {
        if (!data || data.openItems.length === 0) {
          return null;
        }
        // console.log('data', data);
        if (appUser.internal) {
          const last = data.openItems.pop();
          data.openItems.unshift(last);
        }
        return <OpenItems items={data.openItems} internalUser={appUser.internal} />;
      }}/>
    </Box>
  );

};

openItems.defaultProps = {
  url: `${baseUrl}open_items${secret}`,
};

export default openItems;
