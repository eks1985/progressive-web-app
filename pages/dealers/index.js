import React from 'react';
import { useStore } from 'lib/store';
import Box from '@material-ui/core/Box';
import List from 'components/dealer/list';
import DataProviderBatch from 'lib/data-batch';
import { baseUrl, secret } from 'lib/base';

const prepareQuery = (url, customerGuid, country) => {
  return {
    dealers: `${url}token=${customerGuid}&country=${country}&full=true`,
  };
};

const dealers = props => {

  const store = useStore();
  const { appUser, country } = store;
  if (appUser === false || !appUser.internal) {
    return null;
  }
  const { customerGuid } = appUser;
  const query = prepareQuery(props.url, customerGuid, country);

  return (
    <Box margin='0 auto' width='100%' maxWidth='900px'>
      <DataProviderBatch query={query} render={(data, mutate) => {
        return (
          <List
            dealers={data.dealers}
          />
        );
      }}/>
    </Box>
  );

};

dealers.defaultProps = {
  url: `${baseUrl}customers${secret}`,
};

export default dealers;
