import React, { useState } from 'react';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';

const reqType = props => {

  const { reqType, cart, setCart } = props;

  return (
    <Box width='100%' margin='0 auto'>
      <ButtonGroup disableElevation>
        <Button
          size='small'
          color='primary'
          variant={reqType === 'request' ? 'contained': 'outlined'}
          onClick={cart.update.bind(cart, { event: 'changeProp', prop: 'reqType', value: 'request', setCart })}
        >
          Заказ
        </Button>
        <Button
          size='small'
          color='primary'
          variant={reqType === 'question' ? 'contained': 'outlined'}
          onClick={cart.update.bind(cart, { event: 'changeProp', prop: 'reqType', value: 'question', setCart })}
        >
          Вопрос
        </Button>
      </ButtonGroup>
    </Box>
  );

};

export default reqType;
