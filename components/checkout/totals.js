import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useStore } from 'lib/store';
import { useRouter } from 'next/router';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { formatNumb } from 'lib/format';

const useStyles = makeStyles(theme => ({
  container: {
    
  },
}));

const tptals = props => {

  const { totalQty, totalValue } = props;

  return (
    <Box margin='0 auto' width='100%'>
      <Typography variant='subtitle1'>
        {`Всего позиций ${totalQty} на сумму ${formatNumb(totalValue)} Евро`}
      </Typography>
    </Box>
  );

};

export default tptals;
