import React from 'react';
import Box from '@material-ui/core/Box';
import DateSelect from 'components/date-select';

const deliveDate = props => {

  const { cart, setCart } = props;

  const handleSelectDate = selected => {
    cart.update({ event: 'changeProp', prop: 'delivDate', value: selected, setCart });
  };

  const { delivDate } = props;

  return (
    <Box margin='0 auto' width='100%'>
      <DateSelect calendarDate={delivDate} handleSelectDate={handleSelectDate} />
    </Box>
  );

};

export default deliveDate;
