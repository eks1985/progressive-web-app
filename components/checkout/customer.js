import React from 'react';
import { baseUrl, secret } from 'lib/base';
import { makeStyles } from '@material-ui/core/styles';
import { useStore } from 'lib/store';
import DataProviderBatch from 'lib/data-batch';
// import { useRouter } from 'next/router';
// import Box from '@material-ui/core/Box';
// import Button from '@material-ui/core/Button';
// import Typography from '@material-ui/core/Typography';
import Select from 'components/select';

const useStyles = makeStyles(theme => ({
  container: {
    
  },
}));

const prepareQuery = (url, token, country) => {

  return {
    items: `${url}&token=${token}&country=${country}`,
  };

};

const Content = props => {

  const { items, customer, cart, setCart } = props;

  const options = items.map(item => ({ value: item._id, label: item.dcr }));

  return (
    <Select
      option={customer}
      options={options}
      placeholder='Контрагент'
      onSelectHandler={
        sel => {
          cart.update({ event: 'changeProp', prop: 'customer', value: sel, setCart });
        }
      }
    />  
  );

};

const customer = props => {

  const store = useStore();
  const { appUser, country } = store;
  const { cart, customer, setCart } = props;

  const query = prepareQuery(props.url, appUser.customerGuid, country);

  return (
    <DataProviderBatch query={query} render={data => (
      <Content
        items={data.items}
        customer={customer}
        cart={cart}
        setCart={setCart}
      />
    )}/>
  );

};

customer.defaultProps = {
  url: `${baseUrl}customers${secret}`,
};

export default customer;
