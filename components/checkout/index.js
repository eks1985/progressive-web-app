import React, { useState } from 'react';
import { useStore } from 'lib/store';
import Box from '@material-ui/core/Box';
import Dialog from './dialog';
import ReqType from './req-type';
import ActionsRequest from './actions-request';
import ActionsQuestion from './actions-question';
import Customer from './customer';
import DelivDate from './deliv-date';
import Notice from './notice';
import Payment from './payment';
import Product from './product';
import Totals from './totals';
import ValidaionMsg from './validation-msg';
import ItemWrapper from './item-wrapper';
import RequestSent from './requestSent';
import Question from './question';

const checkout = () => {

  const [requestSent, setRequestSent] = useState(false);

  const store = useStore();
  
  if (!store) {
    return null;
  }

  const { cart, setCart, appUser, setCartOpen } = store;

  if (requestSent) {
    return <RequestSent requestSent={requestSent} setRequestSent={setRequestSent} />
  }

  if (!cart) {
    return null;
  }

  const { items, reqType, customer, delivDate, payment, totalQty, totalValue, notice, validMsg } = cart.data;
  const { setActiveVal } = cart;

  const cleanCart = () => {
    setCart(false);
    setCartOpen(false);
  };

  return (
    <Dialog title='Отправка запроса' open={store.cartOpen} close={store.setCartOpen.bind(null, false)}>
      <Box mt={1}>
        <ReqType reqType={reqType} cart={cart} setCart={setCart} />
      </Box>
      {appUser && appUser.internal &&
        <Box mt={2}>
          <Customer customer={customer} cart={cart} setCart={setCart}  /> 
        </Box>
      }
      {items.length > 0 &&
        <Box mt={2}>
          <Totals totalQty={totalQty} totalValue={totalValue} />
        </Box>
      }
      {items.length > 0 &&
        <Box mt={2}>
          <ItemWrapper title='Товары'>
            {items.map((item, i) => {
              return (
                <Product
                  key={i}
                  index={i}
                  item={item}
                  cart={cart}
                  setCart={setCart}
                  setActiveVal={setActiveVal.bind(cart, i, setCart)}
                  appUser={appUser}
                />
              );
            })}
          </ItemWrapper>
        </Box>
      }
      {reqType === 'request'
        ?
          <>
            <Box mt={2}>
              <ItemWrapper title='Поставка'>
                <DelivDate delivDate={delivDate} cart={cart} setCart={setCart} />
              </ItemWrapper>              
            </Box>
            <Box mt={2}>
              <ItemWrapper title='Оплата'>
                <Payment payment={payment} cart={cart} setCart={setCart} />
              </ItemWrapper>              
            </Box>
            <Box mt={2}>
              <Notice notice={notice} cart={cart} setCart={setCart} />
            </Box>
            <Box mt={2}>
              <ValidaionMsg validMsg={validMsg} />              
            </Box>
            <Box mt={1}>
              <ActionsRequest
                cart={cart}
                disabled={validMsg.length > 0}
                cleanCart={cleanCart}
                closeCart={setCartOpen.bind(null, false)}
                setRequestSent={setRequestSent}
              />
            </Box>
          </>
        :
        <>
          <Box mt={2}>
            <Question notice={notice} cart={cart} setCart={setCart} /> 
          </Box>   
          <Box mt={1}>
            <ActionsQuestion
              cart={cart}
              disabled={notice === ''}
              cleanCart={cleanCart}
              closeCart={setCartOpen.bind(null, false)}
              setRequestSent={setRequestSent}
            />
          </Box>
        </>
      }
    </Dialog>
  );

};

export default checkout;