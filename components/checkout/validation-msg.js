import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useStore } from 'lib/store';
import { useRouter } from 'next/router';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles(theme => ({
  container: {
    
  },
}));

const compName = props => {

  const store = useStore();
  const router = useRouter();
  const classes = useStyles();
  const { validMsg } = props;

  return (
    <Box margin='0 auto' width='100%'>
      {validMsg.map((msg, i) => {
        return (
          <Box key={i}>
            <Typography variant='subtitle1' color='primary'>
              {msg}
            </Typography>
          </Box>
        );
      })}
    </Box>
  );

};

export default compName;
