import React, { useState } from 'react';
import { useStore } from 'lib/store';
import { getTodayPlus } from 'lib/dates';
import date from 'date-and-time';
import validator from 'validator';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import Fab from '@material-ui/core/Fab';
import Typography from '@material-ui/core/Typography';
import { validate } from './internals';
import Item from './item';

const payment = props => {

  const [payment, setPayment] = useState(props.payment);
  const [mod, setMod] = useState(false);
  const [vldMsg, setVldMsg] = useState(false);

  const { cart, setCart } = props;

  const store = useStore();

  const addPayment = () => {
    const newStep = {
      type: 'date',
      percent: 100,
      value: date.format(getTodayPlus(7), 'DD-MM-YYYY'),
    }
    setMod(true);
    const updated = [...payment, newStep];
    setVldMsg(validate(updated));
    setPayment(updated);
  };

  const removePayment = index => {
    if (payment.length === 1) {
      return;
    }
    const updated = payment.filter((_, i) => i !== index); 
    setPayment(updated);
    setMod(true);
    setVldMsg(validate(updated));
  };

  const reset = () => {
    setPayment(props.payment);
    setMod(false);
    setVldMsg(false);
  };

  const editPayment = (ind, action) => {

    const { prop, value } = action;
    let valuePrep = value;
    let valid = true;

    if (prop === 'percent' && !validator.isInt(value, { min: 0, max: 100 } )) {
      valuePrep = parseInt(value).toString();
      valid = validator.isInt(valuePrep, { min: 1, max: 100 } );
      if (!valid) {
        return;
      }
    }

    if (prop === 'value' && !validator.isInt(value, { min: 0, max: 100 } )) {
      valuePrep = value.replace('%', '');
      valid = validator.isInt(valuePrep, { min: 1, max: 100 } ) || valuePrep === '';
      if (!valid) {
        return;
      }
    }

    const updated = [...payment];
    const updatedItem = updated[ind];
    updatedItem[prop] = valuePrep;

    if (prop === 'type' && value === 'days') {
      updatedItem.value = '7'; 
    }
    if (prop === 'type' && value === 'date') {
      updatedItem.value = date.format(getTodayPlus(7), 'DD-MM-YYYY'); 
    }

    updated[ind] = updatedItem;
    setPayment(updated);
    setMod(true);
    setVldMsg(validate(updated));

  }

  return (
    <Box margin='0 auto' width='100%' mb={2}>
      {mod &&
        <Box display='flex' justifyContent='center' mb={2}>
          <Button
            color='primary'
            size='small'
            variant='contained'
            disabled={Boolean(vldMsg)}
            onClick={
              () => {
                cart.update({ event: 'changeProp', prop: 'payment', value: payment, setCart });
                setMod(false);
              }
            }
          >
            Сохранить
          </Button>
          <Box ml={2}>
            <Button
              color='primary'
              size='small'
              onClick={reset}
            >
              Отмена
            </Button>
          </Box>
        </Box>
      }
      {mod && vldMsg &&
        <Box>
          <Typography color='primary' variant='body2'>
            {vldMsg}
          </Typography>
        </Box>
      }
      <Box mt={1}>
        {payment.map((item, i) => {
          return (
            <Item
              key={i}
              ind={i}
              {...item}
              removePayment={removePayment.bind(null, i)}
              editPayment={editPayment.bind(null, i)}
            />
          );
        })}
      </Box>
      <Box display='flex' justifyContent='center' mt={2}>
        <Fab
          aria-label='create'
          size='small'
          onClick={addPayment}
        >
          <AddIcon />
        </Fab>
      </Box>
    </Box>
  );

};

export default payment;
