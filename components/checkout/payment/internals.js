
export const validate = payment => {

  let msg = '';
  let i = 0;
  let total = 0;
  for (const step of payment) {
    i++;
    if (step.value === '') {
      msg += `Не задано значение в строке ${i}` + '\n';
    }
    total += parseInt(step.percent);
  }

  if (total !== 100) {
    msg += 'Сумма процентов должна быть равна 100';
  }
  return msg;

};