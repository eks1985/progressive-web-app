import React from 'react';
import NumberFormat from 'react-number-format';
import { makeStyles } from '@material-ui/core/styles';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import TextField from '@material-ui/core/TextField';
import DateSelect from 'components/date-select';
import IconClear from '@material-ui/icons/Clear';
import IconButton from '@material-ui/core/IconButton';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles(theme => ({
  percent: {
    width: '52px',
  },
  days: {
    width: '100px',
  },
  reqBtn: {
    padding: '3px',
    fontSize: '12px',
  },
}));


function NumberFormatCustom(props) {

  const { inputRef, Change, ...other } = props;

  return (
    <NumberFormat
      {...other}
      getInputRef={inputRef}
      thousandSeparator={' '}
      isNumericString
      suffix='%'
    />
  );
  
}

const item = props => {

  const classes = useStyles();
  const {
    type,
    percent,
    value,
    removePayment,
    editPayment
  } = props;

  return (
    <Box display='flex' justifyContent='center' mb={1} margin='0 auto'>
      <ButtonGroup disableElevation>
        <Button
          className={classes.reqBtn}
          size='small'
          color={type === 'date' ? 'primary' : 'default' }
          variant='outlined'
          onClick={editPayment.bind(null, { prop: 'type', value : 'date' })}
        >
          Дата
        </Button>
        <Button
          className={classes.reqBtn}
          size='small'
          color={type === 'days' ? 'primary' : 'default' }
          variant='outlined'
          onClick={editPayment.bind(null, { prop: 'type', value : 'days' })}
        >
          Кредит
        </Button>
      </ButtonGroup>
      {type === 'date' &&
        <Box ml={1}>
          <DateSelect
            calendarDate={value}
            handleSelectDate={
              date => {
                editPayment({ prop: 'value', value: date });
              }
            }
            showCalendarIcon={false}
          />
        </Box>  
      }
      {type === 'days' &&
        <Box ml={1}>
          <TextField
            id='numb'
            className={classes.days}
            size='small'
            margin='none'
            value={value}
            onChange={
              e => {
                editPayment({ prop: 'value', value: e.target.value });
              }
            }
          />  
        </Box>  
      }
      <Box ml={1}>
        <TextField
          id='numb'
          className={classes.percent}
          size='small'
          margin='none'
          value={percent}
          onChange={
            e => {
              editPayment({ prop: 'percent', value: e.target.value });
            }
          }
          InputProps={{
            inputComponent: NumberFormatCustom,
          }}
        />  
      </Box>
      <Box>
        <IconButton
          size='small'
          onClick={removePayment}
        >
          <IconClear />  
        </IconButton>
      </Box>
    </Box>
  );

};

export default item;