import React from 'react';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Dialog from './dialog';

const requestSent = props => {

  const { requestSent, setRequestSent } = props;

  return (
    <Dialog title='Запрос отправлен' open={requestSent} close={setRequestSent.bind(null, false)}>
      <Box m={3}>
        <Box>
          <Typography variant='subtitle1'>
            Ваш запрос успешно отправлен!
          </Typography>
        </Box>
        <Box>
          <Typography variant='subtitle1'>
            Вы получите ответ по электронной почте.
          </Typography>
        </Box>
      </Box>
    </Dialog>
  );

};

export default requestSent;