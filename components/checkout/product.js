import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import IconClear from '@material-ui/icons/Clear';
import IconInc from '@material-ui/icons/Add';
import IconDec from '@material-ui/icons/Remove';
import TextField from '@material-ui/core/TextField';
import NumberFormat from 'react-number-format';
import Modificators from 'components/product-card/modificators';
import EnterPrice from './enter-price';

function NumberFormatCustom(props) {

  const { inputRef, ...other } = props;

  return (
    <NumberFormat
      {...other}
      getInputRef={inputRef}
      thousandSeparator={' '}
      isNumericString
      prefix='€'
    />
  );
  
}

const useStyles = makeStyles(theme => ({
  qty: {
    width: '36px',
    textAlign: 'center',
  },
  value: {
    width: '92px',
    textAlign: 'center',
  },
}));

const product = props => {

  const classes = useStyles();

  const { item: { product, opt, qty, price, value }, index, cart, setCart, setActiveVal, appUser } = props;

  return (
    <React.Fragment>
      <Box margin='0 auto' mt={2} width='100%' display='flex' alignItems='center' justifyContent='space-between'>
        <Box display='flex' alignItems='center'>
          <Box mr={1}>
            <IconButton
              size='small'
              onClick={
                () => {
                  cart.update({ event: 'removeItem', index, setCart });
                }
              }
            >
              <IconClear />  
            </IconButton>
          </Box>
          <Box>
            <Typography variant='body2'>
              {product.title || product.descr}
            </Typography>
          </Box>
        </Box>
        <Box display='flex' alignItems='center' ml={1}>
          {qty > 1 &&
            <IconButton
              size='small'
              onClick={
                () => {
                  cart.update({ event: 'changeProductQty', index, step: -1, setCart });
                }
              }
            >
              <IconDec />
            </IconButton>
          }
          <TextField
            id='cart-qty'
            className={classes.qty}
            size='small'
            margin='none'
            value={qty}
          />
          <IconButton
            size='small'
            onClick={
              () => {
                cart.update({ event: 'changeProductQty', index, step: 1, setCart });
              }
            }
          >
            <IconInc />
          </IconButton>
        </Box>
      </Box>
      <Box margin='0 auto' width='100%' display='flex' alignItems='center' justifyContent='space-around' mt={2}>
        <Box display='flex' alignItems='center'>
          {appUser && appUser.internal &&
            <Box mr={1}>
              <EnterPrice index={index} price={price} cart={cart} setCart={setCart} />
            </Box>
          }
          <Box>
            <TextField
              label='Цена'
              id='cart-price'
              className={classes.value}
              size='small'
              margin='none'
              value={price}
              InputProps={{
                inputComponent: NumberFormatCustom,
              }}
            />
          </Box>
        </Box>
        <Box>
          <TextField
            label='Сумма'
            id='cart-value'
            className={classes.value}
            size='small'
            margin='none'
            value={value}
            InputProps={{
              inputComponent: NumberFormatCustom,
            }}
          />
        </Box>
      </Box>
      {opt && opt.length > 0 &&
        <Box margin='0 auto' textAlign='left' mt={1}>
          <Modificators opt={opt} setActiveVal={setActiveVal} checkout />            
        </Box>
      }
    </React.Fragment>
  );

};

export default product;
