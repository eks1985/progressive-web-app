import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import NumberFormat from 'react-number-format';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import IconEdit from '@material-ui/icons/Edit';
import TextField from '@material-ui/core/TextField';

function NumberFormatCustom(props) {

  const { inputRef, Change, ...other } = props;

  return (
    <NumberFormat
      {...other}
      getInputRef={inputRef}
      thousandSeparator={' '}
      isNumericString
      allowNegative={false}
      prefix='€'
    />
  );
  
}

const useStyles = makeStyles(theme => ({
  value: {
    textAlign: 'center',
  },
}));

const enterPrice = props => {
  
  const [open, setOpen] = useState(false);
  const [price, setPrice] = useState(props.price);

  const { index, cart, setCart } = props;

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleSave = () => {
    setOpen(false);
    cart.update({ event: 'changeProductPrice', index, price: parseInt(price.replaceAll(' ', '')), setCart });
  };

  const classes = useStyles();

  return (
    <div>
      <IconButton
        size='small'
        onClick={handleClickOpen}
      >
        <IconEdit />  
      </IconButton>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby='alert-dialog-title'
        aria-describedby='alert-dialog-description'
      >
        <DialogTitle id='alert-dialog-title'>Введите цену</DialogTitle>
        <DialogContent>
          <TextField
            label='Цена'
            id='cart-value'
            className={classes.value}
            size='small'
            margin='none'
            value={price}
            fullWidth
            InputProps={{
              inputComponent: NumberFormatCustom,
            }}
            onChange={
              e => {
                setPrice(e.target.value.replace('€', ''));
              }
            }
          />
        </DialogContent>
        <DialogActions>
          <Box mt={3} mb={1} display='flex'>
            <Button onClick={handleClose} color='primary' size='small'>
              Отмена
            </Button>
            <Box ml={2}>
              <Button onClick={handleSave} color='primary' size='small' variant='contained' autoFocus>
                Сохранить
              </Button>
            </Box>
          </Box>
        </DialogActions>
      </Dialog>
    </div>
  );

}

export default enterPrice;
