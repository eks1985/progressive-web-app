import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useStore } from 'lib/store';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import { createRequest, updateRequest } from 'lib/api/customer-request';

const useStyles = makeStyles(theme => ({
  btn: {
    margin: theme.spacing(1),
  },
}));


async function sendRequest(cart, store, setRequestSent) {
  if (cart.data.id) {
    await updateRequest(store);
  } else {
    await createRequest(store);
  }
  store.setCart(false);
  setRequestSent(true);
};

const actions = props => {
  
  const classes = useStyles();
  const store = useStore();

  const { cart, disabled, cleanCart, closeCart, setRequestSent } = props;

  return (
    <Box margin='0 auto' width='100%' display='flex' flexWrap='wrap' justifyContent='center'>
      <Button
        disabled={disabled}
        variant='contained'
        color='primary'
        className={classes.btn}
        onClick={
          () => {
            sendRequest(cart, store, setRequestSent);
          }
        }
      >
        Отправить заказ
      </Button>
      <Button
        variant='outlined'
        color='primary'
        className={classes.btn}
        onClick={closeCart}
      >
        Отмена
      </Button>
      <Button
        variant='outlined'
        color='primary'
        className={classes.btn}
        onClick={cleanCart}
      >
        Очистить корзину
      </Button>
    </Box>
  );

};

export default actions;
