import React from 'react';
import TextField from '@material-ui/core/TextField';

const notice = props => {

  const { notice, cart, setCart } = props;

  return (
    <TextField
      id='outlined-basic'
      label='Ваш вопрос'
      variant='outlined'
      fullWidth
      multiline
      rows={4}
      value={notice}
      onChange={
        e => {
          cart.update({ event: 'changeProp', prop: 'notice', value: e.target.value, setCart });
        }
      }
    />
  );

};

export default notice;
