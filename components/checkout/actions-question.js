import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useStore } from 'lib/store';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import { createQuestion } from 'lib/api/customer-request';

const useStyles = makeStyles(theme => ({
  btn: {
    margin: theme.spacing(1),
  },
}));


async function sendQuestion(cart, store, setRequestSent) {
  store.setCart(false);
  await createQuestion(store);
  setRequestSent(true);
};

const actions = props => {
  
  const classes = useStyles();
  const store = useStore();

  const { cart, disabled, closeCart, setRequestSent } = props;

  return (
    <Box margin='0 auto' width='100%' display='flex' flexWrap='wrap' justifyContent='center'>
      <Button
        disabled={disabled}
        variant='contained'
        color='primary'
        className={classes.btn}
        onClick={
          () => {
            sendQuestion(cart, store, setRequestSent);
          }
        }
      >
        Отправить вопрос
      </Button>
      <Button
        variant='outlined'
        color='primary'
        className={classes.btn}
        onClick={closeCart}
      >
        Отмена
      </Button>
    </Box>
  );

};

export default actions;
