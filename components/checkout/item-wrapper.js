import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import IconOK from '@material-ui/icons/Check';

const useStyles = makeStyles(theme => ({
  head: {
    fontSize: theme.typography.pxToRem(15),
    paddingRight: '50px',
    color: theme.palette.primary.main,
  },
  details: {
    display: 'flex',
    flexDirection: 'column',
    padding: theme.spacing(1),
  },
  icon: {
    color: '#4caf50',
  },
}));

const itemWrapper = props => {

  const classes = useStyles();

  const { title, children } = props;

  return (
    <Box width='100%'>
      <Accordion defaultExpanded style={{ border: '1px solid #ddd' }} elevation={0}>
        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
          {/* <Box mr={1}>
            <IconOK className={classes.icon} />
          </Box> */}
          <Typography className={classes.head}>{title}</Typography>
        </AccordionSummary>
        <AccordionDetails className={classes.details}>
          {children}
        </AccordionDetails>
      </Accordion>
    </Box>
  );
};

export default itemWrapper;

