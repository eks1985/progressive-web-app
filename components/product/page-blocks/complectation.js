import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

const styles = theme => ({
  root: {
    padding: '-20px',
  },
});

const Complectation = props => {
  const { classes, complectation } = props;
  const dense = false;
  return (
    <div className={classes.root}>
      <List dense={dense}>
        {
          complectation.map(item => {
            return (
              <ListItem
                button
                key={item}
                style={{ padding: '8px 12px', borderTop: '1px solid #eee' }}
              >
                <ListItemText
                  primaryTypographyProps={{
                    style: { lineHeight: '1.2', fontSize: '14px' },
                  }}
                  primary={item}
                />
              </ListItem>
            );
          })
        }
      </List>
    </div>
  );
};

export default withStyles(styles)(Complectation);
