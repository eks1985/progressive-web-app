import React, { useState } from 'react';
import { useStore } from 'lib/store';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Slide from '@material-ui/core/Slide';
import OfferIcon from '@material-ui/icons/ContactMail';
import IconButton from '@material-ui/core/IconButton';
import SvgIcon from '@material-ui/core/SvgIcon';
import Loading from 'components/loading/indicator';
import { sendClientOfferRequest } from 'lib/api/products';
import { baseUrl0, secret } from 'lib/base';

const useStyles = makeStyles(theme => ({
  dialog: {
    maxWidth: '900px',
    margin: '0 auto',
  },
  appBar: {
    position: 'relative',
    display: 'flex',
  },
  toolbar: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  reqBtn: {
    minWidth: '148px',
    marginRight: theme.spacing(2),
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(2),
  },
}));

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction='up' ref={ref} {...props} />;
});

const clientOffer = props => {
  
  const classes = useStyles();
  
  const [open, setOpen] = useState(false);
  const [done, setDone] = useState(false);
  
  const toggleOpen = () => {
    setOpen(!open);
  }

  const store = useStore();
  const { appUser, country, loading, setLoading } = store;
  const { product, offer, url_create } = props;

  return (
    <React.Fragment>
      <Button
        key='offer-btn'
        className={classes.reqBtn}
        color='primary'
        onClick={toggleOpen}
      >
        <OfferIcon />
        <Box ml={1}>
          Предложение
        </Box>
      </Button>
      <Dialog
        key='offer-items-dlg'
        className={classes.dialog}
        fullScreen
        open={open}
        onClose={toggleOpen}
        TransitionComponent={Transition}
      >
        <AppBar className={classes.appBar}>
          <Toolbar className={classes.toolbar}>
            <Typography>
              Предложение клиенту
            </Typography>
            <Button
              color='inherit'
              onClick={toggleOpen}
            >
              Закрыть
            </Button>
          </Toolbar>
        </AppBar>
        <Paper square>
          <Box mt={2} p={2}>
            {offer.map((item, i) => {
              return (
                <Box key={i} display='flex' alignItems='center'>
                  <Box mr={1}>
                    <IconButton
                      onClick={
                        () => {
                          sendClientOfferRequest(url_create, product, item, country, appUser, setLoading, setDone);    
                        }
                      }
                    >
                      <SvgIcon>
                        <path d='M4,4H20A2,2 0 0,1 22,6V18A2,2 0 0,1 20,20H4C2.89,20 2,19.1 2,18V6C2,4.89 2.89,4 4,4M12,11L20,6H4L12,11M4,18H20V8.37L12,13.36L4,8.37V18Z' />
                      </SvgIcon>  
                    </IconButton>
                  </Box>
                  <Box mr={1}>
                    <IconButton
                      onClick={
                        () => {
                          window.open(item.gdRef);
                        }
                      }
                    >
                      <SvgIcon>
                        <path d='M7.71,3.5L1.15,15L4.58,21L11.13,9.5M9.73,15L6.3,21H19.42L22.85,15M22.28,14L15.42,2H8.58L8.57,2L15.43,14H22.28Z' />
                      </SvgIcon>  
                    </IconButton>
                  </Box>
                  <Box>
                    <Typography variant='subtitle1'>
                      {item.title}
                    </Typography>
                  </Box>
                </Box>
              );    
            })}
            {loading &&
              <Box width='100%' display='flex' justifyContent='center' mt={2}>
                <Loading />
              </Box>
            }
            {!loading && done &&
              <Box width='100%' display='flex' justifyContent='center' textAlign='center' mt={2}>
                <Typography variant='subtitle1'>
                  Ваш запрос успешно отправлен. В ближайшее время вы получите ответ по электронной почте.       
                </Typography>
              </Box>  
            }
          </Box>
        </Paper>
      </Dialog>
    </React.Fragment>
  );

};

clientOffer.defaultProps = {
  url_create: `${baseUrl0}data-create${secret}`
};

export default clientOffer;
