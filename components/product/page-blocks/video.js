import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import IconVideo from '@material-ui/icons/Subscriptions';

const styles = theme => ({
  button: {
    margin: theme.spacing(1),
  },
  extendedIcon: {
    marginRight: theme.spacing(1),
  },
});

const Video = props => {
  const { classes, video, lang, translate } = props;
  return (
    <div className='d-flex justify-content-center mb-2'>
      <Button
        variant='contained'
        aria-label='Show'
        className={classes.button}
        onClick={
          () => {
            window.open(video);
          }
        }
      >
        <IconVideo className={classes.extendedIcon} style={{ color: '#555' }} />
        {translate('Перейти к видео', lang)}
      </Button>
    </div>
  );
};

export default withStyles(styles)(Video);
