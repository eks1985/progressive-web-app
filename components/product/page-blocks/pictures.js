import React from 'react';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import PictureDialog from './picture-dialog';

const Card = props => {
  const { item, current, setCurrent, cond, i } = props;
  const containerStyle = {
    width: cond ? '100%' : '120px',
    cursor: 'pointer',
    borderRadius: '4px',
    margin: '4px',
  };
  const photoStyle = {
    display: 'flex',
    justifyContent: 'center',
    height: 'auto',
    minHeight: cond ? '240px' : '80px',
    width: cond ? '100%' : '120px',
    backgroundImage: `url("${item}")`,
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'contain',
    backgroundPosition: 'center',
    borderRadius: '4px',
  };
  return (
    <Paper
      style={containerStyle}
      elevation={item !== current || cond ? 0 : 5}
      onClick={
        () => {
          if (cond) {
            const link = document.createElement('a');
            document.body.appendChild(link);
            link.href = item;
            link.target = '_blank';
            link.click();
          } else {
            setCurrent(item, i);
          }
        }
      }
    >
      <div style={photoStyle} />
    </Paper>
  );
};

const Pictures = props => {
  const { pictures, current, ind, cond } = props;
  const containerStyle = {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    flexWrap: 'wrap',
    marginTop: '12px',
    paddingBottom: '12px',
  };
  const mainPictContainerStyle = {
    height: cond ? '180px' : '360px',
    width: cond ? '100%' : '600px',
    marginTop: '12px',
    minHeight: cond ? '240px' : 'initial',
    position: 'relative',
  };
  const mainPictStyle = {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: cond ? '180px' : '360px',
    width: cond ? '100%' : '600px',
    backgroundImage: `url("${current}")`,
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'contain',
    backgroundPosition: 'center',
    marginTop: '8px',
  };
  return (
    <div style={containerStyle}>
      {pictures.length > 0 &&
        <Box display='flex' flexWrap='wrap' justifyContent='center' width='100%'>
          {
            pictures.map((item, i) => <Card key={i} item={item} {...props} i={i} />)
          }
        </Box>
      }
      <Box mt={3}>
        <Paper elevation={0} style={mainPictContainerStyle}>
          <div style={mainPictStyle} />
          <PictureDialog pictures={pictures} ind={ind} />
        </Paper>
      </Box>
    </div>
  );
};

class PicturesContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = { current: props.pictures[0], ind: 0 };
  }
  setCurrent = (current, ind) => {
    this.setState({ current, ind });
  }
  render() {
    // console.log('ind', this.state.ind);
    return <Pictures current={this.state.current} ind={this.state.ind} setCurrent={this.setCurrent} {...this.props} />;
  }
}

export default PicturesContainer;
