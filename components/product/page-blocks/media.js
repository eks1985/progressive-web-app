import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles(theme => ({
  button: {
    margin: theme.spacing(1),
    color: 'white',
  },
  extendedIcon: {
    marginRight: theme.spacing(1),
  },
}));

const MediaElem = props => {

  const classes = useStyles();
  const { title, path } = props;
   
  return (
    <Button
      color='primary'
      variant='contained'
      aria-label='Show'
      className={classes.button}
      onClick={
        () => {
          window.open(path);
        }
      }
    >
      {title}
    </Button>
  );
};

const media = props => {

  const classes = useStyles();
  
  const { product } = props;
  const { mpict, mvid, moffers, mleafl } = product;
  const ok = mpict || moffers || mvid || mleafl;

  if (!ok) {
    return null;
  }

  return (
    <Box p={2}>
      <Box display='flex' flexWrap='wrap'>
        {mpict &&
          <MediaElem title='Фото' path={mpict} />
        }
        {mvid &&
          <MediaElem title='Видео' path={mvid} />
        }
        {mleafl &&
          <MediaElem title='Брошюры' path={mleafl} />
        }
        {moffers &&
          <MediaElem title='Кп клиентам' path={moffers} />
        }
      </Box>
      <Box mt={2} textAlign='center'>
        <Typography variant='body2'>
          Все объекты медиабиблиотеки можно сделать доступными оффлайн
        </Typography>
      </Box>
    </Box>
  );
};

export default media;
