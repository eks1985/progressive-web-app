import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import ReactMarkdown from 'react-markdown';
import Box from '@material-ui/core/Box';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(2),
    whiteSpace: 'normal',
    fontSize: '14px',
    textAlign: 'justify',
    color: '#333',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
}));

const details = props => {

  const classes = useStyles();
  const { details } = props;
  
  return (
    <div className={classes.root}>
      <ReactMarkdown>
        {details}
      </ReactMarkdown>
      <Box mt={1} borderTop='4px solid #E1001A' width='25%' />
    </div>
  );
};

export default details;
