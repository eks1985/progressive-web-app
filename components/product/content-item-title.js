import React from 'react';
import Subheader from '@material-ui/core/ListSubheader';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

const contentItemTitle = props => {

  const { title } = props;

  return (
    <Subheader>
      <Box mt={1}>
        <Typography color='primary' variant='subtitle1'>
          {title}
        </Typography>
      </Box>
    </Subheader>
  );
  
};

export default contentItemTitle;
