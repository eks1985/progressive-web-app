import React from 'react';
import { useStore } from 'lib/store';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import LoadIcon from '@material-ui/icons/GetApp';
import { translate } from 'lib/vacab';
import Cart from 'lib/cart';
import ClientOffer from './page-blocks/client-offer';

const useStyles = makeStyles(theme => ({
  root: {
    marginTop: props => props.cond ? '12px' : '60px',
  },
  button: {
    margin: 0,
  },
  iconColor: {
    color: '#E1001A',
  },
  btn: {
    minWidth: '148px',
    marginRight: theme.spacing(2),
    marginTop: theme.spacing(1),
  },
  reqBtn: {
    minWidth: '148px',
    marginRight: theme.spacing(2),
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(2),
  }
}));

const navActions = props => {

  const store = useStore();
  const { lang, permiss, cond, globalOpt, appUser, setCart, setCartOpen } = store;
  const classes = useStyles({ cond });

  if (!permiss) {
    return null;
  }

  const {
    product,
    product: { mkt, offer },
  } = props;

  const handleAddToCart = () => {
    let cart = store.cart;
    if (!cart) {
      cart = new Cart();
    }
    cart.addProduct(product, globalOpt);
    if (appUser) {
      cart.update({ event: 'changeProp', prop: 'customer', value:  appUser.customerCurOpt });   
    }
    setCart(cart);
    setCartOpen(true);
  }

  return (
    <Box display='flex' flexDirection='column' alignItems='center' justifyContent='center' ml={1} className={classes.root}>
      {(permiss.orders.placeOrders || permiss.atoms.ordersSend) &&
        <Button
          color='primary'
          variant='contained'
          className={classes.reqBtn}
          onClick={handleAddToCart}
        >
          {translate('Запросить', lang)}
        </Button>
      }
      {product.country === 1 &&
        <Button
          className={classes.btn}
          onClick={
            () => {
              window.open('https://docs.google.com/presentation/d/e/2PACX-1vSGn_w84IUxId1rN2G75uD78OIYz_pkb3pnapzPDHFzehIcE_5NnHFHr1AWsMwFrAwVnPInrSmXP6Bs/pub?start=false&loop=false&delayms=3000');
            }
          }
        >
          ЛІЗИНГ ВІД 0%
        </Button>
      }
      {mkt && mkt.length > 0 &&
        mkt.map((item, i) => {
          return (
            <Button
              key={i}
              className={classes.btn}
              color='primary'
              onClick={
                () => {
                  window.open(item.path);
                }
              }
            >
              <LoadIcon />
              <Box ml={1}>
                Материалы
              </Box>
            </Button>
          );
        })
      }
      {offer &&
        <ClientOffer product={product} offer={offer} />
      }
    </Box>
  );
};

export default navActions;

