import React from 'react';
import { getProductChipsItems } from 'lib/internals/catalog';
import { translate, getSetTitle } from 'lib/vacab';
import Box from '@material-ui/core/Box';
import ChipsRow from 'components/product-card/chips-row';
import Modificators from 'components/product-card/modificators';
import TechDataTable from 'pages/catalog/products/tech-data-table';
import ContentItem from './content-item';
import ContentItemTitle from './content-item-title';
import SetTitle from './set-title';
import Details from './page-blocks/details';
import Complectation from './page-blocks/complectation';
import Media from './page-blocks/media';
import Pictures from './page-blocks/pictures';
import Video from './page-blocks/video';
import ContentRow from 'pages/content/item';

const prepareContainerStyle = cond => {
  return {
    display: 'flex',
    flex: '1',
    flexDirection: 'column',
    padding: cond ? '0px' : '20px',
    minHeight: '200vh',
    maxWidth: cond ? '360px' : '900px',
    margin: '0 auto',
  };
};

const zeroPaddingStyle = { padding: '0px' };

const content = props => {

  const {
    product,
    opt,
    cond,
    country,
    permiss,
    lang,
    varOpen,
    setVarOpen,
    setActiveVal,
  } = props;

  const {
    details,
    complectation,
    pictures,
    tech,
    adv,
    contentList,
    video,
  } = product;

  const toggleVarOpen = () => {
    setVarOpen(!varOpen);
  };

  const translatedTitle = getSetTitle(product, lang);

  return (
    <div style={prepareContainerStyle(cond)}>
      <ContentItem id='item1' additionStyle={zeroPaddingStyle} cond={cond}>
        <SetTitle
          title={translatedTitle}
          cond={cond}
          country={country}
          permiss={permiss}
          lang={lang}
          translate={translate}
          product={product}
          opt={opt}
        />
        <Box p={1}>
          <ChipsRow items={getProductChipsItems(product, opt)} varOpen={varOpen} toggleVarOpen={toggleVarOpen} />
        </Box>
        {varOpen &&
          <Modificators opt={opt} setActiveVal={setActiveVal} />
        }
        {details &&
          <Details details={details} adv={adv} />
        }
        {contentList && contentList.length > 0 &&
          <Box p={1}>
            {contentList.map((item, i) => {
              return (
                <Box key={i} mt={2}>
                  <ContentRow item={item} renderClient />
                </Box>  
              )
            })}
          </Box>
        }
        {video &&
          <Video
            video={video}
            lang={lang}
            translate={translate}
          />
        }
      </ContentItem>
      {complectation &&
        <ContentItem id='item2'>
          <Box>
            <ContentItemTitle title={translate('Стандартная комплектация', lang)} />
          </Box>
          <Complectation complectation={complectation} />
        </ContentItem>
      }
      {tech &&
        <ContentItem id='item3' additionStyle={{ display: 'flex', flexDirection: 'column' }} cond={cond}>
          <ContentItemTitle title={translate('Характеристики', lang)} />
          <Box pb={2}>
            <TechDataTable tech={tech} />
          </Box>
        </ContentItem>
      }
      {pictures &&
        <ContentItem id='item4' cond={cond} additionStyle={{ width: '100%' }}>
          <Pictures pictures={pictures} cond={cond} />
          <div className='mb-3'></div>
        </ContentItem>
      }
      <ContentItem id='item3' additionStyle={{ display: 'flex', flexDirection: 'column' }} cond={cond}>
        <ContentItemTitle title={translate('Медиабиблиотека', lang)} />
        <Box pb={2}>
          <Media product={product} />
        </Box>
      </ContentItem>
      <div style={{ height: '50vh' }} />
    </div>
  );
};

export default content;



