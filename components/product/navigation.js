/* eslint-disable prefer-destructuring */
import React, { useEffect } from 'react';
import Scroll from 'react-scroll';
import Button from '@material-ui/core/Button';
import { translate } from 'lib/vacab';

const Link = Scroll.Link;
const Events = Scroll.Events;
const scroll = Scroll.animateScroll;
const scrollSpy = Scroll.scrollSpy;

const navItemButtonStyle = {
  width: '200px',
};

const NavigationItem = props => {

  const { to, handleSetActive, title } = props;

  return (
    <Link activeClass='setNavActive' to={to} spy smooth offset={-87} duration={500} onSetActive={handleSetActive}>
      <Button style={navItemButtonStyle}>
        {title}
      </Button>
    </Link>
  );
};

const navigation = props => {

  useEffect(() => {

    scrollSpy.update();
    
    return () => {
      Events.scrollEvent.remove('begin');
      Events.scrollEvent.remove('end');
    }

  }, []);

  const scrollToTop = () => {
    scroll.scrollToTop();
  };
  const scrollToBottom = () => {
    scroll.scrollToBottom();
  };
  
  const scrollTo = () => {
    scroll.scrollTo(100);
  };
  
  const scrollMore = () => {
    scroll.scrollMore(100);
  };

  const handleSetActive = () => {
    // console.log(to);
  };

  const { product, lang } = props;
  const { complectation, tech, pictures } = product;

  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <NavigationItem to='item1' handleSetActive={handleSetActive} title={translate('Описание', lang)} />
      {complectation &&
        <NavigationItem to='item2' handleSetActive={handleSetActive} title={translate('Комплектация', lang)} />
      }
      {tech &&
        <NavigationItem to='item3' handleSetActive={handleSetActive} title={translate('Характеристики', lang)} />
      }
      {pictures &&
        <NavigationItem to='item4' handleSetActive={handleSetActive} title={translate('Фото', lang)} />
      }
    </div>
  );
};

export default navigation;


