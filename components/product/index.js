import React, { useState, useEffect } from 'react';
import { useStore } from 'lib/store';
import { orderBy } from 'lodash';
import NavigationWrapper from './navigation-wrapper';
import Navigation from './navigation';
import NavigationActions from './navigation-actions';
import Content from './content';
import { initOpt } from 'components/product-card/internals';
import withWidth, { isWidthUp } from '@material-ui/core/withWidth';

const prepareContainerStyle = condensed => {
  const baseStyle = { display: 'flex', flex: '1 1 auto' };
  if (condensed) {
    baseStyle.flexDirection = 'column';
    baseStyle.alignItems = 'center';
  }
  return baseStyle;
}

const product = props => {

  const { product, width } = props;
  const productSingle = product[0];
  const store = useStore();
  const { permiss, lang, country } = store;

  const cond = !isWidthUp('md', width);

  let optInit;
  if (productSingle && productSingle.opt) {
    optInit = orderBy(initOpt(productSingle.opt), ['order'], ['asc']);
  }
  if (productSingle && store.globalOpt && productSingle.id === store.globalOpt.id) {
    optInit = orderBy(store.globalOpt.opt, ['order'], ['asc']);
  }

  const [varOpen, setVarOpen] = useState(false);
  const [opt, setOpt] = useState(optInit);
  
  if (!product || product.length === 0) {
    return null;
  }
  
  const { id } = productSingle;

  useEffect(() => {

    if (store.globalOpt && id === store.globalOpt.id) {
      setOpt(store.globalOpt.opt)
    }

  }, [store.globalOpt]);

    
  const setActiveVal = (title, value) => {
    
    const updated = opt.map(item => {
      if (item.title === title) {
        item.values = item.values.map(valueItem => {
          if (valueItem.value === value) {
            return { ...valueItem, active: true };
          }
          const { active, ...other } = valueItem;
          return other;
        })  
      }
      return item;
    });
    
    setOpt(updated);

    store.setGlobalOpt({
      id: product.id,
      opt: updated,
    });
    
  };

  return (
    <div style={prepareContainerStyle(cond)}>
      <NavigationWrapper cond={cond}>
        {!cond &&
          <Navigation product={productSingle} lang={lang} />
        }
        <NavigationActions
          product={productSingle}
          cond={cond}
          permiss={permiss}
          lang={lang}
        />
      </NavigationWrapper>
      <Content
        id={id}
        product={productSingle}
        country={country}
        cond={cond}
        permiss={permiss}
        lang={lang}
        opt={opt}
        setActiveVal={setActiveVal}
        varOpen={varOpen}
        setVarOpen={setVarOpen}
      />
    </div>
  );

}

export default withWidth()(product);

// NavigationWrapper
//   if (no cond) Navigation
//   NavigationActions
//     Запросить
//     Лизинг
//     Маркеинг
//     Запросить
// Content
//     Bredcrambs
//     SetTitle
//     SetProps
//     Details
//     Content
//     Video
//     Complectation
//     Tech
//     Pictures
//     Media




