import React from 'react';
import Paper from '@material-ui/core/Paper';

const contentItem = props => {

  const { id, last, children, additionStyle, cond } = props;
  const style = {
    marginBottom: '30px',
    position: 'relative',
    width: cond ? '100%' : 'initial',
  };
  if (last) {
    style.minHeight = '100vh';
  }
  return (
    <Paper square style={{ ...style, ...additionStyle }} id={id}>
      {children}
    </Paper>
  );
};

contentItem.defaultProps = {
  additionStyle: {},
};

export default contentItem;
