import React from 'react';

const prepareStyles = cond => {

  const baseStyle = {
    wrapper1: {
      display: 'flex', width: '250px', padding: '30px',
    },
    wrapper2: {
      display: 'flex', flexDirection: 'column', position: 'fixed', top: '20vh',
    },
  };
  if (cond) {
    baseStyle.wrapper2.position = 'initial';
    baseStyle.wrapper2.top = 'initial';
    baseStyle.wrapper1.padding = 'initial';
    baseStyle.wrapper1.width = 'initial';
    baseStyle.wrapper1.marginTop = '0px';
  }
  return baseStyle;
  
};

const navigation = props => {

  const { children, cond } = props;

  const styles = prepareStyles(cond);

  return (
    <div style={styles.wrapper1} className='wrapper1'>
      <div style={styles.wrapper2} className='wrapper2'>
        {children}
      </div>
    </div>
  );

};

export default navigation;
