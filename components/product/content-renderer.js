import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import ReactMarkdown from 'react-markdown';
import PictureDialog from 'components/PictureDialog';
import VideoNative from 'components/VideoNative';
import styles from './product.module.css'; // eslint-disable-line

const useStyles = makeStyles(theme => ({
  tx: {
    color: '#555',
    textAlign: 'justify',
    fontSize: '14px',
  },
  tt: {
    lineHeight: '1.3',
    textAlign: 'center',
    marginBottom: theme.spacing(2),
  },
  itemContainer: {
    padding: theme.spacing(2),
    margin: theme.spacing(3),
  }
}));

const Picture = props => {

  const { url, condensed, ph, children } = props;

  const containerStyle = {
    marginTop: '4px',
    justifyContent: 'center',
    flex: '0 0 100%',
    height: condensed ? '200px' : `${ph || 200}px`,
    backgroundImage: `url("${url}")`,
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'contain',
    borderRadius: '4px',
    backgroundPosition: 'center center',
    position: 'relative',
  };

  return (
    <Box mb={2}>
      <Box display='flex'>
        <div style={containerStyle}>
          {children}
        </div>
      </Box>
    </Box>
  );
  
};

const Item = props => {

  const classes = useStyles();
  
  const { item, cond } = props;

  const { tt, tx, htt, pc, ph, inl, vd, mt, id, txtp } = item;

  if (vd !== undefined) {
    return (
      <VideoNative d={{ videoId: vd, mt, tx }} />
    );
  }

  const content = [];

  if (pc) {
    content.push(
      <Picture
        key={id}
        url={pc}
        ph={ph}
        cond={cond}
      >
        <PictureDialog pictures={[pc]} />
      </Picture>
    );
  }

  if (tx) {
    content.push(
      <div className={classes.tx}>
        <ReactMarkdown source={tx} />
      </div>
    );
  }
  if (txtp) {
    content.reverse();
  }

  return (
    <Paper square elevation={inl ? 0 : 1} className={classes.itemContainer} >
      {!htt &&
        <Typography variant='body2' className={classes.tt}>
          {tt}
        </Typography>
      }
      {content}
    </Paper>
  );
};

const contentRenderer = props => {

  const { content, cond } = props;

  return (
    <Box width='100%'>
      {
        content.map(item => (
          <Item key={item.id} item={item} cond={cond} />
        ))
      }
    </Box>
  );
};

export default contentRenderer;
