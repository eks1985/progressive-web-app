import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Prices from 'components/product-card/prices';

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    alignItems: 'center',
    justifyContent: 'space-between',
    minHeight: '60px',
    padding: '16px',
    boxShadow: 'rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px',
    fontSize: '20px',
    background: theme.palette.primary.main,
    color: 'white',
  },
  priceContainer: {
    width: '100%',
    fontSize: '12px',
  },
  priceLabel: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  optionsContainer: {
    fontSize: '12px',
    marginTop: '4px',
    display: 'flex',
    justifyContent: 'center',
    textAlign: 'center',
  },
  titleCond: {
    width: '100%',
    textAlign: 'center',
    marginBottom: '4px',
    color: '#fff',
  },
  title: {
    color: '#fff',
  },
}));

const getModifValue = modif => {

  return modif.values.find(item => item.active).props.alias;

};

const setTitle = props => {

  const {
    cond,
    title,
    product,
    opt,
  } = props;

  if (!opt) {
    return null;
  }

  const optTitle = opt.reduce((res, item, i) => {
    return res += `${getModifValue(item)} ${i === opt.length - 1 ? '' : '/ '}`;
  }, '');
  
  const classes = useStyles();

  return (
    <div className={classes.container}>
      <Typography variant='h6' className={cond ? classes.titleCond: classes.title}>
        {title}
      </Typography>
      <div className={classes.priceContainer}>
        <div className={classes.priceLabel}>
          <Prices product={product} opt={opt} />
        </div>
        <div className={classes.optionsContainer}>
          {optTitle}
        </div>
      </div>
    </div>
  );
};

setTitle.defaultProps = {
  priceType: 'dealer',
  price: 0,
};

export default setTitle;
