import React from 'react';
import { useStore } from 'lib/store';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import IconHistory from '@material-ui/icons/History';
import Cart from 'lib/cart';
import { deleteRequest, confirmRequest } from 'lib/api/customer-request';

async function deleteRequestHandler(id, store, mutate) {
  await deleteRequest(id, store, mutate);
};

async function confirmRequestHandler(order, store, mutate) {
  await confirmRequest(order, store, mutate);
};

const actions = props => {

  const { order, mutate } = props;
  const { id } = order;
  const store = useStore();
  const { setCart, setCartOpen, setMutate, appUser } = store;

  return (
    <Box display='flex' justifyContent='space-between' mt={3} mb={1} alignItems='center'>
      <Box>
        <IconButton size='small'>
          <IconHistory />
        </IconButton>
      </Box>
      <Box display='flex' justifyContent='flex-end'>
        {!order.confirmed &&
          <Box ml={1}>
            <Button
              size='small'
              color='primary'
              onClick={deleteRequestHandler.bind(null, id, store, mutate)}
            >
              Удалить
            </Button>   
          </Box>
        }
        {!order.confirmed &&
          <Box ml={1}>
            <Button
              size='small'
              color='primary'
              onClick={
                () => {
                  const cart = new Cart(order);
                  setCart(cart);
                  setCartOpen(true); 
                  setMutate({ action: mutate });
                }
              }
            >
              Изменить
            </Button> 
          </Box>
        }
        {!order.confirmed && appUser.internal &&
          <Box ml={1}>
            <Button
              size='small'
              variant='contained'
              color='primary'
              onClick={confirmRequestHandler.bind(null, order, store, mutate)}
            >
              Подтвердить
            </Button> 
          </Box>
        }
      </Box>
      </Box>
  );

}

export default actions;