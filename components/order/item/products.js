import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import { formatNumb } from 'lib/format';

const useStyles = makeStyles(theme => ({
  product: {
    fontSize: theme.typography.pxToRem(14),
    color: '#2962ff',
  },
}));

const products = props => {

  const classes = useStyles();
  const { items } = props;

  return (
    <Box mt={2}>
      {items.map((item, i) => {
        const borderTop = i === 0 ? '1px solid #eee' : 'none';
        const borderBottom = i === items.length - 1 ? '1px solid #eee' : 'none';
        return (
          <Box key={i} borderTop={borderTop} borderBottom={borderBottom} className='item-row' display='flex' flexWrap='wrap' justifyContent='space-between' alignItems='center'>
            {/* item */}
            <Box mr={1} width='50%'>
              <Typography variant='subtitle2' className={classes.product}>
                {`${item.qty} x ${item.product.title || item.product.descr}`}
              </Typography>
            </Box>
            {/* prixe and value */}
            <Box width='40%' display='flex' justifyContent='flex-end'>
              <Box width='50%' textAlign='right'>
                <Box>
                  <Typography variant='caption'>
                    Цена 
                  </Typography>
                </Box>
                <Box>
                  <Typography variant='subtitle2'>
                    {`€ ${formatNumb(item.price)}`}
                  </Typography>
                </Box>
              </Box>
              <Box width='50%' textAlign='right'>
                <Box >
                  <Typography  variant='caption'>
                    Сумма
                  </Typography>
                </Box>
                <Box>
                  <Typography variant='subtitle2'>
                    {`€ ${formatNumb(item.value)}`}
                  </Typography>
                </Box>
              </Box>
            </Box>  
          </Box>
        );
      })}
    </Box>
  );
};

export default products;
