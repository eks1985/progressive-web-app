import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useStore } from 'lib/store';
import { useRouter } from 'next/router';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import FormLabel from '@material-ui/core/FormLabel';

const useStyles = makeStyles(theme => ({
  container: {
    
  },
}));

const payments = props => {

  const { payment } = props;

  return (
    <Box margin='0 auto' width='100%' mt={2}>
      <FormLabel>
        Условия оплаты
      </FormLabel>
      <Box>
        {payment.map((item, i) => {
          return (
            <Box key={i} display='flex'>
              <Box width={50}>
                <Typography variant='body2'>
                  {`${item.percent} %`}
                </Typography>
              </Box>
              <Box width={250}>
                <Typography variant='body2'>
                  {`${item.value} ${item.type === 'days' ? ' дней кредит': ''}`}
                </Typography>
              </Box>
            </Box>
          )
        })}
      </Box>
    </Box>
  );

};

export default payments;
