import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import grey from '@material-ui/core/colors/grey';
import blue from '@material-ui/core/colors/blue';
import green from '@material-ui/core/colors/green';
import { formatNumb } from 'lib/format';
import Actions from './actions';
import Products from './products';
import DelivDate from './delivery';
import Payment from './payment';
import Complete from './complete';
import Notice from './notice';

const getColor = stat => {
  switch (stat) {
    case -2:
      return grey[500];
    case -1:
      return '#E1001A';
    default:
      return grey[500];
  }
};

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    marginTop: '1px',
    marginBottom: theme.spacing(2),
    borderLeft: props => `3px solid ${props.color}`,
  },
  accordion: {
    margin: '0px',
  },
  heading: {
    fontSize: theme.typography.pxToRem(14),
    color: theme.palette.text.primary,
  },
  product: {
    fontSize: theme.typography.pxToRem(14),
    color: '#2962ff',
  },
  header: {
    paddingLeft: '8px',
    paddingRight: '8px',
    margin: '0px',
    position: 'relative',
  },
  details: {
    display: 'flex',
    flexDirection: 'column',
    padding: theme.spacing(1),
    fontSize: theme.typography.pxToRem(14),
  },
  stage: {
    color: green[700],
  }
}));

const getStage = order => {

  if (order.processed) {
    return 'Обработан';
  } else if (order.confirmed) {
    return `Подтвержден - ${order.confirmed.user.name || order.confirmed.user.email}`;
  } else {
    return `Создан - ${order.created.user.name || order.confirmed.user.email}`;
  }

};

const order = props => {

  const [expanded, setExpanded] = React.useState(false);

  const handleChange = panel => (_, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  const { order, mutate } = props;
  const { createDate, id, items, delivDate, payment, totalValue, customer, notice } = order;

  const color = getColor(-1);
  const classes = useStyles({ color });
  return (
    <div className={classes.root}>
      <Accordion square className={classes.accordion} expanded={expanded === 'panel1'} onChange={handleChange('panel1')}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-label='Expand'
          aria-controls='additional-actions1-content'
          id='additional-actions1-header'
          className={classes.header}
        >
          <Box width='100%'>
            <Box display='flex' justifyContent='space-between'>
              <Typography variant='body2' className={classes.heading}>{createDate}</Typography>
              <Box ml={2}>
                <Typography variant='body2' className={classes.heading}>{id}</Typography>
              </Box>
            </Box>
            {expanded !== 'panel1' && items.length > 0 &&
              <Box mt={1} display='flex' justifyContent='space-between'>
                <Typography variant='subtitle2' className={classes.product}>{items[0].product.title}</Typography>
                <Box ml={2}>
                  <Typography variant='subtitle2' className={classes.heading}>{`€ ${formatNumb(totalValue)}`}</Typography>
                </Box>
              </Box>
            }
          </Box>
        </AccordionSummary>
        <AccordionDetails className={classes.details}>
          <Box>
            <Typography variant='body2'>
              {`${customer.value} ${customer.label}`}
            </Typography>
          </Box>
          <Box>
            <Typography variant='body2' className={classes.stage}>
              {getStage(order)}
            </Typography>
          </Box>
          {/* <Box mt={1}>
            <Complete />
          </Box> */}
          <Products items={items} />
          <DelivDate delivDate={delivDate} />
          <Payment payment={payment} />
          <Notice notice={notice} />
          <Actions order={order} mutate={mutate} />
        </AccordionDetails>
      </Accordion>
    </div>
  );
};

export default order;
