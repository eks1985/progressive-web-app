import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useStore } from 'lib/store';
import { useRouter } from 'next/router';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import FormLabel from '@material-ui/core/FormLabel';

const useStyles = makeStyles(theme => ({
  container: {
    
  },
}));

const delivDate = props => {

  const { notice } = props;

  return (
    <Box margin='0 auto' width='100%' mt={2}>
      <FormLabel>
        Комментарий
      </FormLabel>
      <Box>
        <Typography variant='body2'>
          {notice}
        </Typography>
      </Box>
    </Box>
  );

};

export default delivDate;
