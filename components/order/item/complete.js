import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useStore } from 'lib/store';
import { useRouter } from 'next/router';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import FormLabel from '@material-ui/core/FormLabel';

const useStyles = makeStyles(theme => ({
  container: {
    
  },
}));

const delivDate = props => {

  const { delivDate } = props;

  return (
    <Box margin='0 auto' width='100%' mt={2} display='flex' alignItems='center'>
      <FormLabel>
        Отгружен на: 
      </FormLabel>
      <Box ml={1} mr={2}>
        <Typography variant='subtitle2'>
          50%
        </Typography>
      </Box>
      <FormLabel>
        Оплачен на: 
      </FormLabel>
      <Box ml={1}>
        <Typography variant='subtitle2'>
          30%
        </Typography>
      </Box>
    </Box>
  );

};

export default delivDate;
