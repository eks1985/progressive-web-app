import React, { useEffect, useState } from 'react';
import Box from '@material-ui/core/Box';
import Filters from 'components/filters';
import Item from './item';

// filters [
//   {
//     "name": "customer",
//     "label": "Дилер",
//     "items": [
//       {
//         "value": "10000008",
//         "label": "MASCHIO GASPARDO RUSSIA"
//       },
//       {
//         "value": "10002000",
//         "label": "Мустанг-Сибирь"
//       }
//     ],
//     "value": [
//       {
//         "value": "10000008",
//         "label": "MASCHIO GASPARDO RUSSIA"
//       },
//       {
//         "value": "10002000",
//         "label": "Мустанг-Сибирь"
//       }
//     ],
//     "mult": true
//   }
// ]

const filter = (items, filters) => {
  const callback = item => {
    for (const filter of filters) {
      if (filter.value.length > 0) {
        const arr = filter.value.map(fvalue => fvalue.value);
        if (filter.name === 'customer') {
          if (!arr.includes(item.customer.value)) {
            return false;
          }
        }
      }
    }
    return true;
  };
  return items.filter(callback);
};

const prepareFilters = items => {

  const filters = [];
  const filter = {
    name: 'customer',
    label: 'Дилер',
    items,
    value: [],
    mult: true,
  };

  filters.push(filter)
  return filters;

};

const list = props => {

  const [filters, setFilters] = useState(prepareFilters(props.filters));
  const [itemsFiltered, setItemsFiltered] = useState(props.items);

  useEffect(() => {
    setItemsFiltered(filter(props.items, filters));
  }, [props.items]);

  const { items, mutate } = props;

  const filtersEdit = action => {
    const {  index, value } = action;
    const copy = [...filters];
    copy[index].value = value;
    setFilters(copy);
  };

  const filtersApply = () => {
    setItemsFiltered(filter(items, filters));
  };

  const filtersReset = () => {
    const copy = [...filters];
    for (const filter of copy) {
      filter.value = [];
    }
    setFilters(copy);
  };

  const actions={
    filtersEdit,
    filtersApply,
    filtersReset, 
  }

  return (
    <Box margin='0 auto' width='100%' maxWidth={600}>
      <Box>
        <Filters filters={filters} {...actions} />
      </Box>
      <Box mt={2}>
        {itemsFiltered.map((order, i) => {
          return <Item order={order} key={i} mutate={mutate} />
        })}
      </Box>
    </Box>
  );

};

export default list;
