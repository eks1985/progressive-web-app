import React from 'react';
import DataProvider from 'lib/data';
import Content from './content';

const categoryHtmlContentRenderer = props => {

  const { content, condensed } = props;
  
  const ids = content.join(',');
  const url = `https://webhooks.mongodb-realm.com/api/client/v2.0/app/mgd-ilrht/service/mgd/incoming_webhook/content?secret=L0sS3Bz1mdCm6Kuz&ids=${ids}`;

  if (!Array.isArray(content)) {
    return null;
  }

  return (
    <DataProvider url={url} render={data => (
      <Content data={data} condensed={condensed} />
    )}/>
  );

};

export default categoryHtmlContentRenderer;

