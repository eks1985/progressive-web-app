import React from 'react';
import Box from '@material-ui/core/Box';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import ReactMarkdown from 'react-markdown';
import PictureDialog from 'components/picture-dialog';

const Picture = ({ url, condensed, ph, children }) => {
  const containerStyle = {
    marginTop: '4px',
    justifyContent: 'center',
    flex: '0 0 100%',
    height: condensed ? '200px' : `${ph || 200}px`,
    backgroundImage: `url("${url}")`,
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'contain',
    borderRadius: '4px',
    backgroundPosition: 'center center',
    position: 'relative',
  };
  return (
    <div className='mb-2'>
      <div className='d-flex'>
        <div style={containerStyle}>
          {children}
        </div>
      </div>
    </div>
  );
};

const Item = ({ item, condensed }) => {
  const { tt, tx, htt, pc, ph, inl } = item;
  return (
    <Paper square elevation={inl ? 0 : 1} className='p-2 mt-3'>
      {!htt &&
        <Typography className='mb-2' variant='body2' style={{ lineHeight: '1.3', textAlign: 'center' }}>
          {tt}
        </Typography>
      }
      {pc &&
        <Picture
          url={pc}
          condensed={condensed}
          ph={ph}
        >
          <PictureDialog pictures={[pc]} />
        </Picture>
      }
      {tx &&
        <div style={{ color: '#555', textAlign: 'justify', fontSize: '14px' }}>
          <ReactMarkdown>
            {tx}
          </ReactMarkdown>
        </div>
      }
    </Paper>
  );
};

const contentRenderer = props => {
  
  const { data, condensed } = props;
  
  return (
    <Box my={3} margin='0 auto' width='100%' maxWidth='900px'>
      {
        data.map(item => (
          <Item key={item._id} item={item} condensed={condensed} />
          ))
        }
    </Box>
  );

};

export default contentRenderer;
