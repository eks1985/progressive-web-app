import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Accessories from 'components/accessories';
import CategoryHtmlContentRender from '../html-renderer';
import { translate } from 'lib/vacab'; 
import Content from './dialog';

const useStyles = makeStyles(theme => ({
  root: {
    background: theme.palette.primary.main,
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'center',
  },
  action: {
    color: '#fafafa',
    border: '1px solid #fafafa',
    margin: theme.spacing(1),
  },
}));

const categoryActions = props => {

  const [open, setOpen] = useState(false);
  const [contentType, setContentType] = useState(''); 

  const toggleOpen = () => {
    setOpen(!open);
  };

  const {
    cond,
    lang,
    country,
    content,
    accessories,
  } = props;

  const classes = useStyles();

  return (
    <Box className={classes.root}>
      {content &&
        <Button
          size='small'
          variant='outlined'
          className={classes.action}
          onClick={
            e => {
              e.preventDefault();
              e.stopPropagation();
              setContentType('description');
              toggleOpen();
            }
          }
          >
          {translate('Описание', lang)}
        </Button>
      }
      {accessories && accessories.items.length > 0 &&
        <Button
          size='small'
          variant='outlined'
          className={classes.action}
          onClick={
            e => {
              e.preventDefault();
              e.stopPropagation();
              setContentType('accessories');
              toggleOpen();
            }
          }
          >
          {translate('Аксессуары', lang)}
        </Button>
      }
      <Content open={open} toggleOpen={toggleOpen} cond={cond}>
        {contentType === 'description' &&
          <CategoryHtmlContentRender content={content} cond={cond} />
        }
        {contentType === 'accessories' &&
          <Accessories items={accessories.items} cond={cond} country={country} />
        }
      </Content>
    </Box>
  );

};

export default categoryActions;

