import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import Slide from '@material-ui/core/Slide';

const useStyles = makeStyles(theme => ({
  dialog: {
    maxWidth: '900px',
    margin: '0 auto',
  },
  appBar: {
    position: 'relative',
    display: 'flex',
    justifyContent: 'center',
  },
}));

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction='up' ref={ref} {...props} />;
});

const content = props => {

  const classes = useStyles();
  
  const { open, toggleOpen } = props;

  return (
    <Dialog
      className={classes.dialog}
      fullScreen
      open={open}
      onClose={toggleOpen}
      TransitionComponent={Transition}
    >
      <AppBar className={classes.appBar}>
        <Toolbar>
          <Button
            color='inherit'
            onClick={
              e => {
                e.stopPropagation();
                toggleOpen();
              }
            }
          >
            Закрыть
          </Button>
        </Toolbar>
      </AppBar>
      <Paper square>
        <Box mt={2} p={1} width='100%' margin='0 auto'>
          {props.children}
        </Box>
      </Paper>
    </Dialog>
  );

};

export default content;