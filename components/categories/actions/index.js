import React from 'react';
import { baseUrl, secret } from 'lib/base';
import { useStore } from 'lib/store';
import DataProviderBatch from 'lib/data-batch';
import Box from '@material-ui/core/Box';
import Content from './content';

const prepareQuery = (props, categoryId) => {

  const { url_accessories } = props;

  return {
    items: `${url_accessories}&categoryId=${categoryId}`,
  };

};

const actions = props => {

  const { categoryId, content } = props;
  const store = useStore();
  const { cond, lang, country } = store;

  const query = prepareQuery(props, categoryId);

  return (
    <Box>
      <DataProviderBatch skipLoading query={query} render={data => (
        <Content
          categoryId={categoryId}
          accessories={data}
          content={content}
          cond={cond}
          lang={lang}
          country={country}
        />
      )}/>
    </Box>
  );

};

actions.defaultProps = {
  url_accessories: `${baseUrl}accessories${secret}`,
};

export default actions;