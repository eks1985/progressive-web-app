import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Box from '@material-ui/core/Box';
import Paper from '@material-ui/core/Paper';
import Loading from 'components/loading/indicator';
import Picture from 'components/picture';
import Image from 'next/image';
import CategoryActions from './actions';
import Collapse from './collapse';
import Edit from './edit';

const getTitle = (lang, category) => {
  switch (lang) {
    case 'ru':
      return category.descr;
    case 'ua':
      return category['descr-ua'] || category.descr;
    case 'en':
      return category['descr-en'] || category.descr;
    case 'it':
      return category['descr-it'] || category.descr;
    default:
      return category.descr;
  }
};

const useStyles = makeStyles(theme => ({
  collapse: {
    padding: theme.spacing(2),
    cursor: 'pointer',
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
    fontSize: '16px',
    margin: theme.spacing(1),
    transition: 'all 300ms cubic-bezier(0.23, 1, 0.32, 1) 0ms',
    textAlign: 'center',
    color: props => props.ext ? '#fff' : 'rgba(0, 0, 0, 0.7)',
    background: props => props.ext ? theme.palette.primary.main : 'initial',
    width: props => props.cond ? '95%' : '600px',
    height: '80px',
  },
  expand: {
    position: 'relative',
    padding: '20px 20px',
    cursor: 'pointer',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    minHeight: '25vh',
    fontSize: '16px',
    width: '100%',
    transition: 'all 300ms cubic-bezier(0.23, 1, 0.32, 1) 0ms',
    textAlign: 'center',
    color: props => props.ext ? '#555' : 'white',
    background: props => props.ext ? 'white' : theme.palette.primary.main,
    padding: props => props.cond ? '20px' : '20px 20px',
  },
  title: {
    color: props => props.ext ? '#555' : 'white',
  },
  children: {
    marginTop: theme.spacing(3),
    maxWidth: '600px',
  },
}));

const categoryExpandable = props => {

  const {
    category,
    content,
    children,
    categoryOpen,
    categoryShowContent,
    id,
    ext,
    cond,
    picture,
    lang,
    setCategoryOpen,
    setCategoryClose,
    setCategoryShowContent,
    setCategoryHideContent,
  } = props

  const classes = useStyles({ cond, ext });

  const toggleOpen = () => {
    
    if (categoryOpen[id]) {
      setCategoryClose(id);
      setCategoryHideContent(id);
    } else {
      setCategoryOpen(id);
      setTimeout(() => {
        setCategoryShowContent(id);
      }, 500);
    }
  };

  const title       = getTitle(lang, category);
  const open        = categoryOpen[id];
  const showContent = categoryShowContent[id];
  const pictureUrl  = picture;

  return (
    <Paper
      className={clsx('categoryExpandable', open ? classes.expand : classes.collapse )}
      onClick={toggleOpen}
      square={open}
      elevation={open ? 0 : 2}
    >
      {open && <Collapse />}
      {!open &&
        <Box display='flex' alignItems='center' flex='1 1 auto'>
          {pictureUrl &&
            <Box mr={3} margin='0 auto' width={80} height={45} position='relative'>
              <Image className='img' src={pictureUrl} alt={`model-${title}`} layout='fill' objectFit='contain' objectPosition='center center' />
            </Box>
          }
          <Box display='flex' justifyContent='center' flex='1 1 auto'>
            {title}
          </Box>
        </Box>
      }
      {open &&
        <div className={classes.title}>
          {title}
        </div>
      }
      <div className={classes.children}>
        {open && showContent && children}
      </div>
      {open && !showContent &&
        <Loading big />
      }
      {open && !ext && showContent &&
        <div>
          <CategoryActions
            categoryId={id}
            content={content}
          />
        </div>
      }
    </Paper>
  );
};

categoryExpandable.defaultProps = {
  title: '',
};

export default categoryExpandable;


