import React, { useState} from 'react';
import { useStore } from 'lib/store';
import { filter } from 'lodash';
import Box from '@material-ui/core/Box';
import ModelsRow from 'components/models/models-row';
import ModelItem from 'components/models/model-item';
import EditCatalog from './edit-catalog';
import CategoryExpandable from './category-expandable';
import AddAnyMachineToCart from 'components/any-machine';

const containerStyle = {
  display: 'flex',
  flexDirection: 'column',
  flex: '1 0 100%',
  justifyContent: 'center',
  alignItems: 'center',
  marginTop: '6px',
  margin: '0 auto',
  maxWidth: '900px',
  marginBottom: '20px',
};

const subCategoryStyle = {
  fontSize: '11px',
  color: '#eee',
  width: '100%',
  fontWeight: 'bold',
  textTransform: 'uppercase',
};

const categoryExpandableContainer = props => {

  const store = useStore();

  const { data } = props;

  const { categories, models } = data;
  const { permiss, cond, lang } = store;
  const [open, setOpen] = useState({});
  const [showContent, setShowContent] = useState({});

  const setCategoryOpen = id => {
    const copy = { ...open };
    copy[id] = true;
    setOpen(copy);
  };

  const setCategoryClose = id => {
    const copy = { ...open };
    copy[id] = false;
    setOpen(copy);
  };

  const setCategoryShowContent = id => {
    const copy = { ...showContent };
    copy[id] = true;
    setShowContent(copy);
  };

  const setCategoryHideContent = id => {
    const copy = { ...showContent };
    open[id] = false;
    setShowContent(copy);
  };

  return (
    <Box mt={2} style={containerStyle}>
      {
        categories.map(category => {
          const { _id } = category;
          const categoryModels = filter(models, { categoryRef: _id });
          const modelsToRender = [];
          const subcategory = categoryModels.reduce((res, item) => {
            if (res.indexOf(item.subcategory) === -1) {
              return res.concat(item.subcategory);
            }
            return res;
          }, []);
          subcategory.forEach(item => {
            if (item) {
              modelsToRender.push(
                <Box my={2} style={subCategoryStyle} key={item}>{item}</Box>
              );
            }
            categoryModels.forEach(model => {
              if (model.subcategory === item) {
                modelsToRender.push(
                  <ModelItem key={model.id} {...model} pictureUrl={model.picture} />
                );
              }
            });
          });
          return (
            <CategoryExpandable
              lang={lang}
              permiss={permiss}
              category={category}
              title={category.descr}
              key={_id}
              id={_id}
              content={category.content}
              picture={category.picture}
              ext={category.ext}
              cond={cond}
              categoryOpen={open}
              categoryShowContent={showContent}
              setCategoryOpen={setCategoryOpen}
              setCategoryClose={setCategoryClose}
              setCategoryShowContent={setCategoryShowContent}
              setCategoryHideContent={setCategoryHideContent}
            >
              <ModelsRow>
                {subcategory.length > 0 &&
                  modelsToRender
                }
              </ModelsRow>
            </CategoryExpandable>
          );
        })
      }
      <AddAnyMachineToCart />
      {permiss && permiss.catalog.editCat &&
        <EditCatalog />
      }
    </Box>
  );
};

export default categoryExpandableContainer;


