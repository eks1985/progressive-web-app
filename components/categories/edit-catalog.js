import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import IconEdit from '@material-ui/icons/Edit';
import IconButton from '@material-ui/core/IconButton';
import { useRouter } from 'next/router';

const useStyles = makeStyles(theme => ({
  iconEdit: {
    color: '#555',
    height: '32px',
    width: '32px',
  },
}));

const editCatalog = () => {

  const router = useRouter();
  const classes = useStyles();

  return (
    <IconButton
      onClick={
        () => {
          router.push('/catalog');
        }
      }
    >
      <IconEdit className={classes.iconEdit} />
    </IconButton>
  );

};

export default editCatalog;

