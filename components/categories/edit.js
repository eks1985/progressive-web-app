import React from 'react';
import { useRouter } from 'next/router';
import IconEdit from '@material-ui/icons/Create';

const edit = props => {

  const { router } = useRouter();
  const { id } = props;

  return (
    <div
    style={{ position: 'absolute', left: '5px', top: '5px' }}
    onClick={
      e => {
        e.stopPropagation();
        router.push(`category-edit/${id}`);
      }
    }
  >
    <IconEdit style={{ color: 'white' }} />
  </div>
  );

};

export default edit;

