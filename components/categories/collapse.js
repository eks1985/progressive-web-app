import React from 'react';
import IconClear from '@material-ui/icons/Clear';

export default () => (
  <div style={{ position: 'absolute', right: '5px', top: '5px' }}>
    <IconClear style={{ color: 'white' }} />
  </div>
);
