import * as React from "react"

function SvgComponent(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={props.width}
      height={props.height}
      viewBox="0 0 512 512"
      {...props}
    >
      <path
        d="M256 320L128 192h64V0h128v192h64L256 320zm176-160h-35.781l80 160H384c0 70.688-57.312 128-128 128-70.687 0-128-57.312-128-128H35.781l80-160H80L0 320v192h512V320l-80-160z"
        fill="#FFF"
      />
    </svg>
  )
}

export default SvgComponent
