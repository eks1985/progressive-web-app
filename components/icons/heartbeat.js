import * as React from "react"

function SvgComponent(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 512 512"
      width={props.width}
      height={props.height}
      {...props}
    >
      <path
        fill="#FFF"
        d="M296.603 247.147l-18.068 47.584-64.848-171.76-55.43 124.176H0v33.391h179.919l31.188-69.867 67.339 178.358 41.197-108.491H512v-33.391z"
      />
    </svg>
  )
}

export default SvgComponent
