import React from 'react';

const style = {
  position: 'absolute',
  bottom: '2px',
  fontSize: '10px',
  fontWeight: 'bold',
  overflow: 'hidden',
  textOverflow: 'ellipsis',
  width: '80%',
}

const profileSubtitle = props => {
  const { title } = props;
  return (
    <div style={style}>
      {title}
    </div>
  );
};

export default profileSubtitle;
