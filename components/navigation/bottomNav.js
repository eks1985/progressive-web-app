import React from 'react';
import { useRouter } from 'next/router'
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import IconCompany from '@material-ui/icons/Layers';
import IconCatalog from '@material-ui/icons/ImportContacts';
import IconOrders from '@material-ui/icons/WorkOutline';
import IconAccount from '@material-ui/icons/AccountCircle';
import IconPrice from '@material-ui/icons/VerticalSplit';
import { translate } from 'lib/vacab';
import { useStore } from 'lib/store';

const labelStyle = { fontWeight: 'bold', padding: '0px', width: '100%', margin: '0 auto', fontSize: '10px', minWidth: '72px' };

const pageIndex = (page, showPriceList) => {
  if (showPriceList) {
    if (page === '/company') {
      return 1;
    }
    if (page === '/price-list') {
      return 2;
    }
    if (page === '/orders') {
      return 3;
    }
    if (page === '/profile') {
      return 4;
    }
  } else {
    if (page === '/company') {
      return 1;
    }
    if (page === '/orders') {
      return 3;
    }
    if (page === '/profile') {
      return 4;
    }
  }
  return 0;
};

const bottomNav = () => {

  const router = useRouter();
  const store = useStore();
  const { permiss } = store;
  const { pathname } = router;

  if (!permiss) {
    return null;
  }

  const { common: { showCompany } } = permiss;
  const showPriceList = permiss.common.showPriceList || permiss.atoms.priceClient || permiss.atoms.priceDealer;
  const showOrders = permiss.common.showOrders || permiss.atoms.ordersList;
  const { country, lang } = store;
  const pind = pageIndex(pathname, showPriceList);
  return (
    <BottomNavigation
      style={{ position: 'fixed', left: '0px', bottom: '0px', width: '100%', background: '#eee', color: '#333', fontSize: '10px', borderTop: '1px solid #ccc', zIndex: '9999' }}
      value={pind}
      showLabels
    >
      <BottomNavigationAction
        style={labelStyle}
        label={translate('Каталог', lang)}
        icon={<IconCatalog />}
        onClick={
          () => {
            router.push('/')
          }
        }
      />
      {showCompany &&
        <BottomNavigationAction
          style={labelStyle}
          label={translate('Компания', lang)}
          icon={<IconCompany />}
          onClick={
            () => {
              router.push('/company')
            }
          }
        />
      }
      {showPriceList &&
        <BottomNavigationAction
          style={labelStyle}
          label={translate('Прайс-лист', lang)}
          icon={<IconPrice />}
          onClick={
            () => {
              router.push('/price-list')
            }
          }
        />
      }
      {showOrders && country < 2 &&
        <BottomNavigationAction
          style={labelStyle}
          label={translate('Заказы', lang)}
          icon={<IconOrders />}
          onClick={
            () => {
              router.push('/orders')
            }
          }
        />
      }
      <BottomNavigationAction
        style={labelStyle}
        label={translate('Аккаунт', lang)}
        icon={<IconAccount />}
        onClick={
          () => {
            router.push('/profile')
          }
        }
      />
    </BottomNavigation>
  );
};

export default bottomNav;
