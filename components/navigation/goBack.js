import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useRouter } from 'next/router';
import IconGoBack from '@material-ui/icons/Reply';
import IconButton from '@material-ui/core/IconButton';

const useStyles = makeStyles(theme => ({
  icon: {
    color: theme.palette.primary.main,
  },
}));

import { useStore } from 'lib/store';

const goBack = () => {

  const classes = useStyles();
  const store = useStore();
  const router = useRouter();

  const { appUser } = store;

  const { pathname } = router;
  if (!appUser) {
    return null;
  }
  if (pathname === '/') {
    return null;
  }
  return (
    <IconButton
      onClick={
        () => {
          router.back();
        }
      }
    >
      <IconGoBack className={classes.icon} />
    </IconButton>
  );
};

export default goBack;

