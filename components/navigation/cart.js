import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useStore } from 'lib/store';
import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';

const useStyles = makeStyles(theme => ({
  badge: {
    top: 1,
    right: -10,
    border: `2px solid ${
      theme.palette.type === 'light' ? theme.palette.grey[200] : theme.palette.grey[900]
    }`,
    color: '#fff',
    background: theme.palette.primary.main,
  },
  icon: {
    color: theme.palette.primary.main,
  },
}));

const cartBadge = props => {

  const classes = useStyles();
  const store = useStore();

  if (!store || !store.cart) {
    return null;
  }

  const { setCartOpen, cart } = store;

  return (
    <IconButton
      aria-label='Cart'
      onClick={setCartOpen.bind(null, true)}
    >
      <Badge badgeContent={cart.data.items.length} color='primary' classes={{ badge: classes.badge }}>
        <ShoppingCartIcon className={classes.icon} />
      </Badge>
    </IconButton>
  );
};

export default cartBadge;