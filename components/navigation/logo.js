import React from 'react';
import Box from '@material-ui/core/Box';
import { useRouter } from 'next/router';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    textTransform: 'uppercase',
    cursor: 'pointer',
    color: theme.palette.primary.main,
    fontWeight: 'bold',
  },
  mg: {
    fontSize: '34px',
    lineHeight: '1em',  
  },
  dealer: {
    fontSize: '14px',
    lineHeight: '1em',
  },
}));

const logo = () => {

  const classes = useStyles();
  const router = useRouter();

  return (
    <Box
      mx={1}
      className={classes.container}
      onClick={
        () => {
          router.push('/')
        }
      }
    >
      <div className={classes.mg}>MG</div>
      <div className={classes.dealer}>Dealer</div>
    </Box>
  );
};

export default logo;
