import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useRouter } from 'next/router';
import { useAuth } from 'lib/auth';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import IconMenu from '@material-ui/icons/Menu';
import IconCatalog from '@material-ui/icons/ImportContacts';
import IconCompany from '@material-ui/icons/Layers';
import IconOrders from '@material-ui/icons/WorkOutline';
import IconAccount from '@material-ui/icons/AccountCircle';
import IconList from '@material-ui/icons/FormatListBulleted';
import IconMedia from '@material-ui/icons/Wallpaper';
import Select from 'react-select';
import { translate } from 'lib/vacab';
import { useStore } from 'lib/store';

const useStyles = makeStyles(theme => ({
  iconMenu: {
    color: theme.palette.primary.main,
  },
  drawer: {
    zIndex: 5000,
  },
  drawerContainer: {
    minWidth: '300px',
  },
  item: {
    color: props => props.active ? theme.palette.primary.main : '#777',
  },
  logout: {
    width: '100%',
  },
}));

const ListItemComponent = props => {
  
  const { path, push, lang, pagePath, pageTitle, secondary, close } = props;
  const classes = useStyles({ active: path === pagePath });

  return (
    <ListItem
      button
      onClick={
        () => {
          push(pagePath);
          close();
        }
      }
    >
      <ListItemIcon>
        {pagePath === '/' &&
          <IconCatalog className={classes.item} />
        }
        {pagePath === '/company' &&
          <IconCompany className={classes.item} />
        }
        {(pagePath === '/price-list' || pagePath === '/price-list-special') &&
          <IconList className={classes.item} />
        }
        {(pagePath === '/orders' || pagePath === '/product-reg' || pagePath === '/warranty' || pagePath === '/open-orders' || pagePath === '/open-items') &&
          <IconOrders className={classes.item} />
        }
        {pagePath === '/profile' &&
          <IconAccount className={classes.item} />
        }
        {pagePath === '/medialib' &&
          <IconMedia className={classes.item} />
        }
      </ListItemIcon>
      <ListItemText primary={translate(pageTitle, lang)} secondary={secondary} />
    </ListItem>
  );
};

const SideList = props => {

  const { customerCurOpt, customersData, setCustomerCurOpt, country, appUser, lang, permiss, signout, close } = props;
  const classes = useStyles();

  const custOptions = customersData.map(item => ({ value: item._id, label: item.dcr }));

  const showPriceList = permiss.catalog.showDealerPrice || permiss.catalog.showClientPrice || permiss.atoms.priceClient || permiss.atoms.priceDealer;
  const showPriceListSh = permiss.catalog.showDealerPrice || permiss.atoms.priceDealer;
  const showOrders = permiss.common.showOrders || permiss.atoms.ordersList;
  const showWc = permiss.common.showWc || permiss.atoms.warrantyClaims;
  const showOpenItems = permiss.common.showOpenItems || permiss.atoms.openItems;

  return (
    <div className={classes.list}>
      <List component='nav'>
        <ListItemComponent {...props} pagePath='/' pageTitle='Каталог' close={close} />
        {showPriceList &&
          <ListItemComponent {...props} pagePath='/price-list' pageTitle='Прайс-лист' close={close} />
        }
        {showPriceListSh &&
          <ListItemComponent {...props} pagePath='/price-list-special' pageTitle='Прайс-лист машин с наработкой' close={close} />
        }
        {showOrders && country < 2 &&
          <ListItemComponent {...props} pagePath='/orders' pageTitle='Заказы' close={close} />
        }
        {showWc && country < 2 &&
          <ListItemComponent {...props} pagePath='/product-reg' pageTitle='Регистрация машин' close={close} />
        }
        {showWc && country < 2 &&
          <ListItemComponent {...props} pagePath='/warranty' pageTitle='Гарантийные запросы' close={close} />
        }
        {showOpenItems && country < 2 &&
          <ListItemComponent {...props} pagePath='/open-items' pageTitle='Дебиторская задолженность' close={close} />
        }
        {/* {showProdPlan &&
          <ListItemComponent {...props} pagePath='/production-plan' pageTitle='План производства' />
        } */}
        {country > 1 &&
          <ListItemComponent {...props} pagePath='/machines-stock-cis' pageTitle='Склад дилера' close={close} />
        }
        <ListItemComponent {...props} pagePath='/medialib' pageTitle='Медиа библиотека' close={close} />
      </List>
      <Divider />
      {appUser &&
        <List component='nav'>
          <ListItemComponent {...props} pagePath='/profile' pageTitle='Аккаунт' secondary={appUser.email} close={close} />
          {customersData.length > 1 &&
            <ListItem>
              <div style={{ width: '100%' }}>
                <Select
                  options={custOptions}
                  value={customerCurOpt}
                  onSelectHandler={
                    selected => {
                      if (selected) {
                        setCustomerCurOpt(selected);
                      }
                    }
                  }
                />
              </div>
            </ListItem>
          }
          {customersData.length === 1 &&
            <ListItem>
              <ListItemText primary={customerCurOpt.label} />
            </ListItem>
          }
          <ListItem button>
            <Button
              variant='outlined'
              className={classes.logout}
              onClick={
                () => {
                  signout();
                  close();
                }
              }
            >
              {translate('Выйти', lang)}
            </Button>
          </ListItem>
        </List>
      }
    </div>
  )
};  

const menu = () => {

  const router = useRouter();
  const store = useStore();
  const auth = useAuth();

  if (!auth) {
    return null;
  }

  const { signout } = auth;
  const { country, permiss, appUser, lang, customersData, customerCurOpt, setCustomerCurOpt } = store;
  const classes = useStyles();

  const [open, setOpen] = useState(false);

  const toggle = () => {
    setOpen(!open);
  };

  const close = () => {
    setOpen(false);
  };

  if (!appUser) {
    return null;
  }

  return (
    <div>
      <IconButton onClick={toggle}>
        <IconMenu className={classes.iconMenu} />
      </IconButton>
      <SwipeableDrawer
        className={classes.drawer}
        open={open}
        onClose={close}
        onOpen={() => {}}
      >
        <div
          className={classes.drawerContainer}
          tabIndex={0}
          role='button'
        >
          <SideList
            path={router.pathname}
            push={router.push}
            appUser={appUser}
            permiss={permiss}
            close={close}
            customersData={customersData}
            customerCurOpt={customerCurOpt}
            setCustomerCurOpt={setCustomerCurOpt}
            country={country}
            lang={lang}
            signout={signout}
          />
        </div>
      </SwipeableDrawer>
    </div>
  );

};

export default menu;
