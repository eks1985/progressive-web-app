import React from 'react';
import Loading from 'components/loading';
import { getFlagUrl, getNextCountry } from 'lib/localization';
import { useStore } from 'lib/store';

const country = () => {

  const store = useStore();

  const { country, permiss, setCountry } = store;

  const photoStyle = {
    display: 'flex',
    justifyContent: 'center',
    backgroundImage: `url("${getFlagUrl(country)}")`,
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    height: '32px',
    width: '32px',
    cursor: 'pointer',
  };

  if (country < 0) {
    return <Loading />;
  }
  return (
    <div
      style={photoStyle}
      onClick={
        () => {
          if (permiss && permiss.common.switchCountry) {
            setCountry(getNextCountry(country));
          }
        }
      }
    />
  );
};

export default country;

