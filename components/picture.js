import React from 'react';

const Picture = props => {
  const { url, height, width, backgroundSize } = props;
  const photoStyle = {
    display: 'flex',
    justifyContent: 'center',
    backgroundImage: `url("${url}")`,
    backgroundRepeat: 'no-repeat',
    backgroundSize,
    backgroundPosition: 'center',
    height,
    width,
  };
  return (
    <div style={photoStyle} />
  );
};

Picture.defaultProps = {
  height: '50px',
  width: '50px',
  backgroundSize: 'cover',
};

export default Picture;
