import React from 'react';
import Button from '@material-ui/core/Button';
import IconSend from '@material-ui/icons/Send';
import Box from '@material-ui/core/Box';
import { useStore } from 'lib/store';
import Cart from 'lib/cart';

const addAnyMachine = () => {

  const { setCart, setCartOpen, appUser } = useStore();

  return (
    <Box mt={2}>
      <Button
        variant='contained'
        color='primary'
        size='small'
        onClick={
          () => {
            const cart = new Cart();
            if (appUser) {
              cart.update({ event: 'changeProp', prop: 'customer', value:  appUser.customerCurOpt });   
            }
            setCart(cart);
            setCartOpen(true);    
          }
        }
      >
        Запрос на произвольную машину
        <Box ml={1}>
          <IconSend />
        </Box>
      </Button>
    </Box>
  );

};

export default addAnyMachine;
