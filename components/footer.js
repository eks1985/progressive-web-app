import React, { useState } from 'react';
import { useStore } from 'lib/store';
import Paper from '@material-ui/core/Paper';
import Drawer from '@material-ui/core/Drawer';
import IconClear from '@material-ui/icons/Clear';
import IconButton from '@material-ui/core/IconButton';
import Box from '@material-ui/core/Box';

const style = {
  container: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    minHeight: '120px',
    background: 'rgb(41, 41, 41)',
    padding: '12px',
    color: '#ccc',
    fontSize: '10px',
  },
};

const Ru = () => {
  return (
    <Paper style={style.container} square elevation={12}>
      <div className='mb-1'>
        <strong>
          ООО &quot;МАСКИО-ГАСПАРДО РУССИЯ&quot;
        </strong>
      </div>
      <div>
        09:00 - 18:00 МСК (перерыв 13:00 - 14:00)
      </div>
      <div>
        404101, г. Волжский, ул. Пушкина 117 Б, офис 114
      </div>
      <div>
        +7 8443 203 100 - info@maschio.com - www.maschionet.com
      </div>
      <Box my={2} display='flex' justifyContent='center' width='100%'>
        <div style={{ borderBottom: '1px solid #555', width: '90%' }}></div>
      </Box>
      <div
        style={{ fontSize: '14px', textDecoration: 'underline', cursor: 'pointer' }}
        onClick={
          () => {
            // contactsOpenHandler();
          }
        }
      >
        Контактные лица
      </div>
    </Paper>
  );
};

const Ua = () => {
  return (
    <Paper style={style.container} square elevation={12}>
      <div className='mb-1'>
        <strong>
          MASCHIO-GASPARDO UKRAINE LLC
        </strong>
      </div>
      <div>
        Office: 08:00 - 17:00 (break 12:00 - 13:00)
      </div>
      <div>
        Warehouse: 08:00 - 17:00 (break 12:00 - 13:00)
      </div>
      <div style={{ textAlign: 'center' }}>
        Lejpcigskaya str., 13-A
      </div>
      <div style={{ textAlign: 'center' }}>
        03143  | Kiev | Ukraine
      </div>
      <div>
        +380 44 5381838 - info@maschio.com.ua - www.maschionet.com
      </div>
      <Box my={2} display='flex' justifyContent='center' width='100%'>
        <div style={{ borderBottom: '1px solid #555', width: '90%' }}></div>
      </Box>
      <div
        style={{ fontSize: '14px', textDecoration: 'underline', cursor: 'pointer' }}
        onClick={
          () => {
            // contactsOpenHandler();
          }
        }
      >
        Contact persons
      </div>
    </Paper>
  );
};

const RuContacts = () => {
  return (
    <div>
      <div className='my-2'>
        <strong>Соколенко Павел</strong> - генеральный директор
      </div>
      <div className='my-2'>
        <strong>Ефимов Кирилл</strong> - исполнительный директор
      </div>
      <div>
        kefimov@maschio.ru, +7 917 8305557
      </div>
      <div className='my-2'>
        <strong>Чевычелов Денис</strong> - бренд менеджер
      </div>
      <div>
        dchevychelov@maschio.ru, +7 917 3303311
      </div>
    </div>
  );
};

const UaContacts = () => {
  return (
    <div>
      <div className='my-2'>
        <strong>Богачева Елена</strong> - директор
      </div>
      <div className='my-2'></div>
      <div>
        <strong>Ворона В&quot;ячеслав Олександрович</strong> - коммерческий директор
      </div>
      <div>
        vvorona@maschio.com.ua
      </div>
      <div className='my-2'></div>
      <div>
        <strong>Ефимов Кирилл</strong> - разработчик
      </div>
      <div>
        kefimov@maschio.ru, +7 917 8305557
      </div>
    </div>
  );
};

const Contacts = props => {

  const { country, open, setOpen, cond } = props;

  return (
    <Drawer
      open={open}
      style={{ position: 'relative' }}
    >
      <Box className='p-3' style={{ minWidth: cond ? '375px' : '500px', fontSize: '12px' }}>
        <div style={{ position: 'absolute', right: '4px', top: '4px' }}>
          <IconButton
            onClick={
              () => {
                setOpen(false);
              }
            }
          >
            <IconClear />
          </IconButton>
        </div>
        {country === 0 ? <RuContacts /> : <UaContacts />}
      </Box>
    </Drawer>
  );

};

const footer = () => {

    const store  = useStore();
    const { country, cond } = store;
    const [contactsOpen, setContactsOpen] = useState(false);

    return (
      <Box width='100%'>
        {country === 0 && <Ru />}
        {country === 1 && <Ua />}
        <Contacts open={contactsOpen} setOpen={setContactsOpen} />
      </Box>
    )

};

export default footer;


