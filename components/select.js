import React from 'react';
import Select from 'react-select';
import Box from '@material-ui/core/Box';

const select = props => {
  const { option, options, onSelectHandler, placeholder, ...other } = props;
  return (
    <Box width='100%'>
      <Select
        {...other}
        style={{ width: '100%' }}
        options={options}
        width='100%'
        value={option || undefined}
        placeholder={placeholder}
        onChange={
          selected => {
            if (selected && selected.value) {
              onSelectHandler(selected);
            } else {
              onSelectHandler(false);
            }
          }
        }
      />
    </Box>
  );
};

select.defaultProps = {
  options: [],
};

export default select;
