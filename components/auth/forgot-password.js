import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
// import { useAuth } from 'lib/auth';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Typography from '@material-ui/core/Typography';
import red from '@material-ui/core/colors/red';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Loading from 'components/loading';
import { translate } from 'lib/vacab';

const useStyles = makeStyles(theme => ({
  margin: {
    marginTop: '12px',
    width: '100%',
  },
  cssLabel: {
    '&$cssFocused': {
      color: red[500],
    },
    fontSize: '20px',
  },
  cssFocused: {},
  cssUnderline: {
    '&:after': {
      borderBottomColor: red[500],
    },
  },
  msgBold: {
    fontWeight: 'bold',
  },
  resetError: {
    color: theme.palette.primary.main,
  },
}));

const DialogContentProgress = props => {

  const { loading, setResetError, resetError } = props;
  const classes = useStyles();

  return (
    <DialogContent>
      <Box>
        <Typography variant='caption' gutterBottom align='justify'>
          {/* Введите адрес вашей электронной почты */}
          Please enter your email
        </Typography>
      </Box>
      <Box>
        <Typography variant='caption' gutterBottom align='justify'>
          {/* Ссылка на восстановление пароля
          будут автоматически отправлена на указанный адрес */}
          Link for new password set up form
          will be send to this address
        </Typography>
      </Box>
      {!loading &&
        <FormControl className={classes.margin}>
          <InputLabel
            formlabelclasses={{
              root: classes.cssLabel,
              focused: classes.cssFocused,
            }}
            htmlFor='email'
          >
            Email
          </InputLabel>
          <Input
            fullWidth
            classes={{
              underline: classes.cssUnderline,
            }}
            id='email'
            onChange={
              () => {
                setResetError(false);
              }
            }
          />
        </FormControl>
      }
      {loading &&
        <Box mt={2} display='flex' justifyContent='center' flex='1 0 auto'>
          <Loading />
        </Box>
      }
      {resetError && !loading &&
        <Typography className={classes.resetError} variant='caption' gutterBottom align='justify'>
          {resetError}
        </Typography>
      }
    </DialogContent>
  );
}; 

const DialogContentSuccess = props => {
  
  const classes = useStyles();
  const { email } = props;

  return (
    <DialogContent>
      <Typography variant='caption' gutterBottom align='justify'>
        {/* Ссылка на восстановление пароля отправлена на электронный адрес */}
        Link for new password set up was sent to your email address
      </Typography>
      <Box>
        <Typography className={classes.msgBold} variant='caption' gutterBottom align='justify'>
          {email}
        </Typography>
      </Box>
    </DialogContent>
  );

}; 

const forgotPassword = props => {

  const [open, setOpen] = useState(false);
  const { auth } = props;
  const { resetLoading, resetSuccess, resetError, resetEmail, resetPassword, setResetError, setResetSuccess, setResetEmail } = auth;

  const handleClickOpen = () => {
    setOpen(true);
    setResetError('');
    setResetSuccess(false);
    setResetEmail('');
  };

  const handleClose = () => {
    setOpen(false);
    setResetError('');
    setResetSuccess(false);
    setResetEmail('');
  };

    return (
      <Box mt={5}>
        <Button
          onClick={handleClickOpen}
        >
          {translate('Сменить пароль', 'en')}
        </Button>
        <Dialog
          open={open}
          onClose={handleClose}
          aria-labelledby='forgot-password-title'
          aria-describedby='forgot-password-descr'
        >
          <DialogTitle id='forgot-password-title'>
            {/* Сброс пароля */}
            Change password
          </DialogTitle>
          {!resetSuccess &&
            <DialogContentProgress setResetError={setResetError} loading={resetLoading} resetError={resetError} />
          }
          {resetSuccess &&
            <DialogContentSuccess email={resetEmail} />
          }
          {!resetLoading && !resetSuccess &&
            <DialogActions>
              <Button onClick={handleClose} color='primary'>
                {/* Отмена */}
                Cancel
              </Button>
              <Button
                onClick={
                  () => {
                    const usrEmail = document.getElementById('email').value.trim();
                    resetPassword(usrEmail);
                  }
                }
              >
                {/* Отправить */}
                Send
              </Button>
            </DialogActions>
          }
          {resetSuccess &&
            <DialogActions>
              <Button onClick={handleClose} color='primary'>
                {/* Закрыть */}
                Close
              </Button>
            </DialogActions>
          }
        </Dialog>
      </Box>
    );
};

export default forgotPassword;

