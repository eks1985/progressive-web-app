import React from 'react';
import { useStore } from 'lib/store';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Layout from 'components/layout';
import Content from './content';

const login = props => {
  const store = useStore();
  const { cond } = store;
  return (
    <Layout>
      <Box display='flex' justifyContent='center'>
        {cond
          ?
            <Content {...props} />
          :
            <Paper elevation={4}>
              <Box p={3}>
                <Content {...props} />
              </Box>
            </Paper>
        }
      </Box>
    </Layout>
  );
};

export default login;
