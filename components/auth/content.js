import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Loading from 'components/loading';
import EmailButton from './email-button';
import TextFields from './text-fields';
import ForgotPassword from './forgot-password';
import OfflineButton from './offline-button';

const useStyles = makeStyles(theme => ({
  msg: {
    color: theme.palette.primary.main,
  },
}));

const contentCond = props => {

  const { auth } = props;
  const classes = useStyles();

  const { signinWithEmail, loading, loginError } = auth;

  useEffect(() => {
    document.addEventListener('keyup', handlePressEnter, false);
    return () => {
      document.removeEventListener('keyup', handlePressEnter, false);
    };
  }, []);

  const handlePressEnter = e => {
    if (e.which === 13) {
      try {
        signinWithEmail();
      } catch (e) {
        console.log(e);
      }
    }
  };

  const online = navigator.onLine;
  return (
    <Box display='flex' alignItems='center' flexDirection='column'>
      <TextFields />
      <Box mt={5}>
        <EmailButton loginHandler={signinWithEmail} login={loading} />
      </Box>
      <div>
        <ForgotPassword {...props} />
      </div>
      <Box display='flex' flexDirection='column' alignItems='center' justifyContent='center' p={5} mt={1} className={classes.msg}>
        {loading &&
          <Loading />
        }
        {loginError &&
          <div>
            {loginError}
          </div>
        }
        {!online &&
          <OfflineButton />
        }
      </Box>
    </Box>
  );

};

export default contentCond;

