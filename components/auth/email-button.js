import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import { translate } from 'lib/vacab';

const useStyles = makeStyles(theme => ({
  btn: {
    minWidth: '200px',
  },
}));

const emailButton = props => {

  const classes = useStyles();

  const { login, loginHandler } = props;

  return (
    <Button
      className={classes.btn}
      variant='contained'
      color='primary'
      disabled={login}
      onClick={loginHandler}
    >
      {translate('Войти', 'en')}
    </Button>
  );

};

export default emailButton;

