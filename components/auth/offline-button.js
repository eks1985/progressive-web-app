import React from 'react';
import { useStore } from 'lib/store';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';

const offlineButton = () => {
  const store = useStore();
  const { appUser, setOffline } = store; 
  if (appUser) {
    return null;
  }
  return (
    <Box my={3} display='flex' flex='1 0 auto' justifyContent='center' alignItems='center' flexDirection='column'>
      <Button
        variant='contained'
        onClick={setOffline}
      >
        Работать оффлайн
      </Button>
    </Box>
  );
};

export default offlineButton;


