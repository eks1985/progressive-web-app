import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import IconAccount from '@material-ui/icons/AccountCircle';
import IconPassword from '@material-ui/icons/Visibility';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import red from '@material-ui/core/colors/red';

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  margin: {
    margin: theme.spacing(1),
    minWidth: '260px',
  },
  cssLabel: {
    '&$cssFocused': {
      color: red[500],
    },
    fontSize: '20px',
  },
  cssFocused: {},
  cssUnderline: {
    '&:after': {
      borderBottomColor: red[500],
    },
  },
}));

const textFields = props => {

  const classes = useStyles();
  const [showPassword, toogleShowPassword] = useState(false);
  const handleToggleShowPassword = () => {
    toogleShowPassword(!showPassword);
  };
  return (
    <Box>
      <Box mt={2}>
        <FormControl className={classes.margin}>
          <InputLabel
            formlabelclasses={{
              root: classes.cssLabel,
              focused: classes.cssFocused,
            }}
            htmlFor='loginLogin'
          >
            Email
          </InputLabel>
          <Input
            autoCapitalize='off'
            fullWidth
            classes={{
              underline: classes.cssUnderline,
            }}
            id='loginLogin'
            startAdornment={
              <InputAdornment position='start'>
                <IconAccount />
              </InputAdornment>
            }
          />
        </FormControl>
      </Box>
      <Box mt={1}>
        <FormControl className={classes.margin}>
          <InputLabel
            formlabelclasses={{
              root: classes.cssLabel,
              focused: classes.cssFocused,
            }}
            htmlFor='loginPassword'
          >
            Password
          </InputLabel>
          <Input
            autoCapitalize='off'
            type={showPassword ? 'text' : 'password'}
            fullWidth
            classes={{
              underline: classes.cssUnderline,
            }}
            id='loginPassword'
            startAdornment={
              <InputAdornment position='start'>
                <IconButton
                  style={{ color: 'black' }}
                  aria-label='show'
                  size='small'
                  onClick={handleToggleShowPassword}
                >
                  <IconPassword />
                </IconButton>
              </InputAdornment>
            }
          />
        </FormControl>
      </Box>
    </Box>
  );
};

export default textFields;

