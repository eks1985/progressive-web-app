import React from 'react';

const ModelsRow = props => {
  
  const { showBorder, children } = props;

  const containerStyle = {
    display: 'flex',
    flex: '0 1 50%',
    justifyContent: 'center',
    flexWrap: 'wrap',
    border: showBorder ? '1px solid #ddd' : 'none',
    minHeight: '15vh',
  };

  return (
    <div className='ModelsRow' style={containerStyle}>
      {children}
    </div>
  );
};

ModelsRow.defaultProps = {
  showBorder: false,
};

export default ModelsRow;
