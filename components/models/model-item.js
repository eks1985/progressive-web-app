import React from 'react';
import Box from '@material-ui/core/Box';
import Image from 'next/image';
import { useRouter } from 'next/router';

const modelItem = props => {

  const router = useRouter();
  const { descr, pictureUrl, id } = props;

  const containerStyle = {
    marginRight: '6px',
    marginBottom: '6px',
    borderRadius: '4px',
    background: 'white',
    border: '1px solid #ddd',
    width: '120px',
    height: '120px',
  };
  const descrContainerStyle = {
    height: '40px',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: '12px',
    color: 'rgba(0, 0, 0, 0.6)',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  };
  return (
    <div
      style={containerStyle}
      onClick={
        e => {
          e.stopPropagation();
          router.push(`/products/${id}`);
        }
      }
    >
      <Box width='100%' height={80} position='relative'>
        {pictureUrl &&
          <Image className='img' src={pictureUrl} alt='category-picture' layout='fill' objectFit='contain' objectPosition='center center' />
        }
      </Box>
      <div style={descrContainerStyle}>
        {descr}
      </div>
    </div>
  );
};

export default modelItem;
