import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import moment from 'moment';
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Summary from './summary';
import Order from './order';
import { formatNumb } from 'lib/format';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    fontSize: theme.typography.pxToRem(13),
    padding: '0px 6px',
    borderLeft: props => props.border,
  },
  head: {
    fontSize: theme.typography.pxToRem(14),
    margin: 0,
    height: '56px',
  },
  details: {
    display: 'flex',
    flexDirection: 'column',
    padding: theme.spacing(1),
  },
}));

const Item = props => {

  const [expanded, setExpanded] = useState(false);
  
  const handleChange = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };
  
  const { item, ind } = props;
  const { week, value, weekData } = item;
  // const weekData = getWeekData(week);

  let border = '3px solid #8bc34a';
  if (weekData.notConfirmed) {
    border = '3px solid #9e9e9e';
  } else {
    if (parseInt(weekData.weekNr) < moment().week() && parseInt(weekData.year) <= moment().year()) {
      border = '3px solid #E1001A';
    }
  }

  const classes = useStyles({ border });

  return (
    <Accordion square className={classes.root} expanded={expanded === ind} onChange={handleChange(ind)}>
      <Summary item={item} weekData={weekData} value={value} />
      {expanded === ind &&
        <AccordionDetails className={classes.details}>
          {item.items.map(item => {
            return <Order key={item.order} order={item} />
          })}
        </AccordionDetails>
      }
    </Accordion>
  );

};

const content = props => {
  const { items, value, expired } = props;

  return (
    <div>
      <Box ml={1} mt={1} mb={1}>
        <Typography variant='body2'>
          {`Orders total value: € ${formatNumb(value)}`}
        </Typography>
      </Box>
      <Box ml={1} mb={1}>
        <Typography variant='body2'>
          {`Orders expired value: € ${formatNumb(expired)}`}
        </Typography>
      </Box>
      {
        items.map((item, ind) => {
          return <Item key={ind} ind={ind} item={item} />
        })
      }
    </div>
  );
};

export default content;
