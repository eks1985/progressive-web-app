import moment from 'moment';

export const getWeekData = week => {
  const weekNr = week.split('-')[1];
  if (weekNr === '00') {
    return {
      notConfirmed: true,
    };
  }
  const year = week.split('-')[0];
  const startWeek = moment().day('Monday').week(parseInt(weekNr, 10)).format('DD-MM-YY');
  return {
    year,
    weekNr,
    startWeek,
  }
};
