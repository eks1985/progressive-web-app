import React, { useEffect, useState } from 'react';
import Box from '@material-ui/core/Box';
import Filters from 'components/filters';
import Content from './content';

const testWeek = (weekItem, filters) => {
  return true;
};

const testOrder = (orderItem, filters) => {
  return true;
};

const prepareOrders = (items, filters) => {
  return items;
};

const prepareRows = (items, filters) => {
  return items;
};

const filter = (items, filters) => {
  const res = [];
  items.forEach(weekItem => {
    if (testWeek(weekItem, filters)) {
      res.push({ week: weekItem.week, value: weekItem.value, items: prepareOrders(weekItem.items, filters) });
    }
  });
  // return res;
  return items;
  // const callback = item => {
  //   for (const filter of filters) {
  //     if (filter.value.length > 0) {
  //       const arr = filter.value.map(fvalue => fvalue.value);
  //       switch (filter.name) {
  //         case 'code':
  //           if (!arr.includes(item.code)) {
  //             return false;
  //           } 
  //           break;
  //         case 'description':
  //           if (!arr.includes(item.descr)) {
  //             return false;
  //           } 
  //           break;
  //         case 'model':
  //           if (!arr.includes(item.model)) {
  //             return false;
  //           } 
  //           break;
  //         default:
  //           return true;
  //       }
  //     }
  //   }
  //   return true;
  // };
  // return items.filter(callback);
};

const prepareFilters = items => {

  const items5 = items//.slice(0, 5);

  const filters = [];
  const descr = [];
  const codes = [];
  const models = [];
  items5.forEach(item1 => {
    item1.items.forEach(item2 => {
      item2.data.data.forEach(item3 => {
        if (!descr.includes(item3.descr)) {
          descr.push(item3.descr);
        }
        if (!codes.includes(item3.code)) {
          codes.push(item3.code);
        }
        if (!models.includes(item3.model)) {
          models.push(item3.model);
        }
      });
    });
  });
  descr.sort();
  codes.sort();
  models.sort();
  const filterCode = {
    name: 'code',
    label: 'Код',
    items: codes.map(item => ({ value: item, label: item })),
    value: [],
    mult: true,
  };
  const filterDescr = {
    name: 'description',
    label: 'Наименование',
    items: descr.map(item => ({ value: item, label: item })),
    value: [],
    mult: true,
  };
  const filterModel = {
    name: 'model',
    label: 'Модель',
    items: models.map(item => ({ value: item, label: item })),
    value: [],
    mult: true,
  };

  filters.push(filterModel);
  filters.push(filterDescr);
  filters.push(filterCode);
  return filters;

};

const list = props => {

  const [filters, setFilters] = useState(prepareFilters(props.orders));
  const [itemsFiltered, setItemsFiltered] = useState(props.orders);

  const { value, expired } = props;

  useEffect(() => {
    setItemsFiltered(filter(props.orders, filters));
  }, [props.items]);

  const filtersEdit = action => {
    const {  index, value } = action;
    const copy = [...filters];
    copy[index].value = value;
    setFilters(copy);
  };

  const filtersApply = () => {
    setItemsFiltered(filter(props.orders, filters));
  };

  const filtersReset = () => {
    const copy = [...filters];
    for (const filter of copy) {
      filter.value = [];
    }
    setFilters(copy);
  };

  const actions={
    filtersEdit,
    filtersApply,
    filtersReset, 
  }

  console.log('itemsFiltered', itemsFiltered);

  return (
    <Box margin='0 auto' width='100%' maxWidth={600} mb={2}>
      {/* <Box>
        <Filters filters={filters} {...actions} />
      </Box> */}
      <Content items={itemsFiltered} value={value} expired={expired} />
    </Box>
  );

};

export default list;
