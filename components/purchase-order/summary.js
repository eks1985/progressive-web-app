import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import { formatNumb } from 'lib/format';

const useStyles = makeStyles(theme => ({
  head: {
    fontSize: theme.typography.pxToRem(14),
    margin: 0,
    height: '56px',
  },
}));

const summary = props => {

  const classes = useStyles();
  const { item, weekData, value } = props;
  const { items } = item;

  return (
    <AccordionSummary expandIcon={<ExpandMoreIcon />} className={classes.head}>
      {weekData.notConfirmed
        ?
          <Box width='100%' display='flex' justifyContent='space-between' alignItems='center'>
            <Box>
              <Box>
                <Typography variant='body2'>
                  Not confirmed
                </Typography>
              </Box>
              <Box>
                <Typography variant='body2'>
                  orders
                </Typography>
              </Box>
            </Box>
            <Typography variant='body2'>
              {`${items.length} orders`} 
            </Typography>
          </Box>
        :
        <Box width='100%' display='flex' justifyContent='space-between' alignItems='center'>
          <Box>
            <Box>
              <Typography variant='body2'>
                {weekData.year}
              </Typography>
            </Box>
            {weekData.year !== 'way' &&
              <Box>
                <Typography variant='body2'>
                  {`week ${weekData.weekNr} (from ${weekData.startWeek})`}
                </Typography>
              </Box>
            }
          </Box>
          <Box textAlign='right'>
            <Box textAlign='right'>
              <Typography variant='body2'>
                {`${items.length} orders`} 
              </Typography>
            </Box>
            <Box textAlign='right'>
              <Typography variant='body2'>
                {`€ ${formatNumb(Math.ceil(value))}`}
              </Typography>
            </Box>
          </Box>
        </Box>
      }
    </AccordionSummary>
  );

};

export default summary;


