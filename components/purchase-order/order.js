import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useStore } from 'lib/store';
import { useRouter } from 'next/router';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { formatNumb } from 'lib/format';

const useStyles = makeStyles(theme => ({
  container: {
    fontSize: theme.typography.pxToRem(14),
  },
  orderNumber: {
    fontWeight: 'bold',
  },
  mac: {
    color: '#1976d2',
  },
}));

const Title = props => {

  const classes = useStyles();
  const { order } = props;
  const { data: { total } } = order;
  const data = order.data.data;
  const row0 = data[0];

  return (
    <Box display='flex' justifyContent='space-between' width='100%' bgcolor='#eee'>
      <Box display='flex'>
        <Box>
          <Typography variant='caption'>
            {row0.werks}
          </Typography>
        </Box>
        <Box ml={1}>
          <Typography variant='caption' className={classes.orderNumber}>
            {order.order}
          </Typography>
        </Box>
        <Box ml={1}>
          <Typography variant='caption'>
            {row0.crDate}
          </Typography>
        </Box>
      </Box>
      <Box>
        <Typography variant='caption'>
          {formatNumb(Math.ceil(total))}
        </Typography>  
      </Box>
    </Box>
  );

};

const Row = props => {
  const classes = useStyles();

  const { row } = props;
  return (
    <Box width='100%' display='flex'>
      <Box flex='0 0 70px'>
        <Typography variant='caption'>
          {row.code}
        </Typography>
      </Box>
      <Box flex='1' ml={1}>
        <Typography variant='caption' className={classes.mac}>
          {row.descr.slice(0, 20)}
        </Typography>
      </Box>
      <Box flex='0 0 20px' ml={1} textAlign='center'>
        <Typography variant='caption'>
          {row.qtyRemain}
        </Typography>
      </Box>
      <Box flex='0 0 50px' ml={1} textAlign='right'>
        <Typography variant='caption'>
          {formatNumb(Math.ceil(row.amount))}
        </Typography>
      </Box>
    </Box>
  );

};

const order = props => {

  const classes = useStyles();
  const { order } = props;

  return (
    <Box mt={1} margin='0 auto' width='100%' className={classes.container}>
      <Title order={order} />
      <Box width='100%'>
        {order.data.data.map((row, ind) => {
          return <Row key={ind} row={row} />
        })}
      </Box>
    </Box>
  );

};

export default order;
