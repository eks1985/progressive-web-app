import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import Visibility from './visibility';
import Shift from './shift';

class LongMenu extends React.Component {
  state = {
    anchorEl: null,
  };

  handleClick = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  render() {
    const { anchorEl } = this.state;
    const open = Boolean(anchorEl);
    return (
      <div>
        <IconButton
          aria-label='More'
          aria-owns={open ? 'long-menu' : null}
          aria-haspopup='true'
          onClick={this.handleClick}
        >
          <MoreVertIcon />
        </IconButton>
        <Menu
          style={{ width: '280px' }}
          id='long-menu'
          anchorEl={anchorEl}
          open={open}
          onClose={this.handleClose}
        >
          <Visibility {...this.props} closeMenu={this.handleClose} />
          <Shift {...this.props} closeMenu={this.handleClose} />
        </Menu>
      </div>
    );
  }
}

export default LongMenu;
