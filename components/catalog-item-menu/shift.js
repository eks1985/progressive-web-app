import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';

const styles = theme => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
});

class SimpleListMenu extends React.Component {
  button = null;

  state = {
    anchorEl: null,
  };

  prepareOptions = () => {
    const { index, qty } = this.props;
    if (index === qty - 1) {
      return ['Вверх'];
    }
    if (index === 0) {
      return ['Вниз'];
    }
    return ['Вверх', 'Вниз'];
  };

  handleClickListItem = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleMenuItemClick = (event, ind, option) => {
    const { shiftData, index, closeMenu } = this.props;
    this.setState({ anchorEl: null },
      () => {
        const shift = option === 'Вверх' ? -1 : 1;
        shiftData(shift, index);
        closeMenu();
      }
    );
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  render() {
    const { classes } = this.props;
    const { anchorEl } = this.state;
    const options = this.prepareOptions();

    return (
      <div className={classes.root}>
        <List component='nav'>
          <ListItem
            button
            aria-haspopup='true'
            aria-controls='lock-menu'
            aria-label='Изменить порядок'
            onClick={this.handleClickListItem}
          >
            <ListItemText
              primary='Изменить порядок'
            />
          </ListItem>
        </List>
        <Menu
          id='lock-menu'
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={this.handleClose}
        >
          {options.map((option, index) => (
            <MenuItem
              key={option}
              onClick={event => this.handleMenuItemClick(event, index, option)}
            >
              {option}
            </MenuItem>
          ))}
        </Menu>
      </div>
    );
  }
}

SimpleListMenu.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SimpleListMenu);
