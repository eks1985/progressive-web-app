import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';

const styles = theme => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
});

const options = [
  'Не отображать',
  'Для менеджера',
  'Для менеджера и клиента',
];

class SimpleListMenu extends React.Component {
  button = null;

  state = {
    anchorEl: null,
  };

  handleClickListItem = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleMenuItemClick = (event, ind) => {
    this.setState({ anchorEl: null }, () => {
      const { closeMenu, data, index, updateData } = this.props;
      const current = data[index];
      const vis = ind;
      const updated = { ...current, vis, mod: true };
      updateData(index, updated);
      if (closeMenu) {
        closeMenu();
      } else {
        this.handleClose();
      }
    });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  render() {
    const { classes, data, index } = this.props;
    const { anchorEl } = this.state;
    const item = data[index];
    const selected = item.vis || 0;
    return (
      <div className={classes.root}>
        <List component='nav'>
          <ListItem
            style={{ width: '280px' }}
            button
            aria-haspopup='true'
            aria-controls='lock-menu'
            aria-label='Видимость'
            onClick={this.handleClickListItem}
          >
            <ListItemText
              primary='Видимость'
              secondary={options[selected]}
            />
          </ListItem>
        </List>
        <Menu
          id='lock-menu'
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={this.handleClose}
        >
          {options.map((option, index) => (
            <MenuItem
              key={option}
              onClick={event => this.handleMenuItemClick(event, index)}
            >
              {option}
            </MenuItem>
          ))}
        </Menu>
      </div>
    );
  }
}

SimpleListMenu.propTypes = {
  classes: PropTypes.object.isRequired,
  updateData: PropTypes.func,
  closeMenu: PropTypes.any,
};

SimpleListMenu.defaultProps = {
  updateData: () => {},
  closeMenu: undefined,
};

export default withStyles(styles)(SimpleListMenu);
