import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useStore } from 'lib/store';
import { useRouter } from 'next/router';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles(theme => ({
  container: {
    
  },
}));

const Item = props => {

  const { item } = props;
  const { code, descr, plant, prod, stock, total } = item;

  return (
    <Box borderTop='1px dashed #ddd' mt={1}>
      <Box>
        <Typography variant='caption'>
          {`[${plant}] `}
        </Typography>
        <Typography variant='caption' color='primary'>
          {`${code} `}
        </Typography>
        <Typography variant='caption'>
          {`${descr.slice(0, 30)}`}
        </Typography>
      </Box>
      <Box display='flex'>
        <Box flex='0 0 50%'>
          <Typography variant='caption'>
            {`Prod: ${prod}`}
          </Typography>
        </Box>
        <Box flex='0 0 50%'>
          <Typography variant='caption'>
            {`Stock: ${stock}`}
          </Typography>
        </Box>
      </Box>
    </Box>
  );

};

const productionPlan = props => {

  const { items } = props;
  console.log('items', items);

  return (
    <Box margin='0 auto' width='100%' p={1}>
      {items.map(item => {
        return <Item key={item._id} item={item} />;
      })}
    </Box>
  );

};

export default productionPlan;
