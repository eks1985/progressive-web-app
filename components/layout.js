import React from 'react';
import { useStore } from 'lib/store';
import { makeStyles } from '@material-ui/core/styles';
import Header from 'components/header';
import Footer from 'components/footer';
import BottomNav from 'components/navigation/bottomNav';
import Loading from 'components/loading';
import Checkout from 'components/checkout';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    minHeight: '100vh',
    background: props => props.gray ? '#eee' : 'initial',
    paddingTop: theme.spacing(8),
  },
}));

const layout = props => {

  const store = useStore();
  const { loading, country } = store;

  const { children } = props;

  const classes = useStyles(props);

  return (
    <div id='layout' className={classes.root}>
      <Header />
      <main>
        <Checkout />
        {country > -1 && loading && <Loading />}
        {country > -1 && children}
      </main>
      <BottomNav />
      <Footer />
    </div>
  );
};

export default layout
