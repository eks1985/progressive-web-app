import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useStore } from 'lib/store';
import { useRouter } from 'next/router';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { formatNumb } from 'lib/format';

const useStyles = makeStyles(theme => ({
  container: {
    
  },
}));

const head = props => {

  const store = useStore();
  const router = useRouter();
  const classes = useStyles();

  const { data } = props;

  const turnoverKeys = Object.keys(data.turnover);

  return (
    <Box m={1} p={1} border='1px solid #ddd'>
      <Box textAlign='center'>
        <Typography variant='body2' color='primary'>
          Turnover   
        </Typography>   
      </Box>
      <Box display='flex'>
        <Box>
          {turnoverKeys.map(year => {
            return (
              <Box key={year}>
                <Typography variant='body2'>
                  {year}   
                </Typography>  
              </Box>
            );
          })}
          <Box>
            <Typography variant='body2'>
              Orders   
            </Typography>  
          </Box>
        </Box>
        <Box flex='1 0 auto' textAlign='right'>
          {turnoverKeys.map(year => {
            return (
              <Box key={year}>
                <Typography variant='body2'>
                  {`${formatNumb(data.turnover[year])} €`}   
                </Typography>  
              </Box>
            );
          })}
          <Box>
            <Typography variant='body2'>
              {`${formatNumb(data.orders)} €`}   
            </Typography>  
          </Box>
        </Box>
      </Box>
      <Box display='flex' mt={2}>
        <Box>
          <Box>
            <Typography variant='body2'>
              Blocked  
            </Typography>  
          </Box>
          <Box>
            <Typography variant='body2'>
              Notice  
            </Typography>  
          </Box>
        </Box>
        <Box flex='1 0 auto' textAlign='right'>
          <Box>
            <Typography variant='body2'>
              {data.notice}   
            </Typography>  
          </Box>
        </Box>
      </Box>
    </Box>
  );

};

export default head;
