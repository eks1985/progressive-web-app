import React from 'react';
import Summary from './summary';
import Details from './details';
import Head from './head';

const content = props => {
  const { item } = props;
  const summary = item.summary || item.header;
  return <>
    {item.props && <Head data={item.props} />}
    <Summary summary={summary} />
    {item.items && <Details items={item.items} />}
  </>;

};

export default content;