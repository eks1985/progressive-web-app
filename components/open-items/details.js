import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useStore } from 'lib/store';
import { useRouter } from 'next/router';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { formatNumb } from 'lib/format';

const useStyles = makeStyles(theme => ({
  accordion: {
    margin: '0px',
  },
  header: {
    paddingLeft: '8px',
    paddingRight: '8px',
    margin: '0px',
    position: 'relative',
  },
  details: {
    display: 'flex',
    flexDirection: 'column',
    padding: theme.spacing(1),
    fontSize: theme.typography.pxToRem(14),
  },
}));

const Totals = props => {

  const classes = useStyles();
  const { order } = props;
  const overdue = `${formatNumb(order.orderSummary.overdue)} ${order.orderSummary.currency}`;

  return (
    <Box width='100%' display='flex'>
      <Box flex='0 0 50%'>
        <Box>
          <Typography variant='caption'>
            {order.order}
          </Typography>
        </Box>
        <Box>
          <Typography variant='caption' color='primary'>
            {order.descr}
          </Typography>
        </Box>
      </Box>
      <Box flex='0 0 50%' textAlign='right'>
        <Box>
          <Typography variant='caption'>
            {`${formatNumb(order.orderSummary.debt)} ${order.orderSummary.currency}`}
          </Typography>
        </Box>
        <Box>
          <Typography variant='caption' color='primary'>
            {order.orderSummary.overdue ? formatNumb(overdue) : ''}
          </Typography>
        </Box>
      </Box>
    </Box>
  );

};

const OrderRow = props => {

  const classes = useStyles();
  const { row } = props;
  const overdue = `${formatNumb(row.overdue)} ${row.currency}`;

  return (
    <Box width='100%' display='flex' mt={1} borderTop='1px dashed #ddd'>
      <Box flex='0 0 20%'>
        <Box>
          <Typography variant='caption'>
            Doc
          </Typography>
        </Box>
        <Box>
          <Typography variant='caption'>
            Doc d
          </Typography>
        </Box>
        <Box>
          <Typography variant='caption'>
            Paym d
          </Typography>
        </Box>
      </Box>
      <Box flex='0 0 30%'>
        <Box>
          <Typography variant='caption'>
            {row.docNr}
          </Typography>
        </Box>
        <Box>
          <Typography variant='caption'>
            {row.docDate}
          </Typography>
        </Box>
        <Box>
          <Typography variant='caption'>
            {row.paymentDate}
          </Typography>
        </Box>
      </Box>
      <Box flex='0 0 50%' textAlign='right'>
        <Box>
          <Typography variant='caption'>
            {`${formatNumb(row.debt)} ${row.currency}`}
          </Typography>
        </Box>
        <Box>
          <Typography variant='caption' color='primary'>
            {row.overdue ? formatNumb(overdue) : ''}
          </Typography>
        </Box>
        <Box>
          <Typography variant='caption' color='primary'>
            {row.overdue ? row.daysOverdue : ''}
          </Typography>
        </Box>
      </Box>
    </Box>
  );

};

const Order = props => {

  const [expanded, setExpanded] = React.useState(false);

  const handleChange = panel => (_, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  const classes = useStyles();

  const { order } = props;

  return (
    <Accordion square className={classes.accordion} expanded={expanded === 'panel1'} onChange={handleChange('panel1')}>
      <AccordionSummary
        expandIcon={<ExpandMoreIcon />}
        aria-label='Expand'
        aria-controls='additional-actions1-content'
        id='additional-actions1-header'
        className={classes.header}
      >
        <Totals order={order} />
      </AccordionSummary>
      {expanded &&
        <AccordionDetails className={classes.details}>
          {order.rows.map((row, i) => {
            return <OrderRow key={`${order.order}-${i}`} row={row} />;   
          })}
        </AccordionDetails>
      }
    </Accordion>
  ); 

};

const details = props => {

  const store = useStore();
  const router = useRouter();
  const classes = useStyles();

  const { items } = props;

  return (
    <Box margin='0 auto' width='100%' p={1}>
      {items.map((orderItem, i) => {
        return (
          <Order key={`${orderItem.order}-${i}`} order={orderItem} />
        );    
      })}
    </Box>
  );

};

export default details;
