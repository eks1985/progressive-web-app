import React, { useState} from 'react';
import Swipeable from 'react-swipeable';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import IconButton from '@material-ui/core/IconButton';
import LeftIcon from '@material-ui/icons/ArrowLeft';
import RightIcon from '@material-ui/icons/ArrowRight';
import Select from 'components/select';
import { move } from 'lib/swipe';
import Content from './content';

const useStyles = makeStyles(() => ({
  iconButton: {
    margin: 0,
  },
}));

const Arrow = props => {
  const { step, options, option, setOption, children } = props;
  const classes = useStyles();
  return (
    <IconButton
      aria-label='nav'
      className={classes.iconButton}
      onClick={
        () => {
          move(step, options, option, setOption);
        }
      }
    >
      {children}
    </IconButton>
  );
};

const openItems = props => {

  const {
    items,
    internalUser,
  } = props;

  
  const options = items.map(item => ({ value: item._id, label: item.props ? item.props.name : item._id }));
  const initOption = internalUser ? options.find(item => item.value === 'total') : options[0];
  const [option, setOption] = useState(initOption);

  const item = items.find(item => item._id === option.value);

  return (
    <Box maxWidth='600px' margin='0 auto' width='100%'>
      <Box p={1} width='100%' minWidth='350px'>
        {items.length > 0 &&
          <Box display='flex' alignItems='center' width='100%' flex='1 0 auto'>
            <Arrow
              step={-1}
              options={options}
              option={option}
              setOption={setOption}
            >
              <LeftIcon fontSize='small' /> 
            </Arrow>
            <Select
              width='100%'
              options={options}
              placeholder='Location'
              option={option}
              onSelectHandler={
                val => {
                  setOption(val);
                }
              }
            />
            <Arrow
              step={1}
              options={options}
              option={option}
              setOption={setOption}
            >
              <RightIcon fontSize='small' /> 
            </Arrow>
          </Box>
        }
      </Box>
      <Swipeable
        flickThreshold={5}
        onSwiped={
          (_, deltaX) => {
            move(deltaX > 50 ? 1 : -1, options, option, setOption);
          }
        }
        style={{ flex: '1 0 auto', minHeight: '100vh' }}
      >
        {items &&
          <Box minHeight='100px' width='100%'>
            <Content item={item} />
          </Box>
        }
      </Swipeable>
    </Box>
  );
};

export default openItems;
