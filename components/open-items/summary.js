import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useStore } from 'lib/store';
import { useRouter } from 'next/router';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { formatNumb } from 'lib/format';

const useStyles = makeStyles(theme => ({
  container: {
    
  },
}));

const CurrencyTotal = props => {

  const { data, currency } = props;
  return (
    <Box m={1} p={1} border='1px solid #ddd'>
      <Box textAlign='center'>
        <Typography variant='body2' color='primary'>
          {`Debt ${currency}`}   
        </Typography>   
      </Box>
      <Box display='flex'>
        <Box>
          <Box>
            <Typography variant='body2'>
              Debt   
            </Typography>  
          </Box>
          <Box>
            <Typography variant='body2'>
              Overdue   
            </Typography>  
          </Box>
          <Box>
            <Typography variant='body2'>
              Adv payment   
            </Typography>  
          </Box>
          <Box>
            <Typography variant='body2'>
              Balance   
            </Typography>  
          </Box>
        </Box>
        <Box flex='1 0 auto' textAlign='right'>
          <Box>
            <Typography variant='body2'>
              {formatNumb(data.debt)}
            </Typography>
          </Box>
          <Box>
            <Typography variant='body2'>
              {formatNumb(data.overdue)}
            </Typography>
          </Box>
          <Box>
            <Typography variant='body2'>
              {formatNumb(data.advanced)}
            </Typography>
          </Box><Box>
            <Typography variant='body2'>
              {formatNumb(data.balance)}
            </Typography>
          </Box>
        </Box>
      </Box>
    </Box>
  );

};

const summary = props => {

  const store = useStore();
  const router = useRouter();
  const classes = useStyles();

  const { summary } = props;

  return (
    <Box margin='0 auto' width='100%'>
      {Object.keys(summary).map(currency => {
        return <CurrencyTotal key={currency} currency={currency} data={summary[currency]} />;    
      })}
    </Box>
  );

};

export default summary;
