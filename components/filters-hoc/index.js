import React, { useState } from 'react';
import Filters from 'components/filters';
import { createOptions, createItems, getLabel, filterData } from './internals';

function filtersHoc(Component, data, options) {

  const { filtersLegend } = options;
  const filtersShape = filtersLegend.map(shapeElem => {
    const { name, index } = shapeElem;
    return {
      name,
      label: getLabel(name),
      items: index ? createItems(index) : createOptions(data, name),
      value: [],
      mult: true,
    };
    
  });

  return function filtersContainer() {

    const [filters, setFilters] = useState(filtersShape);
    const [items, setItems] = useState(data);

    const filtersEdit = action => {
      const { index, value } = action;
      const copy = [...filters];
      copy[index].value = value;
      setFilters(copy);
    };

    const filtersReset = () => {
      const copy = [...filters];
      copy.forEach(filter => {
        filter.value = [];
      });
      setItems(data);
    };

    const filtersApply = () => {
      const filtered = filterData(filters, items);
      setItems(filtered);
    };

    const actions = {
      filtersEdit,
      filtersReset,
      filtersApply,
    };

    return (
      <>
        <Filters filters={filters} {...actions} />
        <Component items={items} />
      </>
    );
  };
}

export default filtersHoc;
