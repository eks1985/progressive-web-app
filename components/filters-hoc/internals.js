
export const createOptions = (collection, name) => {
  const index = collection.reduce((res, elem) => {
    return res.includes(elem[name]) ? res : res.concat(elem[name]);
  }, []);
  return createItems(index);
};

export const createItems = index => {
  return index.map(elem => ({ value: elem, label: elem }));
};

export const getLabel = name => {
  return name[0].toUpperCase() + name.slice(1, name.length);
};

export const filterData = (filters, items) => {
  const callback = item => {
    for (const filter of filters) {
      if (filter.value.length > 0) {
        const arr = filter.value.map(fvalue => fvalue.value);
        if (!arr.includes(item[filter.name])) {
          return false;
        }
      }
    }
    return true;
  };
  return items.filter(callback);
};