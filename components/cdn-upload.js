import React from 'react';
import { useStore } from 'lib/store';
import axios from 'axios';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import classNames from 'classnames';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';

const useStyles = makeStyles(theme => ({
  input: {
    display: 'none',
  },
  rightIcon: {
    marginLeft: theme.spacing(1),
  },
  span: {
    marginLeft: theme.spacing(2),
  }
}));

const uploadFileCloudinary = file => {
  const cloudName = 'mg-dealer';
  const unsignedUploadPreset = 'rl2037di';
  const url = `https://api.cloudinary.com/v1_1/${cloudName}/upload`;
  const fd = new FormData(); /* eslint-disable-line */
  fd.append('upload_preset', unsignedUploadPreset);
  fd.append('tags', 'browser_upload'); // Optional - add tag for image admin in Cloudinary
  fd.append('file', file);

  return axios
  .post(
    url,
    fd,
    {
      headers: { 'X-Requested-With': 'XMLHttpRequest' },
    }
  )
  .then(response => {
    return response.data.secure_url;
  });
};

const fileUpload = props => {

  const {
    handleAddPicture,
    setUploading, 
    name,
    btnLabel,
    limit,
    nolimit
  } = props;
  
  const classes = useStyles();

  const store = useStore();

  return (
    <Box mt={2} display='flex' justifyContent='center' alignItems='center'>
      <input
        name='picture'
        type='file'
        className={classNames('file-upload', classes.input)}
        data-cloudinary-field='image-id'
        id={name}
        onChange={
          e => {
            if (setUploading) {
              setUploading(true);
            } else {
              // store.setLoading(true);
            }
            const file = e.target.files[0];
            if (file.size < limit || nolimit) {
              uploadFileCloudinary(file)
              .then(path => {
                handleAddPicture(path);
                if (setUploading) {
                  setUploading(false);
                } else {
                  // store.setLoading(false);
                }
              })
              .catch(err => {
                console.log('err', err);
              });
            } else {
              const size = parseInt(file.size / 1000, 10);
              document.getElementById('to-big').innerHTML = `${size.toString()} kb (max ${limit / 1000} kb)`;
            }
          }
        }
      />
      <label htmlFor={name}>
        <Button variant='outlined' component='span'>
          {btnLabel}
          <CloudUploadIcon className={classes.rightIcon} />
        </Button>
        <span className={classes.span} id='to-big'></span>
      </label>
    </Box>
  );
};

fileUpload.propTypes = {
  handleAddPicture: PropTypes.func.isRequired,
};

fileUpload.defaultProps = {
  name: 'outlined-button-file',
  btnLabel: 'Загрузить',
  limit: 800000,
  nolimit: false,
};

export default fileUpload;
