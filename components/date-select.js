import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import EventIcon from '@material-ui/icons/Event';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import grey from '@material-ui/core/colors/grey';
import InfiniteCalendar from 'react-infinite-calendar';
import Paper from '@material-ui/core/Paper';
import 'react-infinite-calendar/styles.css';
import { parseDate, getTodaystring, getTomorrow } from 'lib/dates';
import date from 'date-and-time';

const theme = {
  accentColor: '#448AFF',
  floatingNav: {
    background: 'rgba(56, 87, 138, 0.94)',
    chevron: '#FFA726',
    color: '#FFF',
  },
  headerColor: '#0275d8',
  selectionColor: '#559FFF',
  textColor: {
    active: '#FFF',
    default: '#333',
  },
  todayColor: '#FFA726',
  weekdayColor: '#559FFF',
  maxWidth: '360px',
  width: '100%',
};

const styles = {
  container: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    margin: '0 auto',
  },
  input: {
    width: '100px',
  }
};

const iconStyle = {
  color: grey[700],
};

const Calendar = props => {
  const { handleSelectDate, calendarDate, minDate } = props;
  return (
    <Paper square elevation={0} style={{ position: 'absolute', left: '0px', top: '0px', width: '100%', zIndex: '9999' }}>
      <div className='d-flex justify-content-center'>
        <Button
          onClick={
            () => {
              handleSelectDate(calendarDate);
            }
          }
        >
          Закрыть
        </Button>
      </div>
      <InfiniteCalendar
        locale={{
          weekStartsOn: 1,
        }}
        width='100%'
        height={400}
        selected={parseDate(calendarDate)}
        minDate={minDate}
        theme={theme}
        onSelect={
          val => {
            handleSelectDate(date.format(val, 'DD-MM-YYYY'));
          }
        }
      />
    </Paper>
  );
};

Calendar.propTypes = {
  handleSelectDate: PropTypes.func.isRequired,
};

const tomorrow = getTomorrow();

Calendar.defaultProps = {
  pastDateRestriction: true,
  minDate: tomorrow,
};

const DateInput = props => {

  const todayString = getTodaystring();

  const { classes: { container, input }, calendarDate, handleSelectDate, calendarOn, showCalendar, showToday, minDate, showCalendarIcon } = props;
  let val = '';
  if (calendarDate !== todayString) {
    val = calendarDate;
  }
  if (showToday && calendarDate === todayString) {
    val = todayString;
  }
  return (
    <div className={container}>
      <TextField
        id='date-select'
        className={input}
        placeholder=''
        value={val}
        onClick={showCalendar}
        margin='none'
      />
      {showCalendarIcon &&
        <IconButton
          aria-label='Date'
          onClick={showCalendar}
        >
          <EventIcon style={iconStyle} />
        </IconButton>
      }
      {calendarOn && <Calendar handleSelectDate={handleSelectDate} calendarDate={calendarDate} minDate={minDate} />}
    </div>
  );
};

DateInput.propTypes = {
  showCalendar: PropTypes.func.isRequired,
};

DateInput.defaultProps = {
  showToday: false,
  showCalendarIcon: true,
};

export class DateInputContainer extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    const { calendarDate } = props;
    this.state = { calendarOn: false, calendarDate: calendarDate || todayString };
  }
  showCalendar = () => {
    this.setState({ calendarOn: true });
  }
  handleSelectDate = calendarDate => {
    const { handleSelectDate } = this.props;
    this.setState({ calendarOn: false, calendarDate }, () => {
      handleSelectDate(calendarDate);
    });
  }
  render() {
    return (
      <DateInput {...this.props} {...this.state} showCalendar={this.showCalendar} handleSelectDate={this.handleSelectDate} />
    );
  }
}

DateInputContainer.propTypes = {
  handleSelectDate: PropTypes.func.isRequired,
};

export default withStyles(styles)(DateInputContainer);
