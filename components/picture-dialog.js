import React from 'react';
import PropTypes from 'prop-types';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import withMobileDialog from '@material-ui/core/withMobileDialog';
import IconButton from '@material-ui/core/IconButton';
import IconPicture from '@material-ui/icons/Slideshow';
import IconSinglePicture from '@material-ui/icons/AspectRatio';
import IconClose from '@material-ui/icons/Close';
import IconLeft from '@material-ui/icons/ChevronLeft';
import IconRight from '@material-ui/icons/ChevronRight';
import Typography from '@material-ui/core/Typography';
import Swipeable from 'react-swipeable';

const MoveControls = ({ ind, move, pictures }) => (
  <React.Fragment>
    <IconButton
      onClick={
        () => {
          move(-1);
        }
      }
      color='primary'
      autoFocus
    >
      <IconLeft />
    </IconButton>
    <Typography className='ml-2' variant='body2' style={{ color: '#333' }}>
      {ind + 1}
    </Typography>
    <Typography className='mx-2' variant='body2' style={{ color: '#333' }}>
      \
    </Typography>
    <Typography className='mr-2' variant='body2' style={{ color: '#333' }}>
      {pictures.length}
    </Typography>
    <IconButton
      onClick={
        () => {
          move(1);
        }
      }
      color='primary'
      autoFocus
    >
      <IconRight />
    </IconButton>
  </React.Fragment>
);

class PictureDialog extends React.Component {
  state = {
    open: false,
    ind: 0,
  };

  swiped = (e, deltaX) => {
    if (deltaX > 50) {
      this.move();
    } else if (deltaX < -50) {
      this.move(-1);
    }
  }

  move = (step = 1) => {
    const { pictures } = this.props;
    const qty = pictures.length;
    const { ind } = this.state;
    let newInd = step === 1 ? ind + 1 : ind - 1;
    if (ind === 0 && step === -1) {
      newInd = qty - 1;
    }
    if (ind === qty - 1 && step === 1) {
      newInd = 0;
    }
    this.setState({ ind: newInd });
  }

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render() {
    const { fullScreen, pictures } = this.props;
    const { ind, open } = this.state;
    const picture = pictures[ind];

    return (
      <div
        onClick={
          e => {
            e.preventDefault();
            e.stopPropagation();
          }
        }
      >
        <IconButton
          style={{ position: 'absolute', left: '-10px', top: '-10px' }}
          aria-label='Open'
          onClick={
            e => {
              e.preventDefault();
              e.stopPropagation();
              this.handleClickOpen();
            }
          }
        >
          {pictures.length === 1 &&
            <IconSinglePicture />
          }
          {pictures.length > 1 &&
            <IconPicture />
          }
        </IconButton>
        <Dialog
          fullScreen={fullScreen}
          open={open}
          onClose={this.handleClose}
          aria-labelledby='responsive-dialog-title'
        >
          <DialogActions>
            {pictures.length > 1 &&
              <MoveControls ind={ind} move={this.move} pictures={pictures} />
            }
            <IconButton onClick={this.handleClose} color='primary' autoFocus>
              <IconClose />
            </IconButton>
          </DialogActions>
          <DialogContent
            style={{ padding: '0px 4px 4px', margin: '0 auto', display: 'flex', alignItems: 'center' }}
            onClick={
              () => {
                this.move(1);
              }
            }
          >
            <Swipeable
              flickThreshold={5}
              onSwiped={this.swiped}
              style={{ flex: '1 0 auto', width: '100%' }}
            >
              <img
                src={picture}
                alt={picture}
                style={{ maxWidth: '100%', maxHeight: '100%' }}
              />
            </Swipeable>
          </DialogContent>
        </Dialog>
      </div>
    );
  }
}

PictureDialog.propTypes = {
  fullScreen: PropTypes.bool.isRequired,
  pictures: PropTypes.array.isRequired,
};

export default withMobileDialog()(PictureDialog);
