import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Paper from '@material-ui/core/Paper';
import Country from 'components/navigation/country';
import Logo from 'components/navigation/logo';
import GoBack from 'components/navigation/goBack';
import Menu from 'components/navigation/menu';
import Cart from 'components/navigation/cart';
import { useStore } from 'lib/store';

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flex: '1 0 auto',
    justifyContent: 'space-between',
    alignItems: 'center',
    background: '#eee',
    zIndex: '999',
    position: 'fixed',
    top: '0px',
    left: '0px',
    width: '100%',
    height: theme.spacing(7),
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
  },
}));

const SectionWrapper = ({ children }) => (
  <Box display='flex' alignItems='center'>
    {children}
  </Box>
);

const header = props => {

  const classes = useStyles();
  const store = useStore();
  const { appUser } = store; 

  return (
    <Box id='header-container' display='flex' justifyContent='space-between' alignItems='center' px={2}>
      <Paper square className={classes.container}>
        <SectionWrapper>
          {appUser && <Country />}
          <Logo />
        </SectionWrapper>
        <Box>
          <GoBack />
        </Box>
        <SectionWrapper>
          <Cart />
          <Menu />
        </SectionWrapper>
      </Paper>
    </Box>
  );
};

export default header;
