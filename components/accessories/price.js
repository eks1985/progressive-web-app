import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles(theme => ({
  item: {
    // background: '#eee',
    // color: theme.palette.primary.main,
    textAlign: 'center',
    fontWeight: 300,
    // fontSize: '16px',
  }
}));

const Price = props => {

  const classes = useStyles();

  const { price, country } = props;
  if (!price) {
    return null;
  }
  const formatCurrency = 'ru-RU';
  const formatDigits = { minimumFractionDigits: 0 };
  const priceFormatted = price.toLocaleString(formatCurrency, formatDigits);
  return (
    <Box mt={1}>
      <Typography variant='h6' className={classes.item}>
        {`${priceFormatted} ${country === 0 ? '€' : '€'} с НДС`}
      </Typography>
    </Box>
  );
};

export default Price;
