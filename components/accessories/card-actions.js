import React from 'react';
import { useStore } from 'lib/store';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Cart from 'lib/cart';

const cardActions = props => {

  const { accessory } = props;

  const store = useStore();
  const { appUser, setCart, setCartOpen } = store;

  const handleAddToCart = () => {
    let cart = store.cart;
    if (!cart) {
      cart = new Cart();
    }
    cart.addProduct({ ...accessory, accessory: true }); // add mark accessory to destinguish inside cart 
    if (appUser) {
      cart.update({ event: 'changeProp', prop: 'customer', value: appUser.customerCurOpt });   
    }
    setCart(cart);
    setCartOpen(true);
  };

  return (
    <Box width='100%' textAlign='center' borderTop='1px solid #eee'>
      <Box mt={1}>
        <Button
          color='primary'
          size='small'
          onClick={
            e => {
              e.preventDefault();
              e.stopPropagation();
              handleAddToCart();  
            }
          }
        >
          Запросить
        </Button>
      </Box>
    </Box>
  );

};

export default cardActions; 