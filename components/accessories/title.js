import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typograhphy from '@material-ui/core/Typography';

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    alignItems: 'center',
    height: '72px',
    fontSize: '12px',
    lineHeight: '1.3em',
    color: 'white',
    background: theme.palette.primary.main,
    padding: '8px',
    borderTopLeftRadius: '2px',
    borderTopRightRadius: '2px',
    overflow: 'hidden',
  },
  title: {
    textOverflow: 'elipsis',
    overflow: 'hidden',
    height: '100%',
    display: 'flex',
    // alignItems: 'center'
  },
}));


const title = props => {
  
  const { title } = props;
  const classes = useStyles();
  
  return (
    <div className={classes.container}>
      <Typograhphy className={classes.title} variant='caption'>
        {title}
      </Typograhphy>
    </div>
  );

};

title.defaultProps = {
  title: 'Аксессуар',
};

export default title;
