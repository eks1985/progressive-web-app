import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    width: '150px',
    margin: '4px',
    marginBottom: props => props.last ? theme.spacing(8) : theme.spacing(1),
    // marginBottom: theme.spacing(1),
  },
}));

const accessoryCard = props => {
  const { children, last } = props;
  const classes = useStyles({ last });
  return (
    <Paper className={classes.container}>
      {children}
    </Paper>
  );
};

export default accessoryCard;
