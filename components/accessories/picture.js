import React from 'react';
import Image from 'next/image'
import Box from '@material-ui/core/Box';

const picture = props => {

  const { picture } = props;

  return (
    <Box mt={2} margin='0 auto' width={136} height={136} position='relative'>
      {picture &&
        <Image className='img' src={picture} alt='category-picture' layout='fill' objectFit='contain' objectPosition='center center' />
      }
    </Box>
  );

};

export default picture;
