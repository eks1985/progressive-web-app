import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    overflowX: 'auto',
  },
  table: {
    width: '100%',
  },
  item: {
    display: 'flex',
    justifyContent: 'center',
    fontSize: '12px',
    color: '#333',
    background: '#eee',
  }
}));

const codesTable = props => {

  const { data } = props;
  const classes = useStyles();
  return (
    <Paper className={classes.root} elevation={0} square>
      <div className={classes.table}>
        {data.map((row, ind) => {
          return (
            <Box key={`${row.code}-${ind}`} className={classes.item}>
              <Typography variant='caption'>
                {`${row.qty} x ${row.code}`}
              </Typography>
            </Box>
          );
        })}
      </div>
    </Paper>
  );

};

export default codesTable;