import React from 'react';
import Box from '@material-ui/core/Box';
import Card from './card';
import Title from './title';
import Picture from './picture';
import Actions from './card-actions';
import Price from './price';
import Codes from './codes';

const accessories = props => {

  const { items, country } = props;

  return (
    <Box display='flex' flexWrap='wrap' justifyContent='center'>
      {
        items.map((item, i) => {
          const { codes, descr, picture, price, manualPrice } = item;
          return (
            <Card key={i} last={i >= items.length - 1}>
              <Title title={descr} />
              <Codes data={codes} />
              <Picture picture={picture} />
              <Price price={manualPrice || price} country={country} />
              <Box p={1}>
                <Actions accessory={item} />  
              </Box>
            </Card>
          );
        })
      }
    </Box>
  );

};

export default accessories;



