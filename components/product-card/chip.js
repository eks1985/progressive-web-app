import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import IconExpandLess from '@material-ui/icons/ExpandLess';
import IconExpandMore from '@material-ui/icons/ExpandMore';

const useStyles = makeStyles(theme => ({
  chip: {
    textTransform: 'lowercase',
    backgroundColor: props => props.type === 'attr' ? '#ddd' : '#fafafa',
    borderColor: props => props.type === 'mod-active' || props.type === 'variants' ? '#E1001A' : '#dddddd',
    marginRight: theme.spacing(1) / 2,
    marginTop: theme.spacing(1) / 2,
    padding: '0px 6px',
  },
  chipText: {
    fontWeight: '400',
    fontSize: theme.typography.pxToRem(11),
  }
}));

const ExpandIcon = ({ varOpen }) => (
  varOpen
    ? <IconExpandLess style={{ width: '20px', height: '20px', marginRight: '-4px' }} />
    : <IconExpandMore style={{ width: '20px', height: '20px', marginRight: '-4px' }} />
);

const chip = props => {

  // type=attr, mod, mod-active, variants
  // modif - modifictor

  const { text, type, modif, varOpen, toggleVarOpen, defHandler, setActiveVal } = props;

  const classes = useStyles({ type });

  let clickHandler;
  if (type === 'variants') {
    clickHandler = toggleVarOpen;
  } else if (type === 'mod' || type === 'mod-active') {
    clickHandler = () => {
      setActiveVal(modif, text);  
    };
  } else {
    clickHandler = defHandler;
  }

  return (
    <Button
      size='small'
      variant='outlined'
      className={classes.chip}
      onClick={clickHandler}
    >
      <Typography variant='caption' className={classes.chipText}>
        {text}
      </Typography>
      {type === 'variants' && <ExpandIcon varOpen={varOpen} />}
    </Button>
  );

};

chip.defaultProps = {
  text: '',
  type: 'attr',
  defHandler: () => {},
};

export default chip;