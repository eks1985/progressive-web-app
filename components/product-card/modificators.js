import React from 'react';
import { useStore } from 'lib/store';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import ChipsRow from './chips-row';

const useStyles = makeStyles(theme => ({
  title: {
    fontSize: 14,
  },
  variantsContainer: {
    overflowY: props => props.cond ? 'scroll' : 'initial',
  },
}));

const modificators = props => {

  const store = useStore();
  const { cond } = store;
  const classes = useStyles({ cond });

  const { opt, setActiveVal, checkout } = props;

  return (
    <Box p={1} margin='0 auto' height={ checkout ? 'initial' : 240 } position='relative' className={classes.variantsContainer}>
      {opt.map(item => {
        const values = item.values.map(itemValue => {
          return { type: itemValue.active ? 'mod-active' : 'mod', text: itemValue.value }    
        });
        return (
          <Box key={item.title} mt={1}>
            <Typography className={classes.title} color='textSecondary'>
              {item.title}
            </Typography>
            <ChipsRow
              items={values}
              modif={item.title}
              setActiveVal={setActiveVal}
            />
          </Box>
        );
      })}
    </Box>
  );

};

export default modificators;