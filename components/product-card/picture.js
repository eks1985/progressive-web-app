import React from 'react';
import { useRouter } from 'next/router';
import { makeStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import PictureIcon from '@material-ui/icons/Slideshow';
import Box from '@material-ui/core/Box';
import Image from 'next/image';

const useStyles = makeStyles(theme => ({
  imgDialogIcon: {
    position: 'absolute',
    left: 10,
    top: 10,
  },
  container: {
    cursor: 'pointer',
  }
}));

const picture = props => {

  const classes = useStyles();
  const { product, src, setImgDialog } = props;

  const router = useRouter();

  return (
    <Box
      className={classes.container}
      margin='0 auto'
      height={240}
      position='relative'
      onClick={
        () => {
          router.push(`/product/${product._id}`)
        }
      }
    >
      {src &&
        <Image className='img' src={src} alt='category-picture' layout='fill' objectFit='contain' objectPosition='center center' />
      }
      {src && product.pictures.length > 0 &&
        <Box className={classes.imgDialogIcon}>
          <IconButton
            onClick={
              e => {
                e.preventDefault();
                e.stopPropagation();  
                setImgDialog(true);
              }
            }
          >
            <PictureIcon />   
          </IconButton>
        </Box>
      }
    </Box>
  );

};

export default picture;