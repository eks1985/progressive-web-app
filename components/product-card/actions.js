
import React from 'react';
import { useRouter } from 'next/router';
import { useStore } from 'lib/store';
import { makeStyles } from '@material-ui/core/styles';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';
import Cart from 'lib/cart';

const useStyles = makeStyles(theme => ({
  cardActions: {
    borderTop: '1px solid #eee',
    display: 'flex',
    justifyContent: 'center',
  },
}));

const actions  = props => {

  const classes = useStyles();
  const router = useRouter();
  const store = useStore();
  const { permiss } = store;
  const { globalOpt, appUser, setCart, setCartOpen } = store;

  const { product } = props;

  const handleAddToCart = () => {
    let cart = store.cart;
    if (!cart) {
      cart = new Cart();
    }
    cart.addProduct(product, globalOpt);
    if (appUser) {
      cart.update({ event: 'changeProp', prop: 'customer', value:  appUser.customerCurOpt });   
    }
    setCart(cart);
    setCartOpen(true);
  }

  return (
    <CardActions className={classes.cardActions}>
      {(permiss.orders.placeOrders || permiss.atoms.ordersSend || permiss.atoms.requestSend) &&
        <Button
          color='primary'
          variant='outlined'
          onClick={handleAddToCart}
        >
          Запросить
        </Button>
      }
      <Button
        color='primary'
        onClick={
          () => {
            router.push(`/product/${product._id}`);
          }
        }
      >
        Подробнее
      </Button>
    </CardActions>
  );

}

export default actions;

