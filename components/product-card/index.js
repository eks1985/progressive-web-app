import React, { useState, useEffect } from 'react';
import { useStore } from 'lib/store';
import { orderBy } from 'lodash';
import { makeStyles } from '@material-ui/core/styles';
import { getProductChipsItems, getProductPicture } from 'lib/internals/catalog';
import { initOpt } from './internals';
import Box from '@material-ui/core/Box';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import ChipsRow from './chips-row';
import Prices from './prices';
import Title from './title';
import ImgDialog from './ing-dialog';
import ImgCarousel from './img-carousel';
import Menu from './menu';
import Modificators from './modificators';
import Picture from './picture';
import Actions from './actions';

const useStyles = makeStyles(theme => ({
  root: {
    width: 320,
    margin: theme.spacing(1),
  },
  header: {
    background: theme.palette.primary.main,
    color: '#fafafa',
    fontSize: theme.typography.pxToRem(14),
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
    paddingTop: theme.spacing(1),
    paddingBottom: theme.spacing(1),
  },
  menuIconContainer: {
    marginTop: '8px',
  },
  moreVertIcon: {
    color: '#fafafa',
  },
}));

const productCard = props => {
  
  const classes = useStyles();
  const [product, setProduct] = useState(false);
  const [varOpen, setVarOpen] = useState(false);
  const [opt, setOpt] = useState(props.product.opt);
  const [imgDialog, setImgDialog] = useState(false);
  const [ind, setInd] = useState(0);

  const store = useStore();
  
  useEffect(() => {

    if (!product) {
      setProduct(props.product);
      setOpt(orderBy(initOpt(props.product.opt), ['order'], ['asc']))
    }

  }, [props.product]);

  
  if (!product) {
    return null;
  }
  
  const toggleVarOpen = () => {
    setVarOpen(!varOpen);
  }

  const setPictureInd = step => {

    if (ind === product.pictures.length - 1 && step === 1) {
      setInd(0);
      return;
    }

    if (ind === 0 && step === -1) {
      setInd(product.pictures.length - 1);
      return;
    }

    setInd(ind + step);

  };
  
  const setActiveVal = (title, value) => {
    
    const updated = opt.map(item => {
      if (item.title === title) {
        item.values = item.values.map(valueItem => {
          if (valueItem.value === value) {
            return { ...valueItem, active: true };
          }
          const { active, ...other } = valueItem;
          return other;
        })  
      }
      return item;
    });
    
    setOpt(updated);

    store.setGlobalOpt({
      id: product.id,
      opt: updated,
    });
    
  };

  const src = getProductPicture(product);
  
  return (
    <Box display='flex' justifyContent='center' flexDirection='column'>
      <Card className={classes.root}>
        <CardHeader
          className={classes.header}
          action={
            <Box className={classes.menuIconContainer}>
              <Menu product={product} />
            </Box>
          }
          title={<Title title={product.title} />}
        />
        <Box p={1}>
          <ChipsRow items={getProductChipsItems(product, product.opt)} varOpen={varOpen} toggleVarOpen={toggleVarOpen} />
        </Box>
        <Box p={1}>
          <Prices product={product} opt={opt} />
        </Box>
        {varOpen
          ?
            <Modificators opt={opt} setActiveVal={setActiveVal} />
          :
            <Picture product={product} src={src} setImgDialog={setImgDialog} />
        }
        <Actions product={product} />
      </Card> 
      <ImgDialog
        open={imgDialog}
        setOpen={setImgDialog}
        ind={ind}
        setInd={setInd}
        length={product.pictures ? product.pictures.length : 0}
        setPictureInd={setPictureInd}
      >
        <ImgCarousel
          pictures={product.pictures}
          ind={ind}
          setInd={setInd}
          setPictureInd={setPictureInd}
        />  
      </ImgDialog>  
    </Box>
  );

};

export default productCard;
