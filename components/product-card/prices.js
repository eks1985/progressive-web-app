import React from 'react';
import { formatNumb } from 'lib/format';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { getPrice } from 'lib/internals/catalog';
import { useStore } from 'lib/store';

const prices = props => {

  const { product, opt, curr, vat } = props;
  const store = useStore();
  const { permiss } = store;

  const priceD = getPrice('dealer', product, opt).price;
  const priceC = getPrice('farmer', product, opt).price;

  if (!priceD && !priceC) {
    return null;
  }

  let prices = '';
  if (permiss.atoms.priceDealer || permiss.catalog.showDealerPrice) {
    prices = priceD ? `${formatNumb(priceD)} / ${formatNumb(priceC)}` : `${formatNumb(priceC)}`;
  } else {
    prices = `${formatNumb(priceC)}`;
  }


  return (
    <Box display='flex' justifyContent='center'>
      <Typography variant='h6' style={{ fontWeight: 300 }}>
        {`${prices} ${curr} ${vat}`}
      </Typography>
    </Box>
  );

};

prices.defaultProps = {
  curr: '€',
  vat: 'с НДС',
};

export default prices;
