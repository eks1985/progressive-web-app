import React from 'react';
import Swipeable from 'react-swipeable';
import Box from '@material-ui/core/Box';
import Image from 'next/image';

const imgCarousel = props => {

  const { pictures, ind, setPictureInd } = props;
  
  if (!pictures) {
    return null;
  }

  const swiped = (e, deltaX) => {
    if (deltaX > 50) {
      setPictureInd(1);
    } else if (deltaX < -50) {
      setPictureInd(-1);
    }
  };

  return (
    <Swipeable
      flickThreshold={5}
      onSwiped={swiped}
      style={{ flex: '1 0 auto', minHeight: '100vh' }}
    >
      {/* <Box width='100%' height={400}>
        {ind}
      </Box> */}
      <Box mt={2} margin='0 auto' className='img-wrap' width='100%' position='relative'>
        <Image className='img' src={pictures[ind]} alt='category-picture' layout='fill' objectFit='contain' objectPosition='center center' />
      </Box>
    </Swipeable>
  );


};

export default imgCarousel;
