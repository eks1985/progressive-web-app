import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Slide from '@material-ui/core/Slide';
import IconButton from '@material-ui/core/IconButton';
import IconLeft from '@material-ui/icons/ChevronLeft';
import IconRight from '@material-ui/icons/ChevronRight';
import IconClose from '@material-ui/icons/Close';

const useStyles = makeStyles(theme => ({
  appBar: {
    position: 'relative',
    display: 'flex',
    justifyContent: 'center',
  },
  flex: {
    flex: 1,
  },
  icon: {
    color: '#fafafa',
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
  },
  ind: {
    color: '#fafafa',
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
  },
}));

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const itemDialog = props => {

  const { open, setOpen, ind, setPictureInd, length} = props;

  const classes = useStyles();

  return (
    <Box display='flex' flexDirection='column' justifyContent='flex-end' mt={3}>
      <Dialog
        style={{ maxWidth: '900px', margin: '0 auto' }}
        fullScreen
        open={open}
        onClose={
          () => {
            setOpen(false);
          }
        }
        TransitionComponent={Transition}
      >
        <AppBar className={classes.appBar}>
          <Toolbar className={classes.appBar}>
            <IconButton className={classes.icon} onClick={setPictureInd.bind(null, -1)}>
              <IconLeft />
            </IconButton>
            <Typography className={classes.ind}>
              {ind + 1}
            </Typography>
            <Typography className={classes.ind}>
              из
            </Typography>
            <Typography className={classes.ind}>
              {length}
            </Typography>
            <IconButton className={classes.icon} onClick={setPictureInd.bind(null, 1)}>
              <IconRight />
            </IconButton>
            <IconButton className={classes.icon} onClick={setOpen.bind(null, false)}>
              <IconClose />
            </IconButton>
          </Toolbar>
        </AppBar>
        <Paper square>
          <Box mt={2}>
            {props.children}
          </Box>
        </Paper>
      </Dialog>
    </Box>
  );

};

export default itemDialog;

