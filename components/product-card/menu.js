import React, { useState } from 'react';
import { useRouter } from 'next/router';
import { makeStyles } from '@material-ui/core/styles';
import { useStore } from 'lib/store';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import IconButton from '@material-ui/core/IconButton';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import FormControl from '@material-ui/core/FormControl';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import { translate } from 'lib/vacab';
import Cart from 'lib/cart';

const useStyles = makeStyles(() => ({
  moreVertIcon: {
    color: '#fafafa',
  },
  link: {
    cursor: 'pointer',
    color: '#304ffe',
    textDecoration: 'underline',
  },
}));

const MachinePopupMenu = props => {

  const router = useRouter();

  const classes = useStyles();

  const {
    product,
    anchorEl,
    refDialog,
    setAnchorEl,
    setRefDialog,
    linkCopied,
    setLinkCopied,
  } = props;

  const productLink = `https://mg-dealer.com/product/${product.id}`;

  const store = useStore();
  const { permiss, lang, globalOpt, appUser, setCart, setCartOpen } = store;

  const show = permiss && ((permiss.common.showAvail && !product.accessory) || permiss.catalog.showDealerPrice || permiss.catalog.editSet || permiss.orders.placeOrders || permiss.atoms.ordersSend);
  if (!show) {
    return null;
  }

  const handleAddToCart = () => {
    let cart = store.cart;
    if (!cart) {
      cart = new Cart();
    }
    cart.addProduct(product, globalOpt);
    if (appUser) {
      cart.update({ event: 'changeProp', prop: 'customer', value:  appUser.customerCurOpt });   
    }
    setCart(cart);
    setCartOpen(true);
  };

  return (
    <div>
      <IconButton
        style={{ padding: '8px' }}
        aria-owns={anchorEl ? 'machine-menu' : null}
        aria-haspopup='true'
        onClick={
          e => {
            setAnchorEl(e.currentTarget);
          }
        }
      >
        <MoreVertIcon className={classes.moreVertIcon} />
      </IconButton>
      <Menu
        id='machine-menu'
        anchorEl={anchorEl}
        open={Boolean(anchorEl)}
        onClose={setAnchorEl.bind(null, false)}
      >
        {product && permiss && (permiss.orders.placeOrders || permiss.atoms.ordersSend) &&
          <MenuItem
            onClick={
              () => {
                setAnchorEl(null);
                handleAddToCart();
              }
            }
          >
            {translate('Запросить', lang)}
          </MenuItem>
        }
        {permiss.catalog.editSet &&
          <MenuItem
            onClick={
              () => {
                router.push(`/catalog/products/${product._id}`)
                setAnchorEl(null);
              }
            }
          >
            {translate('Редактировать', lang)}
          </MenuItem>
        }
        <MenuItem
          onClick={setRefDialog.bind(null, true)}
        >
          {translate('Поделиться', lang)}
        </MenuItem>
      </Menu>
      <Dialog
        open={refDialog}
        onClose={setRefDialog.bind(null, false)}
        aria-labelledby='forgot-password-title'
        aria-describedby='forgot-password-descr'
      >
        <DialogContent>
          <Box p={2}>
            <FormControl>
              <a
                className={classes.link}
                href={productLink}
                onClick={
                  e => {
                    e.preventDefault();
                  }
                }
              >
                {translate('Ссылка на машину', lang)}
              </a>
            </FormControl>
          </Box>
          {linkCopied &&
            <Box textAlign='center' mt={1}>
              <Typography variant='subtitle1'>
                Ссылка скопирована!
              </Typography>
            </Box>
          }
        </DialogContent>
        <DialogActions style={{ padding: '8x' }}>
          <Button
            variant='contained'
            color='primary'
            size='small'
            onClick={
              () => {
                navigator.clipboard.writeText(productLink).then(function(txt) {
                  setLinkCopied(true);
                }, function(err) {
                  console.error('Async: Could not copy text: ', err);
                });
              }
            }
          >
            Скопировать
          </Button>
          <Button
            onClick={
              () => {
                setRefDialog(false);
                setAnchorEl(null);
              }
            }
            color='primary'
            size='small'
          >
            {translate('Закрыть', lang)}
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

const menu = props => {

  const { product } = props;
  const [anchorEl, setAnchorEl] = useState(null);
  const [refDialog, setRefDialog] = useState(false);
  const [linkCopied, setLinkCopied] = useState(false);

  return (
    <MachinePopupMenu
      product={product}
      anchorEl={anchorEl}
      refDialog={refDialog}
      setAnchorEl={setAnchorEl}
      setRefDialog={setRefDialog}
      linkCopied={linkCopied}
      setLinkCopied={setLinkCopied}
    />
  );

};

export default menu;

