
export const initOpt = opt => {

  if (!opt) {
    return false;
  }

  const keys = Object.keys(opt);
  keys.forEach(key => {
    const cur = opt[key];
    cur.title = key;
    cur.values.forEach(value => {
      if (value.props.default) {
        value.active = true;
      }
    });
  });

  return opt;

};