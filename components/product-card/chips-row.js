import React from 'react';
import Box from '@material-ui/core/Box';
import Chip from './chip';

// items - props of product
// modif - title of modificator, need to set option value on click
// varOpen - variants is open
// toggleVarOpen - toggle 
// setActiveVal - set current chip as active

const chipsRow = props => {

  const { items, modif, varOpen, toggleVarOpen, setActiveVal } = props;

  if (items.length === 0 ) {
    return null;
  }

  return (
    <Box display='flex' flexWrap='wrap'>
      {items.map((item, i) => {
        return <Chip key={i} {...item} modif={modif} varOpen={varOpen} toggleVarOpen={toggleVarOpen} setActiveVal={setActiveVal} />
      })}
    </Box>
  );

};

export default chipsRow;
