import React from 'react';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';

const title = props => {

  const { title } = props;

  return (
    <Box>
      <Typography variant='subtitle1'>
        {title}
      </Typography>
    </Box>
  );

};

export default title;
