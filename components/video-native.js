import React from 'react';

const styles = {
  wrapper: {
    backgroundPosition: 'center center',
    backgroundSize: 'cover',
    paddingBottom: '56.25%',
    height: 0,
    clear: 'both',
    position: 'relative',
  },
  container: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    left: 0,
    top: 0,
    padding: 0,
  },
};

const Y = props => {
  
  const { id, style, d } = props;

  const wrapperExtStyle = { ...styles.wrapper, ...style };
  if (d.mt) {
    wrapperExtStyle.marginTop = `${d.mt}px`;
  }

  const dataOriginId = `https://www.youtube.com/watch?v=${d.videoId};feature=youtu.be`;
  const src = `https://www.youtube.com/embed/${d.videoId}?feature=oembed&amp;wmode=opaque&amp;rel=0;autoplay=${d.autoplay}`;
  return (
    [
      <div key='1' style={{ color: '#555', fontSize: '14px', textAlign: 'center', marginTop: '20px' }}>
        {d.tx}
      </div>,
      <div
        key='2'
        id={id}
        style={wrapperExtStyle}
        className='mg-video'
        itemProp='video'
        itemType='https://schema.org/VideoObject'
        dataoriginalurl={dataOriginId}
      >
        <div className='avia-iframe-wrap' style={styles.container}>
          <iframe
            style={styles.container}
            title={d.title}
            // width='1500'
            // height='844'
            src={src}
            frameBorder='0'
            allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture'
            allowFullScreen='1'
          />
        </div>
      </div>,
    ]
  );
};

Y.defaultProps = {
  d: {},
};

export default Y;
