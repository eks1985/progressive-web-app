/* eslint-disable react/jsx-one-expression-per-line */
import React from 'react';
import Record from './Record';

const ScreenKeyClients = props => {
  const { data } = props;
  if (!data) {
    return null;
  }
  const { allItems } = data;
  if (!allItems) {
    return null;
  }
  return (
    <div style={{ marginBottom: '20px' }}>
      {allItems.map(item => {
        return (
          <Record record={item} key={item._id} />
        );
      })}
    </div>
  );
};

export default ScreenKeyClients;
