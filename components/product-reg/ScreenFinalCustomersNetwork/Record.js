import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ExpandMoreIcon from '@material-ui/icons/Brightness1';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import red from '@material-ui/core/colors/red';
import Button from '@material-ui/core/Button';
// import green from '@material-ui/core/colors/green';
// import yellow from '@material-ui/core/colors/yellow';
// import grey from '@material-ui/core/colors/grey';
import indigo from '@material-ui/core/colors/blue';
import Link from '@material-ui/core/Link';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    marginTop: '1px',
  },
  list: {
    width: '100%',
    backgroundColor: theme.palette.background.paper,
    fontSize: theme.typography.pxToRem(10),
  },
  heading: {
    fontSize: theme.typography.pxToRem(12),
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(12),
    color: theme.palette.text.primary,
  },
  pad: {
    marginRight: 20,
  },
  listText: {
    margin: 0,
    fontSize: theme.typography.pxToRem(12),
  },
  listItemText: {
    fontSize: theme.typography.pxToRem(12),
    color: theme.palette.text.primary,
  },
  listItemTitle: {
    fontSize: theme.typography.pxToRem(12),
    color: theme.palette.text.secondary,
  },
  category: {
    // marginLeft: theme.spacing(1),
    color: red[500],
  },
  pandelSummary: {
    paddingLeft: '8px',
    paddingRight: '8px',
    margin: '0px',
    position: 'relative',
  },
  regId: {
    position: 'absolute',
    top: '3px',
    right: '10px',
    fontSize: '10px',
    color: indigo[500],
    fontWeight: 'bold',
  },
}));

const Record = props => {
  const classes = useStyles();
  const { record } = props;
  return (
    <div className={classes.root}>
      <Accordion square style={{ margin: '0px' }}>
        <AccordionSummary
          aria-label='Expand'
          aria-controls='additional-actions1-content'
          id='additional-actions1-header'
          className={classes.pandelSummary}
        >
          <div className={classes.regId}>
            {`${record.code}`}
          </div>
          <Box display='flex' style={{ flex: '1' }} flexDirection='column'>
            <Box display='flex'>
              <Typography className={classes.secondaryHeading}>{`${record.dscr}`}</Typography>
            </Box>
            <Box display='flex' justifyContent='space-between' alignItems='center' style={{ flex: '1' }}>
              <Typography className={`${classes.secondaryHeading} ${classes.category}`}>{`${record.crgnc} - ${record.crgnr}`}</Typography>
              <Typography className={`${classes.secondaryHeading} ${classes.category}`}>{record.catg}</Typography>
            </Box>
          </Box>
        </AccordionSummary>
        <AccordionDetails style={{ padding: '4px' }}>
          <List className={classes.list} dense>
            {/* <ListItem>
              <Box display='flex' flexDirection='column'>
                <Typography className={classes.listItemText}>{`${record.crgnc} - ${record.crgnr}`}</Typography>
                <Typography className={classes.listItemTitle}>Region</Typography>
              </Box>
            </ListItem> */}
            <ListItem>
              <Box display='flex' flexDirection='column'>
                <Typography className={classes.listItemText}>{`${record.czip || '<Индекс>'} - ${record.ccit || '<Город>'} - ${record.card || '<Улица>'}`}</Typography>
                <Typography className={classes.listItemTitle}>Address</Typography>
              </Box>
            </ListItem>
            <ListItem>
              <Box display='flex' flexDirection='column'>
                <Typography className={classes.listItemText}>{`${record.cfnm || '<Имя>'} - ${record.csnm || '<Фамилия>'}`}</Typography>
                <Typography className={classes.listItemTitle}>Person</Typography>
              </Box>
            </ListItem>
            <ListItem>
              <Box display='flex' flexDirection='column'>
                <Typography className={classes.listItemText}>{`${record.ctel || '<Тел>'} - ${record.ceml || '<Email>'}`}</Typography>
                <Typography className={classes.listItemTitle}>Contacts</Typography>
              </Box>
            </ListItem>
            <ListItem>
              <Box display='flex' flexDirection='column'>
                <Typography className={classes.listItemText}>{`${record.vatc || '<ИНН>'} - ${record.wbid || '<Веб ид>'}`}</Typography>
                <Typography className={classes.listItemTitle}>VAT code | Web id</Typography>
              </Box>
            </ListItem>
            <ListItem>
              <Box display='flex' flexDirection='column'>
                <Typography className={classes.listItemText}>{record.qtym}</Typography>
                <Typography className={classes.listItemTitle}>Qty mac</Typography>
              </Box>
            </ListItem>
            <ListItem>
              <Button variant='outlined' size='small'>
                Показать машины
              </Button>
            </ListItem>
          </List>
        </AccordionDetails>
      </Accordion>
    </div>
  );
};

export default Record;
