/* eslint-disable react/jsx-one-expression-per-line */
import React from 'react';
import Record from './Record';
import Pagination from './Pagination';

const ScreenFinalCustomersNetwork = props => {
  const { data, settings, nextPortion } = props;
  if (!data) {
    return null;
  }
  const { allItems, itemsQty } = data;
  if (!allItems) {
    return null;
  }
  return (
    <div>
      <Pagination nextPortion={nextPortion} itemsQty={itemsQty} settings={settings} />
      {allItems.map(item => {
        return (
          <Record record={item} key={item._id} />
        );
      })}
      <Pagination nextPortion={nextPortion} itemsQty={itemsQty} settings={settings} />
    </div>
  );
};

export default ScreenFinalCustomersNetwork;
