import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import IconButton from '@material-ui/core/IconButton';
import LoadLeft from '@material-ui/icons/ChevronLeft';
import LoadRight from '@material-ui/icons/ChevronRight';

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
}));

const Pagination = props => {
  const classes = useStyles();
  const { nextPortion, itemsQty, settings } = props;
  const { startInd } = settings;
  const maxInd = Math.min(startInd + 10, itemsQty);
  return (
    <Box display='flex' alignItems='center' justifyContent='center' className={classes.root}>
      <IconButton
        aria-label='left'
        onClick={
          () => {
            nextPortion(-1, itemsQty);
          }
        }
      >
        <LoadLeft />
      </IconButton>
      <Box>
        {`${startInd + 1} - ${maxInd} из ${itemsQty}`}
      </Box>
      <IconButton
        aria-label='right'
        onClick={
          () => {
            nextPortion(1, itemsQty);
          }
        }
      >
        <LoadRight />
      </IconButton>
    </Box>
  );
};

export default Pagination;
