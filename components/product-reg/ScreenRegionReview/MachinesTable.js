import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

const useStyles = makeStyles(theme => ({
  table: {
    padding: '4px',
    marginTop: theme.spacing(2),
  },
  cell: {
    fontSize: '12px',
    paddingTop: '6px',
    paddingBottom: '6px',
    lineHeight: 1.2,
  },
  cell1: {
    fontSize: '12px',
    paddingTop: '4px',
    paddingBottom: '6px',
    width: '180px',
    lineHeight: 1.2,
  },
  row: {
    height: '50px',
  },
}));

export default function MachinesTable(props) {
  const classes = useStyles();
  const { machines, year, selectVariant, editFilters, entity, regionName } = props;
  const totalQty = machines.reduce((res, item) => res + item.qty, 0);
  return (
    <TableContainer>
      <Table className={classes.table} aria-label='simple table'>
        <TableHead>
          <TableRow style={{ background: '#eee' }}>
            <TableCell className={classes.cell1}>{year}</TableCell>
            <TableCell className={classes.cell} align='right'>{totalQty}</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {machines.map((row) => (
            <TableRow
              key={row.name}
              className={classes.row}
              onClick={
                () => {
                  editFilters('clear');
                  let values = [];
                  values.push({ value: row._id, label: row._id });
                  editFilters(entity === 'mac' ? 'mdes' : 'dlds', values);
                  values = [];
                  values.push({ value: year, label: year });
                  editFilters('year', values);
                  values = [];
                  values.push({ value: regionName, label: regionName });
                  editFilters('crgnr', values);
                  selectVariant('registrationReport');
                }
              }
            >
              <TableCell className={classes.cell1} component='th' scope='row'>
                {row._id}
              </TableCell>
              <TableCell className={classes.cell} align='right'>{row.qty}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
