/* eslint-disable func-names */
import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Paper from '@material-ui/core/Paper';
import Slide from '@material-ui/core/Slide';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import CloseIcon from '@material-ui/icons/Close';
import Region from './Region';

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

const headCells = [
  { id: 'region', numeric: false, disablePadding: true, label: 'Region' },
  { id: 'ranktotal', numeric: true, disablePadding: false, label: 'Rank total' },
  { id: 'total', numeric: true, disablePadding: false, label: 'Crops total' },
  { id: 'mactotal', numeric: true, disablePadding: false, label: 'Mac total' },
  { id: 'rankprec', numeric: true, disablePadding: false, label: 'Rank precision' },
  { id: 'prec', numeric: true, disablePadding: false, label: 'Crops precision' },
  { id: 'macprecision', numeric: true, disablePadding: false, label: 'Mac precision' },
];

function EnhancedTableHead(props) {
  const { classes, order, orderBy, onRequestSort } = props;
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={headCell.numeric ? 'center' : 'center'}
            padding={headCell.disablePadding ? 'none' : 'default'}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  order: PropTypes.oneOf(['asc', 'desc']).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    padding: '14px',
  },
  paper: {
    width: '100%',
    marginBottom: theme.spacing(2),
    padding: '0px',
  },
  table: {
    minWidth: 900,
    overflow: 'scroll',
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
  appBar: {
    position: 'relative',
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1,
  },
}));

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction='up' ref={ref} {...props} />;
});

const FullScreenDialog = props => {
  const { toggleDialog, data, setRegion, regions, region, selectVariant, editFilters } = props;
  const classes = useStyles();
  return (
    <div>
      <Dialog fullScreen open={region !== false} onClose={toggleDialog} TransitionComponent={Transition}>
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton
              edge='start'
              color='inherit'
              onClick={
                () => {
                  setRegion(false);
                }
              }
              aria-label='close'
            >
              <CloseIcon />
            </IconButton>
            {region &&
              <Typography variant='h6' className={classes.title}>
                {`Region review: ${region.label}`}
              </Typography>
            }
          </Toolbar>
        </AppBar>
        <Box style={{ paddingBottom: '200px' }}>
          <Region
            data={data}
            regions={regions}
            setRegion={setRegion}
            region={region}
            selectVariant={selectVariant}
            editFilters={editFilters}
          />
        </Box>
      </Dialog>
    </div>
  );
};

const Regions = props => {
  const classes = useStyles();
  const [order, setOrder] = React.useState('asc');
  const [orderBy, setOrderBy] = React.useState('region');
  const [dialog, setDialog] = React.useState(false);
  const { regions, data, setRegion, region, selectVariant, editFilters } = props;

  const toggleDialog = () => {
    setDialog(!dialog);
  };

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  return (
    <div className={classes.root}>
      <FullScreenDialog
        dialog={dialog}
        toggleDialog={toggleDialog}
        data={data}
        setRegion={setRegion}
        regions={regions}
        region={region}
        selectVariant={selectVariant}
        editFilters={editFilters}
      />
      <Paper className={classes.paper}>
        <TableContainer>
          <Table
            className={classes.table}
            aria-labelledby='tableTitle'
            size='small'
            aria-label='enhanced table'
          >
            <EnhancedTableHead
              classes={classes}
              order={order}
              orderBy={orderBy}
              onRequestSort={handleRequestSort}
              rowCount={regions.length}
            />
            <TableBody>
              {stableSort(regions, getComparator(order, orderBy))
                .map((row, index) => {
                  const labelId = `regions-table-${index}`;

                  return (
                    <TableRow
                      hover
                      tabIndex={-1}
                      key={row.rfip}
                      onClick={
                        () => {
                          setRegion({ value: row.rfip, label: row.rgnn });
                        }
                      }
                    >
                      <TableCell component='th' id={labelId} scope='row'>
                        {row.rgnn}
                      </TableCell>
                      <TableCell align='center'>{row.ranktotal}</TableCell>
                      <TableCell align='center'>{row.total}</TableCell>
                      <TableCell align='center'>{row.mactotal}</TableCell>
                      <TableCell align='center'>{row.rankprec}</TableCell>
                      <TableCell align='center'>{row.prec}</TableCell>
                      <TableCell align='center'>{row.macprecision}</TableCell>
                    </TableRow>
                  );
                })}
            </TableBody>
          </Table>
        </TableContainer>
      </Paper>
    </div>
  );
};

export default Regions;
