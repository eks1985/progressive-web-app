import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles({
  table: {
    // minWidth: 650,
    padding: '4px',
  },
  cell: {
    fontSize: '12px',
    paddingTop: '6px',
    paddingBottom: '6px',
    lineHeight: 1.2,
    paddingLeft: '4px',
    paddingRight: '4px',
  },
  cell1: {
    fontSize: '12px',
    paddingTop: '4px',
    paddingBottom: '6px',
    width: '180px',
    lineHeight: 1.2,
    paddingLeft: '4px',
    paddingRight: '4px',
  },
  row: {
    height: '50px',
  },
});

export default function CropsTable(props) {
  const classes = useStyles();
  const { zip } = props;
  return (
    <TableContainer>
      <Table className={classes.table} aria-label='simple table'>
        <TableHead>
          <TableRow>
            <TableCell className={classes.cell}>Location</TableCell>
            <TableCell className={classes.cell}>Registration</TableCell>
            <TableCell className={classes.cell}>Dealer</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {zip.map((row) => (
            <TableRow key={row.name} className={classes.row}>
              <TableCell className={classes.cell1} style={{ width: '100px' }}>
                {row.czip}
              </TableCell>
              <TableCell className={classes.cell1}>
                <Box>
                  <Typography variant='caption' style={{ fontSize: '10px', fontWeight: 'bold' }}>
                    {row.regd}
                  </Typography>
                </Box>
                <Box>
                  <Typography variant='caption' style={{ fontSize: '10px', color: '#f44336' }}>
                    {row.mdes}
                  </Typography>
                </Box>
                <Box>
                  <Typography variant='caption' style={{ fontSize: '10px' }}>
                    {row.cdes1}
                  </Typography>
                </Box>
              </TableCell>
              <TableCell className={classes.cell1}>
                {row.dlds}
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
