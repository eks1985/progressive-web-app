import React from 'react';
import Box from '@material-ui/core/Box';
import Regions from './Regions';

const RegionReview = props => {
  const { data, setRegion, region, selectVariant, editFilters } = props;
  if (!data) {
    return null;
  }
  const { regions } = data;

  return (
    <Box>
      <Regions
        data={data}
        regions={regions}
        setRegion={setRegion}
        region={region}
        selectVariant={selectVariant}
        editFilters={editFilters}
      />
    </Box>
  );
};

export default RegionReview;
