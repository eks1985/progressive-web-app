import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import CropsTable from './CropsTable';
import MachinesTable from './MachinesTable';
import IntersectTable from './IntersectTable';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    marginTop: '1px',
  },
  panel: {
    margin: '0px',
  },
  panelDetails: {
    padding: '4px',
  },
  heading: {
    fontSize: theme.typography.pxToRem(12),
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(12),
    color: theme.palette.text.primary,
  },
  pad: {
    marginRight: 20,
  },
  pandelSummary: {
    paddingLeft: '8px',
    paddingRight: '8px',
    margin: '0px',
    position: 'relative',
  },
  contentContainer: {
    maxWidth: '600px',
    margin: '0 auto',
    marginTop: theme.spacing(2),
  },
}));

const RegionReview = props => {
  const classes = useStyles();
  const { data, region, selectVariant, editFilters } = props;

  if (!data) {
    return null;
  }
  const { machines, dealers, intersect, cropsRegion, atlasRegion } = data;

  return (
    <Box className={classes.contentContainer}>
      {cropsRegion &&
        <Accordion square className={classes.panel}>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-label='Expand'
            aria-controls='additional-actions1-content'
            id='additional-actions1-header'
          >
            <Box display='flex' justifyContent='space-between' flex='1 0 auto'>
              <Typography>
                Посевные площади
              </Typography>
              <Typography>
                <Link href={atlasRegion} target='_blank'>
                  Атлас площадей
                </Link>
              </Typography>
            </Box>
          </AccordionSummary>
          <AccordionDetails className={classes.panelDetails}>
            <CropsTable crops={cropsRegion} />
          </AccordionDetails>
        </Accordion>
      }
      {machines &&
        <Accordion square className={classes.panel}>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-label='Expand'
            aria-controls='additional-actions1-content'
            id='additional-actions1-header'
          >
            Машины по типам
          </AccordionSummary>
          <AccordionDetails className={classes.panelDetails}>
            <Box style={{ width: '100%' }}>
              <MachinesTable machines={machines.dyear3} year={2020} selectVariant={selectVariant} editFilters={editFilters} entity='mac' regionName={region.label} />
              <MachinesTable machines={machines.dyear2} year={2019} selectVariant={selectVariant} editFilters={editFilters} entity='mac' regionName={region.label} />
              <MachinesTable machines={machines.dyear1} year={2018} selectVariant={selectVariant} editFilters={editFilters} entity='mac' regionName={region.label} />
            </Box>
          </AccordionDetails>
        </Accordion>
      }
      {dealers &&
        <Accordion square className={classes.panel}>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-label='Expand'
            aria-controls='additional-actions1-content'
            id='additional-actions1-header'
          >
            Машины по дилерам
          </AccordionSummary>
          <AccordionDetails className={classes.panelDetails}>
            <Box style={{ width: '100%' }}>
              <MachinesTable machines={dealers.dyear3} year={2020} selectVariant={selectVariant} editFilters={editFilters} entity='dlr' regionName={region.label} />
              <MachinesTable machines={dealers.dyear2} year={2019} selectVariant={selectVariant} editFilters={editFilters} entity='dlr' regionName={region.label} />
              <MachinesTable machines={dealers.dyear1} year={2018} selectVariant={selectVariant} editFilters={editFilters} entity='dlr' regionName={region.label} />
            </Box>
          </AccordionDetails>
        </Accordion>
      }
      {intersect &&
        <Accordion square className={classes.panel}>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-label='Expand'
            aria-controls='additional-actions1-content'
            id='additional-actions1-header'
          >
            Пересечение дилеров
          </AccordionSummary>
          <AccordionDetails className={classes.panelDetails}>
            <Box style={{ width: '100%' }}>
              <IntersectTable zip={intersect.byzip} />
            </Box>
          </AccordionDetails>
        </Accordion>
      }
      {region &&
        <Accordion square className={classes.panel}>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-label='Expand'
            aria-controls='additional-actions1-content'
            id='additional-actions1-header'
          >
            Активность по региону
          </AccordionSummary>
          <AccordionDetails className={classes.panelDetails}>
            <Typography variant='subtitle1' style={{ padding: '8px', lineHeight: '1.2' }}>
              Лиды / поездки / демо показы (будет все подгружаться из битрикс)
            </Typography>
          </AccordionDetails>
        </Accordion>
      }
    </Box>
  );
};

export default RegionReview;
