import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

const useStyles = makeStyles({
  table: {
    // minWidth: 650,
    padding: '4px',
  },
  cell: {
    fontSize: '12px',
    paddingTop: '6px',
    paddingBottom: '6px',
    lineHeight: 1.2,
  },
  cell1: {
    fontSize: '12px',
    paddingTop: '4px',
    paddingBottom: '6px',
    width: '180px',
    lineHeight: 1.2,
  },
  row: {
    height: '50px',
  },
});

export default function CropsTable(props) {
  const classes = useStyles();
  const { crops } = props;
  return (
    <TableContainer>
      <Table className={classes.table} aria-label='simple table'>
        <TableHead>
          <TableRow>
            <TableCell className={classes.cell1}>Crops</TableCell>
            <TableCell className={classes.cell} align='right'>2018</TableCell>
            <TableCell className={classes.cell} align='right'>2019</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {crops.map((row, i) => (
            <TableRow key={`${row.name}-${i}`} className={classes.row}>
              <TableCell className={classes.cell1} component='th' scope='row'>
                {row.name}
              </TableCell>
              <TableCell className={classes.cell} align='right'>{(Math.round(row.values[0] * 100) / 100).toFixed(2)}</TableCell>
              <TableCell className={classes.cell} align='right'>{(Math.round(row.values[1] * 100) / 100).toFixed(2)}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
