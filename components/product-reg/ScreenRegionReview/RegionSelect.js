import React from 'react';
import Select from 'react-select';
import Box from '@material-ui/core/Box';

const prepareOptions = data => {
  return data.map(item => ({ value: item.rfip, label: item.rgnn }));
};

const CustomSelect = props => {
  const { name, regions, value, onSelect } = props;
  const options = prepareOptions(regions);
  return (
    <Box p={2}>
      <Select
        placeholder={name}
        isClearable
        style={{ zIndex: '9999' }}
        fullWidth
        value={value}
        options={options}
        onChange={
          selected => {
            if (onSelect) {
              onSelect(selected);
            } else {
              console.log('on select handler does not provided');
            }
          }
        }
      />
    </Box>
  );
};

CustomSelect.defaultProps = {
  name: 'Регион',
  data: [],
  value: {},
};

export default CustomSelect;
