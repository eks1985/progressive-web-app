import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionActions from '@material-ui/core/AccordionActions';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/FilterList';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import Box from '@material-ui/core/Box';
import Filter from './Filter';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary,
  },
  icon: {
    verticalAlign: 'bottom',
    height: 20,
    width: 20,
  },
  details: {
    alignItems: 'center',
  },
  pad: {
    marginRight: 20,
  },
}));

const Filters = props => {
  const classes = useStyles();

  const [expanded, setExpanded] = React.useState(false);

  const handleChange = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  const { filters, editFilters, settings, applyFiltersTrigger, filtersLabel } = props;
  if (!settings) {
    return null;
  }
  const hasFilters = Object.keys(settings.filtersBank).length > 0;
  if (!hasFilters) {
    return null;
  }
  if (!filters) {
    return null;
  }

  return (
    <div className={classes.root}>
      <Accordion square expanded={expanded === 'panel1'} onChange={handleChange('panel1')}>
        <AccordionSummary
          aria-controls='panel1c-content'
          id='panel1c-header'
        >
          <div className={classes.pad}>
            <ExpandMoreIcon />
          </div>
          <Typography className={classes.secondaryHeading}>{filtersLabel}</Typography>
        </AccordionSummary>
        <AccordionDetails className={classes.details}>
          <Box display='flex' flexDirection='column' width='100%'>
            {filters.map(filter => {
              return <Filter key={filter.name} {...filter} editFilters={editFilters} />;
            })}
          </Box>
        </AccordionDetails>
        <Divider />
        <AccordionActions>
          <Button
            size='small'
            onClick={
              () => {
                editFilters('clear', null);
                applyFiltersTrigger();
              }
            }
          >
            Clear
          </Button>
          <Button
            size='small'
            color='primary'
            onClick={
              () => {
                applyFiltersTrigger();
                setExpanded(false);
              }
            }
          >
            Apply
          </Button>
        </AccordionActions>
      </Accordion>
    </div>
  );
};

Filters.propTypes = {
  editFilters: PropTypes.func.isRequired,
};

export default Filters;
