import React from 'react';
import Select from 'react-select';
import Box from '@material-ui/core/Box';

const getHashStatus = status => {
  switch (status) {
    case -2:
      return 'Unknown';
    case -1:
      return 'Unregistered';
    case 0:
      return 'Buffer';
    case 1:
      return 'Registered';
    default:
      return '';
  }
};

const prepareOptions = (data, name) => {
  return data.map(item => ({ value: item, label: name === 'Stat' ? getHashStatus(item) : item }));
};

const CustomSelect = props => {
  const { name, data, value, onSelect } = props;
  const options = prepareOptions(data, name);
  return (
    <Box className='mt-2'>
      <Select
        placeholder={name}
        isClearable
        isMulti
        style={{ zIndex: '9999' }}
        fullWidth
        value={value}
        options={options}
        onChange={
          selected => {
            if (onSelect) {
              onSelect(selected);
            } else {
              onSelect(false);
              console.log('on select handler does not provided');
            }
          }
        }
      />
    </Box>
  );
};

export default CustomSelect;
