/* eslint-disable react/jsx-one-expression-per-line */
import React from 'react';
import ScreenSummary from './ScreenSummary';
import ScreenRegistrationReport from './ScreenRegistrationReport';
import ScreenFinalCustomersNetwork from './ScreenFinalCustomersNetwork';
import ScreenGeoMap from './ScreenGeoMap';
import ScreenKeyClients from './ScreenKeyClients';
import ScreenRegionReview from './ScreenRegionReview';
import ScreenAgroatlas from './ScreenAgroAtlas';

const Content = props => {
  const {
    data,
    settings,
    nextPortion,
    selectVariant,
    editFilters,
    applyFiltersTrigger,
    setRegion,
    region,
    setSummaryTotal,
    setSummaryDealers,
    geoMapFilters,
    setGeoMapFilter,
    email,
    isInternalUser,
  } = props;

  // console.log('data', data);

  const { variantCurrent } = settings;

  // console.log('variantCurrent', variantCurrent);

  if (variantCurrent === 'summary') {
    return (
      <ScreenSummary
        data={data}
        selectVariant={selectVariant}
        editFilters={editFilters}
        applyFiltersTrigger={applyFiltersTrigger}
        setSummaryTotal={setSummaryTotal}
        setSummaryDealers={setSummaryDealers}
        settings={settings}
        isInternalUser={isInternalUser}
      />
    );
  }
  if (variantCurrent === 'registrationReport') {
    return (
      <ScreenRegistrationReport
        data={data}
        settings={settings}
        nextPortion={nextPortion}
        editFilters={editFilters}
        applyFiltersTrigger={applyFiltersTrigger}
        selectVariant={selectVariant}
        email={email}
        isInternalUser={isInternalUser}
      />
    );
  }
  if (variantCurrent === 'finalCustomers') {
    return <ScreenFinalCustomersNetwork data={data} settings={settings} nextPortion={nextPortion} />;
  }
  if (variantCurrent === 'keyClients') {
    return <ScreenKeyClients data={data} />;
  }
  if (variantCurrent === 'geoMap') {
    return <ScreenGeoMap data={data} geoMapFilters={geoMapFilters} setGeoMapFilter={setGeoMapFilter} />;
  }
  if (variantCurrent === 'regionReview') {
    return (
      <ScreenRegionReview
        data={data}
        setRegion={setRegion}
        region={region}
        selectVariant={selectVariant}
        editFilters={editFilters}
      />
    );
  }
  if (variantCurrent === 'agroatlas') {
    return <ScreenAgroatlas data={data} />;
  }
  return null;
};

export default Content;
