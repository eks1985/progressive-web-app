import React from 'react';
import Select from './Select';
import Box from '@material-ui/core/Box';

const Filter = props => { // name, source, data, value
  const { editFilters, source } = props;
  
  return (
    <Box mt={1}>
      <Select
        {...props}
        onSelect={
          selected => {
            editFilters(source, selected);
          }
        }
      /> 
    </Box>
  );
};

export default Filter;
