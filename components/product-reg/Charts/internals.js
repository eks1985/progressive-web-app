export const generateSummaryChartConfig = params => {
  const { years, data, name } = params;
  const categories = years;
  const title = name || 'Summary';
  const defaultConfig = {
    chart: {
      type: 'column',
    },
    credits: {
      enabled: false,
    },
    title: {
      text: title,
    },
    xAxis: {
      categories,
    },
    yAxis: {
      title: null,
      min: 0,
      tickInterval: 5,
      stackLabels: {
        enabled: true,
      }
    },
    plotOptions: {
      column: {
        stacking: 'normal',
        dataLabels: {
            enabled: false
        },
      }
    },
    series: [
      {
        name: 'Unknown',
        data: data.Unknown,
        color: '#bdbdbd',
      },
      {
        name: 'Unregistered',
        data: data.Unreg,
        color: '#ff77a9',
      },
      {
        name: 'Buffer',
        data: data.Buffer,
        color: '#ffeb3b',
      },
      {
        name: 'Registered',
        data: data.Reg,
        color: '#8e99f3',
      },
    ],
  };
  return defaultConfig;
};

export const generateClientsClassifCompanyYearsChartConfig = params => {

  if (params === undefined) {
    return {};
  }

  const { years, data } = params;
  const categories = years;

  const defaultConfig = {
    chart: {
      type: 'column',
    },
    credits: {
      enabled: false,
    },
    title: {
      text: 'Clients classif company type',
    },
    xAxis: {
      categories,
    },
    yAxis: {
      title: null,
      min: 0,
      tickInterval: 5,
    },
    plotOptions: {
      series: {
        label: {
          connectorAllowed: false,
        },
      },
    },
    series: [
      {
        name: 'Organization',
        data: data.Organization,
        color: '#8e99f3',
      },
      {
        name: 'Farmer',
        data: data.Farmer,
        color: '#ff77a9',
      },
      {
        name: 'Unknown',
        data: data.Unknown,
        color: '#ffeb3b',
      },
    ],
  };
  return defaultConfig;
};

export const generateClientsClassifCompanyTotalChartConfig = params => {
  if (params === undefined) {
    return {};
  }
  const defaultConfig = {
    colors: [
      '#ff77a9',
      '#8e99f3',
      '#ffd95b',
      '#98ee99',
      '#ffff8b',
      '#73e8ff',
      '#cfff95',
      '#6ff9ff',
      '#be9c91',
      '#64d8cb',
      '#ffa270',
      '#ffff89',
      '#80d6ff',
      '#fffd61',
      '#df78ef',
      '#ff867c',
      '#a7c0cd',
      '#b085f5',
    ],
    credits: {
      enabled: false,
    },
    legend: {
      itemStyle: {
        fontWeight: 'normal',
        fontSize: '10px',
      },
      itemWidth: 150,
      // eslint-disable-next-line
      labelFormatter: function () {
        return `${this.y} - ${this.name}`; // eslint-disable-line
      },
    },
    chart: {
      type: 'pie',
      options3d: {
        enabled: true,
        alpha: 25,
      },
    },
    title: {
      text: 'Clients classif company type',
    },
    plotOptions: {
      pie: {
        innerSize: 35,
        depth: 25,
        size: 180,
        showInLegend: true,
      },
      series: {
        dataLabels: {
          enabled: true,
          distance: 10,
          style: {
            fontSize: '10px',
            fontWeight: 'normal',
          },
        },
      },
    },
    series: [{
      name: 'By company type',
      data: [
        { name: 'Orginizations', y: params.Organization },
        { name: 'Farmer', y: params.Farmer },
        { name: 'Unknown', y: params.Unknown },
      ],
      label: {
        enabled: false,
      },
    }],
  };
  return defaultConfig;
};

export const generateClientsClassifQtyMacChartConfig = data => {
  const defaultConfig = {
    colors: [
      '#ff77a9',
      '#8e99f3',
      '#ffd95b',
      '#98ee99',
      '#ffff8b',
      '#73e8ff',
      '#cfff95',
      '#6ff9ff',
      '#be9c91',
      '#64d8cb',
      '#ffa270',
      '#ffff89',
      '#80d6ff',
      '#fffd61',
      '#df78ef',
      '#ff867c',
      '#a7c0cd',
      '#b085f5',
    ],
    credits: {
      enabled: false,
    },
    legend: {
      itemStyle: {
        fontWeight: 'normal',
        fontSize: '10px',
      },
      itemWidth: 150,
      // eslint-disable-next-line
      labelFormatter: function () {
        return `${this.name} mac - ${this.y} clnt`; // eslint-disable-line
      },
    },
    chart: {
      type: 'pie',
      options3d: {
        enabled: true,
        alpha: 25,
      },
      // height: 300,
    },
    title: {
      text: 'Clients classif qty machines',
    },
    plotOptions: {
      pie: {
        innerSize: 35,
        depth: 25,
        size: 180,
        showInLegend: true,
      },
      series: {
        dataLabels: {
          enabled: true,
          distance: 10,
          style: {
            fontSize: '10px',
            fontWeight: 'normal',
          },
        },
      },
    },
    series: [{
      name: 'By qty machines',
      data: data.mac_stat,
      label: {
        enabled: false,
      },
    }],
  };
  return defaultConfig;
};


export const getIntStatus = status => {
  const statuses = {
    Reg: 1,
    Buffer: 0,
    Unreg: -1,
    Unknown: -2, 
  };
  return statuses[status];
}