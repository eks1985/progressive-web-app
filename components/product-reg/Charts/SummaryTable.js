import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Link from '@material-ui/core/Link';
import { getIntStatus } from './internals';

const useStyles = makeStyles(theme => ({
  tableContainer: {
    boxShadow: 'none',
    position: 'relative',
  },
}));

const SummaryTable = props => {
  const classes = useStyles();
  const { data, years } = props;
  const dataKeys = ['Reg', 'Buffer', 'Unreg', 'Unknown'];

  return (
    <TableContainer component={Paper} className={classes.tableContainer}>
      <Table aria-label='prd-reg-table' size='small'>
        <TableHead>
          <TableRow ke='header'>
            <TableCell>Статус</TableCell>
            {years.map(year => {
              return (
                <TableCell key={`header-${year}`} align='center'>{year}</TableCell>
              );
            })}
          </TableRow>
        </TableHead>
        <TableBody>
          {dataKeys.map(status => {
            const dataRow = data[status];
            return (
              <TableRow key={status}>
                <TableCell key={`${status}`} component='th' scope='row'>
                  {status}
                </TableCell>
                {years.map((year, i) => {
                  return (
                    <TableCell key={`${year}-${status}-${dataRow[i]}`} align='center'>
                      <Link
                        component='button'
                        variant='body2'
                        onClick={
                          () => {
                            console.log('click');
                            window.location = `http://localhost:3000/product-reg/?report=1&year=${year}&stat=${getIntStatus(status)}`;
                          }
                        }
                      >
                        {dataRow[i]}
                      </Link>
                    </TableCell>
                  );
                })}
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default SummaryTable;
