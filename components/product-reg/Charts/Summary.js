import React, { useState } from 'react';
import ReactHighcharts from 'react-highcharts';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import {
  generateSummaryChartConfig,
  generateClientsClassifCompanyYearsChartConfig,
  generateClientsClassifCompanyTotalChartConfig,
  generateClientsClassifQtyMacChartConfig,
} from './internals';
import SummaryTable from './SummaryTable';

const ChartCompanyClassif = props => {
  const { data } = props;
  const config2 = generateClientsClassifCompanyYearsChartConfig(data.clients_years);
  const config3 = generateClientsClassifCompanyTotalChartConfig(data.clients_total);
  const [reportType, setReportType] = useState('by-years');
  return (
    <Box display='flex' flexDirection='column' className='mt-3'>
      <Box m={1}>
        <Button
          variant='outlined'
          onClick={
            () => {
              setReportType(reportType === 'by-years' ? 'total' : 'by-years');
            }
          }
        >
          {reportType === 'by-years' ? 'Show total' : 'Show by years'}
        </Button>
      </Box>
      <ReactHighcharts
        id='clients-classif-company-type-years'
        config={reportType === 'by-years' ? config2 : config3}
      />
    </Box>
  );
};

const ChartQtyMacClassif = props => {
  const { data } = props;
  if (data === undefined) {
    return null;
  }
  const config = generateClientsClassifQtyMacChartConfig(data.clients_qty_mac);
  return (
    <Box display='flex' flexDirection='column' className='mt-3'>
      <ReactHighcharts
        id='clients-classif-qty-mac'
        config={config}
      />
    </Box>
  );
};

const ChartRegSummary = props => {
  const { data, selectVariant, editFilters, applyFiltersTrigger, settings, setSummaryTotal, setSummaryDealers, isInternalUser } = props;
  const { summaryViewType } = settings;
  if (summaryViewType === 'total' && isInternalUser) {
    const chartData = data.smr;
    const config = generateSummaryChartConfig(chartData);
    return (
      <Box className='mt-4' style={{ position: 'relative' }} display='flex' flexDirection='column'>
        {isInternalUser &&
          <Box display='flex' flexGrow='1' justifyContent='flex-end'>
            <Button
              size='small'
              onClick={
                () => {
                  selectVariant('registrationReport');
                  editFilters('clear');
                  applyFiltersTrigger();
                }
              }
            >
              Open report
            </Button>
          </Box>
        }
        <React.Fragment>
          <Box m={1}>
            <Button
              key='set-variant'
              variant='outlined'
              onClick={
                () => {
                  if (summaryViewType === 'total') {
                    setSummaryDealers();
                  }
                  if (summaryViewType === 'bydealers') {
                    setSummaryTotal();
                  }
                }
              }
            >
              {summaryViewType === 'bydealers' ? 'Show collapsed' : 'Expand by dealers'}
            </Button>
          </Box>
          <ReactHighcharts className='mt-3' id='prd-reg' config={config}></ReactHighcharts>
          <SummaryTable key='summary-table' {...chartData} />
        </React.Fragment>
      </Box>
    );
  }
  if (summaryViewType === 'bydealers' || !isInternalUser) {
    return (
      <React.Fragment>
        {isInternalUser &&
          <Box m={1}>
            <Button
              key='set-variant'
              variant='outlined'
              onClick={
                () => {
                  if (summaryViewType === 'total') {
                    setSummaryDealers();
                  }
                  if (summaryViewType === 'bydealers') {
                    setSummaryTotal();
                  }
                }
              }
            >
              {summaryViewType === 'bydealers' ? 'Show collapsed' : 'Expand by dealers'}
            </Button>
          </Box>
        }
        {data.smr_dlr.data_list.map(item => {
          const chartData = { years: data.smr_dlr.years, data: item.data, name: item.name };
          const config = generateSummaryChartConfig(chartData);
          return (
            <Box key={item.id} className='mt-2' style={{ position: 'relative' }} display='flex' flexDirection='column'>
              {isInternalUser &&
                <Box display='flex' flexGrow='1' justifyContent='flex-end'>
                  <Button
                    size='small'
                    onClick={
                      () => {
                        selectVariant('registrationReport');
                        const values = [];
                        values.push({ value: item.name, label: item.name });
                        editFilters('dlds', values);
                      }
                    }
                  >
                    Open report
                  </Button>
                </Box>
              }
              <React.Fragment>
                <ReactHighcharts key={`chart-${item.id}`} className='mt-3' id={`prd-reg-${item.id}`} config={config}></ReactHighcharts>
                <SummaryTable key={`summary-table-${item.id}`} {...chartData} />
              </React.Fragment>
            </Box>
          );
        })}
      </React.Fragment>
    );

  }
  return null;
};

const summary = props => {
  const {
    data,
    selectVariant,
    editFilters,
    applyFiltersTrigger,
    setSummaryTotal,
    setSummaryDealers,
    settings,
    isInternalUser,
  } = props;
  const { summary } = data;

  return (
    <div id='summary-container' className='mt-1' style={{ width: '100%' }}>
      {summary &&
        <ChartRegSummary
          data={summary}
          selectVariant={selectVariant}
          editFilters={editFilters}
          applyFiltersTrigger={applyFiltersTrigger}
          setSummaryTotal={setSummaryTotal}
          setSummaryDealers={setSummaryDealers}
          settings={settings}
          isInternalUser={isInternalUser}
        />
      }
      {summary && isInternalUser &&
        <ChartCompanyClassif data={summary} />
      }
      {isInternalUser &&
        <Box className='mt-4 mb-4'>
          <ChartQtyMacClassif data={summary} />
        </Box>
      }
      {!isInternalUser &&
        <Box m={2} display='flex' justifyContent='space-around'>
          <Button
            variant='outlined'
            color='primary'
            size='small'
            onClick={
              () => {
                const url = 'https://mg-dealer.com/product-reg?report=1';
                window.location = url;
              }
            }
          >
            Открыть отчет
          </Button>
          <Button
            variant='outlined'
            color='primary'
            size='small'
            onClick={
              () => {
                const url = 'https://mg-dealer.com/product-reg?report=1&stat=-1';
                window.location = url;
              }
            }
          >
            Подтвердить остатки
          </Button>
        </Box>
      }
    </div>
  );
};

export default summary;

