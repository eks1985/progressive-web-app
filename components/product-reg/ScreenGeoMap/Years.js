import React from 'react';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    '& > *': {
      margin: theme.spacing(1),
    },
  },
}));

export default function Years(props) {
  const classes = useStyles();
  const { year, setYear } = props;

  return (
    <div className={classes.root}>
      <ButtonGroup size='small' color='primary' aria-label='outlined primary button group'>
        <Button
          variant={year === 'all' ? 'contained' : 'outlined'}
          onClick={
            () => {
              setYear('all');
            }
          }
        >
          All years
        </Button>
        <Button
          variant={year === '2018' ? 'contained' : 'outlined'}
          onClick={
            () => {
              setYear('2018');
            }
          }
        >
          2018
        </Button>
        <Button
          variant={year === '2019' ? 'contained' : 'outlined'}
          onClick={
            () => {
              setYear('2019');
            }
          }
        >
          2019
        </Button>
        <Button
          variant={year === '2020' ? 'contained' : 'outlined'}
          onClick={
            () => {
              setYear('2020');
            }
          }
        >
          2020
        </Button>
      </ButtonGroup>
    </div>
  );
}
