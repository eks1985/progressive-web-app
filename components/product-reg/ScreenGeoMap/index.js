/* eslint-disable react/jsx-one-expression-per-line */
import React, { useState, useEffect } from 'react';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

function useWindowSize() {
  const isClient = typeof window === 'object';

  function getSize() {
    return {
      width: isClient ? window.innerWidth : undefined,
      height: isClient ? window.innerHeight : undefined,
    };
  }

  const [windowSize, setWindowSize] = useState(getSize);

  useEffect(() => {
    if (!isClient) {
      return false;
    }

    function handleResize() {
      setWindowSize(getSize());
    }

    window.addEventListener('resize', handleResize);
    return () => window.removeEventListener('resize', handleResize);
  }, []); // Empty array ensures that effect is only run on mount and unmount

  return windowSize;
}

const ScreenGeoMap = props => {

  const size = useWindowSize();
  const { data } = props;
  if (!data) {
    return null;
  }
  const { category } = data;
  if (!category) {
    return null;
  }

  return (
    <div>
      <Typography style={{ textAlign: 'center' }}>
        All machines
      </Typography>
      <iframe
        key='all'
        title='Machines registration chart'
        style={{ background: '#FFFFFF', border: 'none', borderRadius: '2px', boxShadow: '0 2px 10px 0 rgba(70, 76, 79, .2)' }}
        width='100%'
        height={size.height}
        src='https://charts.mongodb.com/charts-mg-dealer-mumzu/embed/charts?id=c19f7427-9570-4c9e-9d19-3d5164143140&autorefresh=86400&theme=light'
      />
      {category.map((item, i) => {
        return (
          <Box mt={2} key={i}>
            <Typography style={{ textAlign: 'center' }}>
              {item}
            </Typography>
            <iframe
              key={item}
              title='Machines registration chart'
              style={{ background: '#FFFFFF', border: 'none', borderRadius: '2px', boxShadow: '0 2px 10px 0 rgba(70, 76, 79, .2)' }}
              width='100%'
              height={size.height}
              src={`https://charts.mongodb.com/charts-mg-dealer-mumzu/embed/charts?id=c19f7427-9570-4c9e-9d19-3d5164143140&autorefresh=86400&theme=light&filter={"cat":%20"${item}"%20}`}
            />
          </Box>
        );
      })}
    </div>
  );
};

export default ScreenGeoMap;
