import React from 'react';
import Box from '@material-ui/core/Box';
import FilterSelect from './FilterSelect';

const Filter = props => {
  const { category, years, geoMapFilters, setGeoMapFilter } = props;
  const { mapCategory, mapYear } = geoMapFilters;
  return (
    <Box display='flex' flexGrow={1}>
      <FilterSelect title='Category' name='mapCategory' values={category} value={mapCategory} onSelect={setGeoMapFilter} />
      <FilterSelect title='Years' name='mapYear' values={years} value={mapYear} onSelect={setGeoMapFilter} />
    </Box>
  );

};

export default Filter;
