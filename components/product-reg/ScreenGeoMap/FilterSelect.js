import React from 'react';
import Select from 'react-select';
import Box from '@material-ui/core/Box';

const prepareOptions = data => {
  return data.map(item => ({ value: item, label: item }));
};

const CustomSelect = props => {
  const { title, name, values, value, onSelect } = props;

  const options = prepareOptions(values);
  return (
    <Box p={2} width='100%' maxWidth='300px'>
      <Select
        placeholder={title}
        isClearable
        style={{ zIndex: '9999' }}
        fullWidth
        value={value}
        options={options}
        onChange={
          selected => {
            if (onSelect) {
              onSelect(name, selected);
              // document.getElementById('chartmaps').contentWindow.location.reload(true);
            } else {
              console.log('on select handler does not provided');
            }
          }
        }
      />
    </Box>
  );
};

CustomSelect.defaultProps = {
  name: '',
  data: [],
  value: {},
};

export default CustomSelect;
