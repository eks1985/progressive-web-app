import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const useStyles = makeStyles(theme => ({
  panel: {
    margin: '0px',
    // boxShadow: 'none',
  },
  panelDetails: {
    padding: '4px',
    borderTop: '1px solid #ccc',
    display: 'flex',
    flexDirection: 'column',
  },
  summary: {
    fontSize: theme.typography.pxToRem(14),
    padding: theme.spacing(1),
  },
}));

const Panel = props => {
  const { title, children } = props;
  const classes = useStyles();
  return (
    <Box mb={1}>
      <Accordion square className={classes.panel}>
        <AccordionSummary
          className={classes.summary}
          expandIcon={<ExpandMoreIcon />}
          aria-label='Expand'
          aria-controls='additional-actions1-content'
          id='additional-actions1-header'
        >
          {title}
        </AccordionSummary>
        <AccordionDetails className={classes.panelDetails}>
          {children}
        </AccordionDetails>
      </Accordion>
    </Box>
  );
};

export default Panel;
