/* eslint-disable no-unused-vars */
import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import ListItemText from '@material-ui/core/ListItemText';
import ListItem from '@material-ui/core/ListItem';
import List from '@material-ui/core/List';
import Panel from './Panel';
import PictureDialog from './PictureDialog';

const useStyles = makeStyles(theme => ({
  container: {
    maxWidth: '600px',
    margin: '0 auto',
  },
}));

const ScreenAgroatlas = props => {
  const { data } = props;
  const { atlCountry, districtsIndex, regionsIndex } = data;
  const [open, toggleDialog] = useState(false);
  const [currentItem, setCurrentItem] = useState(false);
  const toggleDialogHandler = item => {
    toggleDialog(!open);
    setCurrentItem(item);
  };
  // console.log('atlas data', data);
  if (!data) {
    return null;
  }
  const classes = useStyles();
  return (
    <Box className={classes.container}>
      <PictureDialog open={open} toggleDialog={toggleDialogHandler} currentItem={currentItem} />
      <Panel title='Country stat'>
        <List dense>
          {atlCountry.map((row, i) => {
            const { item } = row;
            return (
              <ListItem
                key={`${item}-${i}`}
                button
                onClick={() => {
                  toggleDialogHandler(row);
                }}
              >
                <ListItemText
                  primary={item}
                />
              </ListItem>
            );
          })}
        </List>
      </Panel>
      {Object.keys(districtsIndex).map(ind => {
        const district = districtsIndex[ind];
        const regions = regionsIndex[ind];
        const { dnme } = district[0];
        return (
          <Panel title={dnme} key={dnme}>
            <Panel title='Посевные площади по регионам'>
              <List dense>
                {regions.map((row, i) => {
                  const { rgnn } = row;
                  return (
                    <ListItem
                      key={`${rgnn}-${i}`}
                      button
                      onClick={() => {
                        toggleDialogHandler(row);
                      }}
                    >
                      <ListItemText
                        primary={rgnn}
                      />
                    </ListItem>
                  );
                })}
              </List>
            </Panel>
            <List dense>
              {district.map((row, i) => {
                const { item } = row;
                return (
                  <ListItem
                    key={`${item}-${i}`}
                    button
                    onClick={() => {
                      toggleDialogHandler(row);
                    }}
                  >
                    <ListItemText
                      primary={item}
                    />
                  </ListItem>
                );
              })}
            </List>
          </Panel>
        );
      })}
    </Box>
  );
};

export default ScreenAgroatlas;
