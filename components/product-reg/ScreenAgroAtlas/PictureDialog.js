/* eslint-disable prefer-template */
/* eslint-disable func-names */
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import Picture from 'components/Picture';

const useStyles = makeStyles(theme => ({
  appBar: {
    position: 'relative',
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1,
  },
}));

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction='up' ref={ref} {...props} />;
});

const FullScreenDialog = props => {
  const { toggleDialog, open, currentItem } = props;
  const { item, path, rgnn, dnme } = currentItem;
  const classes = useStyles();

  return (
    <Dialog fullScreen open={open} onClose={toggleDialog} TransitionComponent={Transition}>
      <AppBar className={classes.appBar}>
        <Toolbar>
          <IconButton edge='start' color='inherit' onClick={toggleDialog} aria-label='close'>
            <CloseIcon />
          </IconButton>
          <Typography variant='subtitle2' className={classes.title}>
            {`${item} ${rgnn ? ' - ' + rgnn : ''} ${dnme ? ' - ' + dnme : ''}`}
          </Typography>
        </Toolbar>
      </AppBar>
      <Picture
        url={path}
        width='initial'
        height='100%'
        backgroundSize='contain'
      />
    </Dialog>
  );
};

FullScreenDialog.defaultProps = {
  item: 'Screen title',
};

export default FullScreenDialog;
