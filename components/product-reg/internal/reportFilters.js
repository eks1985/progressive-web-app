
const filterByArray = (data, filter) => {
  const filterKeys = Object.keys(filter);
  const res = [];
  data.forEach(item => {
    let test = true;
    filterKeys.forEach(key => {
      if (test) {
        test = filter[key].includes(item[key]);
      }
    });
    if (test) {
      res.push(item);
    }
  });
  return res;
};


export const filterData = (data, filters) => {
  console.log('filters', filters);
  if (!filters) {
    return data;
  }
  const res = {};
  filters.forEach(item => {
    const { value } = item;
    if (value !== null && value.length !== 0) {
      res[item.source] = value.map(item => item.value);
    }
  });
  if (data === undefined) {
    return data;
  }
  return filterByArray(data, res);
};

export const getStatLabel = value => {
  switch (value) {
    case -2:
      return 'Unknown';
    case -1:
      return 'Unregistered';
    case 0:
      return 'Buffer';
    case 1:
      return 'Registered';
    default:
      return 'Unknown';
  }
};

export const getParsedFilters = data => {
  // console.log('data', data);
  if (data.stat) {
    data.stat = parseInt(data.stat, 10);
  }
  const keys = Object.keys(data);
  const bank = {};
  const values = [];
  keys.forEach(key => {
    if (key !== 'report') {
      const filterValuesRaw = data[key];
      let filterValuesRes;
      if (Array.isArray(filterValuesRaw)) {
        filterValuesRes = filterValuesRaw;
      } else {
        filterValuesRes = [filterValuesRaw];
      }
      bank[key] = filterValuesRes;
      values.push({ name: key, values: filterValuesRes.map(item => ({ label: key === 'stat' ? getStatLabel(item) : item, value: item })) });
    }
  });
  return { bank, values };
};
