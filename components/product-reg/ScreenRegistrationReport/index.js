/* eslint-disable react/jsx-one-expression-per-line */
import React from 'react';
import Record from './Record';
import Pagination from './Pagination';

const ScreenRegistrationReport = props => {
  const { data, settings, nextPortion, selectVariant, editFilters, applyFiltersTrigger, email } = props;
  if (!data) {
    return null;
  }
  const { allItems, itemsQty } = data;
  if (!allItems) {
    return null;
  }
  return (
    <div>
      <Pagination nextPortion={nextPortion} itemsQty={itemsQty} settings={settings} />
      {allItems.map(item => {
        return (
          <Record
            record={item}
            key={item._id}
            selectVariant={selectVariant}
            editFilters={editFilters}
            applyFiltersTrigger={applyFiltersTrigger}
            email={email}
          />
        );
      })}
      <Pagination nextPortion={nextPortion} itemsQty={itemsQty} settings={settings} />
    </div>
  );
};

export default ScreenRegistrationReport;
