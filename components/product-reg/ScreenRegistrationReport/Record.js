/* eslint-disable no-mixed-operators */
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import red from '@material-ui/core/colors/red';
import green from '@material-ui/core/colors/green';
import yellow from '@material-ui/core/colors/yellow';
import grey from '@material-ui/core/colors/grey';
import indigo from '@material-ui/core/colors/blue';
import Link from '@material-ui/core/Link';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';

import StockConfirm from './StockConfirm';

const getCircleColorByStat = stat => {
  switch (stat) {
    case -2:
      return grey[500];
    case -1:
      return '#E1001A';
    case 0:
      return yellow[500];
    case 1:
      return green[500];
    default:
      return grey[500];
  }
};

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    marginTop: '1px',
    marginBottom: theme.spacing(2),
    borderLeft: props => `3px solid ${props.color}`,
  },
  list: {
    width: '100%',
    backgroundColor: theme.palette.background.paper,
    fontSize: theme.typography.pxToRem(10),
  },
  heading: {
    fontSize: theme.typography.pxToRem(12),
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(12),
    color: theme.palette.text.primary,
  },
  secondaryHeadingClient: {
    fontSize: theme.typography.pxToRem(12),
    color: '#2962ff',
    // fontWeight: 'bold',
  },
  pad: {
    marginRight: 20,
  },
  listText: {
    margin: 0,
    fontSize: theme.typography.pxToRem(12),
  },
  listItemText: {
    fontSize: theme.typography.pxToRem(12),
    color: theme.palette.text.primary,
  },
  listItemTitle: {
    fontSize: theme.typography.pxToRem(12),
    color: theme.palette.text.secondary,
  },
  model: {
    marginLeft: theme.spacing(1),
    color: red[500],
  },
  pandelSummary: {
    paddingLeft: '8px',
    paddingRight: '8px',
    margin: '0px',
    position: 'relative',
  },
  regId: {
    position: 'absolute',
    top: '3px',
    right: '10px',
    fontSize: '10px',
    color: '#2962ff',
    // fontWeight: 'bold',
  },
  confirm: {
    position: 'absolute',
    bottom: '3px',
    right: '10px',
    fontSize: '10px',
    color: indigo[500],
    fontWeight: 'bold',
  },
}));

const Record = props => {
  const { record, selectVariant, editFilters, email } = props;
  const color = getCircleColorByStat(record.stat);
  const classes = useStyles({ color });
  return (
    <div className={classes.root}>
      <Accordion square style={{ margin: '0px' }}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-label='Expand'
          aria-controls='additional-actions1-content'
          id='additional-actions1-header'
          className={classes.pandelSummary}
        >
          <div className={classes.regId}>
            {record.rgid ? `${record.year} - ${record.rgid}` : `${record.year}`}
          </div>
          <div>
            <Box display='flex'>
              <Typography variant='body2' className={classes.secondaryHeading}>{`${record.pcde} | ${record.psn}`}</Typography>
              <Typography variant='body2' className={`${classes.secondaryHeading} ${classes.model}`}>{record.mdes}</Typography>
            </Box>
            <Typography variant='caption' className={classes.secondaryHeadingClient}>{`${record.dlid} ${record.dlds}`}</Typography>
          </div>
        </AccordionSummary>
        <AccordionDetails style={{ padding: '4px' }} display='flex'>
          <List className={classes.list} dense>
            <ListItem>
              <Box display='flex' flexDirection='column'>
                <Typography className={classes.listItemText}>{record.pdsc}</Typography>
                <Typography className={classes.listItemTitle}>Description</Typography>
              </Box>
            </ListItem>
            {record.reg &&
              <React.Fragment>
                <ListItem>
                  <Box display='flex' flexDirection='column'>
                    <Typography className={classes.listItemText}>
                      <Link
                        href='#'
                        onClick={
                          () => {
                            editFilters('clear');
                            const values = [];
                            values.push({ value: record.cdes1, label: record.cdes1 });
                            editFilters('dscr', values);
                            selectVariant('finalCustomers');
                          }
                        }
                      >
                        {record.cdes1}
                      </Link>
                    </Typography>
                    <Typography className={classes.listItemTitle}>Client</Typography>
                  </Box>
                </ListItem>
                <ListItem>
                  <Box display='flex' flexDirection='column'>
                    <Typography className={classes.listItemText}>{`${record.crgnc} - ${record.crgnr}`}</Typography>
                    <Typography className={classes.listItemTitle}>Region</Typography>
                  </Box>
                </ListItem>
                <ListItem>
                  <Box display='flex' flexDirection='column'>
                    <Typography className={classes.listItemText}>{`${record.czip || '<Индекс>'} - ${record.ccit || '<Город>'} - ${record.card || '<Улица>'}`}</Typography>
                    <Typography className={classes.listItemTitle}>Address</Typography>
                  </Box>
                </ListItem>
                <ListItem>
                  <Box display='flex' flexDirection='column'>
                    <Typography className={classes.listItemText}>{`${record.cfnm || '<Имя>'} - ${record.csnm || '<Фамилия>'}`}</Typography>
                    <Typography className={classes.listItemTitle}>Person</Typography>
                  </Box>
                </ListItem>
                <ListItem>
                  <Box display='flex' flexDirection='column'>
                    <Typography className={classes.listItemText}>{`${record.ctel || '<Тел>'} - ${record.ceml || '<Email>'}`}</Typography>
                    <Typography className={classes.listItemTitle}>Contacts</Typography>
                  </Box>
                </ListItem>
                <ListItem>
                  <Box display='flex' flexDirection='column'>
                    <Typography className={classes.listItemText}>{`${record.ttyp || '<Тип>'} - ${record.tmod || '<Модель>'} - ${record.tpow || '<Мощность>'}`}</Typography>
                    <Typography className={classes.listItemTitle}>Tractor</Typography>
                  </Box>
                </ListItem>
              </React.Fragment>
            }
            <ListItem>
              <Box display='flex' flexDirection='column'>
                <Typography className={classes.listItemText}>{`${record.invd || '<Дата инв>'} - ${record.shpd || '<Дата отгр>'} - ${record.nerp || '<Док>'} - ${record.dest || '<Выгрузка>'}`}</Typography>
                <Typography className={classes.listItemTitle}>Invoice | Shpipment | Doc nr | Dest </Typography>
              </Box>
            </ListItem>
            {record.reg &&
              <React.Fragment>
                <ListItem>
                  <Box display='flex' flexDirection='column'>
                    <Typography className={classes.listItemText}>{`${record.regd || '<Дата рег>'} - ${record.wexp || '<Гар срок>'} - ${record.dupd || '<Дата обнов>'}`}</Typography>
                    <Typography className={classes.listItemTitle}>Reg | War exp | Reg upd </Typography>
                  </Box>
                </ListItem>
                <ListItem>
                  <Box display='flex' flexDirection='column'>
                    <Typography className={classes.listItemText}>{record.rusr}</Typography>
                    <Typography className={classes.listItemTitle}>@work user</Typography>
                  </Box>
                </ListItem>
              </React.Fragment>
            }
            {((record.stat === -1) || (record.stat === 0)) &&
              <StockConfirm
                record={record}
                cnfdate={record.cnfdate}
                cnfhist={record.cnfhist}
                email={email}
              />
            }
          </List>
          {record.psn}
        </AccordionDetails>
      </Accordion>
    </div>
  );
};

export default Record;
