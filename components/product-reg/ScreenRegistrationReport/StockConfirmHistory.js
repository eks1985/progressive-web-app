import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/CancelOutlined';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import { blue } from '@material-ui/core/colors';
import Button from '@material-ui/core/Button';
import Loading from 'components/Loading';

const useStyles = makeStyles({
  avatar: {
    backgroundColor: blue[100],
    color: blue[600],
  },
});

function SimpleDialog(props) {

  const classes = useStyles();

  const { open, setOpen, cnfhist, progress, setToDelete } = props;

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <Dialog onClose={handleClose} aria-labelledby='simple-dialog-title' open={open}>
      <DialogTitle id='simple-dialog-title'>История подтверждений остатков</DialogTitle>
      {progress &&
        <Loading />
      }
      <List>
        {cnfhist && cnfhist.length > 0 && cnfhist.map((item, ind) => (
          <ListItem
            key={item.d}
          >
            <ListItemText primary={item.d} secondary={item.u} />
            {ind === cnfhist.length - 1 &&
              <ListItemSecondaryAction>
                <IconButton
                  edge='end'
                  aria-label='delete'
                  onClick={
                    () => {
                      setToDelete(item.d);
                      // setOpen(false);
                    }
                  }
                >
                  <DeleteIcon />
                </IconButton>
              </ListItemSecondaryAction>
            }
          </ListItem>
        ))}
      </List>
      <DialogActions>
        <Button onClick={handleClose} color='primary'>
          Закрыть
        </Button>
      </DialogActions>
    </Dialog>
  );
}

SimpleDialog.propTypes = {
  open: PropTypes.bool.isRequired,
};


export default SimpleDialog;
