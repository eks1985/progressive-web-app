/* eslint-disable react/jsx-one-expression-per-line */
import React from 'react';
import Summary from './Charts/Summary';

const ScreenSummary = props => {
  const { data, selectVariant, editFilters, applyFiltersTrigger, setSummaryTotal, setSummaryDealers, settings, isInternalUser } = props;
  if (!data) {
    return null;
  }
  return (
    <Summary
      data={data}
      key='summary-chart'
      selectVariant={selectVariant}
      editFilters={editFilters}
      applyFiltersTrigger={applyFiltersTrigger}
      setSummaryTotal={setSummaryTotal}
      setSummaryDealers={setSummaryDealers}
      settings={settings}
      isInternalUser={isInternalUser}
    />
  );
};

export default ScreenSummary;
