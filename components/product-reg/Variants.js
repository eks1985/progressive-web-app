/* eslint-disable func-names */
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import ListItemText from '@material-ui/core/ListItemText';
import ListItem from '@material-ui/core/ListItem';
import List from '@material-ui/core/List';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import Link from '@material-ui/core/Link';
import Box from '@material-ui/core/Box';
import { variants } from './internal/reportVariants';
import SendToEmail from './SendToEmail';

const useStyles = makeStyles(theme => ({
  appBar: {
    position: 'relative',
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1,
  },
}));

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction='up' ref={ref} {...props} />;
});

const variantsDialog = props => {
  const { toggleVariant, selectVariant, settings, editFilters } = props;
  const { variantCurrent, variantOpen } = settings;
  const variantTitle = variants[variantCurrent].title;
  const classes = useStyles();
  const handleSelect = variantKey => {
    selectVariant(variantKey);
    toggleVariant();
  };

  return (
    <div>
      <Box display='flex' p={1} justifyContent='space-between' alignItems='center'>
        <Box display='flex'>
          <Box ml={1} mr={1} display='flex' alignItems='center'>
            <Link href='#' onClick={toggleVariant}>
              {variantTitle}
            </Link>
          </Box>
          <Button variant='outlined' size='small' color='primary' onClick={toggleVariant}>
            Change
          </Button>
        </Box>
        <SendToEmail />
      </Box>
      <Dialog fullScreen open={variantOpen} onClose={toggleVariant} TransitionComponent={Transition}>
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton edge='start' color='inherit' onClick={toggleVariant} aria-label='close'>
              <CloseIcon />
            </IconButton>
            <Typography variant='h6' className={classes.title}>
              Select report type
            </Typography>
          </Toolbar>
        </AppBar>
        <List style={{ paddingBottom: '60px' }}>
          {Object.keys(variants).map(variantKey => {
            const variant = variants[variantKey];
            if (variant.visible === false) {
              return null;
            }
            const { title, descr } = variant;
            return (
              <ListItem
                key={variantKey}
                button
                onClick={() => {
                    handleSelect(variantKey);
                    editFilters('clear');
                  }
                }
              >
                <ListItemText
                  primary={title}
                  secondary={descr}
                />
              </ListItem>
            );
          })}
        </List>
      </Dialog>
    </div>
  );
};

export default variantsDialog;