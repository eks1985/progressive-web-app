import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import IconEmail from '@material-ui/icons/Email';

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
}));

const SendToEmail = props => {
  // const classes = useStyles();
  // const { nextPortion, itemsQty, settings } = props;
  return (
    <IconButton
      color='primary'
      aria-label='email'
      onClick={
        () => {
          // sendToEmail();
        }
      }
    >
      <IconEmail />
    </IconButton>
  );
};

export default SendToEmail;
