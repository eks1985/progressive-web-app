import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useStore } from 'lib/store';
import { useRouter } from 'next/router';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import FormLabel from '@material-ui/core/FormLabel';
import Switch from '@material-ui/core/Switch';
import Select from 'components/select';
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import date from 'date-and-time';

const useStyles = makeStyles(theme => ({
  label: {
     fontSize: '14px', 
  },
  details: {
    display: 'flex',
    flexDirection: 'column',
    padding: theme.spacing(1),
  },
}));

const compName = props => {

  const { user } = props;
  console.log(user);

  const classes = useStyles();

  const langList = ['Russian', 'Ukranian', 'English', 'Italian'];
  const langOpt = langList.map(lang => ({ label: lang, value: lang }));
  const { settings } = user;
  const { hideDPrice, autoupdateOffline, lastUpdate, lang } = settings;

  console.log('lang', lang);

  return (
    <Box mt={3} p={2} margin='0 auto' width='100%'>
      <Box>
        <Typography variant='body2'>
          {user.customerCurOpt.value}
        </Typography>
      </Box>
      <FormLabel className={classes.label}>
        Контрагент код
      </FormLabel>
      <Box mt={2}>
        <Typography variant='body2'>
          {user.customerCurOpt.label}
        </Typography>
      </Box>
      <FormLabel className={classes.label}>
        Контрагент наименование
      </FormLabel>
      <Box mt={2}>
        <Select
          options={langOpt}
          option={{ value: lang, label: lang }}
          onSelectHandler={
            value => {
              console.log('value', value);
            }
          }
        />
        <FormLabel className={classes.label}>
          Язык
        </FormLabel>
      </Box>
      <Box mt={1}>
        <Switch
          color='primary'
          checked={!hideDPrice}
          onChange={
            e => {
              // applySettings({ hideDPrice: !e.target.checked });
              // updateSettings();
            }
          }
          value='checkedB'
        />
        <FormLabel>Отображать дилерские цены</FormLabel>
      </Box>
      <Box textAlign='center' mt={2}>
        <Accordion defaultExpanded>
          <AccordionSummary expandIcon={<ExpandMoreIcon />}>
            <Typography>Режим оффлайн</Typography>
          </AccordionSummary>
          <AccordionDetails className={classes.details}>
            <Box>
              <Switch
                checked={autoupdateOffline}
                onChange={
                  () => {
                    // applySettings({ autoupdateOffline: !autoupdateOffline });
                    // updateSettings();
                  }
                }
                value='checkedB'
                color='secondary'
              />
              <FormLabel>Автоматически обновлять оффлайн данные каждые 3 дня</FormLabel>
            </Box>
            {lastUpdate &&
              <Box>
                <FormLabel>
                  <span>
                    Дата последнего обновления оффлайн данных
                  </span>
                  <span className='ml-2'>
                    {date.format(new Date(lastUpdate), 'DD.MM.YYYY HH:mm:ss')}
                  </span>
                </FormLabel>
              </Box>
            }
            <Box mt={2} textAlign='center'>
              <Button
                variant='outlined'
                onClick={
                  () => {
                    const d = new Date();
                    // loadDataForOffline();
                    // applySettings({ autoupdateOffline: true, lastUpdate: d });
                    // updateSettings();
                  }
                }
              >
                Загрузить в offline
              </Button>
            </Box>
          </AccordionDetails>
        </Accordion>
      </Box>

    </Box>
  );

};

export default compName;
