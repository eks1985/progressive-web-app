import React from 'react';

import Circle from './circle';
import Wrapper from './wrapper';

const loadingIndicator = ({ big }) => {
  return (
    <Wrapper big={big}>
      <Circle />
    </Wrapper>
  );
};

export default loadingIndicator;
