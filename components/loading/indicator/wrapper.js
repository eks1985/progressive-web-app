import styled from 'styled-components';

const Wrapper = styled.div`
  width: ${props => props.big ? '48px' : '20px'};
  height: ${props => props.big ? '48px' : '20px'};
  position: relative;
`;

Wrapper.defaultProps = {
  big: true,
};

export default Wrapper;
