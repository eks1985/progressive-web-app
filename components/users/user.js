import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Checkbox from '@material-ui/core/Checkbox';
import FormLabel from '@material-ui/core/FormLabel';
import Box from '@material-ui/core/Box';
import IconButton from '@material-ui/core/IconButton';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import { Typography } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  root: {
    borderTop: '1px dashed #ccc',
    position: 'relative',
  },
  title: {
    fontSize: '8px',
    // fontWeight: 'bold',
    textTransform: 'uppercase',
    color: theme.palette.primary.main,
  },
  value: {
    fontSize: '14px',
    // fontWeight: props => props.name ? 'bold' : 'normal',
    textTransform: props => props.name ? 'uppercase' : 'initial',
    lineHeight: '1.1'
  },
  index: {
    // position: 'absolute', right: '5px', top: '5px'
  },
  
}));

const Title = ({ title }) => {
  const classes = useStyles();
  return (
    <Box mt={1}>
      <Typography variant='caption' className={classes.title}>
        {title}
      </Typography>
    </Box>
  );
};

const Value = ({ value, name }) => {
  const classes = useStyles({ name });
  return (
    <Box className={classes.value}>
      <Typography variant='body2'>
        {value}
      </Typography>
    </Box>
  );
};

const Index = ({ index }) => {
  const classes = useStyles();
  return (
    <div className={classes.index}>
      {`#${index + 1}`}
    </div>
  );
};

const getNamePrepared = name => {
  switch (name) {
    case 'price-client':
      return 'Цены клиент';
    case 'price-dealer':
      return 'Цены дилер';
    case 'stock-dealer':
      return 'Склад дилера';
    case 'orders-list':
      return 'Список заказов';
    case 'orders-send':
      return 'Отправка заказов';
    case 'open-items':
      return 'Дебиторка';
    case 'warranty-claims':
      return 'Гарантия';
    case 'product-registration':
      return 'Регистрация машин';
    default:
      return name;
  }
};

const AccessItem = props => {
  const { name } = props;
  const namePrepared = getNamePrepared(name);
  return (
    <div>
      <Checkbox
        checked
        color='primary'
      />
      <FormLabel>{namePrepared}</FormLabel>
    </div>
  );
};

const User = props => {

  const { user, index } = props;
  const classes = useStyles();
  const [open, setOpen] = useState(false);

  return (
    <Box pt={1} className={classes.root}>
      <Box display='flex' alignItems='center'>
        <Box display='flex' justifyContent='space-between' alignItems='center'>
          <IconButton
            onClick={
              () => {
                setOpen(!open);
              }
            }
          >
            {open && <ExpandLessIcon />}
            {!open && <ExpandMoreIcon />}
          </IconButton>
          <Index index={index} />
          <Box ml={1}>
            <Value value={user.name || '<Не задано имя>'} name />
          </Box>
        </Box>
      </Box>
      {open &&
        <Box with='100%'>
          {user.manager &&
            <Title title='Менеджер' />
          }
          {user.manager &&
            <Value value={user.manager} />
          }
          {user.email &&
            <Title title='email' />
          }
          {user.email &&
            <Value value={user.email} />
          }
          {user.internalRole &&
            <Title title='Должность' />
          }
          {user.internalRole &&
            <Value value={user.internalRole} />
          }
          {user.accessLevel &&
            <Title title='Уровень доступа' />
          }
          {user.accessLevel &&
            <Value value={user.accessLevel} />
          }
          {user.area &&
            <Title title='Регион' />
          }
          {user.area &&
            <Value value={user.area} />
          }
          {user.tel &&
            <Title title='Телефон' />
          }
          {user.tel &&
            <Value value={user.tel} />
          }
          {user.id &&
            <Title title='Id' />
          }
          {user.id &&
            <Value value={user.id} />
          }
          {user.roles &&
            <Box mt={2} display='flex' flexWrap='wrap'>
              {user.roles.map((role, i) => {
                return <AccessItem key={`${role}${i}`} name={role} />;
              })}
            </Box>
          }
        </Box>
      }
    </Box>
  );
};

export default User;
