import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import User from './user';

const useStyles = makeStyles(theme => ({
  accordion: {
    margin: '0px',
  },
  header: {
    paddingLeft: '8px',
    paddingRight: '8px',
    margin: '0px',
    position: 'relative',
  },
  details: {
    display: 'flex',
    flexDirection: 'column',
    padding: theme.spacing(1),
    fontSize: theme.typography.pxToRem(14),
  },
}));

const Dealer = props => {

  const { dealer, users } = props;

  const [expanded, setExpanded] = React.useState(false);

  const handleChange = panel => (_, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  const classes = useStyles();

  return (
    <Accordion square className={classes.accordion} expanded={expanded === 'panel1'} onChange={handleChange('panel1')}>
      <AccordionSummary
        expandIcon={<ExpandMoreIcon />}
        aria-label='Expand'
        aria-controls='additional-actions1-content'
        id='additional-actions1-header'
        className={classes.header}
      >
        <Box display='flex' justifyContent='space-between' width='100%'>
          <Typography variant='subtitle1'>
            {dealer}
          </Typography>
          <Typography variant='subtitle1'>
            {users.length}
          </Typography>
        </Box>
      </AccordionSummary>
      {expanded &&
        <AccordionDetails className={classes.details}>
          {users.map((user, i) => {
            return <User key={`${dealer}-${i}`} user={user} index={i} />;   
          })}
        </AccordionDetails>
      }
    </Accordion>
  );

};

const users = props => {

  const { items } = props;
  const { dealers, users } = items;

  return (
    <Box margin='0 auto' width='100%'>
      <Box m={1}>
        <Typography variant='subtitle1'>
          {`Всего пользователей: ${users.length}`}
        </Typography>
      </Box>
      {dealers.map(dealer => {
        const dealerUsers = users.filter(user => user.customerDescr === dealer);
        return <Dealer key={dealer} dealer={dealer} users={dealerUsers} />;
      })}
    </Box>
  );

};

export default users;
