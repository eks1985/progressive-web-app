import React from 'react';
import Select from 'react-select';

const CustomSelect = props => {
  const { label, items, value, mult, onSelect } = props;
  return (
    <Select
      placeholder={label}
      isClearable
      isMulti={mult}
      style={{ zIndex: '9999' }}
      fullWidth
      value={value}
      options={items}
      onChange={
        selected => {
          if (onSelect) {
            onSelect(selected);
          } else {
            console.log('on select handler does not provided');
          }
        }
      }
    />
  );
};

export default CustomSelect;
