
export const calcFiltersLabel = filters => {
  let res = '';
  for (const item of filters) {
    const { value } = item;
    for (const valueElem of value) {
      res += valueElem.label + ', ';
    }
  }
  return res.slice(0, -2);
};