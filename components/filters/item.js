import React from 'react';
import Box from '@material-ui/core/Box';
import Select from './select';

const Filter = props => { 

  const { index, filtersEdit, ...other } = props;
  
  return (
    <Box mt={2}>
      <Select
        {...other}
        onSelect={
          selected => {
            filtersEdit({ index, value: selected });
          }
        }
      /> 
    </Box>
  );
  
};

export default Filter;
