import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionActions from '@material-ui/core/AccordionActions';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/FilterList';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import Box from '@material-ui/core/Box';
import Filter from './item';
import { calcFiltersLabel } from './internals';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary,
  },
  details: {
    alignItems: 'center',
  },
  actions: {
    margin: theme.spacing(1),
  },
  pad: {
    marginRight: 20,
  },
}));

const filters = props => {

  const classes = useStyles();

  const [expanded, setExpanded] = useState(false);

  const handleChange = (panel) => (_, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  const { filters, filtersEdit, filtersApply, filtersReset } = props;

  const filtersLabel = calcFiltersLabel(filters);

  return (
    <div className={classes.root}>
      <Accordion square expanded={expanded === 'filters-accordion'} onChange={handleChange('filters-accordion')}>
        <AccordionSummary
          aria-controls='filters-accordion-content'
          id='filters-accordion-header'
        >
          <div className={classes.pad}>
            <ExpandMoreIcon />
          </div>
          <Typography
            className={classes.secondaryHeading}>
            {filtersLabel}
          </Typography>
        </AccordionSummary>
        <AccordionDetails className={classes.details}>
          {filters &&
            <Box display='flex' flexDirection='column' width='100%'>
              {filters.map((filter, index) => {
                return <Filter key={filter.name} index={index} {...filter} filtersEdit={filtersEdit} />;
              })}
            </Box>
          }
        </AccordionDetails>
        <Divider />
        <AccordionActions className={classes.actions}>
          <Button
            size='small'
            onClick={
              () => {
                filtersReset();
                setExpanded(false);
              }
            }
          >
            Очистить
          </Button>
          <Button
            size='small'
            color='primary'
            variant='contained'
            onClick={
              () => {
                filtersApply();
                setExpanded(false);
              }
            }
          >
            Применить
          </Button>
        </AccordionActions>
      </Accordion>
    </div>
  );
};

filters.propTypes = {
  filtersEdit: PropTypes.func.isRequired,
  filtersReset: PropTypes.func.isRequired,
  filtersApply: PropTypes.func.isRequired,
  filters: PropTypes.array.isRequired,
};

export default filters;
