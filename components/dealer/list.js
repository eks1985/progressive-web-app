import React, { useEffect, useState } from 'react';
import Box from '@material-ui/core/Box';
import Filters from 'components/filters';
import Content from './content';

const filter = (items, filters) => {
  const callback = item => {
    // let res = true;
    for (const filter of filters) {
      if (filter.value.length > 0) {
        const arr = filter.value.map(fvalue => fvalue.value);
        switch (filter.name) {
          case 'customer':
            if (!arr.includes(item.dcr)) {
              return false;
            } 
            // return arr.includes(item.dcr);
            break;
          case 'region':
            if (!arr.includes(item.rgnD)) {
              return false;
            } 
            // return arr.includes(item.rgnD);
            break;
          case 'manager':
            if (!arr.includes(item.manName)) {
              return false;
            } 
            // return arr.includes(item.manName);
            break;
          default:
            return true;
        }
      }
    }
    return true;
  };
  return items.filter(callback);
  // return items;
};

const prepareFilters = items => {

  const filters = [];
  const filterDealer = {
    name: 'customer',
    label: 'Дилер',
    items: items.map(item => ({ value: item.dcr, label: item.dcr })),
    value: [],
    mult: true,
  };
  const regions = items.reduce((res, item) => {
    if (!res[item.rgnD]) {
      res[item.rgnD] = true;
    }
    return res;
  }, {});
  const filterRegion = {
    name: 'region',
    label: 'Регион',
    items: Object.keys(regions).sort().map(key => ({ value: key, label: key })),
    value: [],
    mult: true,
  };
  const managers = items.reduce((res, item) => {
    if (!res[item.manName]) {
      res[item.manName] = true;
    }
    return res;
  }, {});
  const filterManager = {
    name: 'manager',
    label: 'Менеджер',
    items: Object.keys(managers).sort().map(key => ({ value: key, label: key })),
    value: [],
    mult: true,
  };

  filters.push(filterDealer);
  filters.push(filterRegion);
  filters.push(filterManager);
  return filters;

};

const list = props => {

  const [filters, setFilters] = useState(prepareFilters(props.dealers));
  const [itemsFiltered, setItemsFiltered] = useState(props.dealers);

  useEffect(() => {
    setItemsFiltered(filter(props.dealers, filters));
  }, [props.items]);

  const filtersEdit = action => {
    const {  index, value } = action;
    const copy = [...filters];
    copy[index].value = value;
    setFilters(copy);
  };

  const filtersApply = () => {
    setItemsFiltered(filter(props.dealers, filters));
  };

  const filtersReset = () => {
    const copy = [...filters];
    for (const filter of copy) {
      filter.value = [];
    }
    setFilters(copy);
  };

  const actions={
    filtersEdit,
    filtersApply,
    filtersReset, 
  }

  // console.log('itemsFiltered', itemsFiltered);

  return (
    <Box margin='0 auto' width='100%' maxWidth={600}>
      <Box>
        <Filters filters={filters} {...actions} />
      </Box>
      {/* <Box mt={2}>
        {itemsFiltered.map((order, i) => {
          return <Item order={order} key={i} mutate={mutate} />
        })}
      </Box> */}
      <Content items={itemsFiltered} />
    </Box>
  );

};

export default list;
