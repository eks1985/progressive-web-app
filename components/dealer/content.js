import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import moment from 'moment';
import IconPlace from '@material-ui/icons/Place';
import IconDirector from '@material-ui/icons/PersonPin';
import IconCall from '@material-ui/icons/Call';
import IconManager from '@material-ui/icons/Person';
import IconMoneyOff from '@material-ui/icons/MoneyOff';
import IconTurnover from '@material-ui/icons/MonetizationOn';
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    fontSize: theme.typography.pxToRem(13),
    padding: '0px 6px',
  },
  head: {
    fontSize: theme.typography.pxToRem(14),
    padding: '6px',
    paddingRight: '50px',
  },
  details: {
    display: 'flex',
    flexDirection: 'column',
    padding: theme.spacing(1),
  },
  icon: {
    color: '#777',
  },
}));

const Item = props => {

  const classes = useStyles();
  const currency = '';
  const year0 = moment().year();
  const year1 = year0 - 1;
  const year2 = year0 - 2;

  const [expanded, setExpanded] = useState(false);

  const handleChange = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  const { item } = props;

  const { id, dcr, dcrOf, dstr, rgnC, rgnD, aCt, aSt, aP, aE, discM, discS, aDir, manName, turn0, turn1, turn2 } = item;
  return (
    <Accordion square key={id} className={classes.root} expanded={expanded === id} onChange={handleChange(id)}>
      <AccordionSummary expandIcon={<ExpandMoreIcon />} className={classes.head}>
        {`${id} - ${dcr}`}
      </AccordionSummary>
      {expanded === id &&
        <AccordionDetails className={classes.details}>
          <Box bgcolor='#eee' borderRadius='4px' height={32} display='flex' justifyContent='center' alignItems='center' >
            <Typography variant='subtitle2'>
              {dcrOf}
            </Typography>
          </Box>
          {manName &&
            [
              <Box mt={2} mb={1} key='0' display='flex' alignItems='center'>
                <IconManager className={classes.icon} />
                <Box ml={1}>
                  <span>Основной менеджер:</span>
                  <Box ml={1}>
                    <span>{manName}</span>
                  </Box>
                </Box>
              </Box>,
              <Divider key='1' />,
            ]
          }
          <Box mt={2} mb={1} display='flex' alignItems='center'>
            <IconPlace className={classes.icon} />
            <Box ml={1}>
              <div>
                {dstr}
              </div>
              <div>
                {`${rgnC} - ${rgnD}`}
              </div>
              <div>
                {`${aCt} - ${aSt}`}
              </div>
            </Box>
          </Box>
          <Divider />
          <Box  mt={2} mb={1} display='flex' alignItems='center'>
            <IconCall className={classes.icon} />
            <Box ml={1}>
              <div>
                {aP}
              </div>
              <div>
                {aE}
              </div>
            </Box>
          </Box>
          <Divider />
          {aDir &&
            <Box mt={2} mb={1} display='flex' alignItems='center'>
              <IconDirector className={classes.icon} />
              <Box ml={1}>
                {aDir}
              </Box>
            </Box>
          }
          <Divider />
          <Box mt={2} mb={1} display='flex' alignItems='center'>
            <IconMoneyOff className={classes.icon} />
            <Box ml={1} display='flex'>
              <Box>
                <div>
                  Скидка на технику:
                </div>
                <div>
                  Скидка на запчасти:
                </div>
              </Box>
              <Box ml={1}>
                <div>
                  {discM}
                </div>
                <div>
                  {discS}
                </div>
              </Box>
            </Box>
          </Box>
          <Box mt={2} mb={1} display='flex' alignItems='center'>
            <IconTurnover className={classes.icon} />
            <Box ml={1} display='flex'>
              <Box>
                <div>
                  {`Оборот ${year0} ${currency}`}
                </div>
                <div>
                  {`Оборот ${year1} год ${currency}`}
                </div>
                <div>
                  {`Оборот ${year2} год ${currency}`}
                </div>
              </Box>
              <Box ml={1} textAlign='right'>
                <div>
                  {`${turn0.toLocaleString('ru-RU', { minimumFractionDigits: 0 })}`}
                </div>
                <div>
                  {`${turn1.toLocaleString('ru-RU', { minimumFractionDigits: 0 })}`}
                </div>
                <div>
                  {`${turn2.toLocaleString('ru-RU', { minimumFractionDigits: 0 })}`}
                </div>
              </Box>
            </Box>
          </Box>
        </AccordionDetails>
      }
    </Accordion>
  );

};

const content = props => {
  const { items } = props;

  return (
    <div>
      <Box mt={1} mb={1}>
        <Typography variant='caption'>
          {`${items.length} контрагентов`}
        </Typography>
      </Box>
      {
        items.map((item, i) => {
          return <Item key={i} item={item} />
        })
      }
    </div>
  );
};

export default content;
